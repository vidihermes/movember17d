import React,{Component} from 'react';
import IconButton from 'material-ui/IconButton';
import Button from 'material-ui/Button';
import { CircularProgress } from 'material-ui/Progress';

import FormatBoldIcon from 'material-ui-icons/FormatBold';
import FormatUnderlinedIcon from 'material-ui-icons/FormatUnderlined';
import FormatItalicIcon from 'material-ui-icons/FormatItalic';
import FormatStrikethroughIcon from 'material-ui-icons/FormatStrikethrough';
import CodeIcon from 'material-ui-icons/Code';
import FormatListBulletedIcon from 'material-ui-icons/FormatListBulleted';
import FormatListNumberedIcon from 'material-ui-icons/FormatListNumbered';
import FormatQuoteIcon from 'material-ui-icons/FormatQuote';
import BorderAllIcon from 'material-ui-icons/BorderAll';
import BorderBottomIcon from 'material-ui-icons/BorderBottom';
import BorderRightIcon from 'material-ui-icons/BorderRight';
import BorderVerticalIcon from 'material-ui-icons/BorderVertical';
import BorderHorizontalIcon from 'material-ui-icons/BorderHorizontal';

import Html from 'slate-html-serializer';
import { Editor, getEventTransfer} from 'slate-react';
import { Block} from 'slate';

function swap(json){
	var ret = {};
	for(var key in json){
		ret[json[key]] = key;
	}
	return ret;
}

const nodeSerializer=(node,children)=>{
	//console.log(node)
	let attributes=node.attributes||{};
	switch (node.type) {
		case 'heading-one': return <h1 {...attributes}>{children}</h1>
		case 'heading-two': return <h2 {...attributes}>{children}</h2>
		case 'heading-three': return <h3 {...attributes}>{children}</h3>
		case 'heading-four': return <h4 {...attributes}>{children}</h4>
		case 'heading-five': return <h5 {...attributes}>{children}</h5>
		case 'heading-six': return <h6 {...attributes}>{children}</h6>
		case 'quote': return <blockquote {...attributes}>{children}</blockquote>
		case 'list-item': return <li {...attributes}>{children}</li>
		case 'bulleted-list': return <ul {...attributes}>{children}</ul>
		case 'numbered-list': return <ol {...attributes}>{children}</ol>
		case 'table': return <table><tbody {...attributes}>{children}</tbody></table>
		case 'table-row': return <tr {...attributes}>{children}</tr>
		case 'table-cell': return <td {...attributes}>{children}</td>
		//case 'link': {
		//	const { data } = node
		//	const href = data.get('href')
		//	return <a href={href} {...attributes}>{children}</a>
		//}
		//case 'image': {
		//	const src = node.data.get('src')
		//	const className = isSelected ? 'active' : null
		//	const style = { display: 'block' }
		//	return <img src={src} className={className} style={style} {...attributes} />
		//}
		case 'paragraph':return <p {...attributes}>{children}</p>
	}
}

const markSerializer=(node,children) => {
	switch (node.type) {
		case 'bold': return <strong>{children}</strong>
		case 'underline': return <u>{children}</u>
		case 'italic': return <em>{children}</em>
		case 'strikethrough': return <del>{children}</del>
		case 'code': return <code>{children}</code>
	}
}

const BLOCK_TAGS = {
  p: 'paragraph',
  li: 'list-item',
  ul: 'bulleted-list',
  ol: 'numbered-list',
  blockquote: 'quote',
  pre: 'code',
  h1: 'heading-one',
  h2: 'heading-two',
  h3: 'heading-three',
  h4: 'heading-four',
  h5: 'heading-five',
  h6: 'heading-six',
  table:'table',
  tr:'table-row',
  th:'table-cell',
  td:'table-cell',
}

const BLOCK_TAGS_SWAP = swap(BLOCK_TAGS);

const MARK_TAGS = {
  strong: 'bold',
  i: 'italic',
  em: 'italic',
  u: 'underline',
  s: 'strikethrough',
  del:'strikethrough',
  code: 'code'
}
const MARK_TAGS_SWAP = swap(MARK_TAGS);

const RULES = [
  {
    deserialize(el, next) {
      const block = BLOCK_TAGS[el.tagName.toLowerCase()]
      if (!block) return
      return {
        object: 'block',
        type: block,
        nodes: next(el.childNodes)
      }
    },
	serialize(obj, children) {
      if (obj.object === 'block' && BLOCK_TAGS_SWAP[obj.type]) {
        return nodeSerializer(obj,children); //renderNode({...obj,children,node:{type:BLOCK_TAGS_SWAP[obj.type]}})
      }
    }
  },
  {
    deserialize(el, next) {
      const mark = MARK_TAGS[el.tagName.toLowerCase()]
      if (!mark) return
      return {
        object: 'mark',
        type: mark,
        nodes: next(el.childNodes)
      }
    },
	serialize(obj, children) {
		if (obj.object === 'mark' && MARK_TAGS_SWAP[obj.type]) {
		  return markSerializer(obj,children); //renderNode({...obj,children,node:{type:BLOCK_TAGS_SWAP[obj.type]}})
		}
	}
  },
  {
    // Special case for code blocks, which need to grab the nested childNodes.
    deserialize(el, next) {
      if (el.tagName.toLowerCase() !== 'pre') return
      const code = el.childNodes[0]
      const childNodes = code && code.tagName.toLowerCase() === 'code'
        ? code.childNodes
        : el.childNodes

      return {
        object: 'block',
        type: 'code',
        nodes: next(childNodes)
      }
    }
  },
  {
    // Special case for images, to grab their src.
    deserialize(el, next) {
      if (el.tagName.toLowerCase() !== 'img') return
      return {
        object: 'block',
        type: 'image',
        isVoid: true,
        nodes: next(el.childNodes),
        data: {
          src: el.getAttribute('src')
        }
      }
    },
	serialize(obj, children) {
		if (obj.object === 'block' && obj.type==='image') {
		  return <img alt={''} src={obj.data.get('src')} />
		}
	}
  },
  {
    // Special case for links, to grab their href.
    deserialize(el, next) {
      if (el.tagName.toLowerCase() !== 'a') return
      return {
        object: 'inline',
        type: 'link',
        nodes: next(el.childNodes),
        data: {
          href: el.getAttribute('href')
        }
      }
    },
	serialize(obj, children) {
		if (obj.object === 'inline' && obj.type==='link') {
		  return <a href={obj.data.get('href')}>{children}</a>
		}
	}
  }
];

const serializer = new Html({ rules: RULES });

//const CodeNode=(props)=>{
//	return <pre {...props.attributes}><code>{props.children}</code></pre>
//}
const DEFAULT_NODE = 'paragraph';
const BLOCK_TYPES = [
	{label: 'H1', m: 'heading-one'},
	{label: 'H2', m: 'heading-two'},
	{label: 'H3', m: 'heading-three'},
	{label: 'H4', m: 'heading-four'},
	{label: 'H5', m: 'heading-five'},
	{label: 'Quote', m: 'quote', icon:<FormatQuoteIcon/>},
	//{label: 'LI', m: 'list-item', icon:<FormatListBulletedIcon/>},
	{label: 'UL', m: 'bulleted-list', icon:<FormatListBulletedIcon/>},
	{label: 'OL', m: 'numbered-list', icon:<FormatListNumberedIcon/>},
];

const INLINE_STYLES=[
	{label:'Bold',	m:'bold', icon:<FormatBoldIcon/>},
	{label:'Italic', m:'italic', icon:<FormatItalicIcon/>},
	{label:'Underline', m: 'underline', icon:<FormatUnderlinedIcon/>},
	{label:'Code', m: 'code', icon:<CodeIcon/>},
	{label:'Strikethrough', m: 'strikethrough', icon:<FormatStrikethroughIcon/>},
];

class StyleButton extends Component {
	onToggle = (e) => {
		e.preventDefault();
		this.props.onToggle(this.props.m);
	}
	render() {
		let {active,icon,label,cn,spanCn,disabled}=this.props;
		return <IconButton className={cn} aria-label={label} onMouseDown={this.onToggle}
			color={active?'primary':'default'}
			disabled={disabled}>
			{!!icon && icon}
			{!icon && <span className={spanCn}>{label}</span>}
		</IconButton>
	}
}

const InlineStyleControls = (props) => {
	const {hasMark,onToggle} = props;
	return <div className="RichEditor-controls" style={{display:'inline-block'}}>
		{INLINE_STYLES.map(type =>
			<StyleButton
				key={type.label}
				active={hasMark(type.m)}
				label={type.label}
				icon={type.icon}
				m={type.m}
				cn={props.btnClass}
				onToggle={onToggle}
				spanCn={props.btnSpanClass}/>
		)}
	</div>
};

const BlockStyleControls = (props) => {
	const {hasBlock,onToggle} = props;
	return <div className="RichEditor-controls" style={{display:'inline-block'}}>
		{BLOCK_TYPES.map((type) =>
			<StyleButton
				key={type.label}
				active={hasBlock(type.m)}
				label={type.label}
				onToggle={onToggle}
				m={type.m}
				icon={type.icon}
				cn={props.btnClass}
				spanCn={props.btnSpanClass}/>
		)}
	</div>
};


class TableControls extends Component {
	parentByType=(type)=>{
		let {document,blocks}=this.props.value();
		let b=blocks.get(0);
		let boss=(n)=>{
			return document.getClosestBlock(n.key);
		}
		while (b && b.type!==type) {
			b=boss(b);
		}
		return b;
	}
	createCell=(c,r)=>{
		return {"object": "block","type": "table-cell",
		"nodes": [{
			"object": "text",
			...(r < 1?{
					"leaves": [{
					"text": `Title ${c+1}`,
						"marks": [{"type": "bold"}]
					}]
				}:{
					"leaves": [{"text": ``}]
				})
		}]};
	}
	insertRow=type=>{
		let table=this.parentByType('table');
		if(table){
			let rowNodes=table.nodes;
			let colNodes=rowNodes.get(0).nodes;			
			let cels=[];
			for(let i=1;i<=colNodes.size;i++){
				cels.push(this.createCell(i,rowNodes.size));
			}
			let blk=Block.create({
				"object": "block",
				"type": "table-row",
				nodes:cels,
			})
			let change=this.props.value().change();
			change.insertNodeByKey(table.key,rowNodes.size,blk);
			this.props.onChange(change);
		}
	}
	insertColumn=type=>{
		let table=this.parentByType('table');
		if(table){
			let change=this.props.value().change();
			let rowNodes=table.nodes;
			let colNodes=rowNodes.get(0).nodes;
			let n=colNodes.size-1;
			//let cels=[];
			for(let i=0;i<rowNodes.size;i++){
				change.insertNodeByKey(rowNodes.get(i).key,n+1,
					this.createCell(n+1,i)
				);
			}
			this.props.onChange(change);
		}
	}
	getColIndex=(row,cell)=>{
		for(let i=0;i<row.nodes.size;i++){
			if(row.nodes.get(i)===cell) return i;
		}
		return -1;
	}
	deleteRow=type=>{
		let row=this.parentByType('table-row');
		let table=this.parentByType('table');
		if(row && table){
			let k=table.nodes.size<=1?table.key:row.key;
			let change=this.props.value().change();
			change.removeNodeByKey(k);
			this.props.onChange(change);
		}
	}
	deleteColumn=type=>{
		let {blocks}=this.props.value(),
			row=this.parentByType('table-row'),
			table=this.parentByType('table'),
			cell=blocks.get(0);
		if(row && table){
			let change=this.props.value().change();
			if(row.nodes.size <=1){
				change.removeNodeByKey(table.key);
				return this.props.onChange(change);
			}
			let idx=this.getColIndex(row,cell);
			if(idx >= 0){
				let dlt=[];
				for(let i=0;i<table.nodes.size;i++){
					dlt.push(table.nodes.get(i).nodes.get(idx).key);
				}
				dlt.forEach(v=>{
					change.removeNodeByKey(v);
				});
				this.props.onChange(change);
			}
		}
	}
	insertTable=type=>{
		let change=this.props.value().change();
		let rows=[];
		for(let r=0;r<2;r++){
			let cels=[];
			for(let c=0;c<2;c++){
				cels.push(this.createCell(c,r));
			}
			rows.push({"object": "block","type": "table-row",
					nodes:cels});
		}
		change.insertBlock({
			"object": "block",
			"type": "table",
			"nodes":rows});

		this.props.onChange(change);
	}
	render(){
		const {hasBlock,btnClass} = this.props,
			isInTable=hasBlock('table-cell') || hasBlock('table-row');
		return <div className="RichEditor-controls" style={{display:'inline-block'}}>
				<StyleButton label={'insert-table'}
					disabled={isInTable}
					onToggle={this.insertTable} icon={<BorderAllIcon />} cn={btnClass}/>
				<StyleButton label={'insert-row'}
					disabled={!isInTable}
					onToggle={this.insertRow} icon={<BorderBottomIcon />} cn={btnClass}/>
				<StyleButton label={'insert-column'}
					disabled={!isInTable}
					onToggle={this.insertColumn} icon={<BorderRightIcon />} cn={btnClass}/>
				<StyleButton label={'delete-row'}
					disabled={!isInTable}
					color={'accent'}
					onToggle={this.deleteRow} icon={<BorderHorizontalIcon style={isInTable?{color:'red'}:{}}/>} cn={btnClass}/>
				<StyleButton label={'delete-column'}
					disabled={!isInTable}
					color={'red'}
					onToggle={this.deleteColumn} icon={<BorderVerticalIcon style={isInTable?{color:'red'}:{}}/>} cn={btnClass}/>
		</div>
	}
}

export default class RichEditor extends Component{
	state = {
		loading:false,
		value: serializer.deserialize(this.props.value||'<p></p>') //Value.fromJSON(initialValue)
	}
	componentWillReceiveProps(np){
		if(np.value){
			this.setState({
				value: serializer.deserialize(np.value||'<p></p>') 
			})
		}
	}
	onChange = ({ value }) => {
		this.setState({ value })
	}
	save=()=>{
		this.setState({loading:true},()=>{
			if(this.props.onSave){
				this.props.onSave(serializer.serialize(this.state.value));
			}
		})
	}
	cancel=()=>{
		if(this.props.onCancel){
			this.props.onCancel();
		}
	}
	onKeyDown = (event, change) => {
		const tm=(b)=>{
			event.preventDefault();
			change.toggleMark(b)
			return true;
		}
		
		const { value } = change;
		const { document, selection } = value;
		const { startKey } = selection;
		
		//console.log('key==>',event.key);
		if(['Backspace','Delete','Enter'].indexOf(event.key)>=0){
			const startNode = document.getDescendant(startKey);
			
			if (selection.isAtStartOf(startNode)) {
				const previous = document.getPreviousText(startNode.key)
				const prevBlock = document.getClosestBlock(previous.key)
				
				if (prevBlock.type === 'table-cell') {
					event.preventDefault()
					return true
				}
			}
			if (value.startBlock.type !== 'table-cell') return;
			switch (event.key) {
				case 'Backspace': return this.onBackspace(event, change)
				case 'Delete': return this.onDelete(event, change)
				case 'Enter': return this.onEnter(event, change)
			}
		}
	
		const isMod = (event.metaKey && !event.ctrlKey) || event.ctrlKey;
		if (!isMod) return;

		switch (event.key) {
			case 'b': return tm('bold')
			case 'i': return tm('italic')
			case 'u': return tm('underline')
			case '-': return tm('strikethrough')
			case '`': return tm('code')
			case 'z':
				event.preventDefault();
				return this.onChange(value.change().undo());
			case 'y':
				event.preventDefault();
				return this.onChange(value.change().redo());
		}
		return true;
	}
	renderNode = (props) => {
		const { attributes, children, node, isSelected} = props;
		//let textAlign;
		switch (node.type) {
			case 'quote': return <blockquote {...attributes}>{children}</blockquote>
			case 'heading-one': return <h1 {...attributes}>{children}</h1>
			case 'heading-two': return <h2 {...attributes}>{children}</h2>
			case 'heading-three': return <h3 {...attributes}>{children}</h3>
			case 'heading-four': return <h4 {...attributes}>{children}</h4>
			case 'heading-five': return <h5 {...attributes}>{children}</h5>
			case 'heading-six': return <h6 {...attributes}>{children}</h6>
			case 'list-item': return <li {...attributes}>{children}</li>
			case 'bulleted-list': return <ul {...attributes}>{children}</ul>
			case 'numbered-list': return <ol {...attributes}>{children}</ol>
			case 'table': return <table><tbody {...attributes}>{children}</tbody></table>
			case 'table-row': return <tr {...attributes}>{children}</tr>
			case 'table-cell': return <td {...attributes}>{children}</td>
			case 'link': {
				const { data } = node
				const href = data.get('href')
				return <a href={href} {...attributes}>{children}</a>
			}
			case 'image': {
				const src = node.data.get('src')
				const className = isSelected ? 'active' : null
				const style = { display: 'block' }
				return <img src={src} className={className} style={style} {...attributes} />
			}
			case 'paragraph':<p {...attributes}>{children}</p>
		}
	}
	renderMark = (props) => {
		switch (props.mark.type) {
			case 'bold': return <strong>{props.children}</strong>
			case 'underline': return <u>{props.children}</u>
			case 'italic': return <em>{props.children}</em>
			case 'strikethrough': return <del>{props.children}</del>
			case 'code': return <code>{props.children}</code>
		}
	}
	toggle=(type)=>{
		//event.preventDefault()
		const { value } = this.state
		const change = value.change().toggleMark(type)
		this.onChange(change)
	}
	blockToggle=(type)=>{
		const { value } = this.state;
		const change = value.change();
		const { document } = value
		
		if (type !== 'bulleted-list' && type !== 'numbered-list') {
			const isActive = this.hasBlock(type)
			const isList = this.hasBlock('list-item')
			if (isList) {
				change
					.setBlock(isActive ? DEFAULT_NODE : type)
					.unwrapBlock('bulleted-list')
					.unwrapBlock('numbered-list')
			} else {
				change
					.setBlock(isActive ? DEFAULT_NODE : type)
			}
		}
		
		// Handle the extra wrapping required for list buttons.
		else {
			const isList = this.hasBlock('list-item')
			const isType = value.blocks.some((block) => {
				return !!document.getClosest(block.key, parent => parent.type === type)
			})
			if (isList && isType) {
				change
					.setBlock(DEFAULT_NODE)
					.unwrapBlock('bulleted-list')
					.unwrapBlock('numbered-list')
			} else if (isList) {
				change
					.unwrapBlock(type === 'bulleted-list' ? 'numbered-list' : 'bulleted-list')
					.wrapBlock(type)
			} else {
				change
					.setBlock('list-item')
					.wrapBlock(type)
			}
		}
		this.onChange(change);
	}
	hasMark = (type) => {
		const { value } = this.state
		return value.activeMarks.some(mark => mark.type === type)
	}
	hasBlock = (type) =>{
		const { value } = this.state
		return value.blocks.some(node => node.type === type)
	}
	setEditorComponent=(ref) => {
        this.editorREF = ref;
    }
	
	onBackspace = (event, change) => {
		const { value } = change
		if (value.startOffset !== 0) return
		event.preventDefault()
		return true
	}
	
	onDelete = (event, change) => {
		const { value } = change
		if (value.endOffset !== value.startText.text.length) return
		event.preventDefault()
		return true
	}
	
	onEnter = (event, change) => {
		event.preventDefault()
		return true
	}
	
	change=()=>{
		return this.state.value.change();
	}
	block=(type)=>{
		return this.state.value.blocks;
	}
	document=()=>{
		return this.state.value.document;
	}
	_value=()=>{
		return this.state.value;
	}
	onPaste = (event, change) => {
		const transfer = getEventTransfer(event)
		if (transfer.type !== 'html') return
		const { document } = serializer.deserialize(transfer.html)
		change.insertFragment(document)
		return true
	}
	render() {
		const {className,tbClassName,contentClassName,btnClass,btnSpanClass}=this.props,
			{loading}=this.state;
		return <div className={'texteditor '+className}>
			<div className={tbClassName}>
				<InlineStyleControls
					hasMark={this.hasMark}
					btnClass={btnClass}
					btnSpanClass={btnSpanClass}
					onToggle={this.toggle}
				/><BlockStyleControls
					hasBlock={this.hasBlock}
					btnClass={btnClass}
					btnSpanClass={btnSpanClass}
					onToggle={this.blockToggle}
				/>
				<TableControls
					hasBlock={this.hasBlock}
					btnClass={btnClass}
					btnSpanClass={btnSpanClass}
					onToggle={this.blockToggle}
					value={this._value}
					onChange={this.onChange}
				/>
				<div style={{float:'right'}}>
					<div style={{position: 'relative',display:'inline-block'}}>
						<Button raised disabled={loading} color={'primary'} onClick={this.save}>Save</Button>
						{loading && <CircularProgress size={24} style={{
								color: 'teal',//green[500],
								position: 'absolute',
								top: '50%',
								left: '50%',
								marginTop: -12,
								marginLeft: -12,
							}} />}
					</div> &nbsp; 
					<Button raised color={'secondary'} onClick={this.cancel}>Cancel</Button>
				</div>
			</div>
			<div className={contentClassName+' markdown-body'}>
				<Editor
					ref={this.setEditorComponent}
					value={this.state.value}
					onChange={this.onChange}
					onKeyDown={this.onKeyDown}
					renderNode={this.renderNode}
					renderMark={this.renderMark}
					onPaste={this.onPaste}
				/>
			</div>
		</div>
	}
}
