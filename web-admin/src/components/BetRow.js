import React,{Component} from 'react';
//import { withStyles } from 'material-ui/styles';

//let //closingTime='14:00:00',
//	difTime=0;

export const dow={
	'1':'Mon',
	'2':'Tue',
	'3':'Wed',
	'4':'Thu',
	'5':'Fri',
	'6':'Sat',
	'7':'Sun',
};

//export const addDays=(date, days)=>{
//	var result = new Date(date);
//	result.setDate(result.getDate() + days);
//	return result;
//};

//export const setTimes=(date, times)=>{
//	let t=times.split(':'),
//		h=parseInt(t[0],10),
//		m=parseInt(t[1],10),
//		s=parseInt(t[2]||'0',10),
//		d=new Date(date);
//	d.setHours(h);
//	d.setMinutes(m);
//	d.setSeconds(s);
//	return d;
//};

//export const serverTime=()=>{
//	return new Date((new Date()).getTime() + difTime);
//};

//export const drawDay={
//	'1':['Wed','Sat','Sun'],
//	'2':['Sat','Sun'],
//	'3':['Wed'],
//	'6':['Sat'],
//	'7':['Sun'],
//};

//export const drawDate=()=>{
//	let drawDays=[3,6,7];
//	let st=serverTime();
//	
//	if((st.getDay() < 1) && (st < setTimes(st,closingTime))) st=addDays(st, -7);
//	return drawDays.map(v=>setTimes(addDays(st, v - st.getDay()),closingTime));
//};

const getDow=(d)=>{
	for(let i in dow){
		if(d.toUpperCase() === dow[i].toUpperCase()) return i;
	}
};

//export const enableDay=(d)=>{
//	let st=serverTime(),
//		dd=drawDate();
//	for(let i=0;i<drawDay[d].length;i++){
//		let v=drawDay[d][i],
//			k=getDow(v);
//		for(let ii=0;ii<dd.length;ii++){
//			let dg=(dd[ii].getDay()+7-1)%7+1;
//			if(dg === parseInt(k,10)){
//				if(dd[ii] < st) return false;
//			}
//		}
//	}
//	return true;
//};

export const enableDay=(d,drawDay,drawDate)=>{
	let //st=serverTime(),
		dd=(drawDate||[]).map(v=>new Date(v.date));
	for(let i=0;i<drawDay[d].length;i++){
		let v=drawDay[d][i],
			k=getDow(v);
		for(let ii=0;ii<dd.length;ii++){
			//let dg=(dd[ii].getDay()+7-1)%7+1;
			let k2=(dd[ii].getDay()+7-1)%7+1;
			if((parseInt(k2,10) === parseInt(k,10)) &&
			   !drawDate[ii].enable ) return false;
		}
		//for(let ii=0;ii<dd.length;ii++){
		//	let dg=(dd[ii].getDay()+7-1)%7+1;
		//	if(dg === parseInt(k,10)){
		//		if(dd[ii] < st) return false;
		//	}
		//}
	}
	return true;
};

export const defaultDay=(drawDay,drawDate)=>{
	for(let i in drawDay){
		if(enableDay(i,drawDay,drawDate)) return i;
	}
};

//console.log(drawDate());

//let _dividend={
//	rebate:{
//		'1D':5,
//		'2D':5,
//		'3D':5,
//		'4D':10,
//	},
//	strike_comm:{
//		"4D":{
//			"1234":5
//		}
//	}
//};

//export const gameValidate={
//	'4D':/^\d{4}$/,
//	'3D':/^\d{3}$/,
//	'2D':/^\d{2}$/,
//};

export const defBetList='4D 3D 2D'.split(' ');

//export const calcFactor=()=>{
//	return _dividend;
//};

export const game='4D 3D 2D'.split(' ');
export const betType='- R I'.split(' ');

export class BetRow extends Component{
	state={
		d:this.props.day,
		g:'4D',
		n:'',
		b:'',
		s:'',
		bt:'-',
		err:{}, //error
		inc:false, //incomplete
		complete:false,
		disable:false,
	}
	timer=null
	fireChange=()=>{
		clearTimeout(this.timer);
		this.timer=setTimeout(()=>{
			this.props.onChange()
		},250);
	}
	change=(k)=>{
		return (e)=>{
			let value=e.target.value;
			this.setState({[k]:value},()=>{
				if(k==='g'||k==='n') this.nAfterChange()
				else if(k==='b'||k==='s') this.bsAfterChange(k)
				else if(k==='bt') this.btAfterChange(k)
				else this.fireChange();
			});
		}
	}
	componentWillReceiveProps(np){
		let {disable,d}=this.state;
		if(np.hasOwnProperty('disable') && np.disable!==disable){
			this.setState({disable:np.disable});
		}
		if(np.hasOwnProperty('day') && np.day !== d){
			this.setState({d:np.day});
		}
	}
	bsAfterChange=(k)=>{
		let {err}=this.state,
			v=this.state[k],
			v2=parseFloat(v||'0'),
			e=isNaN(v2);
		if(!e && err[k]) {
			delete err[k];
			this.setState({err})
		}
		if(e && !err[k]) {
			err[k]=`invalid ${k==='b'?'big':'small'} amount`;
			this.setState({err});
		}
		this.checkIncomplete();
	}
	btAfterChange=()=>{
		let {g,s,b,bt}=this.state;
		if(!this.props.currentPrize[g][bt==='I'?'i':'normal'].small && s) this.setState({s:''});
		if(!this.props.currentPrize[g][bt==='I'?'i':'normal'].big && b) this.setState({b:''});
		this.checkIncomplete();
	}
	nAfterChange=()=>{
		let {g,n,bt,err}=this.state;
		if(!this.props.currentPrize[g].i && bt==='I') this.setState({bt:'-'},this.btAfterChange)
		else this.btAfterChange();
		let vld=!n?true:this.props.gameValidation(g,n);
		if(vld && err.n) {
			delete err.n;
			this.setState({err})
		}
		if(!vld && !err.n) {
			err.n='invalid numb for this bet';
			this.setState({err});
		}
		this.checkIncomplete();
	}
	checkIncomplete=()=>{
		let {n,b,s,err}=this.state;
		if(Object.keys(err).length || (!n && !parseFloat(b||'0') && !parseFloat(s||'0'))) return this.setState({inc:false,complete:false},this.fireChange);
		
		this.setState({
			inc:!(n && (parseFloat(b||'0') || parseFloat(s||'0'))),
			complete:n && (parseFloat(b||'0') || parseFloat(s||'0'))
		},this.fireChange);
	}
	keyDown=(e)=>{
		let {idx} = this.props,
			tn=this.tn||['g','n','b','s','bt'];
		if(e.keyCode===38 || e.keyCode===40){
			e.preventDefault();
			this.props.onNext({
				cy:parseFloat(idx),
				cx:tn.indexOf(e.target.name),
				y:e.keyCode===40?1:-1,
				x:0});
		} else if(e.keyCode===37 || e.keyCode===39){
			e.preventDefault();
			this.props.onNext({
				cy:parseFloat(idx),
				cx:tn.indexOf(e.target.name),
				y:0,
				x:e.keyCode===37?-1:1});
		}
	}
	setFocus=(name)=>{
		if(this.refs[name].disabled) return false;
		//console.log('this.refs[name].disabled==>',this.refs[name].disabled)
		if(this.refs[name].setSelectionRange) this.refs[name].setSelectionRange(0, this.refs[name].value.length);
		setTimeout(()=>{this.refs[name].focus();},10);
		return true;
	}
	setDay=d=>{
		this.setState({d})
	}
	setBet=g=>{
		this.setState({g})
	}
	render(){
		const {idx,classes,currentPrize}=this.props;
		const {err,inc,complete,d,g,n,b,s,bt,disable}=this.state;
		return <tr className={
					classes.tblRow+(
					Object.keys(err).length?' '+classes.errRow:(
						inc?' '+classes.incRow:(complete?' '+classes.validRow:'')
					))}>
			<td>{idx}</td>
			{!this.props.noD && <td>{d}</td>}
			<td>
				<select
					disabled={disable}
					className={classes.select}
					ref={"g"}
					onKeyDown={this.keyDown}
					onChange={this.change('g')}
					name={'g'}
					value={g}>
						{game.map(v=><option key={v} value={v}>{v}</option>)}
				</select>
			</td>
			<td>
				<input
					disabled={disable}
					type={"tel"}
					ref={"n"}
					onKeyDown={this.keyDown}
					className={classes.number+(err.n?' '+classes.numberErr:'')}
					onChange={this.change('n')}
					name={'n'}
					value={n}/>
			</td>
			<td>
				<input
					type={"tel"}
					ref={"b"}
					onKeyDown={this.keyDown}
					className={classes.number+(err.b?' '+classes.numberErr:'')}
					onChange={this.change('b')}
					disabled={!((this.props.currentPrize[g]||{})[bt==='I'?'i':'normal']||{}).big || disable}
					name={'b'}
					value={b}/>
			</td>
			<td>
				<input
					type={"tel"}
					ref={"s"}
					onKeyDown={this.keyDown}
					className={classes.number+(err.s?' '+classes.numberErr:'')}
					onChange={this.change('s')}
					disabled={!((this.props.currentPrize[g]||{})[bt==='I'?'i':'normal']||{}).small || disable}
					name={'s'}
					value={s}/>
			</td>
			<td>
				<select
					disabled={disable}
					className={classes.select}
					ref={"bt"}
					onKeyDown={this.keyDown}
					onChange={this.change('bt')}
					name={'bt'}
					value={bt}>
						{betType.map(v=><option disabled={(v==='I'?!(currentPrize[g]||{})['i']:!(currentPrize[g]||{})['normal'])} key={v} value={v}>{v}</option>)}
				</select>
			</td>
		</tr>
	}
}

export class WildcardBetRow extends BetRow{
	state={
		d:this.props.day,
		g:'4D',
		n:'',
		b:'',
		s:'',
		bt:'*',
		err:{}, //error
		inc:false, //incomplete
		complete:false,
		disable:false,
	}
	tn=['g','n','b','s']
	render(){
		const {idx,classes,currentPrize}=this.props;
		const {err,inc,complete,d,g,n,b,s,disable}=this.state;
		return <tr className={
					classes.tblRow+(
					Object.keys(err).length?' '+classes.errRow:(
						inc?' '+classes.incRow:(complete?' '+classes.validRow:'')
					))}>
			<td>{idx}</td>
			<td>{d}</td>
			<td>
				<select
					className={classes.select}
					ref={"g"}
					onKeyDown={this.keyDown}
					onChange={this.change('g')}
					name={'g'}
					value={g}>
						{game.map(v=><option key={v} value={v}>{v}</option>)}
				</select>
			</td>
			<td>
				<input
					type={"tel"}
					ref={"n"}
					onKeyDown={this.keyDown}
					className={classes.number+(err.n?' '+classes.numberErr:'')}
					onChange={this.change('n')}
					name={'n'}
					value={n}/>
			</td>
			<td>
				<input
					type={"tel"}
					ref={"b"}
					onKeyDown={this.keyDown}
					className={classes.number+(err.b?' '+classes.numberErr:'')}
					onChange={this.change('b')}
					disabled={!((this.props.currentPrize[g]||{})['normal']||{}).big}
					name={'b'}
					value={b}/>
			</td>
			<td>
				<input
					type={"tel"}
					ref={"s"}
					onKeyDown={this.keyDown}
					className={classes.number+(err.s?' '+classes.numberErr:'')}
					onChange={this.change('s')}
					disabled={!((this.props.currentPrize[g]||{})['normal']||{}).small}
					name={'s'}
					value={s}/>
			</td>
		</tr>
	}
}
//export const BetRow withStyles(styles, { withTheme: true })(BetRow);