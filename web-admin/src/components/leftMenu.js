import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import List, { ListItem, ListItemText } from 'material-ui/List';
import { MenuItem } from 'material-ui/Menu';
import Collapse from 'material-ui/transitions/Collapse';
import ExpandLess from 'material-ui-icons/ExpandLess';
import ExpandMore from 'material-ui-icons/ExpandMore';
import Divider from 'material-ui/Divider';
//import withRoot from './withRoot';
import {appEvent} from './events';
import {Get} from './ajax';
import {ServerDate,TZDiff} from './game';

const $=window.$;

const styles = theme => ({
	root: {
		width: '100%',
		maxWidth: 360,
		//background: theme.palette.background.paper,
		//background:theme.palette.secondary.main,
	},
	root2: {
		background:theme.palette.primary.main,
		color:'black',
	},
	root2Sub:{
		padding:[5,16],
		fontSize:'0.875rem',
		//color:theme.palette.grey[300],
		color:'black',
	},
	nested: {
		paddingLeft: theme.spacing.unit * 4,
		//'&:last-child':{
		//	boxShadow:[0,2, 2, 'rgba(0,0,0,0.1)'],
		//	marginBottom:6,
		//}
	},
	subMenu:{
		backgroundColor:theme.palette.secondary.light,
		boxShadow:[0,2, 10, 'rgba(0,0,0,0.5)'],
		marginBottom:6,
	},
	menu:{
		transition:['margin','0.2s'],
		boxShadow:[0,1, 5, 'rgba(0,0,0,0.1)'],
		//marginTop:-1,
		backgroundColor:theme.palette.secondary.light, //'#fff'
	},
	expanded:{
		marginTop:1,
		boxShadow:[0,0, 5, 'rgba(0,0,0,0.1)']
		//boxShadow:[1,1,1]
	}
});

let sty={
	font:{
		fontFamily: `"Roboto", "Helvetica", "Arial", sans-serif`,
	}
};
sty={
	...sty,
	label:{...sty.font,
		color:'rgba(0,0,0,0.54)',
		padding:'5px 16px',
		fontSize:'0.875rem'
	},
	value:{...sty.font,
		padding:'2px 16px 5px 32px',
		fontSize:'1rem'
	}
}
function pad(s,n){
	s=s.toString();
	return n-s.length>0?('0'.repeat(n-s.length))+s:s;
}

function timeLeft(t){
	let s= Math.floor(t=t/1000)%60,
		m= Math.floor(t=t/60)%60,
		h= Math.floor(t=t/60)%24,
		d= Math.floor(t=t/24);
	let r=d>0?(d +' Day'+(d>1?'s':'')+' and '):'';
	return r+[pad(h,2),pad(m,2),pad(s,2)].join(':')+' Hours';
}

class LeftMenu extends React.Component {
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}
	static contextTypes = {
		intl: PropTypes.object
	}
	state = {
		open: true,
		credit:0,
		balance:0,
		me:{},
		downCounter:'',
		drawDate:{}
	}
	interval=null
	sm=false
	handleClick=k=>{
		return ()=>{
			this.setState({[k]:!this.state[k]});
		}
	}
	route=uri=>{
		return ()=>{
			this.props.history.push(uri);
			if(this.sm) appEvent.fire('drawerToggle');
		}
	}
	meChange=me=>{
		let {credit,balance}=me||{};
		this.setState({
			me,
			credit:parseFloat(credit||'0'),
			balance:parseFloat(balance||'0'),
		})
	}
	getSm=()=>{
		this.sm=$(window).width() < 960;
	}
	firstDd=(ds)=>{
		ds=ds||{};
		let ks=Object.keys(ds);
		for(let i=0;i<ks.length;i++){
			if(!ds[ks[i]].drawn) return ds[ks[i]];
		}
	}
	leftDate=()=>{
		let {drawDate}=this.state;
		if(drawDate.close){
			ServerDate((e,r)=>{
				if(drawDate.close - r < 0){
					this.setState({
						downCounter:timeLeft(drawDate.draw - r),
						dcLabel:'Close, Time Left to Draw'
					});
				} else {
					this.setState({
						downCounter:timeLeft(drawDate.close - r),
						dcLabel:'Time Left to Close'
					});
				}
			})
		}
	}
	componentWillMount(){
		appEvent.on('meChange',this.meChange);
		this.getSm();
		$(window).on('resize',this.getSm);
		Get('completedrawdate',{},(e,r)=>{
			console.log('r==>',r);
			if(e) return console.log('e==>',e);
			
			TZDiff((e,tzDiff)=>{
				console.log('date diff==>',tzDiff);
				let fdd=this.firstDd(r.d||{});
				this.setState({drawDate:{
					...fdd,
					close:fdd.close+(tzDiff * 3600 * 1000),
					draw:fdd.draw+(tzDiff * 3600 * 1000),
				}})
			})
		});
		this.interval=setInterval(this.leftDate,1000);
	}
	componentWillUnmount(){
		appEvent.off('meChange',this.meChange)
		$(window).off('resize',this.getSm);
		clearInterval(this.interval);
	}
	formatNumb=n=>{
		return this.context.intl.formatNumber(n,{style: 'decimal',minimumFractionDigits:2,maximumFractionDigits:2});
	}
	render() {
		const { classes, location} = this.props,
			loc=location.pathname||'',
			{me,drawDate,downCounter,dcLabel} = this.state;
		
		return (
			<div>
				<List className={classes.root2}>
					<ListItem>
						Next Draw Time
					</ListItem>
					<li>
						<List disablePadding>
							<li className={classes.root2Sub}>Closing Time:</li>
							<li style={sty.value}>{(new Date(drawDate.close)).toLocaleString()}</li>
							<li className={classes.root2Sub}>Draw Time :</li>
							<li style={sty.value}>{(new Date(drawDate.draw)).toLocaleString()}</li>

							<li className={classes.root2Sub}>{dcLabel}:</li>
							<li style={sty.value}>{downCounter}</li>
						</List>
					</li>
				</List>
				<Divider />
				<List className={classes.root}>
					<ListItem button onClick={this.handleClick('adminMenu')} className={classes.menu+(this.state.adminMenu?' '+classes.expanded:'')}>
						<ListItemText primary="Admin" />
						{this.state.adminMenu ? <ExpandLess /> : <ExpandMore />}
					</ListItem>
					<Collapse component="li" in={this.state.adminMenu} timeout="auto" unmountOnExit>
						<List disablePadding className={classes.subMenu}>
							<MenuItem selected={!!loc.match(/^\/me/)} button className={classes.nested} onClick={this.route('/me')}>
								<ListItemText primary="Profile" />
							</MenuItem>
							{(me||{}).type!=='sadmin' && <MenuItem selected={!!loc.match(/^\/sadmin/)} button className={classes.nested} onClick={this.route('/sadmin')}>
								<ListItemText primary="Super Admin" />
							</MenuItem>}
							<MenuItem selected={!!loc.match(/^\/admins/)} button className={classes.nested} onClick={this.route('/admins')}>
								<ListItemText primary="Admins" />
							</MenuItem>
							<MenuItem selected={!!loc.match(/^\/sagents/)} button className={classes.nested} onClick={this.route('/sagents')}>
								<ListItemText primary="Super Agents" />
							</MenuItem>
							<MenuItem button className={classes.nested}>
								<ListItemText primary="Payment" />
							</MenuItem>
							<MenuItem button className={classes.nested}>
								<ListItemText primary="Payment History" />
							</MenuItem>
						</List>
					</Collapse>
	
					<ListItem button onClick={this.handleClick('betsMenu')} className={classes.menu+(this.state.betsMenu?' '+classes.expanded:'')}>
						<ListItemText primary="Bets" />
						{this.state.betsMenu ? <ExpandLess /> : <ExpandMore />}
					</ListItem>
					<Collapse component="li" in={this.state.betsMenu} timeout="auto" unmountOnExit>
						<List disablePadding className={classes.subMenu}>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/placebet$/)} onClick={this.route('/placebet')}>
								<ListItemText primary="Place Bet" />
							</MenuItem>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/placebet1d$/)} onClick={this.route('/placebet1d')}>
								<ListItemText primary="Place Bet 1D" />
							</MenuItem>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/placemassbet$/)} onClick={this.route('/placemassbet')}>
								<ListItemText primary="Mass Bet" />
							</MenuItem>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/placewildcardbet/)} onClick={this.route('/placewildcardbet')}>
								<ListItemText primary="Wildcard Bet" />
							</MenuItem>
							<MenuItem selected={!!loc.match(/^\/savedbets/)} button className={classes.nested} onClick={this.route('/savedbets')}>
								<ListItemText primary="Saved Bets" />
							</MenuItem>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/summarybets/) || !!loc.match(/^\/detailbets/)} onClick={this.route('/summarybets')}>
								<ListItemText primary="Summary Bets" />
							</MenuItem>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/tickets/)} onClick={this.route('/tickets')}>
								<ListItemText primary="Tickets" />
							</MenuItem>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/htickets/)} onClick={this.route('/htickets')}>
								<ListItemText primary="History Tickets" />
							</MenuItem>
						</List>
					</Collapse>
					
					<ListItem button onClick={this.handleClick('reportsMenu')} className={classes.menu+(this.state.reportsMenu?' '+classes.expanded:'')}>
						<ListItemText primary="Reports" />
						{this.state.reportsMenu ? <ExpandLess /> : <ExpandMore />}
					</ListItem>
					<Collapse component="li" in={this.state.reportsMenu} timeout="auto" unmountOnExit>
						<List disablePadding className={classes.subMenu}>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/fullreport/)} onClick={this.route('/fullreport')}>
								<ListItemText primary="Full Reports" />
							</MenuItem>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/strikereport/)} onClick={this.route('/strikereport')}>
								<ListItemText primary="Strike Reports" />
							</MenuItem>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/summaryreport/)} onClick={this.route('/summaryreport')}>
								<ListItemText primary="Summary Reports" />
							</MenuItem>
						</List>
					</Collapse>
					
					<ListItem button onClick={this.handleClick('optionMenu')} className={classes.menu+(this.state.optionMenu?' '+classes.expanded:'')}>
						<ListItemText primary="Options" />
						{this.state.optionMenu ? <ExpandLess /> : <ExpandMore />}
					</ListItem>
					<Collapse component="li" in={this.state.optionMenu||false} timeout="auto" unmountOnExit>
						<List disablePadding className={classes.subMenu}>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/logs/)} onClick={this.route('/logs')}>
								<ListItemText primary="Logs" />
							</MenuItem>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/notif/)} onClick={this.route('/notif')}>
								<ListItemText primary="Notification" />
							</MenuItem>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/announcement/) || !!loc.match(/^\/$/)} onClick={this.route('/announcement')}>
								<ListItemText primary="Announcement" />
							</MenuItem>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/agreement/) || !!loc.match(/^\/$/)} onClick={this.route('/agreement')}>
								<ListItemText primary="Agreement" />
							</MenuItem>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/help/) || !!loc.match(/^\/$/)} onClick={this.route('/help')}>
								<ListItemText primary="Help" />
							</MenuItem>
						</List>
					</Collapse>
					<ListItem button onClick={this.handleClick('settingMenu')} className={classes.menu+(this.state.settingMenu?' '+classes.expanded:'')}>
						<ListItemText primary="Settings" />
						{this.state.settingMenu ? <ExpandLess /> : <ExpandMore />}
					</ListItem>
					<Collapse component="li" in={this.state.settingMenu||false} timeout="auto" unmountOnExit>
						<List disablePadding className={classes.subMenu}>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/result/)} onClick={this.route('/result')}>
								<ListItemText primary="Results" />
							</MenuItem>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/prize/)} onClick={this.route('/prize')}>
								<ListItemText primary="Regulations" />
							</MenuItem>
						</List>
					</Collapse>
					<MenuItem button className={classes.menu} selected={!!loc.match(/^\/downline/)} onClick={this.route('/downline')}>
						<ListItemText primary="Manage Downline" />
					</MenuItem>
					<div style={{height:'100px'}} />
				</List>
			</div>
		);
	}
}

export default withStyles(styles)(LeftMenu);
//export default withRoot(withStyles(styles, { withTheme: true })(LeftMenu));