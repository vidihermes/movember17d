import React,{Component} from 'react';
import PropTypes from 'prop-types';
import Button from 'material-ui/Button';
import Toolbar from 'material-ui/Toolbar';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
//import Grid from 'material-ui/Grid';
import Typography from 'material-ui/Typography';
import {blueGrey} from 'material-ui/colors';
//import Badge from 'material-ui/Badge';
import MenuIcon from 'material-ui-icons/Menu';
//import Notifications from 'material-ui-icons/Notifications';
import { withStyles } from 'material-ui/styles';
import withRoot from './withRoot';
import {appEvent} from './events';
import {Get} from './ajax';
import {DateDiff,Me,Config} from './game';

import {NatifButton} from './NotifMenu';

const drawerWidth = 240;

const styles = theme => ({
	flex:{
		flex:1,
	},
	appBar: {
		//color:'#000',
		backgroundColor:theme.palette.primary.dark,
		//background:'#4c342e',
		color:theme.palette.grey[100],
		//boxShadow:'none',
		position: 'absolute',
		marginLeft: drawerWidth,
		[theme.breakpoints.up('md')]: {
			width: `calc(100% - ${drawerWidth}px)`,
		},
	},
	toolBar:{
		padding:[0,0],
		[theme.breakpoints.up('md')]: {
			padding:[0,16],
		},
	},
	navIconHide: {
		[theme.breakpoints.up('md')]: {
			display: 'none',
		},
	},
	marqueBar:{
		//backgroundColor:blueGrey['400'],
		//background:'linear-gradient(0deg,#a27631 0%, #feed84 90%,#a27631 100%)',
		//background:'linear-gradient(0deg,'+theme.palette.primary.main+' 0%,'+theme.palette.common.white+' 50%,'+theme.palette.primary.main+' 100%)',
		//background:'linear-gradient(0deg,#a57738 0%,#feed85 50%,#a57738 100%)',
		background:theme.palette.primary.light,
		//backgroundColor:theme.palette.primary.main,
		//color:theme.palette.grey[900],
		display:'flex',
		alignItems:'stretch',
		height:28
	},
	marqueDate:{
		//backgroundColor:'#fff',
		//backgroundColor:blueGrey['300'],
		backgroundColor:theme.palette.primary.main,
		//backgroundColor:theme.palette.primary.light,
		//background:'linear-gradient(0deg,'+theme.palette.primary.dark+' 0%,'+theme.palette.common.white+' 50%,'+theme.palette.primary.dark+' 100%)',
		//color:theme.palette.grey[900],
		//padding:2,
		textAlign:'right',
		whiteSpace:'nowrap',
		display:'flex',
		flexDirection:'column',
		justifyContent:'center'
	},
	marqueCnt:{
		display:'flex',
		flexDirection:'column',
		justifyContent:'center',
		flex:1
	},
	marqueCntText:{
		//flex:1,
		fontSize:'0.975rem',
		//padding:[5,0],
		//color:'white',
	},
	marqueDateText:{
		//color:'white',
		fontSize:'1rem',
		padding:[0,5],
	},
	rightItem:{
		display:'inline-flex',
		lineHeight:'1.4em',
		minHeight:36,
		minWidth:36,
		padding:8,
		boxSizing:'border-box',
		position:'relative',
		flex:'middle',
	},
	link:{
		color: 'white',
		textDecoration:'none',
		cursor:'pointer',
		'&:hover': {
			textDecoration:'underline',
		}
	}
});

export class TitleBar extends Component {
	static propTypes = {
		classes: PropTypes.object.isRequired,
		theme: PropTypes.object.isRequired,
	}
	state={
		title:'Home',
		notifCnt:9,
		userId:'',
		date:'',
		runningText:'',
	}
	titleChange=(title)=>{
		this.setState({title},()=>{
			document.title=this.state.title;
		});
	}
	dateServerInterval=null
	dateInterval=null
	dateDiff=0;
	configChange=({running_text})=>{
		this.setState({runningText:(running_text||{}).text});
	}
	componentWillMount(){
		appEvent.on('titleChange',this.titleChange);
		Config((e,r)=>{
			if(e) return;
			this.configChange(r);
		});
		Me((e,me)=>{
			this.setState({userId:(me||{}).id||''},()=>{
				if(!me) this.logedOut();
			});
		});
		
		DateDiff((e,r)=>{this.dateDiff=r;});
		this.dateServerInterval=setInterval(()=>{
			DateDiff((e,r)=>{
				this.dateDiff=r;
			});
		},30*1000);
		this.dateInterval=setInterval(()=>{
			this.setState({date:(new Date((new Date()).getTime()+this.dateDiff)).toLocaleString()});
		},1000);
		appEvent.on('configChange',this.configChange);
	}
	logedOut=()=>{
		window.location.href=window.root_url||'';
	}
	logout=()=>{
		Get('logout',{},(e,r)=>{
			window.location.href=window.root_url;
		})
	}
	componentWillUnmount(){
		appEvent.off('titleChange',this.titleChange);
		appEvent.off('configChange',this.configChange);
		clearInterval(this.dateServerInterval);
		clearInterval(this.dateInterval);
	}
	route(uri){
		return ()=>{
			this.props.history.push(uri);
		}
	}
	render(){
		const { classes } = this.props,
			{runningText} = this.state;
		return(<AppBar className={classes.appBar}>
			<Toolbar style={{minHeight:'48px'}} className={classes.toolBar}>
				<IconButton
					color="inherit"
					aria-label="open drawer"
					onClick={()=>{appEvent.fire('drawerToggle')}}
					className={classes.navIconHide}
				>
					<MenuIcon />
				</IconButton>
				<Typography type="title" color="inherit" noWrap className={classes.flex}>
					{this.state.title}
				</Typography>
				<div>
					<span className={classes.rightItem}><a className={classes.link} onClick={this.route('/me')}> {this.state.userId} </a></span>
					<NatifButton classes={classes}/>
					<Button color="inherit" onClick={this.logout} >Logout</Button>
				</div>
			</Toolbar>
			<div className={classes.marqueBar}>
				<div className={classes.marqueCnt}>
					<Typography noWrap className={classes.marqueCntText}><marquee style={{verticalAlign:'middle'}}>&nbsp;{runningText}</marquee></Typography>
				</div>
				<div className={classes.marqueDate}>
					<Typography className={classes.marqueDateText}>{this.state.date}</Typography>
				</div>
			</div>
		</AppBar>)
	}
}

//export default withStyles(styles, { withTheme: true })(Index);
export default withRoot(withStyles(styles, { withTheme: true })(TitleBar));
