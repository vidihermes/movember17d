import React,{Component} from 'react';
import Grid from 'material-ui/Grid';
import Divider from 'material-ui/Divider';
import List,{ ListItem, ListItemText} from 'material-ui/List';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Typography from 'material-ui/Typography';
import IconButton from 'material-ui/IconButton';
import Button from 'material-ui/Button';
import Switch from 'material-ui/Switch';
//import SaveIcon from 'material-ui-icons/Save';
//import ModeEditIcon from 'material-ui-icons/ModeEdit';
import CloseIcon from 'material-ui-icons/Close';
//import RefreshIcon from 'material-ui-icons/Refresh';
//import { CircularProgress } from 'material-ui/Progress';
//import { MenuItem } from 'material-ui/Menu';
import Collapse from 'material-ui/transitions/Collapse';
//import Snackbar from 'material-ui/Snackbar';

import ExpandLess from 'material-ui-icons/ExpandLess';
import ExpandMore from 'material-ui-icons/ExpandMore';

import withRoot from '../components/withRoot';
import {appEvent} from '../components/events';
import {Get,Put} from '../components/ajax';
import {Game,Config,Alias,UserType} from '../components/game';
import {SnackBar,ChangePassword,ChangeStatus,BreadCrumbs,EditableItem,CreditEditable,ChangeCredit,Classes} from '../components/dialog';

const styles = theme => ({
	...Classes(theme),
	root: {
		display:'flex',
		flexDirection:'column',
		width: '100%',
		height: '100%',
		fontFamily: `"Roboto", "Helvetica", "Arial", sans-serif`,
	},
	content:{
		flex:1,
		overflowY:'auto',
		[theme.breakpoints.up('sm')]: {
			display:'flex',
			flexDirection:'row',
			overflowY:'hidden',
		}
		
	},
	headerContent:{
		padding:[10,5,0,5]
	},
	sub:{
		width: '100%',
		//float:'left',
		padding:5,
		boxSizing:'border-box',
		[theme.breakpoints.up('sm')]: {
			//width: '50%',
			flex:1,
			overflowY:'auto'
		},
	},
	sub2:{
		width: '100%',
		paddingLeft:24,
		boxSizing:'border-box',
	},
	item:{
		display:'flex',
		padding:[8,16]
	},
	item2:{
		display:'flex',
		padding:[0,0,6,20]
	},
	itemSwitch:{
		display:'flex',
		padding:[0,16]
	},
	label:{
		whiteSpace:'nowrap',
		fontSize:'12pt',
		padding:'0px 10px',
	},
	label2:{
		whiteSpace:'nowrap',
		fontSize:'12pt',
		padding:'0px 10px',
	},
	value:{
		fontSize:'12pt',
		paddingLeft:5,
	},
	valueContainer:{
		display:'flex',
		alignItems:'center'
	},
	value2:{
		fontSize:'12pt',
		paddingLeft:0,
		display:'flex',
		alignItems:'center'
	},
	button: {
		//margin:[-10,0],//margin: theme.spacing.unit,
		width:30,
		height:30,
	},
	wrapper: {
		//margin: theme.spacing.unit,
		position: 'relative',
		display:'inline-block'
	},
	buttonProgress: {
		color: 'teal',//green[500],
		position: 'absolute',
		top: '50%',
		left: '50%',
		marginTop: -12,
		marginLeft: -12,
	},
	button2: {
		margin: 4//theme.spacing.unit,
	},
	input:{
		padding:[4,10],
		fontSize:'12pt',
		flex:1,
		width:'100%',
		marginLeft:-6
	},
	after:{
		fontSize:'10pt',
		padding:[0,5],
		color:'rgba(0,0,0,0.5)'
	},
	err:{
		borderColor:'#f00',
		outlineColor:'rgba(255,0,0,0.1)'
	},
	
	footer:{
		padding:[0,10],
		borderTop:[1,'solid','#eee'],
		justifyContent:'space-between',
		display:'flex',
		alignItems:'center',
		[theme.breakpoints.down('sm')]: {
			display:'block'
		},
	},
	footerLeft:{
		display:'flex',
		alignItems:'center',
		flex:1,
		flexWrap:'wrap'
	},
	bc:{
		flex:1
	},
	bcContent:{
		whiteSpace:'nowrap'
	},
	footerRight:{
		[theme.breakpoints.down('md')]: {
			display:'flex',
			justifyContent:'space-around',
		},
	},
	bold:{
		fontWeight:'bold'
	}
});

const SwitchInput=p=><ListItem className={p.c.itemSwitch}>
	<Grid item xs={5} className={p.c.label}>{p.label}</Grid>
	<Grid item xs={7} className={p.c.value}>
		<Switch onChange={e=>{p.onChange(e.target.checked)}}
		checked={!!p.checked}/>
	</Grid>
</ListItem>


//class StrikeItem extends Component{
//	state={
//		exp:false,
//		numb:'',
//		value:0,
//		values:this.props.values,
//		add:false,
//		numbValid:true,
//		valueValid:true,
//		game:this.props.game,
//	}
//	handleClick=()=>{
//		this.setState({exp:!this.state.exp});
//	}
//	change=(k)=>{
//		return (e)=>{
//			let vl=this.props.game.validate,
//				val=this.props.validation,
//				v=e.target.value,
//				st={[k]:v};
//			if(k==='numb') st.numbValid=!!((v||'').match(new RegExp(vl)))
//			else if(val) st.valueValid=val(v);
//			this.setState(st);
//		}
//	}
//	add=()=>{
//		this.setState({
//			numb:'',
//			value:0,
//			add:true,
//			numbValid:true,
//			valueValid:true,
//		},()=>{
//			this.refs.numb.focus();
//		});
//	}
//	remove=(k)=>{
//		return ()=>{
//			let {values}=this.state;
//			values=values||{};
//			delete values[k];
//			this.setState({values},()=>{
//				this.props.onChange(values);
//			});
//		}
//	}
//	ok=()=>{
//		let {values,numb,value,numbValid,valueValid}=this.state;
//		if(!(numb && numbValid && valueValid && value)){
//			if(!numbValid || !numb) return this.refs.numb.focus();
//			if(!valueValid || !value) return this.refs.value.focus();
//			return;
//		}
//		values={...values||{},[numb]:value};
//		this.setState({values,add:false},()=>{
//			this.props.onChange(values);
//		})
//	}
//	cancel=()=>{
//		this.setState({add:false})
//	}
//	componentWillReceiveProps(np){
//		if(JSON.stringify(np.values) !== JSON.stringify(this.state.values))
//			this.setState({values:np.values});
//	}
//	render(){
//		let {classes,after}=this.props;
//		let {numbValid,valueValid}=this.state;
//		
//		return <div className={classes.sRoot}>
//			<ListItem button onClick={this.handleClick} className={classes.sButton}>
//				<ListItemText primary="Upline Strike Commission" />
//				{this.state.exp ? <ExpandLess /> : <ExpandMore />}
//			</ListItem>
//			<Collapse component="li" in={this.state.exp} timeout="auto" unmountOnExit>
//				<List disablePadding className={classes.sPlace}>
//					{!!Object.keys(this.state.values).length && <ListItem className={classes.sItem} style={{fontWeight:'bold'}}>
//						<div className={classes.sItem2}>Number</div>
//						<div className={classes.sItem2}>Commision</div>
//						<div style={{width:'30px'}}/>
//					</ListItem>}
//					{Object.keys(this.state.values).map((k,i)=><ListItem className={classes.sItem} key={k}>
//						<div className={classes.sItem2}>{k}</div>
//						<div className={classes.sItem2}>{this.state.values[k]+' %'}</div>
//						<div>
//							<IconButton className={classes.button} aria-label="reload" onClick={this.remove(k)}>
//								<CloseIcon/>
//							</IconButton>
//						</div>
//					</ListItem>)}
//					{this.state.add && <ListItem className={classes.sAddForm}>
//						<div className={classes.sAddRow}>
//							<div className={classes.sFormItem}>{"Number"}</div>
//							<div className={classes.sFormItem}>
//								<input ref={"numb"} className={classes.sInput+(!numbValid?' '+classes.err:'')} value={this.state.numb} onChange={this.change('numb')}/>
//							</div>
//						</div>
//						<div className={classes.sAddRow}>
//							<div className={classes.sFormItem}>{"Commission"}</div>
//							<div className={classes.sFormItem}>
//								<div className={classes.sFormValue}>
//								<input ref={"value"} className={classes.sInput+(!valueValid?' '+classes.err:'')} value={this.state.value} onChange={this.change('value')}/>
//								<div className={classes.after}>{after}</div>
//								</div>
//							</div>
//						</div>
//						<div className={classes.sAddRow}>
//							<Button raised className={classes.sAddBtn} onClick={this.ok}>OK</Button>
//							<Button raised className={classes.sAddBtn} onClick={this.cancel}>Cancel</Button>
//						</div>
//					</ListItem>}
//					{!this.state.add && <ListItem className={classes.sAddForm}>
//						<Button raised className={classes.sAddBtn} onClick={this.add}>Add</Button>
//					</ListItem>}
//				</List>
//			</Collapse>
//		</div>
//	}
//}

const Item=p=><ListItem className={p.c.item}>
	<Grid item xs={4} className={p.c.label}>{p.label}</Grid>
	<Grid item xs={1} className={p.c.label}>{":"}</Grid>
	<Grid item xs={7} className={p.c.value+(p.valueCn?' '+p.valueCn:'')}>{p.value}</Grid>
</ListItem>

class UserDetail extends Component{
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}
	static contextTypes = {
		intl: PropTypes.object
	}
	state={
		id:'',
		name:'',
		config:{},
		game:{},
		errOpen:false,
		errMessage:'',
		ccOpen:false,
		csOpen:false,
		cpOpen:false,
		cepOpen:false,
		csOK:null,
		successOpen:false,
	}
	csClose=()=>{
		this.setState({csOpen:false});
	}
	ccClose=()=>{
		this.setState({ccOpen:false});
	}
	cpClose=()=>{
		this.setState({cpOpen:false});
	}
	cpOK=({password,confirm})=>{
		if(!(!!password && password === confirm))
			return this.setState({errOpen:true,errMessage:'invalid new password or confirm new password'});
		//this.setState({cpOpen:false});
		Put('user/'+this.state.id,{password},(e,r)=>{
			if(e) {
				console.log('e==>',e);
				return this.setState({errOpen:true,errMessage:e.toString()});
			}
			console.log('r==>',r);
			this.setState({
				cpOpen:false,
				successOpen:true,
				successMsg:'Password updated'
			});
		});
	}
	cepClose=()=>{
		this.setState({cepOpen:false});
	}
	cepOK=({password,confirm})=>{
		if(!(!!password && password === confirm))
			return this.setState({errOpen:true,errMessage:'invalid new password or confirm emergency password'});
		Put('user/'+this.state.id,{fake_password:password},(e,r)=>{
			if(e) {
				console.log('e==>',e);
				return this.setState({errOpen:true,errMessage:e.toString()});
			}
			this.setState({
				cepOpen:false,
				successOpen:true,
				successMsg:'Emergency password updated'
			});
		});
	}
	onSuccessClose=()=>{
		this.setState({successOpen:false});
	}
	valueChange=(k)=>{
		return v=>{
			this.setState({[k]:v});
		}
	}
	acChange=(k)=>{
		return (v,cb)=>{
			let {agent_config}=this.state;
			agent_config=agent_config||{};
			//agent_config[k]=v;
			//this.setState({agent_config});
			let fld={};
			fld['agent_config['+k+']']=v;
			Put('user/'+this.state.id,fld,(e,r)=>{
				console.log('e==>',e);
				console.log('r==>',r);
				if(e) {
					return this.setState({errOpen:true,errMessage:e.toString()},()=>{
						if(cb) cb(e);
					});
				}
				if(cb) cb(null,r);
				agent_config[k]=v;
				this.setState({agent_config});
				//if(r && r.d) UpdateMe(r.d);
			})
		}
	}
	configValidation=(game,k)=>{
		if(['it','bit','sit'].indexOf(k) > -1)
			return v=>{
				v=(v||'0').toString();
				if(!(v.match(/^[\d,.]+$/))) return false;
				let v2=parseFloat(v);
				if(isNaN(v2)) return false;
				return true;
			}
		return v=>{
			v=(v||'0').toString();
			if(!(v.match(/^[\d,.]+$/))) return false;
			let v2=parseFloat(v);
			if(isNaN(v2)) return false;
			let min=parseFloat(((this.state.game[game]||{})[Alias[k]+'_min'])|| 0),
				max=parseFloat(((this.state.game[game]||{})[Alias[k]+'_max'])|| 20);
			return !((v2 < min) || (v2 > max));
		}
	}
	closeErr=()=>{
		this.setState({errOpen:false,errField:''});
	}
	configSave=(g,k)=>{
		return (value,cb)=>{
			let fld={};
			fld['config['+g+']['+k+']']=value;
			Put('user/'+this.state.id,fld,(e,r)=>{
				if(e) {
					return this.setState({errOpen:true,errMessage:e.toString()},()=>{
						cb(e);
					});
				}
				cb(null,r);
				//if(r && r.d) UpdateMe(r.d);
			})
		}
	}
	scSave=g=>{
		return (values,cb)=>{
			let fld={};
			Object.keys(values).forEach(n=>{
				fld['config['+g+'][sc]['+n+']']=values[n];
			})
			Put('user/'+this.state.id,fld,(e,r)=>{
				if(e) {
					return this.setState({errOpen:true,errMessage:e.toString()},()=>{
						if(cb) cb(e);
					});
				}
				if(cb) cb(null,r);
				//if(r && r.d) UpdateMe(r.d);
			})
		}
	}
	nameSave=(name,cb)=>{
		Put('user/'+this.state.id,{name},(e,r)=>{
			console.log('e==>',e);
			console.log('r==>',r);
			if(e){
				return this.setState({errOpen:true,errMessage:e.toString()},()=>{
					cb(e);
				});
			}
			cb(null,r);
		});
	}
	route=uri=>{
		return (e)=>{
			if(e && e.stopPropagation) e.stopPropagation();
			this.props.history.push(uri);
		}
	}
	componentWillReceiveProps(np){
		if(np.match.params.id !== this.state.id){
			this.ready(np.match.params.id);
		}
	}
	ready=(id)=>{
		Get('user/'+id,{withbalance:1,withcreditallocated:1,withactivebet:1,withpath:1},(e,r)=>{
			if(e) return console.log('e==>',e);
			if(r && r.d ){
				console.log('r==>',r);
				let st=r.d;
				this.setState(st,()=>{
					appEvent.fire('titleChange','User Detail : '+this.state.id);
				});
			}
		});
	}
	componentWillMount(){
		this.ready(this.props.match.params.id);

		Game((e,game)=>{
			this.setState({game});
		});
		
		Config((e,sConfig)=>{
			this.setState({
				sConfig,
				//credit:((sConfig||{}).default_credit||{})[this.memberType]||'0'
			});
		})
	}
	csClick=(status)=>{
		return ()=>{
			let {id}=this.state;
			let title={
				'terminate':`Terminate User ${id} ?`,
				'lock':`Lock User ${id} ?`,
				'suspend':`Suspend User ${id} ?`,
				'unsuspend':`Unsuspend User ${id} ?`,
				'unlock':`Unlock User ${id} ?`
			};
			this.setState({
				csOpen:true,
				csTitle:title[status],
				csOK:({reason})=>{
					Put('status/'+id,{status,why:reason},(e,r)=>{
						//this.setState({csOpen:false})
						if(e) {
							console.log('e==>',e);
							return this.setState({csOpen:false,errOpen:true,errMessage:(e||{}).e.toString()});
						}
						return this.setState({csOpen:false},()=>{
							this.ready(id);
						});
					})
					//console.log(reason)
					//this.setState({csOpen:false});
				}
			})
		}
	}
	creditClick=(type)=>{
		return ()=>{
			let {id}=this.state;
			let title={
				'increase':`Increase Credit Limit of ${id}`,
				'decrease':`Decrease Credit Limit of ${id} ?`,
				'edit':`Update Credit Limit of ${id} ?`,
			};
			this.setState({
				ccOpen:true,
				ccTitle:title[type],
				ccOK:({reason,amount})=>{
					//return console.log({type,reason,amount});
					Put('credit/'+id,{type,amount,why:reason},(e,r)=>{
						console.log('r==>',r);
						if(e) {
							console.log('e==>',e);
							return this.setState({ccOpen:false,errOpen:true,errMessage:(e||{}).e.toString()});
						}
						return this.setState({ccOpen:false},()=>{
							this.ready(id);
						});
					})
				}
			});
		}
	}
	formatNumb=n=>{
		return this.context.intl.formatNumber(n,{style: 'decimal',minimumFractionDigits:2,maximumFractionDigits:2});
	}
	render(){
		let { classes} = this.props,
			{id,type,name,credit,balance,credit_allocated,active_bet,config,agent_config,status,active,game,path,errOpen,errMessage,cpOpen,cepOpen,csTitle,csOpen,csOK,ccOpen,ccOK,ccTitle,successOpen,successMsg} = this.state;
		status=status||'normal';
		agent_config=agent_config||{};
		
		return <div className={classes.root}>
			<div className={classes.content}>
				<List className={classes.sub}>
					<Typography type="subheading" gutterBottom>Profile</Typography>
					<Divider />
					<Item c={classes} label={"Id"} value={id} valueCn={classes.bold} />
					<EditableItem c={classes}
						value={name}
						//onChange={this.nameChange}
						label={"Name"}
						rootClass={"item"}
						type={"text"}
						onSave={this.nameSave}/>
					<Item c={classes} label={"Type"} value={UserType[type]} />
					<Divider />
					{/*<CreditEditable c={classes} label={'Credit Limit'} valueCn={classes.valueContainer} value={this.formatNumb(credit||0)} onPlus={this.creditClick('increase')} onMin={this.creditClick('decrease')}/>*/}
					<CreditEditable c={classes} label={'Credit Limit'} valueCn={classes.valueContainer} value={this.formatNumb(credit||0)} onEdit={this.creditClick('edit')}/>
					{!!balance && <Item c={classes} label={"Balance"} value={this.formatNumb(balance||0)} />}
					{!!credit_allocated && <Item c={classes} label={"Credit Allocated"} value={this.formatNumb(credit_allocated||0)} />}
					{!!active_bet && <Item c={classes} label={"Active Bet"} value={this.formatNumb(active_bet||0)} />}
					<Item c={classes} label={"Credit Left"} value={this.formatNumb(parseFloat(credit||0)+parseFloat(balance||0)-parseFloat(credit_allocated||0)-parseFloat(active_bet||0))} />
					{type==='agent' && <Divider />}
					{type==='agent' && <SwitchInput c={classes} label={'Create Intake'} checked={!!JSON.parse(agent_config.ci||false)} onChange={this.acChange('ci')} />}
					{type==='agent' && <SwitchInput c={classes} label={'Create Agent'} checked={!!JSON.parse(agent_config.ca||false)} onChange={this.acChange('ca')} />}
					{type==='agent' && <SwitchInput c={classes} label={'Place Bet'} checked={!!JSON.parse(agent_config.pb||false)} onChange={this.acChange('pb')} />}
					
					<Divider />
					<Item c={classes} label={"Status"} value={!active?'Terminated':status||'Active'} />
					<Divider />
					<div style={{display:'flex',flexDirection:'row'}}>
						<div style={{flex:1}}>
							{active && <Button raised color={'secondary'} className={classes.button2} onClick={this.csClick('terminate')}>Terminate</Button>}
							{active && status!=='locked' && <Button raised color={'secondary'} className={classes.button2} onClick={this.csClick('lock')}>Lock</Button>}
							{active && status!=='suspended' && <Button raised color={'secondary'} className={classes.button2} onClick={this.csClick('suspend')}>Suspend</Button>}
						</div>
						<div>
							{active && status==='locked' && <Button raised color={'secondary'} className={classes.button2} onClick={this.csClick('unlock')}>Unlock</Button>}
							{active && status==='suspended' && <Button raised color={'secondary'} className={classes.button2} onClick={this.csClick('unsuspend')}>Unsuspend</Button>}
						</div>
					</div>
				</List>
				<List className={classes.sub}>
					<Typography type="body2" gutterBottom>Configuration</Typography>
					<Divider />
					{['4D','3D','2D','1D'].map(g=><div className={classes.sub2} key={g}>
						<Typography type="subheading" gutterBottom>{g}</Typography>
						{(type==='agent' || type==='player') && g!=='1D' && <EditableItem
							c={classes}
							value={(config[g]||{}).rb}
							def={'0'}
							after={`% (${(game[g]||{})[Alias.rb+'_min']|| '0'} - ${(game[g]||{})[Alias.rb+'_max']|| '20'})`}
							label={"Rebate"}
							onSave={this.configSave(g,'rb')}
							validation={this.configValidation(g,'rb')}/>}
						{(type==='agent' && !!agent_config.ci && g!=='1D') && <EditableItem
							c={classes}
							label={"Big Intake"}
							value={(config[g]||{}).bit}
							def={'0'}
							after={`$`}
							onSave={this.configSave(g,'bit')}
							validation={this.configValidation(g,'bit')}/>}
						{(type==='agent' && !!agent_config.ci && g!=='1D') && <EditableItem
							c={classes}
							label={"Small Intake"}
							value={(config[g]||{}).sit}
							def={'0'}
							after={`$`}
							onSave={this.configSave(g,'sit')}
							validation={this.configValidation(g,'sit')}/>}
						{(type==='agent' && !!agent_config.ci && g==='1D') && <EditableItem
							c={classes}
							label={"Intake"}
							value={(config[g]||{}).it}
							def={'0'}
							after={`$`}
							onSave={this.configSave(g,'it')}
							validation={this.configValidation(g,'it')}/>}
						{/*((type==='player' || !!agent_config.pb) && g!=='1D') && <StrikeItem classes={classes}
							after={`% (${(game[g]||{})[Alias.sc+'_min']|| '0'} - ${(game[g]||{})[Alias.sc+'_max']|| '20'})`}
							values={(config[g]||{}).sc||{}}
							game={game[g]}
							onChange={this.scSave(g)}
							validation={this.configValidation(g,'sc')}/>*/}
						{((type==='player' || !!agent_config.pb) && g!=='1D') && <EditableItem
							c={classes}
							label={"Strike Commission"}
							value={(config[g]||{}).sc}
							def={'0'}
							after={`% (${(game[g]||{})[Alias.sc+'_min']|| '0'} - ${(game[g]||{})[Alias.sc+'_max']|| '20'})`}
							onSave={this.configSave(g,'sc')}
							validation={this.configValidation(g,'sc')}/>}
					</div>)}
				</List>
			</div>
			<div className={classes.footer}>
				<div className={classes.footerLeft}>
					<div className={classes.bc}>
						{!!path && <BreadCrumbs className={classes.bcContent} data={path} route={this.route} first={'/me'} prefix={'/user/'}/>}
					</div>
					{type==='agent' && <Button raised color={'secondary'} className={classes.button2} onClick={this.route('/downline/'+id)}>Show Downline</Button>}
				</div>				
				<div className={classes.footerRight}>
					<Button raised color={'secondary'} className={classes.button2} onClick={()=>{this.setState({cpOpen:true})}}>Change Password</Button>
					<Button raised color={'secondary'} className={classes.button2} onClick={()=>{this.setState({cepOpen:true})}}>Change Emergency Password</Button>
				</div>
			</div>
			<SnackBar open={errOpen} onClose={this.closeErr} message={errMessage} classes={{root:classes.errorBox}}/>
			<SnackBar open={successOpen} onClose={this.onSuccessClose} message={successMsg} classes={{root:classes.succesBox}}/>
			{ccOpen && <ChangeCredit open={ccOpen} title={ccTitle} onClose={this.ccClose} onOK={ccOK}/>}
			{csOpen && <ChangeStatus open={csOpen} title={csTitle} onClose={this.csClose} onOK={csOK}/>}
			{cpOpen && <ChangePassword open={cpOpen} onClose={this.cpClose} onOK={this.cpOK}/>}
			{cepOpen && <ChangePassword open={cepOpen} onClose={this.cepClose} onOK={this.cepOK}
					title={'Change Emergency Password'}
					label={'New Emergency Password'}
					confirmLabel={'Confirm Emergency Password'}
				/>}
		</div>
	}
}

export default withRoot(withStyles(styles, { withTheme: true })(UserDetail));