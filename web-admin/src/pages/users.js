import React,{Component} from 'react';
import Divider from 'material-ui/Divider';
import List,{ ListItem} from 'material-ui/List';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import IconButton from 'material-ui/IconButton';
import Button from 'material-ui/Button';
import PersonIcon from 'material-ui-icons/Person';

import withRoot from '../components/withRoot';
import {appEvent} from '../components/events';
import {Me,UserType} from '../components/game';
import {Get} from '../components/ajax';
import {BreadCrumbs,TableClasses,Classes} from '../components/dialog';

const styles = theme => ({
	...TableClasses(theme),
	...Classes(theme),
	root: {
		width: '100%',
		display:'flex',
		flexDirection:'column',
		flex:1,
		height:'100%',
	},
	sub:{
		width: '100%',
		padding:5,
		boxSizing:'border-box',
		flex:1,
		overflow:'auto',
	},
	sub2:{
		width: '100%',
		paddingLeft:24,
		boxSizing:'border-box',
	},
	item:{
		display:'flex',
	},
	item2:{
		display:'flex',
		padding:[6,0,6,20]
	},
	button: {
		//margin:[-10,0],//margin: theme.spacing.unit,
		width:24,
		height:24,
	},
	button2: {
		margin: theme.spacing.unit,
	},
	tableRow:{
		padding:[0,0],
		borderTop:[1,'solid','#eee'],
		transition:'background .2s'
	},
	agentRow:{
		cursor:'pointer',
		'&:hover':{
			backgroundColor:theme.palette.secondary.light
		}
	},
	col:{
		flex:1,
		borderLeft:[1,'solid','#eee'],
		//borderRight:[1,'solid','rgba(0,0,0)'],
		textAlign:'center',
		padding:[5,2],
		'&:first-child':{
			borderLeft:[0,'solid','#ddd'],
		}
	},
});

class Users extends Component{
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}
	static contextTypes = {
		intl: PropTypes.object
	}
	state={
		members:[],
		me:{},
		id:this.props.match.params.id||'',
		user:{},
		//openAddAgent:this.props.match.params.action==='addAgent',
		//openAddPlayer:this.props.match.params.action==='addPlayer',
		//createAgent:false,
		pageType:'',
	}
	meChange=me=>{
		this.setState({
			me,
		});
	}
	componentWillReceiveProps(np){
		if(this.state.pageType==='downline'){
			let id=this.getId(np);
			if(id !== this.state.id){
				this.setState({id},()=>{
					this.ready();
				})
			}
		}
	}
	ready=()=>{
		let {pageType,id}=this.state;
		if(pageType==='admin'){
			appEvent.fire('titleChange','Admin List');
			Get('user',{type:'admin'},(e,r)=>{
				this.setState({members:r.d});
			});
		} else if(pageType==='sagent'){
			appEvent.fire('titleChange','Super Agent List');
			Get('user',{type:'sagent',withbalance:1,withcreditallocated:1,withactivebet:1},(e,r)=>{
				console.log('r==>',r);
				this.setState({members:r.d});
			});
		} else {
			if(id){
				appEvent.fire('titleChange',`Manage Downline of ${id}`);
				Get('user',{member_of:id,withbalance:1,withcreditallocated:1,withactivebet:1},(e,r)=>{
					console.log('u==>',r);
					if(e) return console.error('e==>',e);
					if(r && r.d) this.setState({members:r.d});
				});
				Get('user/'+id,{withpath:true},(e,r)=>{
					//console.log('r.d==>',r.d);
					if(e) return console.error('e==>',e);
					if(r && r.d) this.setState({user:r.d});
				});
				return;
			}
			appEvent.fire('titleChange','Manage Downline');
			Get('user',{type:'sagent',withbalance:1,withcreditallocated:1,withactivebet:1},(e,r)=>{
				console.log('r==>',r);
				this.setState({members:r.d});
			});
		}
	}
	getId=(p)=>{
		return (((p||{}).match||{}).params).id||'';
	}
	getPageType=(p)=>{
		let path=((p||{}).match||{}).path||'';
		if(path.match(/^\/admin/)) return 'admin'
		if(path.match(/^\/sagent/)) return 'sagent'
		return 'downline';
	}
	componentWillMount(){
		let pageType=this.getPageType(this.props),
			id=this.getId(this.props);

		this.setState({pageType,id},()=>{
			this.ready();
		});
		
		Me((e,r)=>{
			this.meChange(r);
		});
		
		console.log('this.getId(this.props)',this.getId(this.props));
	}
	componentWillUnmount(){
		//appEvent.off('meChange',this.meChange);
	}
	route=uri=>{
		return (e)=>{
			if(e && e.stopPropagation) e.stopPropagation();
			console.log(uri)
			this.props.history.push(uri);
		}
	}
	rowClick=(id,type)=>{
		return (e)=>{
			if(e.stopPropagation) e.stopPropagation();
			let {pageType,me}=this.state;
			if(pageType!=='downline'){
				if(!(me||{}).id) return;
				if(id===me.id) return this.route('/me')();
				this.route('/'+pageType+'s/profile/'+id)();
			} else {
				this.route('/downline/'+id)();
			}
		}
	}
	formatNumb=n=>{
		return this.context.intl.formatNumber(n,{style: 'decimal',minimumFractionDigits:2,maximumFractionDigits:2});
	}
	render(){
		const { classes } = this.props,
			{pageType,members,id,me,user} = this.state,
			meType=(me||{}).type||'admin',
			createAgent=(user||{}).type==='sagent'||((user||{}).agent_config||{}).ca;
		let path=JSON.parse(JSON.stringify((user||{}).path||[]));
		if(path.length) path.unshift('Super Agents');

		return <div className={classes.root}>
			{pageType!=='downline' && (pageType==='sagent' || meType!=='admin') &&
				<div className={classes.tableFooter}>
					<div />
					<div className={classes.footerButtons}>
						<Button raised color={"secondary"} className={classes.button2} onClick={()=>{
							this.route('/'+pageType+'s/'+(pageType==='admin'?'addAdmin':'add'))();
						}}>Add {pageType==='sagent'?'Super Agent':'Admin'}</Button>
					</div>
				</div>
			}
			{pageType==='downline' && !!id &&
				<div className={classes.tableFooter}>
					<div>
						{!!path.length && <BreadCrumbs data={path} route={this.route} />}
					</div>
					<div className={classes.footerButtons}>
						<div>{id===me.id?"Manage downline":"Manage "+id+" downline"}</div>
						<div className={classes.footerSub}>
							<Button raised disabled={!createAgent} color={"secondary"} className={classes.button2} onClick={()=>{
								this.route('/downline/'+id+'/addAgent')();
							}}>Add Agent</Button>
							<Button raised color={"secondary"} className={classes.button2} onClick={()=>{
								this.route('/downline/'+id+'/addPlayer')();
							}}>Add Player</Button>
						</div>
					</div>
				</div>
			}
			<List className={classes.sub}>
				<Divider />
				<ListItem className={classes.tableTitle}>
					<div className={classes.col}>No</div>
					<div className={classes.col+' '+classes.col2}>ID</div>
					<div className={classes.col+' '+classes.col4}>Name</div>
					<div className={classes.col+' '+classes.col2}>Type</div>
					{(pageType==='sagent'||(pageType==='downline')) && <div className={classes.col+' '+classes.col3}>Credit Limit</div>}
					{(pageType==='sagent'||(pageType==='downline')) && <div className={classes.col+' '+classes.col3}>Credit Left</div>}
					{pageType==='downline' && <div className={classes.col+' '+classes.col}>{""}</div>}
				</ListItem>
				{members.map((v,k)=><ListItem className={classes.tableRow+' '+(v.type!=='player'?classes.agentRow:'')} key={v.id} onClick={v.type!=='player'?this.rowClick(v.id,v.type):null}>
					<div className={classes.col}>{k+1}</div>
					<div className={classes.col+' '+classes.col2+' '+classes.cols}>{v.id}</div>
					<div className={classes.col+' '+classes.col4+' '+classes.cols}>{v.name}</div>
					<div className={classes.col+' '+classes.col2+' '+classes.cols}>{UserType[v.type]}</div>
					{(pageType==='sagent'||(pageType==='downline')) && <div className={classes.col+' '+classes.col3+' '+classes.coln}>{this.formatNumb(v.credit||0)}</div>}
					{(pageType==='sagent'||(pageType==='downline')) && <div className={classes.col+' '+classes.col3+' '+classes.coln}>{this.formatNumb(parseFloat(v.credit||0)+parseFloat(v.balance||0)-parseFloat(v.credit_allocated||0)-parseFloat(v.active_bet||0))}</div>}
					{pageType==='downline' && <div className={classes.col+' '+classes.col}><IconButton className={classes.button}
						onClick={(e)=>{
							//e.preventDefault();
							if(v.type==='sagent') return this.route('/sagents/profile/'+v.id)(e);
							this.route('/user/'+v.id)(e);
						}}
					><PersonIcon/></IconButton></div>}
				</ListItem>)}
				<Divider />
			</List>
		</div>
	}
}

export default withRoot(withStyles(styles, { withTheme: true })(Users));