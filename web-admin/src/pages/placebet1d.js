import React,{Component} from 'react';
import PropTypes from 'prop-types';
//import Autosuggest from 'react-autosuggest';
import { withStyles } from 'material-ui/styles';
import withRoot from '../components/withRoot';
import {appEvent} from '../components/events';
//import IconButton from 'material-ui/IconButton';
import Button from 'material-ui/Button';
//import Paper from 'material-ui/Paper';
//import { MenuItem } from 'material-ui/Menu';
import { CircularProgress } from 'material-ui/Progress';

//import PersonIcon from 'material-ui-icons/Person';
//import SearchIcon from 'material-ui-icons/Search';

import {Me,Config,Game,/*RequestDrawDate,*/RequestDrawDates/*,CurrentPrize*/,saveTmpBet,loadTmpBet,defDay,enabledDay} from '../components/game';
import {/*Get,*/Post,flatten} from '../components/ajax';
//import {/*defBetList,enableDay,defaultDay,calcFactor,*/BetRow} from '../components/BetRow';
import {SnackBar,Confirm, BetResult, SearchBar,Classes} from '../components/dialog';

import {TicketDetail} from './tickets';
//import {formatNumber} from 'react-intl';

const styles = theme =>{
	window.theme=theme;
	return {
		...Classes(theme),
		root: {
			flexGrow: 1,
			//marginTop: theme.spacing.unit * 3,
			backgroundColor: theme.palette.background.paper,
			padding:10
		},
		hRow:{
			padding:[5,0],
			fontSize:'12pt',
			display:'flex',
			alignItems:'center'
		},
		dateItem:{
			padding:[0,5]
		},
		disable:{
			color:'rgba(0,0,0,0.5)'
		},

		hSelect:{
			fontSize:'12pt'
		},
		hErr:{
			color:'red'
		},
		number:{
			padding:[2,10],
			fontSize:18,
			width:'100%',
			boxSizing:'border-box',
		},
		numberErr:{
			borderColor:'red'
		},
		select:{
			fontSize:18,
		},
		toolbar:{
			padding:[10,0]
		},
		errRow:{
			background:'rgba(255,0,0,0.2)',
		},
		validRow:{
			background:'rgba(0,255,0,0.2)',
		},
		table:{
			border:[1,'solid','#000'],
			maxWidth:600,
		},
		tableRow:{
			display:'flex',
			flexDirection:'row',
			textAlign:'center',
			alignItems:'center',
			//borderBottom:[1,'solid','#aaa']
		},
		tableHead:{
			fontWeight:'bold',
			borderBottom:[1,'solid','#aaa']
		},
		td:{
			minWidth:40,
			padding:1,
			fontSize:18
		},
		td2:{
			padding:1,
			flex:1,
			minWidth:50,
			fontSize:18,
			alignSelf:'stretch',
		},
		tdh:{
			minWidth:40,
			//padding:1,
			fontSize:18,
			borderRight:[1,'solid','#aaa'],
			padding:[5,0]
		},
		td2h:{
			//padding:1,
			flex:1,
			minWidth:50,
			fontSize:18,
			borderRight:[1,'solid','#aaa'],
			padding:[5,0]
		},
		tInput:{
			padding:[3,10],
			width:'100%',
			fontSize:20,
			boxSizing:'border-box'
		},
		tSelect:{
			height:'100%',
			width:'100%',
			fontSize:18
		},
		wrapper: {
			margin: theme.spacing.unit,
			position: 'relative',
			display:'inline-block'
		},
		buttonProgress: {
			color: 'teal',//green[500],
			position: 'absolute',
			top: '50%',
			left: '50%',
			marginTop: -12,
			marginLeft: -12,
		},
		sItem2:{
			textAlign:'center'
		},
		sFirstItem:{
			textAlign:'left',
			padding:[10,0],
			//borderBottom:[1,'solid','#eee']
			fontSize:'12pt'
		},
		col:{},
		colT:{}
	}
};

//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions
//function escapeRegExpChars(string) {
//	return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
//}


function createBlankBets(lng){
	let items=[];
	for(let i=0;i<lng;i++){
		items.push({n:'o',m:'1',a:'',er:false,em:true});
	}
	return items;
}

function createBlankItems(lng){
	let items=[];
	for(let i=0;i<lng;i++){
		items.push([null,null,null]);
	}
	return items;
}

export const bList={
	o:'Odd',
	e:'Even',
	b:'Big',
	s:'Small'
}

export const mList={
	'1':'1st Prize',
	'2':'2nd Prize',
	'3':'3rd Prize',
}

class PlaceBet1D extends Component{
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}
	static contextTypes = {
		intl: PropTypes.object
	}
	_uPermute={}
	items=createBlankItems(10)
	//ns='1 2 3 4 5 6 7 8 9 10'.split(' ')
	state={
		me:{},
		id:'',
		player:{},
		suggestions: [],
		searchMode:false,
		drawDate:[],
		drawDay:{},
		closeTime:'13:00',
		//game:{},
		currentPrize:{},
		ready:false,
		
		clearEnable:false,
		buttonEnable:false,
		
		//drawDate:drawDate().map(v=>v.toDateString()).join(' | '),
		
		// reload saat update user
		credit:0,
		balance:0,
		//available:800,
		
		total:0,
		//cf:{},//calcFactor(),
		//\reload saat update user
		
		eItems:[], //error items
		iItems:[], //incomplete items
		//big:0,
		//small:0,
		day:'1',//defaultDay(),//'1',
		//defBet:'4D',
		defBet:'o',
		defMatch:'1',
		err:false,
		
		loading:false,
		confirmOpen:false,
		successOpen:false,
		errorOpen:false,
		bets:createBlankBets(10),
		resultOpen:false,
		betResult:{}

	}

	calc=()=>{
		let {day,bets,drawDay}=this.state,
			sTotal=bets.filter(v=>!v.er && !v.em)
				.reduce((ttl,v)=>(ttl+=parseFloat(v.a)),0);		
		this.setState({total:sTotal*(drawDay[day]||[]).length},()=>{
			let {total,balance,credit}=this.state;
			this.setState({err:total > credit+balance});
		});
	}
	dayChange=(e)=>{
		this.setState({day:e.target.value},this.calc);
	}

	setPlayer=o=>{
		console.log('player==>',o)
		let credit=parseFloat(o.credit||'0'),
			balance=parseFloat(o.balance||'0'),
			credit_allocated=parseFloat(o.credit_allocated||'0'),
			active_bet=parseFloat(o.active_bet||'0');
			//available=credit+balance-credit_allocated-active_bet;
		this.setState({
			id:o.id,
			searchMode:false,
			player:o,
			credit,
			balance,
			credit_allocated,
			active_bet,
			//available,
			cf:o.config||{},
			ready:true,
		},()=>{
			this.calc();
		});
	}
	saveTmpBet=()=>{
		let {bets,defBet,defMatch}=this.state;
		let dt=bets.map(v=>{
			let {n,m,a,er,em}=v;
			if(!er && !em) return {n,m,a};
			if(!er) return {n,m,a:''};
			return {n:defBet,m:defMatch,a:''};
		})
		saveTmpBet('1D',dt);
	}
	loadTmpBet=()=>{
		let {bets/*,defBet,defMatch*/}=this.state;
		let bts=loadTmpBet('1D')||[];
		bts.forEach((v,k)=>{
			let {n,m,a/*,em*/}=v;
			bets[k]={n,m,a,er:false,em:!parseFloat(a)};
		});
		this.setState({bets},()=>{
			this.calc();
		});
	}
	clear=()=>{
		let {bets,defBet,defMatch}=this.state;
		bets.forEach((v,k)=>{
			bets[k]={
				n:defBet,
				m:defMatch,
				a:'',
				em:true};
		});
		this.setState({bets},()=>{
			this.calc();
		});
	}
	
	submit=()=>{
		this.setState({
			confirmOpen:true,
			confirmOnOK:()=>{
				this.setState({confirmOpen:false,loading:true},()=>{
					this.processSubmit();
				});
			},
		});
	}
	processSubmit=()=>{
		let {id,me,day,bets}=this.state;
		let dt={place_to:id,day_id:day,form_type:'1D'};
		
		dt.detail=bets.filter(v=>(!v.er && !v.em)).map(v=>{
			let {n,m,a,/*er,em*/}=v;
			return {n,m,a};
		});
		if(dt.detail.length){
			Post('bet',flatten(dt),(e,r)=>{
				this.setState({loading:false});
				if(e){
					console.log('e==>',e);
					return this.setState({errorOpen:true,errorMsg:((e||{}).e||'').toString()});
				}
				this.setState({successOpen:true,successMsg:'Bet has been placed'},()=>{
					this.clear();
					if(id===me.id){
						Me((e,me)=>{
							this.setState({me},()=>{
								this.setPlayer(me);
							})
						},true);
					} else {
						if(this.seachBar && this.seachBar.findId) this.seachBar.findId(id);
					}
					this.setState({resultOpen:true,betResult:r.d});
				});
			});
		}
	}
	save=()=>{
		let {bets}=this.state,
			dt={form_type:'1D',detail:{}};
		dt.detail.bets=bets.filter(v=>!v.er && !v.em).map(v=>{
			let {n,m,a}=v;
			return {n,m,a};
		});
		if(dt.detail.bets.length){
			Post('savebet',flatten(dt),(e,r)=>{
				if(e){
					console.log('e==>',e);
					return this.setState({errorOpen:true,errorMsg:e.toString()});
				}
				return this.setState({successOpen:true,successMsg:'Bets have been saved'});
			});
		}
	}
	componentWillMount(){
		appEvent.fire('titleChange','Place 1D Bets');
		Game((e,game)=>{
			console.log('game==>',game);
			//this.setState({config});
			this.setState({game})
		});
		Config((e,config)=>{
			//console.log('config==>',JSON.stringify(config));
			//this.setState({config});
			this.setState({
				drawDay:config.draw_day,
				closeTime:config.bet_time.close,
			});
			
			RequestDrawDates((e,drawDates)=>{
				this.setState({
					drawDates,
					day:defDay(config.draw_day,drawDates)});
			});
			
		});
		Me((e,me)=>{
			this.setState({me},()=>{
				if(me && me.id && me.can_play && me.status !== 'suspended' && me.status !=='locked') {
					this.setPlayer(me);
				} else {
					if(this.seachBar && this.seachBar.searchClick) this.seachBar.searchClick();
				}
			});
		},true);
	}
	componentDidMount(){
		this.loadTmpBet();
	}
	componentWillUnmount(){
		this.saveTmpBet();
	}
	formatNumb=n=>{
		return this.context.intl.formatNumber(n,{style: 'decimal',minimumFractionDigits:2,maximumFractionDigits:2});
	}
	onSuccessClose=()=>{
		this.setState({successOpen:false});
	}
	onErrorClose=()=>{
		this.setState({errorOpen:false});
	}
	
	onInputChange=(i,t)=>{
		return (e)=>{
			let {bets}=this.state;
			let v=e.target.value;
			if(t==='a'){
				let v2=parseFloat(v||'0');
				bets[i].er=isNaN(v2);
				bets[i].em=!v2;
			}
			bets[i][t]=v;
			this.setState({bets},this.calc);
		}
	}
	keyDown=(y,x)=>{
		const rpo=i=>({y:Math.floor(i/3),x:i%3});
		
		return (e)=>{
			if(!(e.keyCode>=37 && e.keyCode<=40)) return;
			
			e.preventDefault();
			let po=y*3+x;
			if(e.keyCode===39) po++
			else if(e.keyCode===37) po--
			else if(e.keyCode===40) po+=3
			else if(e.keyCode===38) po-=3;

			let r=rpo((po+10*3)%(10*3));
			let el=this.items[r.y][r.x];
			if(el.setSelectionRange) el.setSelectionRange(0, el.value.length);
			setTimeout(()=>{el.focus();},10);
		}
	}
	defChange=(t,s)=>{
		return e=>{
			let value=e.target.value;
			this.setState({[s]:value},()=>{
				let {bets}=this.state;
				bets.filter(v=>v.em && !v.er).forEach(v=>{v[t]=value});
				this.setState({bets});
			})
		}
	}
	confirmClose=()=>{
		this.setState({confirmOpen:false});
	}
	//meClick=(e)=>{
	//	this.findId(this.state.me.id);
	//}
	render(){
		const { ready, /*searchMode,*/ id, day,defBet,defMatch,/*eItems,iItems,*/ drawDates,drawDay, credit,balance,credit_allocated,active_bet,me, err,
			/*suggestions, currentPrize,*/
			successOpen, successMsg, errorOpen, errorMsg,
			bets, total,loading, confirmOpen,confirmOnOK, resultOpen,betResult} = this.state;
		let available=parseFloat(credit||0)+parseFloat(balance||0)-parseFloat(credit_allocated||0)-parseFloat(active_bet||0);
		const ers=bets.reduce((ers,v)=>ers+(v.er?1:0),0);
		const formatNumber=this.formatNumb;
		const { classes } = this.props;
		return <div className={classes.root}>
			<SearchBar label={'Place To'}
				getRef={(e)=>{this.seachBar=e;}}
				id={id}
				me={(me && me.can_play && me.status !== 'suspended' && me.status !=='locked'?me:false)}
				allowSearch={me.type!=='player'}
				searchParams={{active:true,can_play:true}}
				userDetailValidate={d=>d.active && d.can_play && d.status!=='suspended' && d.status!=='locked'}
				userDetailParams={{withbalance:1,withcreditallocated:1,withactivebet:1}}
				onSearchMode={()=>{this.setState({ready:false})}}
				onFound={this.setPlayer}/>
			<div className={classes.hRow}>
				<div>Draw Date:</div>
				<div>
					{Object.keys(drawDates||{}).map((v,k)=>
						<span key={k} className={classes.dateItem+(drawDates[v].closed?' '+classes.disable:'')}>{new Date(drawDates[v].close).toDateString()}</span>)}
				</div>
			</div>
			
			<div className={classes.hRow}>
				Select Day :
				<select disabled={!ready || loading} className={classes.hSelect} name={"day"} value={day} onChange={this.dayChange}>
					{Object.keys(drawDay).map(d=><option disabled={!enabledDay(d,drawDay||{},drawDates||{})} key={d} value={d}>{d+" = <"+drawDay[d].join(', ')+"> "}</option>)}
				</select>
				&nbsp;
				Default Bet :
				<select className={classes.hSelect} name={"defBet"} value={defBet} onChange={this.defChange('n','defBet')}>
					{Object.keys(bList).map(k=><option key={k} value={k}>{bList[k]}</option>)}
				</select>&nbsp;
				Default Match :
				<select disabled={!ready || loading} className={classes.hSelect} name={"defMatch"} value={defMatch} onChange={this.defChange('m','defMatch')}>
					{Object.keys(mList).map(k=><option key={k} value={k}>{mList[k]}</option>)}
				</select>
			</div>
			<div className={classes.hRow+(!ready?' '+classes.disable:'')}>
				Credit Limit : {!ready?'-':formatNumber(credit)} &nbsp; &nbsp;
				Balance : {!ready?'-':formatNumber(balance)} &nbsp; &nbsp;
				Available : {!ready?'-':formatNumber(available)}
			</div>
			<div className={[
					classes.hRow,
					total>available?classes.hErr:null
				].join(' ')}>
				Total : {formatNumber(total)}
			</div>
			<div className={classes.toolbar}>
				<Button disabled={(!total && !ers) || loading} raised color={"secondary"} onClick={this.clear}>Clear</Button> &nbsp;
				<Button disabled={!total || loading} raised color={"secondary"} onClick={this.save}>Save</Button> &nbsp;
				<div className={classes.wrapper}>
					<Button disabled={!total || !!ers || err || loading || !ready || total>available} raised color={"primary"} onClick={this.submit}>Submit</Button>
					{loading && <CircularProgress size={24} className={classes.buttonProgress} />}
				</div>
			</div>
			<div className={classes.table}>
			<div className={classes.tableRow+' '+classes.tableHead}>
				<div className={classes.tdh}>No</div>
				<div className={classes.tdh}>D</div>
				<div className={classes.td2h}>Bet</div>
				<div className={classes.td2h}>Match</div>
				<div className={classes.td2h}>Amount</div>
			</div>
			{bets.map((v,k)=><div className={classes.tableRow+(v.er?' '+classes.errRow:(!v.em?' '+classes.validRow:''))} key={k}>
					<div className={classes.td}>{k+1}</div>
					<div className={classes.td}>{day}</div>
					<div className={classes.td2}><select disabled={loading || !ready} ref={e=>{this.items[k][0]=e;}} className={classes.tSelect} value={v.n} onChange={this.onInputChange(k,'n')} onKeyDown={this.keyDown(k,0)}>
						{Object.keys(bList).map(k=><option key={k} value={k}>{bList[k]}</option>)}
					</select></div>
					<div className={classes.td2}><select disabled={loading || !ready} ref={e=>{this.items[k][1]=e;}} className={classes.tSelect} value={v.m} onChange={this.onInputChange(k,'m')} onKeyDown={this.keyDown(k,1)}>
						{Object.keys(mList).map(k=><option key={k} value={k}>{mList[k]}</option>)}
					</select></div>
					<div className={classes.td2}>
						<input disabled={loading || !ready} ref={e=>{this.items[k][2]=e;}} type={'tel'} className={classes.tInput} value={v.a} onChange={this.onInputChange(k,'a')} onKeyDown={this.keyDown(k,2)} />
					</div>
				</div>)}
			</div>
			{resultOpen && <BetResult open={resultOpen} onClose={()=>{this.setState({resultOpen:false})}}>
				<div className={classes.hRow}>Ticket Page Id : {betResult.page.id}</div>
				<TicketDetail tickets={betResult.tickets} formatNumber={this.formatNumb} v={betResult.page} classes={classes}/>
			</BetResult>}
			{confirmOpen && <Confirm open={confirmOpen} title={'Place Bet?'} msg={'Are you sure you want to place this bets?'} onClose={this.confirmClose} onOK={confirmOnOK}/>}
			<SnackBar open={successOpen} onClose={this.onSuccessClose} message={successMsg} classes={{root:classes.succesBox}}/>
			<SnackBar open={errorOpen} onClose={this.onErrorClose} message={errorMsg} classes={{root:classes.errorBox}}/>
		</div>
	}
}

export default withRoot(withStyles(styles, { withTheme: true })(PlaceBet1D));