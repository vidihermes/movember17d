import React,{Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';

import Button from 'material-ui/Button';
import {appEvent} from '../components/events';
import {Get,Put,Post,flatten} from '../components/ajax';
import {PrizeRank,CompleteDrawDate,ServerDate,timeOffsetDiff} from '../components/game';
import {SnackBar} from '../components/dialog';

const styles = theme => ({
	root: {
		width: '100%',
		display:'flex',
		flexDirection:'column',
		flex:1,
		height:'100%',
	},
	place:{
		flex:1,
		padding:20,
		overflow:'auto',
	},
	table:{
		textAlign:'center'
	},
	thead:{
		backgroundColor:theme.palette.primary.main,
	},
	input:{
		padding:[0,10],
		boxSizing:'border-box'
	},
	err:{
		border:[1,'solid','red'],
		outline:[1,'solid','red'],
	},
	footer:{
		borderTop:[1,'solid','#ddd'],
		textAlign:'right'
	},
	button2:{
		margin:10
	},
	succesBox:{
		backgroundColor:theme.palette.primary.main,
		color:theme.palette.text.primary
	},
	errorBox:{
		backgroundColor:theme.palette.error.main
	},
});

function createBlankResult(){
	let rslt={
		'1':'',
		'2':'',
		'3':'',
		's':[],
		'c':[],
	};
	for(let i=0;i<10;i++){
		rslt.s.push('');
		rslt.c.push('');
	}
	return rslt;
}

function rand(){
	return (Math.floor(1000 + Math.random() * 9000)).toString();
}

function createRandomResult(){
	let rslt={
		'1':rand(),
		'2':rand(),
		'3':rand(),
		's':[],
		'c':[],
	};
	for(let i=0;i<10;i++){
		rslt.s.push(rand());
		rslt.c.push(rand());
	}
	return rslt;
}

function createBlankValidation(){
	let rslt={
		'1':{em:true,er:false},
		'2':{em:true,er:false},
		'3':{em:true,er:false},
		's':[],
		'c':[],
	};
	for(let i=0;i<10;i++){
		rslt.s.push({em:true,er:false});
		rslt.c.push({em:true,er:false});
	}
	return rslt;
}

class Result extends Component{
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}
	state={
		result:createBlankResult(),
		vld:createBlankValidation(),
		successOpen:false,
		errorOpen:false,
		dd:null,
		serverDate:null,
		drawEnable:false,
		force:true,
		timeoffset:null,
	}
	interval=null
	onErrorClose=()=>{
		this.setState({errorOpen:false})
	}
	onSuccessClose=()=>{
		this.setState({successOpen:false})
	}
	updateDrawButton=()=>{
		let {serverDate,dd}=this.state;
		if(serverDate && dd){
			this.setState({drawEnable:serverDate >= dd.close && serverDate <= dd.draw});
		}
	}
	checkServerDate=()=>{
		ServerDate((e,r)=>{
			this.setState({serverDate:r},()=>{
				this.updateDrawButton();
			});
		});
	}
	componentWillMount(){
		appEvent.fire('titleChange','Results');
		timeOffsetDiff((e,timeoffset)=>{
			console.log('timeoffset==>',timeoffset);
			this.setState({timeoffset});
		});
		CompleteDrawDate((cdd)=>{
			//console.log('componentWillMount==>',cdd)
			//if(r && r.d){
				let kys=Object.keys(cdd).filter(k=>!cdd[k].drawn);
				let dd=cdd[kys[0]];
				//console.log('dd==>',dd);
				this.setState({dd},()=>{
					this.updateDrawButton();
				});
			//}
		});
		Get('getcreateresult',{},(e,r)=>{
			console.log('rrr==>',r);
			if(e) return console.error('e==>',e);
			if(r && r.d){
				let {apply_date,...st}=r.d;
				apply_date=(new Date(apply_date)).getTime();
				this.setState({...st,apply_date});
			}
		});
		this.interval=setInterval(this.checkServerDate,5000);
	}
	componentWillUnmount(){
		clearInterval(this.interval);
	}
	random=()=>{
		this.setState({result:createRandomResult()})
	}
	change=(idx)=>{
		return (e)=>{
			let {result,vld}=this.state;
			let value=e.target.value;
			
			result[idx]=value;
			vld[idx].em=!value;
			vld[idx].er=value && !this.valid(value);
			this.setState({result,vld});
		}
	}
	change2=(c,idx)=>{
		return (e)=>{
			let {result,vld}=this.state;
			let value=e.target.value;
			result[c][idx]=value;
			vld[c][idx].em=!value;
			vld[c][idx].er=value && !this.valid(value);
			this.setState({result,vld});
		}
	}
	valid=(v)=>{
		return !!(v||'').match(/^\d{4}$/);
	}
	save=()=>{
		let {id,result}=this.state;
		Put('result/'+id,flatten({result}),(e,r)=>{
			console.log('rrr==>',r);
			if(e) {
				console.error('e==>',e);
				return this.setState({
					errorOpen:true,
					errorMsg:e.toString()
				});
			}
			if(r){
				return this.setState({
					successOpen:true,
					successMsg:'result updated'
				});
			}
		})
	}
	draw=()=>{
		let {id,result,force}=this.state;
		//console.log('result==>',result)
		Post('drawresult/'+id,flatten({result,force}),(e,r)=>{
			console.log('dr==>',r);
			if(e) {
				console.error('e==>',e.e);
				return this.setState({
					errorOpen:true,
					errorMsg:e.e
				});
			}
			if(r){
				return this.setState({
					successOpen:true,
					successMsg:'result drawn'
				},()=>{
					this.setState(r.d);
				});
			}
		})
	}
	render(){
		let {classes}=this.props,
			{apply_date,id,result,vld,successOpen,errorOpen,successMsg,errorMsg,drawn,drawEnable, force,timeoffset}=this.state;
			//console.log('result==>',result);
			//console.log('vld==>',vld);
			//console.log('drawn==>',drawn);
		return <div className={classes.root}>
			<div className={classes.place+' markdown-body'}>
				<table className={classes.table}>
					<thead><tr>
						<th className={classes.thead}>{apply_date?(new Date(apply_date+timeoffset)).toDateString():'Draw Date'}</th>
						<th className={classes.thead}>Draw No.{id}</th>
					</tr></thead>
					<tbody>
						{['1','2','3'].map(v=><tr key={v}>
							<td>{PrizeRank[v]}</td>
							<td><input disabled={drawn} type={'tel'} className={classes.input+' '+(vld[v].er?classes.err:'')} value={result[v]} onChange={this.change(v)}/></td>
						</tr>)}
					</tbody>
					<thead><tr>
						<th className={classes.thead} colSpan={2}>Starter Prizes</th>
					</tr></thead>
					<tbody>
						{[0,1,2,3,4].map(v=><tr key={v}>
							<td><input disabled={drawn} type={'tel'} className={classes.input+' '+(vld.s[v].er?classes.err:'')} value={result.s[v]} onChange={this.change2('s',v)}/></td>
							<td><input disabled={drawn} type={'tel'} className={classes.input+' '+(vld.s[v+5].er?classes.err:'')} value={result.s[v+5]} onChange={this.change2('s',v+5)}/></td>
						</tr>)}
					</tbody>
					<thead><tr>
						<th className={classes.thead} colSpan={2}>Consolation Prizes</th>
					</tr></thead>
					<tbody>
						{[0,1,2,3,4].map(v=><tr key={v}>
							<td><input disabled={drawn} type={'tel'} className={classes.input+' '+(vld.c[v].er?classes.err:'')} value={result.c[v]} onChange={this.change2('c',v)}/></td>
							<td><input disabled={drawn} type={'tel'} className={classes.input+' '+(vld.c[v+5].er?classes.err:'')} value={result.c[v+5]} onChange={this.change2('c',v+5)}/></td>
						</tr>)}
					</tbody>
				</table>
			</div>
			<div className={classes.footer}>
				<Button disabled={drawn} raised color={"secondary"} className={classes.button2} onClick={this.random}>Random</Button>
				<Button disabled={drawn} raised color={"secondary"} className={classes.button2} onClick={this.save}>Save</Button>
				<Button disabled={(drawn || !drawEnable) && !force} raised color={"primary"} className={classes.button2} onClick={this.draw}>Draw</Button>
			</div>
			<SnackBar open={successOpen} onClose={this.onSuccessClose} message={successMsg} classes={{root:classes.succesBox}}/>
			<SnackBar open={errorOpen} onClose={this.onErrorClose} message={errorMsg} classes={{root:classes.errorBox}}/>
		</div>
	}
}

export default withStyles(styles, { withTheme: true })(Result);