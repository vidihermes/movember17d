import React,{Component} from 'react';
import Grid from 'material-ui/Grid';
import Divider from 'material-ui/Divider';
import List,{ ListItem, ListItemText} from 'material-ui/List';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Typography from 'material-ui/Typography';
import IconButton from 'material-ui/IconButton';
import Button from 'material-ui/Button';
import Switch from 'material-ui/Switch';
//import SaveIcon from 'material-ui-icons/Save';
//import ModeEditIcon from 'material-ui-icons/ModeEdit';
import CloseIcon from 'material-ui-icons/Close';
import RefreshIcon from 'material-ui-icons/Refresh';
import { CircularProgress } from 'material-ui/Progress';
//import { MenuItem } from 'material-ui/Menu';
import Collapse from 'material-ui/transitions/Collapse';
//import Snackbar from 'material-ui/Snackbar';

import ExpandLess from 'material-ui-icons/ExpandLess';
import ExpandMore from 'material-ui-icons/ExpandMore';

import withRoot from '../components/withRoot';
import {appEvent} from '../components/events';
import {flatten,Get,Post} from '../components/ajax';
import {Game,Me,Config,Alias,UserType} from '../components/game';
import {SnackBar} from '../components/dialog';

const styles = theme => ({
	root: {
		display:'flex',
		flexDirection:'column',
		width: '100%',
		height: '100%',
		fontFamily: `"Roboto", "Helvetica", "Arial", sans-serif`,
	},
	content:{
		flex:1,
		overflowY:'auto',
		[theme.breakpoints.up('sm')]: {
			display:'flex',
			flexDirection:'row',
			overflowY:'none',
		}
		
	},
	headerContent:{
		padding:[10,5,0,5]
	},
	footer:{
		textAlign:'right',
	},
	sub:{
		width: '100%',
		//float:'left',
		padding:5,
		boxSizing:'border-box',
		[theme.breakpoints.up('sm')]: {
			//width: '50%',
			flex:1,
			overflowY:'auto'
		},
	},
	sub2:{
		width: '100%',
		paddingLeft:24,
		boxSizing:'border-box',
	},
	item:{
		display:'flex',
		padding:[2,16]
	},
	item2:{
		display:'flex',
		padding:[6,0,6,20]
	},
	label:{
		whiteSpace:'nowrap',
		fontSize:'12pt',
		padding:'0px 10px',
	},
	label2:{
		whiteSpace:'nowrap',
		fontSize:'12pt',
		padding:'0px 10px',
	},
	value:{
		fontSize:'12pt',
		paddingLeft:5,
	},
	value2:{
		fontSize:'12pt',
		paddingLeft:0,
		display:'flex',
		alignItems:'center'
	},
	button: {
		//margin:[-10,0],//margin: theme.spacing.unit,
		width:30,
		height:30,
	},
	wrapper: {
		margin: theme.spacing.unit,
		position: 'relative',
		display:'inline-block'
	},
	buttonProgress: {
		color: 'teal',//green[500],
		position: 'absolute',
		top: '50%',
		left: '50%',
		marginTop: -12,
		marginLeft: -12,
	},
	button2: {
		margin: theme.spacing.unit,
	},
	title:{
		...theme.typography.title,
		justifyContent:'center'
	},
	input:{
		padding:[4,10],
		fontSize:'12pt',
		flex:1,
		width:'100%',
		marginLeft:-6
	},
	after:{
		fontSize:'10pt',
		padding:[0,5],
		color:'rgba(0,0,0,0.5)'
	},
	err:{
		borderColor:'#f00',
		outlineColor:'rgba(255,0,0,0.1)'
	},
	
	// strike commission
	sRoot:{
		margin:[0,0,0,20],
		backgroundColor:'rgba(100,0,0,0.1)'
	},
	sButton:{
		//display:'flex',
		padding:[4,10]
	},
	sItem:{
		textAlign:'center',
		backgroundColor:'#efefef',
		padding:0,
		'&:nth-child(even)':{
			backgroundColor:'#e4e4e4'
		}
	},
	sItem2:{
		flex:1,
		backgroundColor:'rgba(0,0,255,0.1)',
		padding:6,
		'&:nth-child(even)':{
			backgroundColor:'rgba(0,0,255,0.05)'
		}
	},
	sAddForm:{
		flexDirection:'column',
		alignItems:'stretch',
		padding:0,
	},
	sAddRow:{
		display:'flex',
		flexDirection:'row',
		alignItems:'center',
		backgroundColor:'rgba(255,255,255,0.7)',
		'&:nth-child(odd)':{
			backgroundColor:'rgba(255,255,255,0.5)'
		}
	},
	sAddBtn:{
		flex:1,
		margin:5
	},
	sFormItem:{
		flex:1,
		'&:nth-child(odd)':{
			padding:[0,10]
		}
	},
	sFormValue:{
		flexDirection:'row',
		display:'flex',
		alignItems:'center',
	},
	sInput:{
		flex:1,
		width:'100%',
		padding:[5,10],
		fontSize:'12pt'
	}
	//\strike commission
});

class CustomInput extends Component{
	state={
		editMode:false,
		value:this.props.value||this.props.def||'',
		valid:true,
		after:this.props.after||'',
		err:this.props.err||false,
	}
	componentWillMount(){
		this.id=this.props.id||(this.props.label+(new Date().getTime()));
		this._change(this.state.value);
	}
	componentWillUnmount(){
		//this.id=this.props.id||(this.props.label+(new Date().getTime()));
		appEvent.fire('addMemberErrPop',this.id);
	}
	_change=value=>{
		this.setState({value},()=>{
			if(this.props.onChange) this.props.onChange(value);
			let val=this.props.validation;
			if(val){
				let valid=val(value);
				this.setState({valid},()=>{
					if(valid) appEvent.fire('addMemberErrPop',this.id);
					else appEvent.fire('addMemberErrPush',this.id);
				})
			}
		});
	}
	change=e=>{
		this._change(e.target.value);
	}
	componentWillReceiveProps(np){
		if(np.err !== this.state.err)
			this.setState({err:np.err});
		if(np.value !== this.state.value)
			this.setState({value:np.value})
		if(np.after !== this.state.after)
			this.setState({after:np.after})
	}
	focus=()=>{
		this.refs.input.focus();
	}
	render(){
		let {value,valid,err,after}=this.state;
		let p=this.props;
		return <ListItem className={p.cn||p.c.item}>
			<Grid item xs={4} className={p.c.label}>{p.label}</Grid>
			<Grid item xs={1} className={p.c.label}>{":"}</Grid>
			<Grid item xs={7} className={p.c.value2}>
				<input ref={"input"}
					disabled={!!p.disabled}
					className={p.c.input+(!valid||err?' '+p.c.err:'')}
					type={p.type||"text"}
					value={value}
					onChange={this.change} />
				<div className={p.c.after}>{after}</div>
			</Grid>
		</ListItem>
	}
}

//let alias={
//	rb:'rebate',
//	it:'intake',
//	sc:'strike_comm'
//};
			
class AddSU extends Component{
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}
	//memberType=this.props.match.params.action==='addAgent'?'agent':'player'
	//member_of=this.props.match.params.id
	state={
		id:'',
		name:'',
		password:'',
		rePassword:'',
		fake_password:'',
		me:{},
		errOpen:false,
		errMessage:'',
		errField:'',
		errSubs:[],
		pageType:'addAdmin'
	}
	valueChange=(k)=>{
		return v=>{
			this.setState({[k]:v});
		}
	}
	cancel=()=>{
		let {pageType}=this.state;
		if(pageType==='addSuperAdmin'){
			this.props.history.replace('/sadmin');
		} else {
			this.props.history.replace('/admins');
		}
	}
	submit=()=>{
		this.setState({
				loading: true,
			},
			()=>{
				this.processSubmit(this.state);
			});
	}
	getId=()=>{
		let {pageType} = this.state;
		Get('id',{type:pageType==='addAdmin'?'admin':'sadmin'},(e,r)=>{
			if(e) return;
			if(r && r.d) this.setState({id:r.d})
		});
	}
	processSubmit=(v)=>{
		let {pageType}=this.state;
		v=JSON.parse(JSON.stringify(v));
		//if(this.member_of) v.member_of=this.member_of;
		//if(this.memberType==='player') delete v.agent_config;
		delete v.loading;
		delete v.me;
		delete v.pageType;
		delete v.errOpen;
		delete v.errMessage;
		delete v.errField;
		delete v.errSubs;
		delete v.sConfig;
		if(!v.password)
			return this.setState({errOpen:true,errMessage:'invalid password',errField:'password',loading:false});
		if(!(v.password === v.rePassword))
			return this.setState({errOpen:true,errMessage:'Password does not match the confirm password',errField:'rePassword',loading:false});
		if(!v.fake_password)
			return this.setState({errOpen:true,errMessage:'invalid emergency password',errField:'fake_password',loading:false});
		
		let vv=flatten(v);
		//console.log('vv==>',vv);
		Post(pageType==='addSuperAdmin'?'sadmin':'admin',vv,(e,r)=>{
			if(e) {
				this.setState({
					loading:false
				},()=>{
					//window.alert(((e||{}).e||{}).toString()); //console.log('e==>',e);
					this.setState({errOpen:true,errMessage:((e||{}).e||{}).toString()||'unknown error',loading:false});
				});
				return;
			}
			if(pageType==='addSuperAdmin'){
				this.props.history.replace('/sadmin');
			} else {
				this.props.history.replace('/admins');
			}
		})
	}
	configValidation=(game,k)=>{
		return v=>{
			v=(v||'0').toString();
			if(!(v.match(/^[\d,.]+$/))) return false;
			let v2=parseFloat(v);
			if(isNaN(v2)) return false;
			let min=parseFloat(((this.state.game[game]||{})[Alias[k]+'_min'])|| 0),
				max=parseFloat(((this.state.game[game]||{})[Alias[k]+'_max'])|| 20);
			return !((v2 < min) || (v2 > max));
		}
	}
	closeErr=()=>{
		this.setState({errOpen:false,errField:''});
	}
	pushErrSubs=f=>{
		let errSubs=this.state.errSubs||[];
		errSubs.push(f);
		this.setState({errSubs});
	}
	popErrSubs=f=>{
		let errSubs=this.state.errSubs||[];
		do {
			let i=errSubs.indexOf(f);
			if(i > -1) errSubs.splice(i,1);
		} while (errSubs.indexOf(f) > -1);
		this.setState({errSubs});
	}
	componentWillMount(){
		//console.log('this.props==>',this.props);
		if(this.props.match && this.props.match.params && this.props.match.params.action){
			this.setState({pageType:this.props.match.params.action},()=>{
				let {pageType}=this.state;
				appEvent.fire('titleChange',`Create New ${pageType==='addAdmin'?UserType['admin']:UserType['sadmin']}`);
			});
		}
		appEvent.on('addMemberErrPush',this.pushErrSubs);
		appEvent.on('addMemberErrPop',this.popErrSubs);
		//Me((e,me)=>{
		//	this.meChange(me);
		//});
		//Game((e,game)=>{
		//	this.setState({game});
		//});
		
		//Config((e,sConfig)=>{
		//	this.setState({
		//		sConfig,
		//		credit:((sConfig||{}).default_credit||{})[this.memberType]||'0'
		//	});
		//})
	}
	componentDidMount(){
		this.getId();
		this.refs.name.focus();
	}
	componentWillUnmount(){
		appEvent.off('addMemberErrPush',this.pushErrSubs);
		appEvent.off('addMemberErrPop',this.popErrSubs);
	}
	render(){
		const { classes} = this.props,
			{memberType} = this,
			{id,name,credit,config,agent_config,loading,game,errOpen,errMessage,errField,errSubs} = this.state;
		return <div className={classes.root}>
			<div className={classes.content}>
				<List className={classes.sub}>
					<Typography type="subheading" gutterBottom>Profile</Typography>
					<Divider />
					<CustomInput ref={"name"} label={'Name'} value={name} c={classes} onChange={this.valueChange('name')}/>
					<ListItem className={classes.item}>
						<Grid item xs={4} className={classes.label}>{'Id'}</Grid>
						<Grid item xs={1} className={classes.label}>{":"}</Grid>
						<Grid item xs={7} className={classes.value2}>
							<input disabled={true} className={classes.input} value={id} />
							<IconButton className={classes.button} aria-label="reload" onClick={this.getId}>
								<RefreshIcon/>
							</IconButton>
						</Grid>
					</ListItem>
					<CustomInput label={'Password'} value={this.state.password} c={classes} err={errField==='password'} type={"password"} onChange={this.valueChange('password')}/>
					<CustomInput label={'Confirrm Password'} value={this.state.rePassword} err={errField==='rePassword'} c={classes} type={"password"} onChange={this.valueChange('rePassword')}/>
					<CustomInput label={'Emergency Password'} value={this.state.fake_password} c={classes} err={errField==='fake_password'} type={"password"} onChange={this.valueChange('fake_password')}/>					
					<Divider />
				</List>
			</div>
			<div className={classes.footer}>
				<Divider />
				<div className={classes.wrapper}>
					<Button
						raised
						disabled={loading || !!errSubs.length}
						color={"primary"}
						onClick={this.submit}>Submit</Button>
					{loading && <CircularProgress size={24} className={classes.buttonProgress} />}
				</div>
				<Button raised className={classes.button2} color={"secondary"} onClick={this.cancel}>Cancel</Button>
			</div>
			<SnackBar open={errOpen} onClose={this.closeErr} message={errMessage}/>
		</div>
	}
}

export default withRoot(withStyles(styles, { withTheme: true })(AddSU));