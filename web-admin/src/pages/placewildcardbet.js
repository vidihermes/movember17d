import React,{Component} from 'react';
import PlaceBet,{createNs} from './placebet';
import {WildcardBetRow} from '../components/BetRow';

export const Table=p=><table style={{border:'1px solid #000'}} cellSpacing={"0"} cellPadding={"1"}>
	<thead><tr style={{border:'1px solid #000', textAlign:'center', fontWeight:'bold'}}>
		<th className={p.classes.th}>No</th>
		<th className={p.classes.th}>D</th>
		<th className={p.classes.th}>Bet</th>
		<th className={p.classes.th}>Num</th>
		<th className={p.classes.th}>Big</th>
		<th className={p.classes.th}>Small</th>
	</tr></thead><tbody>
		{p.children}
	</tbody></table>;

let reG={
	'4D':/^[\d\*]{4}$/,
	'3D':/^[\d\*]{3}$/,
	'2D':/^[\d\*]{2}$/,
}

function validation(g,num){
	return !!((num||'').match(reG[g]) && (num||'').match(/^\d*\*\d*$/));
}

const notes=(p)=><div style={{padding:'10px',
    borderLeft:'10px #eee solid',
    color:'#34a',
    fontFamily:'monospace'}}>
	<div>Example: 7*77 x $1 bet = (7077,7177,7277....7877,7977) x $1</div>
	<div>Total cost for 7*77= $10</div>
</div>

export default class PlaceMassBet extends Component{
	render(){
		return <PlaceBet
			pageName={'wildcard'}
			ns={createNs(1,6)}
			tn={['g','n','b','s']}
			gameValidation={validation}
			notes={notes}
			renderContent={({classes,ready,loading,day,currentPrize,comp})=>
						<Table classes={classes}>
							{createNs(1,6).map(k=><WildcardBetRow disable={!ready || loading}
								ref={e=>{comp.items[k]=e}}
								key={k}
								idx={k}
								day={day}
								classes={classes}
								onNext={comp.onNext}
								onChange={comp.itemChange}
								gameValidation={comp.gameValidation}
								currentPrize={currentPrize}/>
							)}
						</Table>}
		/>
	}
}
