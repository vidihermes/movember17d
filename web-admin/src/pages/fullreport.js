import React,{Component} from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import Divider from 'material-ui/Divider';
import List,{ ListItem} from 'material-ui/List';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import IconButton from 'material-ui/IconButton';
//import Button from 'material-ui/Button';
import Collapse from 'material-ui/transitions/Collapse';
//import PersonIcon from 'material-ui-icons/Person';
//import CloseIcon from 'material-ui-icons/Close';
//import ArrowForwardIcon from 'material-ui-icons/ArrowForward';

//import OpenInNewIcon from 'material-ui-icons/OpenInNew';
import ExpandLess from 'material-ui-icons/ExpandLess';
import ExpandMore from 'material-ui-icons/ExpandMore';

import withRoot from '../components/withRoot';
import {appEvent} from '../components/events';
import {/*saveTmpBet,*/Dow,Me,toDbDate} from '../components/game';
import {Get/*,Delete*/} from '../components/ajax';
import {SearchBar,TableClasses,Classes} from '../components/dialog';
import {bList,mList} from './placebet1d';

const styles = theme => ({
	...TableClasses(theme),
	...Classes(theme),
	root: {
		width: '100%',
		display:'flex',
		flexDirection:'column',
		flex:1,
		height:'100%',
		//padding:[20],
	},
	sub:{
		width: '100%',
		padding:[5,20],
		boxSizing:'border-box',
		flex:1,
		overflow:'auto',
	},
	sub2:{
		width: '100%',
		paddingLeft:24,
		boxSizing:'border-box',
	},
	item:{
		display:'flex',
	},
	item2:{
		display:'flex',
		padding:[6,0,6,20]
	},
	button: {
		//margin:[-10,0],//margin: theme.spacing.unit,
		//margin:0,
		width:24,
		height:24,
	},
	button2: {
		margin: theme.spacing.unit,
	},
	colA:{
		padding:[0,5]
	},
	colT:{
		flexGrow:9,
		borderLeft:[0,'solid','#eee'],
	},
	collapsible:{
		margin:[0,0,0,0],
		padding:[0,0,0,0],
	},
	sItem2:{
		textAlign:'center'
	},
	sFirstItem:{
		textAlign:'left',
		padding:[5,0,20,0],
		//borderBottom:[1,'solid','#eee']
		fontSize:'14pt'
	},
	r:{
		textAlign:'right',
	}
});

let Head=(p)=><thead><tr>
	{p.t.map((v,k)=><th key={k}>{v}</th>)}
</tr></thead>

let Head2=(p)=><thead><tr>
	{p.t.map((v,k)=>k===0?<th key={k}>{v}</th>:<th key={k} className={p.c.r}>{p.formatNumb(v)}</th>)}
</tr></thead>

let Row=(p)=><tr>
	{p.r.map((v,k)=>k===0?<td key={k}>{v}</td>:<td key={k} className={p.c.r}>{p.formatNumb(v)}</td>)}
</tr>

export const aMothAgo=function(){
	let d=new Date();
	d.setMonth(d.getMonth()-1);
	return d;
}

class FullReport extends Component{
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}
	static contextTypes = {
		intl: PropTypes.object
	}
	state={
		members:{},
		ready:false,
		me:{},
		id:'',
		fromDate:aMothAgo(),
		toDate:new Date(),
		data:{},
	}
	ready=()=>{
		let {id,fromDate,toDate}=this.state;
		if(!id) return;
		let prm={user_id:id};
		if(fromDate) prm.fromdate=toDbDate(fromDate);
		if(toDate) prm.todate=toDbDate(toDate);
		Get('fullreport',prm,(e,r)=>{
			console.log('r==>',r)
			if(e) return console.error(e);
			if(r && r.d) this.setState({data:r.d,ready:true});
		});
		//let {me}=this.state;
		
		//Get('summarybet/'+me.id,{},(e,r)=>{
		//	if(e) return console.error(e);
		//	if(r && r.d) this.setState({members:r.d,ready:true});
		//});
	}
	componentWillMount(){
		appEvent.fire('titleChange','Full Report');
		Me((e,r)=>{
			this.meChange(r);
		})
	}
	meChange=me=>{
		this.setState({
			me,
			id:me.id
		},()=>{
			this.ready();
		});
	}
	componentWillUnmount(){
		
	}
	route=uri=>{
		return (e)=>{
			if(e && e.stopPropagation) e.stopPropagation();
			console.log(uri)
			this.props.history.push(uri);
		}
	}
	formatNumb=n=>{
		return this.context.intl.formatNumber(n,{style: 'decimal',minimumFractionDigits:2,maximumFractionDigits:2});
	}
	dateChange=(k)=>{
		return (value)=>{
			let st={};
			st[k]=value;
			this.setState(st,this.ready);
		}
	}
	setUser=(d)=>{
		this.setState({id:d.id},this.ready)
	}
	render(){
		const { classes } = this.props,
			{members, ready,me,id,fromDate,toDate,data} = this.state;
		let bet={big:0,small:0,amt:0,rb:0,lrb:0,strike:0,sc:0,net:0},
			pbc=0;
			
		if(data && data.bet && data.bet.length){
			//let big=0,small=0,amt=0,rb=0,lrb=0,strike=0,sc=0,net=0;
			data.bet.forEach(b=>{
				bet.big+=b.big;
				bet.small+=b.big;
				let a=b.big+b.small,
					r=b.big_rebate+b.small_rebate,
					_lrb=a-r,
					s=b.big_strike+b.small_strike,
					_sc=b.big_strike_comm+b.small_strike_comm,
					ssc=s-_sc,
					n=_lrb-ssc;
				bet.amt+=a;
				bet.rb+=r;
				bet.lrb+=_lrb;
				bet.strike+=s;
				bet.sc+=_sc;
				bet.net+=n;
			});
		}
		
		if(data && data.pbc && data.pbc.length){
			//let big=0,small=0,amt=0,rb=0,lrb=0,strike=0,sc=0,net=0;
			data.pbc.forEach(v=>{
				pbc+=(v.big+v.small-v.big_rebate-v.small_rebate)-(v.big_strike+v.small_strike-v.big_strike_comm-v.small_strike_comm);
			});
		}
		
		return <div className={classes.root}>
			<div className={classes.tableFooter}>
				<div className={classes.footerSub2}>
					<SearchBar label={'User Id'}
						id={id}
						me={me}
						userDetailValidate={d=>(d.active)}
						onFound={this.setUser}/>
					<div className={classes.footerSub2}>
						&nbsp; &nbsp;
						From &nbsp;
						<DatePicker className={classes.date}
							selected={moment(fromDate)}
							onChange={this.dateChange('fromDate')}
							{...(toDate?{maxDate:toDate}:{maxDate:moment()})}
						/>
						&nbsp; &nbsp;
						To &nbsp;
						<DatePicker className={classes.date}
							selected={moment(toDate)}
							onChange={this.dateChange('toDate')}
							maxDate={moment()}
							{...(fromDate?{minDate:moment(fromDate)}:{})}
						/>
					</div>
				</div>
			</div>
			<div className={classes.sub+' markdown-body'}>
				<h3>Full Report {id} from {(new Date(fromDate)).toDateString()} to {(new Date(toDate)).toDateString()}</h3>
				<table>
					<Head t={['Account','Big','Small','Amount','Rebate','LessRebate','Strike','Strike Comm','Net']}/>
					<tbody>
						{(data.bet||[]).map((v,k)=><Row key={k} formatNumb={this.formatNumb} c={classes} r={[
							v.user_id,
							v.big,
							v.small,
							-(parseFloat(v.big)+parseFloat(v.small)),
							v.big_rebate+v.small_rebate,
							-(v.big+v.small-v.big_rebate-v.small_rebate),
							v.big_strike+v.small_strike,
							v.big_strike_comm+v.small_strike_comm,
							-((v.big+v.small-v.big_rebate-v.small_rebate)-(v.big_strike+v.small_strike-v.big_strike_comm-v.small_strike_comm))
						]}/>)}
					</tbody>
					<Head2 t={['SubTotal',
							  bet.big,
							  bet.small,
							  bet.amt,
							  bet.rb,
							  bet.lrb,
							  bet.strike,
							  bet.sc,
							  bet.net]}
						c={classes}
						formatNumb={this.formatNumb} />
				</table>
				{!!data.personalTicket && !!data.personalTicket.length && <h4>Personal Ticket</h4>}
				{!!data.personalTicket && !!data.personalTicket.length && <table>
					<Head t={['Account','Big','Small','Amount']}/>
					<tbody>
						{(data.personalTicket||[]).map((v,k)=><Row key={k} formatNumb={this.formatNumb} c={classes} r={[
							v.user_id,
							v.big,
							v.small,
							-(parseFloat(v.big)+parseFloat(v.small))
						]}/>)}
					</tbody>
				</table>}
				
				<h4>Ticket Intake</h4>
				<table>
					<Head t={['Account','Big','Small','Amount','Rebate','LessRebate','Strike','StrikeComm','Net']}/>
					<tbody>
						{(data.intake||[]).map((v,k)=><Row key={k} formatNumb={this.formatNumb} c={classes} r={[
							v.user_id,
							v.big,
							v.small,
							-(parseFloat(v.big)+parseFloat(v.small)),
							v.big_rebate+v.small_rebate,
							(v.big+v.small-v.big_rebate-v.small_rebate),
							-(v.big_strike+v.small_strike),
							v.big_strike_comm+v.small_strike_comm,
							(v.big+v.small-v.big_rebate-v.small_rebate)-(v.big_strike+v.small_strike-v.big_strike_comm-v.small_strike_comm)
						]}/>)}
					</tbody>
				</table>
				
				<h4>Payment Balance and Consolidation</h4>
				<table>
					<Head t={['Account','Big','Small','Amount','Rebate','LessRebate','Strike','StrikeComm','Net']}/>
					<tbody>
						{(data.pbc||[]).map((v,k)=><Row key={k} formatNumb={this.formatNumb} c={classes} r={[
							v.user_id,
							v.big,
							v.small,
							-(parseFloat(v.big)+parseFloat(v.small)),
							v.big_rebate+v.small_rebate,
							-(v.big+v.small-v.big_rebate-v.small_rebate),
							v.big_strike+v.small_strike,
							v.big_strike_comm+v.small_strike_comm,
							-((v.big+v.small-v.big_rebate-v.small_rebate)-(v.big_strike+v.small_strike-v.big_strike_comm-v.small_strike_comm))
						]}/>)}
					</tbody>
				</table>
				<table>
					<Head2 t={['Net Profit/Loss',bet.net-pbc]}
							c={classes}
							formatNumb={this.formatNumb} />
				</table>
			</div>
		</div>
	}
}

export default withRoot(withStyles(styles, { withTheme: true })(FullReport));