import React,{Component} from 'react';
import Grid from 'material-ui/Grid';
import Divider from 'material-ui/Divider';
import List,{ ListItem, ListItemText} from 'material-ui/List';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Typography from 'material-ui/Typography';
import IconButton from 'material-ui/IconButton';
import Button from 'material-ui/Button';
//import Switch from 'material-ui/Switch';
//import SaveIcon from 'material-ui-icons/Save';
//import ModeEditIcon from 'material-ui-icons/ModeEdit';
import CloseIcon from 'material-ui-icons/Close';
//import RefreshIcon from 'material-ui-icons/Refresh';
//import { CircularProgress } from 'material-ui/Progress';
//import { MenuItem } from 'material-ui/Menu';
import Collapse from 'material-ui/transitions/Collapse';
//import Snackbar from 'material-ui/Snackbar';

import ExpandLess from 'material-ui-icons/ExpandLess';
import ExpandMore from 'material-ui-icons/ExpandMore';

import withRoot from '../components/withRoot';
import {appEvent} from '../components/events';
import {Get,Put} from '../components/ajax';
import {Game,Config,Alias,UserType} from '../components/game';
import {SnackBar,ChangePassword,ChangeStatus,BreadCrumbs,EditableItem,CreditEditable,ChangeCredit} from '../components/dialog';

const styles = theme => ({
	root: {
		display:'flex',
		flexDirection:'column',
		width: '100%',
		height: '100%',
		fontFamily: `"Roboto", "Helvetica", "Arial", sans-serif`,
	},
	content:{
		flex:1,
		overflowY:'auto',
		[theme.breakpoints.up('sm')]: {
			display:'flex',
			flexDirection:'row',
			overflowY:'hidden',
		}
		
	},
	headerContent:{
		padding:[10,5,0,5]
	},
	sub:{
		width: '100%',
		//float:'left',
		padding:5,
		boxSizing:'border-box',
		[theme.breakpoints.up('sm')]: {
			//width: '50%',
			flex:1,
			overflowY:'auto'
		},
	},
	sub2:{
		width: '100%',
		paddingLeft:24,
		boxSizing:'border-box',
	},
	item:{
		display:'flex',
		padding:[8,16]
	},
	item2:{
		display:'flex',
		padding:[0,0,6,20]
	},
	itemSwitch:{
		display:'flex',
		padding:[0,16]
	},
	label:{
		whiteSpace:'nowrap',
		fontSize:'12pt',
		padding:'0px 10px',
	},
	label2:{
		whiteSpace:'nowrap',
		fontSize:'12pt',
		padding:'0px 10px',
	},
	value:{
		fontSize:'12pt',
		paddingLeft:5,
	},
	valueContainer:{
		display:'flex',
		alignItems:'center'
	},
	value2:{
		fontSize:'12pt',
		paddingLeft:0,
		display:'flex',
		alignItems:'center'
	},
	button: {
		//margin:[-10,0],//margin: theme.spacing.unit,
		width:30,
		height:30,
	},
	wrapper: {
		//margin: theme.spacing.unit,
		position: 'relative',
		display:'inline-block'
	},
	buttonProgress: {
		color: 'teal',//green[500],
		position: 'absolute',
		top: '50%',
		left: '50%',
		marginTop: -12,
		marginLeft: -12,
	},
	button2: {
		margin: 4//theme.spacing.unit,
	},
	input:{
		padding:[4,10],
		fontSize:'12pt',
		flex:1,
		width:'100%',
		marginLeft:-6
	},
	after:{
		fontSize:'10pt',
		padding:[0,5],
		color:'rgba(0,0,0,0.5)'
	},
	err:{
		borderColor:'#f00',
		outlineColor:'rgba(255,0,0,0.1)'
	},
	
	footer:{
		padding:[0,10],
		borderTop:[1,'solid','#eee'],
		justifyContent:'space-between',
		display:'flex',
		alignItems:'center',
		[theme.breakpoints.down('sm')]: {
			display:'block'
		},
	},
	footerLeft:{
		display:'flex',
		alignItems:'center',
		flex:1,
		flexWrap:'wrap'
	},
	bc:{
		flex:1
	},
	bcContent:{
		whiteSpace:'nowrap'
	},
	footerRight:{
		[theme.breakpoints.down('md')]: {
			display:'flex',
			justifyContent:'space-around',
		},
	},
	bold:{
		fontWeight:'bold'
	}
});


const Item=p=><ListItem className={p.c.item}>
	<Grid item xs={4} className={p.c.label}>{p.label}</Grid>
	<Grid item xs={1} className={p.c.label}>{":"}</Grid>
	<Grid item xs={7} className={p.c.value+(p.valueCn?' '+p.valueCn:'')}>{p.value}</Grid>
</ListItem>

class SuperAgentDetail extends Component{
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}
	static contextTypes = {
		intl: PropTypes.object
	}
	state={
		id:'',
		name:'',
		config:{},
		game:{},
		errOpen:false,
		errMessage:'',
		csOpen:false,
		cpOpen:false,
		cepOpen:false,
		csOK:null
	}
	csClose=()=>{
		this.setState({csOpen:false});
	}
	ccClose=()=>{
		this.setState({ccOpen:false});
	}
	cpClose=()=>{
		this.setState({cpOpen:false});
	}
	cpOK=({password,confirm})=>{
		if(!(!!password && password === confirm))
			return this.setState({errOpen:true,errMessage:'invalid new password or confirm new password'});
		//this.setState({cpOpen:false});
		Put('user/'+this.state.id,{password},(e,r)=>{
			if(e) {
				console.log('e==>',e);
				return this.setState({errOpen:true,errMessage:e.toString()});
			}
			console.log('r==>',r);
			this.setState({cpOpen:false});
		});
	}
	cepClose=()=>{
		this.setState({cepOpen:false});
	}
	cepOK=({password,confirm})=>{
		if(!(!!password && password === confirm))
			return this.setState({errOpen:true,errMessage:'invalid new password or confirm emergency password'});
		Put('user/'+this.state.id,{fake_password:password},(e,r)=>{
			if(e) {
				console.log('e==>',e);
				return this.setState({errOpen:true,errMessage:e.toString()});
			}
			this.setState({cepOpen:false});
		});
	}
	valueChange=(k)=>{
		return v=>{
			this.setState({[k]:v});
		}
	}

	configValidation=(game,k)=>{
		return v=>{
			v=(v||'0').toString();
			if(!(v.match(/^[\d,.]+$/))) return false;
			let v2=parseFloat(v);
			if(isNaN(v2)) return false;
			return true;
		}
	}
	closeErr=()=>{
		this.setState({errOpen:false,errField:''});
	}
	configSave=(g,k)=>{
		return (value,cb)=>{
			let fld={};
			fld['config['+g+']['+k+']']=value;
			Put('user/'+this.state.id,fld,(e,r)=>{
				if(e) {
					return this.setState({errOpen:true,errMessage:e.toString()},()=>{
						cb(e);
					});
				}
				cb(null,r);
				//if(r && r.d) UpdateMe(r.d);
			})
		}
	}

	nameSave=(name,cb)=>{
		Put('user/'+this.state.id,{name},(e,r)=>{
			console.log('e==>',e);
			console.log('r==>',r);
			if(e){
				return this.setState({errOpen:true,errMessage:e.toString()},()=>{
					cb(e);
				});
			}
			cb(null,r);
		});
	}
	route=uri=>{
		return (e)=>{
			if(e && e.stopPropagation) e.stopPropagation();
			this.props.history.push(uri);
		}
	}
	ready=(id)=>{
		Get('user/'+id,{withbalance:1,withcreditallocated:1},(e,r)=>{
			if(e) return console.log('e==>',e);
			if(r && r.d ){
				console.log('r==>',r);
				let st=r.d;
				this.setState(st,()=>{
					appEvent.fire('titleChange','Super Agent '+this.state.id+' Detail');
				});
			}
		});
	}
	componentWillMount(){
		//console.log('this.props==>',this.props);
		this.ready(this.props.match.params.id);

		//Game((e,game)=>{
		//	this.setState({game});
		//});
		//
		//Config((e,sConfig)=>{
		//	this.setState({
		//		sConfig,
		//		//credit:((sConfig||{}).default_credit||{})[this.memberType]||'0'
		//	});
		//})
	}
	csClick=(status)=>{
		return ()=>{
			let {id}=this.state;
			let title={
				'terminate':`Terminate User ${id} ?`,
				'lock':`Lock User ${id} ?`,
				'suspend':`Suspend User ${id} ?`,
				'unsuspend':`Unsuspend User ${id} ?`,
				'unlock':`Unlock User ${id} ?`
			};
			this.setState({
				csOpen:true,
				csTitle:title[status],
				csOK:({reason})=>{
					Put('status/'+id,{status,why:reason},(e,r)=>{
						//this.setState({csOpen:false})
						if(e) {
							console.log('e==>',e);
							return this.setState({csOpen:false,errOpen:true,errMessage:(e||{}).e.toString()});
						}
						return this.setState({csOpen:false},()=>{
							this.ready(id);
						});
					})
					//console.log(reason)
					//this.setState({csOpen:false});
				}
			})
		}
	}
	creditClick=(type)=>{
		return ()=>{
			let {id}=this.state;
			let title={
				'increase':`Increase Credit Limit of ${id}`,
				'decrease':`Decrease Credit Limit of ${id} ?`,
			};
			this.setState({
				ccOpen:true,
				ccTitle:title[type],
				ccOK:({reason,amount})=>{
					//return console.log({type,reason,amount});
					Put('credit/'+id,{type,amount,why:reason},(e,r)=>{
						console.log('r==>',r);
						if(e) {
							console.log('e==>',e);
							return this.setState({ccOpen:false,errOpen:true,errMessage:(e||{}).e.toString()});
						}
						return this.setState({ccOpen:false},()=>{
							this.ready(id);
						});
					})
				}
			});
		}
	}
	formatNumb=n=>{
		return this.context.intl.formatNumber(n,{style: 'decimal',minimumFractionDigits:2,maximumFractionDigits:2});
	}
	render(){
		let { classes} = this.props,
			{id,type,name,active,status,credit,balance,credit_allocated,config,errOpen,errMessage,cpOpen,cepOpen,csTitle,csOpen,csOK,ccOpen,ccOK,ccTitle} = this.state;
		
		return <div className={classes.root}>
			<div className={classes.content}>
				<List className={classes.sub}>
					<Typography type="subheading" gutterBottom>Profile</Typography>
					<Divider />
					<Item c={classes} label={"Id"} value={id} valueCn={classes.bold} />
					<EditableItem c={classes}
						value={name}
						//onChange={this.nameChange}
						label={"Name"}
						rootClass={"item"}
						type={"text"}
						onSave={this.nameSave}/>
					<Item c={classes} label={"Type"} value={UserType[type]} />
					<Divider />
					<CreditEditable c={classes} label={'Credit Limit'} valueCn={classes.valueContainer} value={this.formatNumb(credit||0)} onPlus={this.creditClick('increase')} onMin={this.creditClick('decrease')}/>
					{!!balance && <Item c={classes} label={"Balance"} value={this.formatNumb(balance||0)} />}
					{!!credit_allocated && <Item c={classes} label={"Credit Allocated"} value={this.formatNumb(credit_allocated||0)} />}
					<Item c={classes} label={"Credit Left"} value={this.formatNumb(parseFloat(credit||0)+parseFloat(balance||0)-parseFloat(credit_allocated||0))} />
					<Divider />
					<Item c={classes} label={"Status"} value={!active?'Terminated':status||'Active'} />
					<Divider />
					<div style={{display:'flex',flexDirection:'row'}}>
						<div style={{flex:1}}>
							{active && <Button raised color={'secondary'} className={classes.button2} onClick={this.csClick('terminate')}>Terminate</Button>}
							{active && status!=='locked' && <Button raised color={'secondary'} className={classes.button2} onClick={this.csClick('lock')}>Lock</Button>}
							{active && status!=='suspended' && <Button raised color={'secondary'} className={classes.button2} onClick={this.csClick('suspend')}>Suspend</Button>}
						</div>
						<div>
							{active && status==='locked' && <Button raised color={'secondary'} className={classes.button2} onClick={this.csClick('unlock')}>Unlock</Button>}
							{active && status==='suspended' && <Button raised color={'secondary'} className={classes.button2} onClick={this.csClick('unsuspend')}>Unsuspend</Button>}
						</div>
					</div>
				</List>
				<List className={classes.sub}>
					<Typography type="body2" gutterBottom>Configuration</Typography>
					<Divider />
					{['4D','3D','2D','1D'].map(g=><div className={classes.sub2} key={g}>
						<Typography type="subheading" gutterBottom>{g}</Typography>
						<EditableItem
							c={classes}
							value={(config[g]||{}).it}
							def={'0'}
							after={`$`}
							//onChange={this.intakeChange(g)}
							label={"Maximum Intake"}
							onSave={this.configSave(g,'it')}
							validation={this.configValidation(g,'it')}/>
					</div>)}
				</List>
			</div>
			<div className={classes.footer}>
				<div className={classes.footerLeft}>
					<Button raised color={"secondary"} className={classes.button2} onClick={this.route('/downline/'+id)}>Manage Downline</Button>
				</div>
				<div className={classes.footerRight}>
					<Button raised color={"secondary"} className={classes.button2} onClick={()=>{this.setState({cpOpen:true})}}>Change Password</Button>
					<Button raised color={"secondary"} className={classes.button2} onClick={()=>{this.setState({cepOpen:true})}}>Change Emergency Password</Button>
				</div>
			</div>
			<SnackBar open={errOpen} onClose={this.closeErr} message={errMessage}/>
			{ccOpen && <ChangeCredit open={ccOpen} title={ccTitle} onClose={this.ccClose} onOK={ccOK}/>}
			{csOpen && <ChangeStatus open={csOpen} title={csTitle} onClose={this.csClose} onOK={csOK}/>}
			{cpOpen && <ChangePassword open={cpOpen} onClose={this.cpClose} onOK={this.cpOK}/>}
			{cepOpen && <ChangePassword open={cepOpen} onClose={this.cepClose} onOK={this.cepOK}
					title={'Change Emergency Password'}
					label={'New Emergency Password'}
					confirmLabel={'Confirm Emergency Password'}
				/>}
		</div>
	}
}

export default withRoot(withStyles(styles, { withTheme: true })(SuperAgentDetail));