import React,{Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';

import {appEvent} from '../components/events';
import {Get} from '../components/ajax';
import {PrizeRank} from '../components/game';

const styles = theme => ({
	root: {
		width: '100%',
		display:'flex',
		flexDirection:'column',
		flex:1,
		height:'100%',
	},
	place:{
		flex:1,
		padding:20,
		overflow:'auto',
	},
	table:{
		textAlign:'center'
	},
	thead:{
		backgroundColor:theme.palette.primary.main,
	}
});

class Prize extends Component{
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}
	state={
		prize:{},
	}
	componentWillMount(){
		appEvent.fire('titleChange','Current Prize Regulation');
		Get('currentprize',{},(e,r)=>{
			if(e) return console.error('e==>',e);
			if(r && r.d && r.id){
				//console.log('r==>',r);
				this.setState({prize:r.d});
			}
		})
	}

	render(){
		const {classes}=this.props,
			{prize}=this.state;
		return <div className={classes.root}>
			<div className={classes.place+' markdown-body'}>
				{['4D','3D','2D'].map(g=><div key={g}>
					<h3>{g} Prize Structure</h3>

					{(prize[g]||{}).normal && <h4>Prize Amounts for Ordinary Entry, System Entry or {g} Roll for every $1 Bet</h4>}
					{(prize[g]||{}).normal && <table className={classes.table}>
						<thead><tr>
							<th className={classes.thead}>Price Category</th>
							<th className={classes.thead}>Big Bet</th>
							<th className={classes.thead}>Small Bet</th>
						</tr></thead>
						<tbody>
							{['1','2','3','s','c'].map(pc=><tr key={pc}>
								<td>{PrizeRank[pc]}</td>
								<td>{(((prize[g]||{}).normal||{}).big||{})[pc]||'-'}</td>
								<td>{(((prize[g]||{}).normal||{}).small||{})[pc]||'-'}</td>
							</tr>)}
						</tbody>
					</table>}
					{!!(prize[g]||{}).i && ['big','small'].map(bs=><div key={bs}>
						{(prize[g]||{}).i[bs] && <h4>Prize Amounts for iBet for every $1 Bet on {bs==='big'?'Big':'Small'} Bet</h4>}
						{(prize[g]||{}).i[bs] && <table className={classes.table}>
							<thead><tr>
								<th className={classes.thead}>Price Category</th>
								<th className={classes.thead}>4 different digits</th>
								<th className={classes.thead}>2 different and 1 pair digits</th>
								<th className={classes.thead}>2 pairs of digits</th>
								<th className={classes.thead}>3 same of digits</th>
							</tr></thead>
							<tbody>
								{['1','2','3','s','c'].map(pc=><tr key={pc}>
									<td>{PrizeRank[pc]}</td>
									<td>{(prize[g]||{}).i[bs]['24'][pc]||'-'}</td>
									<td>{(prize[g]||{}).i[bs]['12'][pc]||'-'}</td>
									<td>{(prize[g]||{}).i[bs]['6'][pc]||'-'}</td>
									<td>{(prize[g]||{}).i[bs]['4'][pc]||'-'}</td>
								</tr>)}
							</tbody>
						</table>}
					</div>)}
				</div>)}
				<h3>1D Prize</h3>
				<table className={classes.table}>
					<thead><tr>
						<th className={classes.thead}>Bet</th>
						<th className={classes.thead}>Prize Amout</th>
					</tr></thead>
					<tbody>
						<tr>
							<td>Big/Small/Odd/Even</td>
							<td>{(prize['1D']||{})['2']?(((prize['1D']||{})['2']-1)*100)+' %':'-'}</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div className={classes.footer}>
				
			</div>
		</div>
	}
}

export default withStyles(styles, { withTheme: true })(Prize);