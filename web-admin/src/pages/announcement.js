import React,{Component} from 'react';
import PropTypes from 'prop-types';
import Button from 'material-ui/Button';
import { withStyles } from 'material-ui/styles';
import List, { ListItem, ListItemText } from 'material-ui/List';

import {Get,Put} from '../components/ajax';
import {Config,UpdateConfig} from '../components/game';
import {appEvent} from '../components/events';
import {SnackBar,EditableItem2} from '../components/dialog';
import RichEditor from '../components/richeditor';
import {humanize} from '../components/NotifMenu';
const $=window.$;

const styles = theme => ({
	root: {
		width: '100%',
		flexGrow: 1,
		//marginTop: theme.spacing.unit * 3,
		backgroundColor: theme.palette.background.paper,
		padding:10,
		//background: theme.palette.background.paper,
		//background:theme.palette.secondary.main,
		display:'flex',
		flexDirection:'column',
		height:'100%',
		boxSizing:'border-box',
	},
	item:{
		display:'flex',
		padding:[2,16]
	},
	input:{
		padding:[4,10],
		fontSize:'12pt',
		flex:1,
		width:'100%',
		marginLeft:-6
	},
	label:{
		whiteSpace:'nowrap',
		fontSize:'12pt',
		padding:'0px 10px',
	},
	value2:{
		flex:1,
		fontSize:'12pt',
		paddingLeft:0,
		display:'flex',
		alignItems:'center'
	},
	place:{
		flex:1,
		display:'flex',
		flexDirection:'column',
		alignItems:'stretch'
	},
	readOnly:{
		flex:1,
		overflow:'auto',
		border:[1,'solid','#ddd'],
		padding:5
	},
	editor:{
		flex:1,
		display:'flex',
		flexDirection:'column',
	},
	editorContent:{
		flex:1,
		height:'100%',
		overflow:'auto',
		border:[1,'solid','#ddd'],
		padding:5
		//borderBottom:[1,'solid','#ddd']
	},
	editorButton:{
		width:34,
		height:34
	},
	editorSpanButton:{
		fontSize:18,
		display:'inline-block',
		height:24,
		width:24,
		textAlign:'center',
		verticalAlign:'middle',
		lineHeight:'24px',
		fontWeight:'bold'
	}
});

class Announcement extends Component{
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}
	state={
		runningText:'',
		announcement:'',
		announcEdit:false,
		pageType:'announcement'
	}
	onChange = (editorState) => {
		this.setState({editorState});
	}
	runningTextSave=(text,cb)=>{
		Put('runningtext',{text},(e,r)=>{
			//console.log('e==>',e);
			//console.log('r==>',r);
			if(e){
				return this.setState({errOpen:true,errMessage:e.toString()},()=>{
					cb(e);
				});
			}
			//if(r.d){
			//	UpdateConfig({running_text:r.d.value});
			//}
			cb(null,r);
		});
	}
	componentWillMount(){
		//console.log('this.props==>',this.props);
		if((this.props.match||{}).path){
			let pageType=(this.props.match||{}).path.replace(/^\/(\w*)/,'$1');
			if(!pageType) pageType='announcement';
			this.setState({pageType},()=>{
				appEvent.fire('titleChange',humanize(pageType));
				Config((e,r)=>{
					//console.log('r==>',r)
					if(r && r.running_text){
						this.setState({runningText:r.running_text.text});
					}
				})
				Get(pageType,{},(e,r)=>{
					//console.log('r==>',r);
					if(e) {
						console.error('e==>',e);
					}
					if(r && r.d && r.d.content){
						let announcement=r.d.content;
						this.setState({announcement},()=>{
							$('#announcement').html(announcement);
						});
					}
				});
			});
		}
	}
	announcSave=(content,cb)=>{
		let {pageType}=this.state;
		Put(pageType,{content},(e,r)=>{
			//console.log('e==>',e);
			//console.log('r==>',r);
			if(e){
				return this.setState({errOpen:true,errMessage:e.toString()},()=>{
					if(cb) cb(e);
				});
			}
			if(r.d){
				this.setState({announcement:r.d.content,announcEdit:false},()=>{
					$('#announcement').html(this.state.announcement);
				});
			}
			if(cb) cb(null,r);
		});
	}
	//<div id={"announch"} className={"markdown-body"} />
	//<Editor style={{height:'100%'}} editorState={this.state.editorState} onChange={this.onChange} />
	render(){
		const {classes}=this.props,
			{runningText,announcEdit,announcement,pageType}=this.state;
		return <List className={classes.root}>
			{pageType==='announcement' && <EditableItem2 c={classes}
				value={runningText}
				//onChange={this.nameChange}
				label={"Running Text : "}
				rootClass={"item"}
				type={"text"}
				onSave={this.runningTextSave}/>}
			<ListItem className={classes.place}>
				<div>{humanize(pageType)} : &nbsp;
					{!announcEdit && <Button raised color={'secondary'} onClick={()=>{this.setState({announcEdit:true})}}>Edit</Button>}
				</div>
				{!announcEdit && <div className={classes.readOnly+' markdown-body'} id='announcement' />}
				{!!announcEdit && <RichEditor
					value={announcement}
					className={classes.editor}
					tbClassName={classes.editorToolbar}
					contentClassName={classes.editorContent}
					btnClass={classes.editorButton}
					btnSpanClass={classes.editorSpanButton}
					onSave={this.announcSave}
					onCancel={()=>{
						this.setState({announcEdit:false},()=>{
							$('#announcement').html(announcement);
						})
					}}
				/>}
			</ListItem>
		</List>
	}
}

//export default Announcement;
export default withStyles(styles)(Announcement);