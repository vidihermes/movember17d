import React,{Component} from 'react';
import Grid from 'material-ui/Grid';
import Divider from 'material-ui/Divider';
import List,{ ListItem,ListItemText} from 'material-ui/List';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import Collapse from 'material-ui/transitions/Collapse';

import ExpandLess from 'material-ui-icons/ExpandLess';
import ExpandMore from 'material-ui-icons/ExpandMore';


import withRoot from '../components/withRoot';
import {appEvent} from '../components/events';
import {Me, Game, Alias, UpdateMe,UserType} from '../components/game';
import {Put,Get} from '../components/ajax';
import {SnackBar,ChangePassword,EditableItem} from '../components/dialog';

const styles = theme => ({
	root: {
		display:'flex',
		flexDirection:'column',
		width: '100%',
		height: '100%',
	},
	content:{
		flex:1,
		overflowY:'auto',
		[theme.breakpoints.up('sm')]: {
			display:'flex',
			flexDirection:'row',
			overflowY:'hidden',
		}
		
	},
	sub:{
		width: '100%',
		float:'left',
		padding:5,
		boxSizing:'border-box',
		[theme.breakpoints.up('sm')]: {
			width: '50%',
			flex:1,
			overflowY:'auto'
		},
	},
	sub2:{
		width: '100%',
		paddingLeft:24,
		boxSizing:'border-box',
	},
	item:{
		display:'flex',
		padding:[8,16]
	},
	//item2:{
	//	display:'flex',
	//	padding:[6,0,6,20]
	//},
	label:{
		whiteSpace:'nowrap',
		fontSize:'12pt',
		padding:'0px 10px',
	},
	value:{
		fontSize:'12pt',
		paddingLeft:5,
	},
	value2:{
		fontSize:'12pt',
		paddingLeft:0,
		display:'flex',
		alignItems:'center',
	},
	button: {
		//margin:[-10,0],//margin: theme.spacing.unit,
		width:30,
		height:30,
	},
	wrapper: {
		//margin: theme.spacing.unit,
		position: 'relative',
		display:'inline-block'
	},
	buttonProgress: {
		color: 'teal',//green[500],
		position: 'absolute',
		top: '50%',
		left: '50%',
		marginTop: -12,
		marginLeft: -12,
	},
	button2: {
		margin: theme.spacing.unit,
	},
	title:{
		...theme.typography.title,
		justifyContent:'center'
	},
	input:{
		padding:[5,10],
		fontSize:'12pt',
		flex:1,
		width:'100%',
		marginLeft:-6
	},
	// strike commission
	sRoot:{
		margin:[0,0,0,20],
		backgroundColor:'rgba(100,110,120,0.1)'
	},
	sButton:{
		//display:'flex',
		padding:[4,10]
	},
	sItem:{
		textAlign:'center',
		backgroundColor:'#efefef',
		padding:0,
		'&:nth-child(even)':{
			backgroundColor:'#e4e4e4'
		}
	},
	//sItem2:{
	//	flex:1,
	//	backgroundColor:'rgba(0,0,255,0.1)',
	//	padding:6,
	//	'&:nth-child(even)':{
	//		backgroundColor:'rgba(0,0,255,0.05)'
	//	}
	//},
	empty:{
		color:'rgba(0,0,0,0.5)',
		backgroundColor:'rgba(255,255,255,0.5)',
		padding:10,
		textAlign:'center'
	},
	//\strike commission
	after:{
		fontSize:'10pt',
		padding:[0,5],
		color:'rgba(0,0,0,0.5)'
	},
	err:{
		borderColor:'#f00',
		outlineColor:'rgba(255,0,0,0.1)'
	},
	footer:{
		padding:[0,10],
		borderTop:[1,'solid','#eee'],
		justifyContent:'space-between',
		display:'flex',
		alignItems:'center',
		[theme.breakpoints.down('sm')]: {
			display:'block'
		},
	},
	footerLeft:{
		display:'flex',
		alignItems:'center',
		flex:1,
		flexWrap:'wrap'
	},
	bc:{
		flex:1
	},
	bcContent:{
		whiteSpace:'nowrap'
	},
	footerRight:{
		[theme.breakpoints.down('md')]: {
			display:'flex',
			justifyContent:'space-around',
		},
	},
	bold:{
		fontWeight:'bold'
	}
});

const Item=({c,label,value,valueCn})=><ListItem className={c.item}>
	<Grid item xs={4} className={c.label}>{label}</Grid>
	<Grid item xs={1} className={c.label}>{":"}</Grid>
	<Grid item xs={7} className={c.value+(valueCn?' '+valueCn:'')}>{value}</Grid>
</ListItem>

//const Item2=p=><ListItem className={p.c.item2}>
//	<Grid item xs={4} className={p.c.label}>{p.label}</Grid>
//	<Grid item xs={1} className={p.c.label}>{":"}</Grid>
//	<Grid item xs={7} className={p.c.value}>{p.value}</Grid>
//</ListItem>

class Admin extends Component{
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}
	state={
		id:'',
		name:'',
		status:'',
		me:{},
		active:true,
		errOpen:false,
		errMessage:'',
		cpOpen:false,
		cepOpen:false,
	}
	cpClose=()=>{
		this.setState({cpOpen:false});
	}
	cpOK=({password,confirm})=>{
		if(!(!!password && password === confirm))
			return this.setState({errOpen:true,errMessage:'invalid new password or confirm new password'});
		//this.setState({cpOpen:false});
		Put('user/'+this.state.id,{password},(e,r)=>{
			if(e) {
				console.log('e==>',e);
				return this.setState({errOpen:true,errMessage:e.toString()});
			}
			console.log('r==>',r);
			this.setState({cpOpen:false});
		});
	}
	cepClose=()=>{
		this.setState({cepOpen:false});
	}
	cepOK=({password,confirm})=>{
		if(!(!!password && password === confirm))
			return this.setState({errOpen:true,errMessage:'invalid new password or confirm emergency password'});
		Put('user/'+this.state.id,{fake_password:password},(e,r)=>{
			if(e) {
				console.log('e==>',e);
				return this.setState({errOpen:true,errMessage:e.toString()});
			}
			this.setState({cepOpen:false});
		});
	}
	meChange=me=>{
		//if(me.type==='sadmin') return this.props.history.replace('/me');
		this.setState({me});
	}
	nameSave=(name,cb)=>{
		Put('user/'+this.state.id,{name},(e,r)=>{
			console.log('e==>',e);
			console.log('r==>',r);
			if(e){
				return this.setState({errOpen:true,errMessage:e.toString()},()=>{
					cb(e);
				});
			}
			cb(null,r);
		});
	}
	//intakeSave=(g)=>{
	//	return (it,cb)=>{
	//		let fld={};
	//		fld['config['+g+'][it]']=it;
	//		Put('me',fld,(e,r)=>{
	//			if(e) {
	//				return this.setState({errOpen:true,errMessage:e.toString()},()=>{
	//					cb(e);
	//				});
	//			}
	//			cb(null,r);
	//			if(r && r.d) UpdateMe(r.d);
	//		})
	//	}
	//}
	//configValidation=(game,k)=>{
	//	return v=>{
	//		//console.log('v==>',v)
	//		v=(v||'0').toString();
	//		if(!(v.match(/^[\d,.]+$/))) return false;
	//		let v2=parseFloat(v);
	//		if(isNaN(v2)) return false;
	//		let min=parseFloat(((this.state.game[game]||{})[Alias[k]+'_min'])|| 0),
	//			max=parseFloat(((this.state.game[game]||{})[Alias[k]+'_max'])|| 20);
	//		return !((v2 < min) || (v2 > max));
	//	}
	//}
	closeErr=()=>{
		this.setState({errOpen:false})
	}
	route=uri=>{
		return (e)=>{
			if(e && e.stopPropagation) e.stopPropagation();
			this.props.history.push(uri);
		}
	}
	ready=(id)=>{
		Get('user',{id,type:'admin',limit:1},(e,r)=>{
			console.log('r==>',r);
			if(e) return console.log('error==>',e);
			if(r && r.d && r.d[0]){
				let st={...r.d[0]};
				this.setState(st,()=>{
					console.log('st==>',st)
				});
			}
		});
	}
	componentWillMount(){
		//appEvent.on('MeErr',this.showError);
		if(this.props.match && this.props.match.params && this.props.match.params.id){
			let id=this.props.match.params.id;
			appEvent.fire('titleChange','Admin '+id+' Profile ');
			this.ready(id);
		}
		Me((e,me)=>{
			this.meChange(me);
		});
		//Game((e,game)=>{
		//	this.setState({game});
		//});
	}
	componentWillUnmount(){
		appEvent.off('meChange',this.meChange);
		//appEvent.off('MeErr',this.showError);
	}
	render(){
		let { classes } = this.props,
			{id,name,type,status,active,me,errOpen,errMessage,cpOpen,cepOpen} = this.state;
			console.log('id==>',id)
		//	console.log('agent_config==>',agent_config);
		return <div className={classes.root}>
			<div className={classes.content}>
				{!id && <div style={{textAlign:'center',flex:1}}>
					<div className={classes.empty}>Admin account not found</div>
				</div>}
				{!!id && <List className={classes.sub}>
					<Typography type="subheading" gutterBottom>Profile</Typography>
					<Divider />
					<Item c={classes} label={"Id"} value={id} valueCn={classes.bold}/>
					{(me||{}).type !=='developer' && <Item c={classes} label={"Name"} value={name}/>}
					{(me||{}).type ==='developer' && <EditableItem c={classes}
						value={name}
						//onChange={this.nameChange}
						label={"Name"}
						rootClass={"item"}
						type={"text"}
						onSave={this.nameSave}/>}
					<Item c={classes} label={"Type"} value={UserType[type]} />
					<Divider />
					<Item c={classes} label={"Status"} value={active?'Active':(status?status:'Terminated')} />
				</List>}
			</div>
			{((me||{}).type ==='developer' || (me||{}).type ==='sagent') && !!id && <div className={classes.footer}>
				<div className={classes.footerLeft} />
				<div className={classes.footerRight}>
					<Button raised color={"secondary"} className={classes.button2} onClick={()=>{this.setState({cpOpen:true})}}>Change Password</Button>
					<Button raised color={"secondary"} className={classes.button2} onClick={()=>{this.setState({cepOpen:true})}}>Change Emergency Password</Button>
				</div>
			</div>}
			<SnackBar open={errOpen} onClose={this.closeErr} message={errMessage}/>
			{cpOpen && <ChangePassword open={cpOpen} onClose={this.cpClose} onOK={this.cpOK}/>}
			{cepOpen && <ChangePassword open={cepOpen} onClose={this.cepClose} onOK={this.cepOK}
					title={'Change Emergency Password'}
					label={'New Emergency Password'}
					confirmLabel={'Confirm Emergency Password'}
				/>}
		</div>
	}
}

export default withRoot(withStyles(styles, { withTheme: true })(Admin));