import React,{Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import withRoot from '../components/withRoot';
import {appEvent} from '../components/events';
import Button from 'material-ui/Button';
import { CircularProgress } from 'material-ui/Progress';

import {Me,Config,Game,CurrentPrize,saveTmpBet,loadTmpBet} from '../components/game';
import {Get,Post,flatten} from '../components/ajax';
import {defBetList,BetRow} from '../components/BetRow';
import {SnackBar,Confirm, BetResult, SearchBar,Classes,TableClasses,BetClasses} from '../components/dialog';

import {TicketDetail} from './tickets';

const styles = theme =>{
	window.theme=theme;
	return {
		...TableClasses(theme),
		...Classes(theme),
		...BetClasses(theme)
	}
};


const perm=s=>{
	var permArr = [],
		usedChars = [];
	
	function permute(input) {
		var i, ch;
		for (i = 0; i < input.length; i++) {
			ch = input.splice(i, 1)[0];
			usedChars.push(ch);
			if (!input.length) {
				permArr.push(usedChars.slice());
			}
			permute(input);
			input.splice(i, 0, ch);
			usedChars.pop();
		}
		return permArr
	};
	return permute(s.split('')).map(function(v){return v.join('')})
}

const uniqPermute=s=>{
	return Array.from(new Set(perm(s)));
}

export const createNs=(from,to)=>{
	var ns=[];
	for(let i=from;i<=to;i++){
		ns.push(i.toString());
	}
	return ns;
}

export const Table=p=><table style={{border:'1px solid #000'}} cellSpacing={"0"} cellPadding={"1"}>
	<thead><tr style={{border:'1px solid #000', textAlign:'center', fontWeight:'bold'}}>
		<th className={p.classes.th}>No</th>
		<th className={p.classes.th}>Bet</th>
		<th className={p.classes.th}>Num</th>
		<th className={p.classes.th}>Big</th>
		<th className={p.classes.th}>Small</th>
		<th className={p.classes.th}>T</th>
	</tr></thead><tbody>
		{p.children}
	</tbody></table>;

class PlaceFixBet extends Component{
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}
	static contextTypes = {
		intl: PropTypes.object
	}
	_uPermute={}
	items={}
	ns=this.props.ns||createNs(1,10);
	pageName=this.props.pageName||'fixed'
	state={
		me:{},
		id:'',
		player:{},
		suggestions: [],
		searchMode:false,
		game:{},
		currentPrize:{},
		ready:false,
		
		clearEnable:false,
		buttonEnable:false,
		cf:{},//calcFactor(),
		
		eItems:[], //error items
		iItems:[], //incomplete items
		big:0,
		small:0,
		defBet:'4D',
		
		err:false,
		
		loading:false,
		confirmOpen:false,
		successOpen:false,
		errorOpen:false,
		resultOpen:false,
		betResult:{}
	}
	itemChange=o=>{
		this.calc();
	}
	uPermute=(numb)=>{
		if(this._uPermute[numb]) return this._uPermute[numb];
		return this._uPermute=uniqPermute(numb);
	}
	calc=()=>{
		let {err,cf}=this.state;
		let ibig=0,
			ismall=0,
			eItems=[],
			iItems=[],
			dl=1;//(drawDay[day]||[]).length;
		
		this.ns.forEach(k=>{
			let {g,n,b,s,bt,err,inc,complete}=this.items[k].state;
			//console.log({g,n,b,s,bt,err,inc,complete});
			if(complete){
				let anumb=[n];
				if(bt==='R'){
					anumb=this.uPermute(n);
				}
				let l=bt==='*'?10:anumb.length;
				ibig += l * parseFloat(b||'0');
				ismall += l * parseFloat(s||'0');
			} else if(inc){
				iItems.push(k);
			} else if(Object.keys(err).length){
				eItems.push(k);
			}
		});
		let st={eItems,iItems,big:ibig*dl,
			small:ismall*dl};
		if(err) st.err=false;
		this.setState(st);
	}
	defBetChange=(e)=>{
		this.ns.forEach(k=>{
			let {err,inc,complete}=this.items[k].state;
			if(!(Object.keys(err)||[]).length && !inc && !complete){
				this.items[k].setBet(e.target.value);
			}
		});
		this.setState({defBet:e.target.value});
	}
	onNext=({cx,cy,x,y})=>{
		let tn=this.props.tn||['g','n','b','s','bt'],
			rowCnt=this.ns.length,
			xTmp=x,
			yTmp=y;
		if(!!x){
			if(cx+x > tn.length-1){x=0; cx=0; y=1;}
			else if(cx+x < 0){x=0; cx=tn.length-1; y=-1}
		} else if(!!y) {
			if(cy+y > rowCnt){y=0; cy=1; x=1;}
			else if(cy+y < 1){y=0; cy=rowCnt; x=-1}
		}
		cy=((cy+y)+(rowCnt-1))%rowCnt+1;
		cx=(cx+x+tn.length)%tn.length;
		if(!this.items[cy.toString()].setFocus(tn[cx]))
			this.onNext({cx,cy,x:xTmp,y:yTmp});
	}
	setPlayer=o=>{
		console.log('player==>',o)
		this.setState({
			id:o.id,
			searchMode:false,
			player:o,
			ready:true,
		},()=>{
			this.calc();
		});
	}
	gameValidation=(g,num)=>{
		if(this.props.gameValidation) return this.props.gameValidation(g,num);
		let{game}=this.state;
		this.gameValidate=this.gameValidate||{};
		this.gameValidate[g]=this.gameValidate[g]||new RegExp(game[g].validate);
		return !!(num||'').match(this.gameValidate[g]);
	}
	saveTmpBet=()=>{
		let dt={};
		this.ns.forEach(k=>{
			let {err,g,n,b,s,bt}=this.items[k].state;
			if(!(Object.keys(err).length)){
				dt[k]={g,n,b,s,bt};
			}
		});
		saveTmpBet(this.pageName,dt);
	}
	loadTmpBet=()=>{
		let bts=loadTmpBet(this.pageName)||{};
		Object.keys(bts).forEach(k=>{
			let v=bts[k]
			this.items[k].setState(v,this.items[k].checkIncomplete);
		})
	}
	clear=()=>{
		let {defBet}=this.state;
		this.ns.forEach(k=>{
			this.items[k].setState({g:defBet,n:'',b:'',s:'',bt:'-',err:{},inc:false,complete:false});
		});
		this.setState({eItems:[],iItems:[],big:0,small:0,err:false},()=>{
			this.calc();
		})
	}
	
	submit=()=>{
		this.setState({
			confirmOpen:true,
			confirmOnOK:()=>{
				this.setState({confirmOpen:false,loading:true},()=>{
					this.processSubmit();
				});
			},
		});
	}
	processSubmit=()=>{
		let {id,me}=this.state;
		let dt={place_to:id,form_type:this.pageName,detail:[]};
		//let i=0;
		this.ns.forEach(k=>{
			let {g,n,b,s,bt,complete}=this.items[k].state;
			if(complete){
				dt.detail.push({g,n,b:b||0,s:s||0,bt});
			}
		});
		//return console.log('hsl==>',flatten(dt));
		//return console.log('dt==>',JSON.stringify(dt));
		if(dt.detail.length > 0){
			Post('fixbet',flatten(dt),(e,r)=>{
				this.setState({loading:false});
				if(e){
					console.log('e==>',e);
					return this.setState({errorOpen:true,errorMsg:(e||{}).msg?e.msg:e.toString()});
				}
				this.setState({successOpen:true,successMsg:'Bet has been placed'},()=>{
					this.clear();
					if(id===me.id){
						Me((e,me)=>{
							this.setState({me},()=>{
								this.setPlayer(me);
							})
						},true);
					} else {
						if(this.seachBar && this.seachBar.findId) this.seachBar.findId(id);
					}
					//this.setState({resultOpen:true,betResult:r.d});
				});
			});
		}
	}
	componentWillReceiveProps(np){
		this.getProps(np,()=>{
			let {id}=this.state;
			if(id){
				return this.setPlayer({id});
				//if(this.seachBar && this.seachBar.findId) this.seachBar.findId(id);
			}
		})
	}
	getProps=(p,cb)=>{
		let m=p.match;
		m=m||{};
		let {id}=this.state;
		let {userid}=m;
		if(id!==userid){
			this.setState({
				id:m.params.userid||'',
			},cb);
		}
	}
	componentWillMount(){
		appEvent.fire('titleChange','Place Fixed Bets');
		Game((e,game)=>{
			console.log('game==>',game);
			//this.setState({config});
			this.setState({game})
		});
		CurrentPrize((e,currentPrize)=>{
			console.log('currentPrize==>',currentPrize);
			//this.setState({config});
			this.setState({
				currentPrize:currentPrize,
			});
		});
		this.componentWillReceiveProps(this.props);
		Me((e,me)=>{
			this.meChange(me);
		},true);
	}
	meChange=me=>{
		this.setState({
			me,
		},()=>{
			let {id,me}=this.state;
			if(!id && me.can_play){
				this.setPlayer(me);
			}
		});
	}
	componentDidMount(){
		this.loadTmpBet();
	}
	componentWillUnmount(){
		this.saveTmpBet();
	}
	formatNumb=n=>{
		return this.context.intl.formatNumber(n,{style: 'decimal',minimumFractionDigits:2,maximumFractionDigits:2});
	}
	onSuccessClose=()=>{
		this.setState({successOpen:false});
	}
	onErrorClose=()=>{
		this.setState({errorOpen:false});
	}
	confirmClose=()=>{
		this.setState({confirmOpen:false});
	}
	RenderContent=(p)=>{
		if(this.props.renderContent) return this.props.renderContent(p);
		let {classes}=this.props;
		let {ready,loading,currentPrize}=this.state;
		return <Table classes={classes}>
			{this.ns.map(k=><BetRow disable={!ready || loading}
				ref={e=>{this.items[k]=e}}
				key={k}
				noD={true}
				idx={k}
				classes={classes}
				onNext={this.onNext}
				onChange={this.itemChange}
				gameValidation={this.gameValidation}
				currentPrize={currentPrize}/>)}
		</Table>
	}
	route=uri=>{
		return (e)=>{
			if(e && e.stopPropagation) e.stopPropagation();
			console.log(uri)
			this.props.history.push(uri);
		}
	}
	render(){
		const { ready, searchMode, id, big,small,defBet,eItems,iItems,err, me,
			suggestions, currentPrize,
			successOpen, successMsg, errorOpen, errorMsg,loading, confirmOpen,confirmOnOK, resultOpen,betResult} = this.state;
		const formatNumber=this.formatNumb;
		const { classes } = this.props;
		return <div className={classes.root}>
			<div className={classes.sub}>
				<div className={classes.tableFooter}>
					<div className={classes.footerSub2}>
						<SearchBar label={'Place To'}
							getRef={(e)=>{this.seachBar=e;}}
							id={id}
							me={(me && me.can_play && me.status !== 'suspended' && me.status !=='locked'?me:false)}
							allowSearch={me.type!=='player'}
							searchParams={{active:true,can_play:true}}
							userDetailValidate={d=>d.active && d.can_play && d.status!=='suspended' && d.status!=='locked'}
							onSearchMode={()=>{this.setState({ready:false})}}
							onFound={(o)=>{this.route('/placefixbet/'+o.id)()}}/>
						<div className={classes.footerSub2}>
							&nbsp; &nbsp;
							<a className={classes.link} onClick={()=>{this.route('/viewfixbet/'+id)()}}>&nbsp; View &nbsp;</a>
						</div>
					</div>
				</div>
				<div className={classes.hRow}>
					Default Bet :
					<select disabled={!ready || loading} className={classes.hSelect} name={"defBet"} value={defBet} onChange={this.defBetChange}>
						{defBetList.map(d=><option key={d} value={d}>{d}</option>)}
					</select>
				</div>
				<div className={[
						classes.hRow
					].join(' ')}>
					Big : {formatNumber(big)} &nbsp; &nbsp;
					Small : {formatNumber(small)} &nbsp; &nbsp; &nbsp; &nbsp;
					Total : {formatNumber(big+small)}
				</div>
				<div className={classes.toolbar}>
					<Button color={'secondary'} disabled={!(!!eItems.length || !!iItems.length || big || small) || loading} raised onClick={this.clear}>Clear</Button> &nbsp;
					<div className={classes.wrapper}>
						<Button disabled={!(big || small) || loading || err || !ready ||!!eItems.length||!!iItems.length} raised color={"primary"} onClick={this.submit}>Place Fixed</Button>
						{loading && <CircularProgress size={24} className={classes.buttonProgress} />}
					</div>
				</div>
				<this.RenderContent {...{classes,ready,loading,currentPrize}} comp={this}/>
			</div>
			{resultOpen && <BetResult open={resultOpen} onClose={()=>{this.setState({resultOpen:false})}}>
				<div className={classes.hRow}>Ticket Page Id : {betResult.page.id}</div>
				<TicketDetail tickets={betResult.tickets} formatNumber={this.formatNumb} v={betResult.page} classes={classes}/>
			</BetResult>}
			{confirmOpen && <Confirm open={confirmOpen} title={'Place Fixed Bet?'} msg={'Are you sure you want to place fixed for this bets?'} onClose={()=>{
				this.setState({confirmOpen:false})
			}} onOK={confirmOnOK}/>}
			<SnackBar open={successOpen} onClose={this.onSuccessClose} message={successMsg} classes={{root:classes.succesBox}}/>
			<SnackBar open={errorOpen} onClose={this.onErrorClose} message={errorMsg} classes={{root:classes.errorBox}}/>
		</div>
	}
}

export default withRoot(withStyles(styles, { withTheme: true })(PlaceFixBet));