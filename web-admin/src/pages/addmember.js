import React,{Component} from 'react';
import Grid from 'material-ui/Grid';
import Divider from 'material-ui/Divider';
import List,{ ListItem, ListItemText} from 'material-ui/List';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Typography from 'material-ui/Typography';
import IconButton from 'material-ui/IconButton';
import Button from 'material-ui/Button';
import Switch from 'material-ui/Switch';
//import SaveIcon from 'material-ui-icons/Save';
//import ModeEditIcon from 'material-ui-icons/ModeEdit';
import CloseIcon from 'material-ui-icons/Close';
import RefreshIcon from 'material-ui-icons/Refresh';
import { CircularProgress } from 'material-ui/Progress';
//import { MenuItem } from 'material-ui/Menu';
import Collapse from 'material-ui/transitions/Collapse';
//import Snackbar from 'material-ui/Snackbar';

import ExpandLess from 'material-ui-icons/ExpandLess';
import ExpandMore from 'material-ui-icons/ExpandMore';

import withRoot from '../components/withRoot';
import {appEvent} from '../components/events';
import {flatten,Get,Post} from '../components/ajax';
import {Game,Me,Config,Alias} from '../components/game';
import {SnackBar} from '../components/dialog';

const styles = theme => ({
	root: {
		display:'flex',
		flexDirection:'column',
		width: '100%',
		height: '100%',
		fontFamily: `"Roboto", "Helvetica", "Arial", sans-serif`,
	},
	content:{
		flex:1,
		overflowY:'auto',
		[theme.breakpoints.up('sm')]: {
			display:'flex',
			flexDirection:'row',
			overflowY:'none',
		}
		
	},
	headerContent:{
		padding:[10,5,0,5]
	},
	footer:{
		textAlign:'right',
	},
	sub:{
		width: '100%',
		//float:'left',
		padding:5,
		boxSizing:'border-box',
		[theme.breakpoints.up('sm')]: {
			//width: '50%',
			flex:1,
			overflowY:'auto'
		},
	},
	sub2:{
		width: '100%',
		paddingLeft:24,
		boxSizing:'border-box',
	},
	item:{
		display:'flex',
		padding:[2,16]
	},
	item2:{
		display:'flex',
		padding:[6,0,6,20]
	},
	label:{
		whiteSpace:'nowrap',
		fontSize:'12pt',
		padding:'0px 10px',
	},
	label2:{
		whiteSpace:'nowrap',
		fontSize:'12pt',
		padding:'0px 10px',
	},
	value:{
		fontSize:'12pt',
		paddingLeft:5,
	},
	value2:{
		fontSize:'12pt',
		paddingLeft:0,
		display:'flex',
		alignItems:'center'
	},
	button: {
		//margin:[-10,0],//margin: theme.spacing.unit,
		width:30,
		height:30,
	},
	wrapper: {
		margin: theme.spacing.unit,
		position: 'relative',
		display:'inline-block'
	},
	buttonProgress: {
		color: 'teal',//green[500],
		position: 'absolute',
		top: '50%',
		left: '50%',
		marginTop: -12,
		marginLeft: -12,
	},
	button2: {
		margin: theme.spacing.unit,
	},
	title:{
		...theme.typography.title,
		justifyContent:'center'
	},
	input:{
		padding:[4,10],
		fontSize:'12pt',
		flex:1,
		width:'100%',
		marginLeft:-6
	},
	after:{
		fontSize:'10pt',
		padding:[0,5],
		color:'rgba(0,0,0,0.5)'
	},
	err:{
		borderColor:'#f00',
		outlineColor:'rgba(255,0,0,0.1)'
	},
	
	// strike commission
	sRoot:{
		margin:[0,0,0,20],
		backgroundColor:'rgba(100,0,0,0.1)'
	},
	sButton:{
		//display:'flex',
		padding:[4,10]
	},
	sItem:{
		textAlign:'center',
		backgroundColor:'#efefef',
		padding:0,
		'&:nth-child(even)':{
			backgroundColor:'#e4e4e4'
		}
	},
	sItem2:{
		flex:1,
		backgroundColor:'rgba(0,0,255,0.1)',
		padding:6,
		'&:nth-child(even)':{
			backgroundColor:'rgba(0,0,255,0.05)'
		}
	},
	sAddForm:{
		flexDirection:'column',
		alignItems:'stretch',
		padding:0,
	},
	sAddRow:{
		display:'flex',
		flexDirection:'row',
		alignItems:'center',
		backgroundColor:'rgba(255,255,255,0.7)',
		'&:nth-child(odd)':{
			backgroundColor:'rgba(255,255,255,0.5)'
		}
	},
	sAddBtn:{
		flex:1,
		margin:5
	},
	sFormItem:{
		flex:1,
		'&:nth-child(odd)':{
			padding:[0,10]
		}
	},
	sFormValue:{
		flexDirection:'row',
		display:'flex',
		alignItems:'center',
	},
	sInput:{
		flex:1,
		width:'100%',
		padding:[5,10],
		fontSize:'12pt'
	}
	//\strike commission
});

class CustomInput extends Component{
	state={
		editMode:false,
		value:this.props.value||this.props.def||'',
		valid:true,
		after:this.props.after||'',
		err:this.props.err||false,
	}
	componentWillMount(){
		this.id=this.props.id||(this.props.label+(new Date().getTime()));
		this._change(this.state.value);
	}
	componentWillUnmount(){
		//this.id=this.props.id||(this.props.label+(new Date().getTime()));
		appEvent.fire('addMemberErrPop',this.id);
	}
	_change=value=>{
		this.setState({value},()=>{
			if(this.props.onChange) this.props.onChange(value);
			let val=this.props.validation;
			if(val){
				let valid=val(value);
				this.setState({valid},()=>{
					if(valid) appEvent.fire('addMemberErrPop',this.id);
					else appEvent.fire('addMemberErrPush',this.id);
				})
			}
		});
	}
	change=e=>{
		this._change(e.target.value);
	}
	componentWillReceiveProps(np){
		if(np.err !== this.state.err)
			this.setState({err:np.err});
		if(np.value !== this.state.value)
			this.setState({value:np.value})
		if(np.after !== this.state.after)
			this.setState({after:np.after})
	}
	focus=()=>{
		this.refs.input.focus();
	}
	render(){
		let {value,valid,err,after}=this.state;
		let p=this.props;
		return <ListItem className={p.cn||p.c.item}>
			<Grid item xs={4} className={p.c.label}>{p.label}</Grid>
			<Grid item xs={1} className={p.c.label}>{":"}</Grid>
			<Grid item xs={7} className={p.c.value2}>
				<input ref={"input"}
					disabled={!!p.disabled}
					className={p.c.input+(!valid||err?' '+p.c.err:'')}
					type={p.type||"text"}
					value={value}
					onChange={this.change} />
				<div className={p.c.after}>{after}</div>
			</Grid>
		</ListItem>
	}
}

const SwitchInput=p=><ListItem className={p.c.item}>
	<Grid item xs={5} className={p.c.label}>{p.label}</Grid>
	<Grid item xs={7} className={p.c.value}>
		<Switch onChange={e=>{p.onChange(e.target.checked)}}
		checked={p.checked}/>
	</Grid>
</ListItem>


//class StrikeItem extends Component{
//	state={
//		exp:false,
//		numb:'',
//		value:0,
//		values:this.props.values,
//		add:false,
//		numbValid:true,
//		valueValid:true,
//		game:this.props.game,
//	}
//	handleClick=()=>{
//		this.setState({exp:!this.state.exp});
//	}
//	change=(k)=>{
//		return (e)=>{
//			let vl=this.props.game.validate,
//				val=this.props.validation,
//				v=e.target.value,
//				st={[k]:v};
//			if(k==='numb') st.numbValid=!!((v||'').match(new RegExp(vl)))
//			else if(val) st.valueValid=val(v);
//			this.setState(st);
//		}
//	}
//	add=()=>{
//		this.setState({
//			numb:'',
//			value:0,
//			add:true,
//			numbValid:true,
//			valueValid:true,
//		},()=>{
//			this.refs.numb.focus();
//		});
//	}
//	remove=(k)=>{
//		return ()=>{
//			let {values}=this.state;
//			values=values||{};
//			delete values[k];
//			this.setState({values});
//			this.props.onChange(values);
//		}
//	}
//	ok=()=>{
//		let {values,numb,value,numbValid,valueValid}=this.state;
//		if(!(numb && numbValid && valueValid && value)){
//			if(!numbValid || !numb) return this.refs.numb.focus();
//			if(!valueValid || !value) return this.refs.value.focus();
//			return;
//		}
//		values={...values||{},[numb]:value};
//		this.setState({values,add:false},()=>{
//			this.props.onChange(values);
//		})
//	}
//	cancel=()=>{
//		this.setState({add:false})
//	}
//	render(){
//		let {classes,after}=this.props;
//		let {numbValid,valueValid}=this.state;
//		
//		return <div className={classes.sRoot}>
//			<ListItem button onClick={this.handleClick} className={classes.sButton}>
//				<ListItemText primary="Upline Strike Commission" />
//				{this.state.exp ? <ExpandLess /> : <ExpandMore />}
//			</ListItem>
//			<Collapse component="li" in={this.state.exp} timeout="auto" unmountOnExit>
//				<List disablePadding className={classes.sPlace}>
//					{!!Object.keys(this.state.values).length && <ListItem className={classes.sItem} style={{fontWeight:'bold'}}>
//						<div className={classes.sItem2}>Number</div>
//						<div className={classes.sItem2}>Commision</div>
//						<div style={{width:'30px'}}/>
//					</ListItem>}
//					{Object.keys(this.state.values).map((k,i)=><ListItem className={classes.sItem} key={k}>
//						<div className={classes.sItem2}>{k}</div>
//						<div className={classes.sItem2}>{this.state.values[k]+' %'}</div>
//						<div>
//							<IconButton className={classes.button} aria-label="reload" onClick={this.remove(k)}>
//								<CloseIcon/>
//							</IconButton>
//						</div>
//					</ListItem>)}
//					{this.state.add && <ListItem className={classes.sAddForm}>
//						<div className={classes.sAddRow}>
//							<div className={classes.sFormItem}>{"Number"}</div>
//							<div className={classes.sFormItem}>
//								<input ref={"numb"} className={classes.sInput+(!numbValid?' '+classes.err:'')} value={this.state.numb} onChange={this.change('numb')}/>
//							</div>
//						</div>
//						<div className={classes.sAddRow}>
//							<div className={classes.sFormItem}>{"Commission"}</div>
//							<div className={classes.sFormItem}>
//								<div className={classes.sFormValue}>
//								<input ref={"value"} className={classes.sInput+(!valueValid?' '+classes.err:'')} value={this.state.value} onChange={this.change('value')}/>
//								<div className={classes.after}>{after}</div>
//								</div>
//							</div>
//						</div>
//						<div className={classes.sAddRow}>
//							<Button raised className={classes.sAddBtn} onClick={this.ok}>OK</Button>
//							<Button raised className={classes.sAddBtn} onClick={this.cancel}>Cancel</Button>
//						</div>
//					</ListItem>}
//					{!this.state.add && <ListItem className={classes.sAddForm}>
//						<Button raised className={classes.sAddBtn} onClick={this.add}>Add</Button>
//					</ListItem>}
//				</List>
//			</Collapse>
//		</div>
//	}
//}

//let alias={
//	rb:'rebate',
//	it:'intake',
//	sc:'strike_comm'
//};
			
class AddMember extends Component{
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}
	memberType=this.props.match.params.action==='addAgent'?'agent':'player'
	member_of=this.props.match.params.id
	state={
		id:'',
		name:'',
		password:'',
		rePassword:'',
		fake_password:'',
		credit:0,
		agent_config:{
			ci:this.memberType==='player'?false:true, //create intake
			ca:this.memberType==='player'?true:false, //create agent
			pb:this.memberType==='player'?true:false, //place bet
		},
		config:{},
		game:{},
		errOpen:false,
		errMessage:'',
		errField:'',
		errSubs:[]
	}
	me='' //_Me().id||''
	meChange=me=>{
		let {id}=me||{};
		this.me=id;
		this.updateTitle();
	}
	configChange=(game,k)=>{
		return v=>{
			let {config} = this.state;
			config[game]=config[game]||{};
			config[game][k]=v;
			this.setState({config});
		}
	}
	valueChange=(k)=>{
		return v=>{
			this.setState({[k]:v});
		}
	}
	acChange=(k)=>{
		return v=>{
			let {agent_config}=this.state;
			agent_config[k]=v;
			this.setState({agent_config});
		}
	}
	cancel=()=>{
		//this.props.history.goBack();
		this.props.history.replace('/downline'+(this.me!==this.member_of?'/'+this.member_of:''));
	}
	submit=()=>{
		this.setState({
				loading: true,
			},
			()=>{
				this.processSubmit(this.state);
			});
	}
	getId=()=>{
		//Get('id',{type:this.memberType||'agent'},(e,r)=>{
		Get('nestedid',{type:this.memberType||'agent',parent:this.member_of},(e,r)=>{
			if(e) {
				return console.log('e==>',e);
			}
			if(r && r.d) this.setState({id:r.d})
		});
	}
	processSubmit=(v)=>{
		v=JSON.parse(JSON.stringify(v));
		if(this.member_of) v.member_of=this.member_of;
		if(this.memberType==='player') delete v.agent_config;
		delete v.loading;
		delete v.game;
		delete v.errOpen;
		delete v.errMessage;
		delete v.errField;
		delete v.errSubs;
		delete v.sConfig;
		if(!v.password)
			return this.setState({errOpen:true,errMessage:'invalid password',errField:'password',loading:false});
		if(!(v.password === v.rePassword))
			return this.setState({errOpen:true,errMessage:'Password does not match the confirm password',errField:'rePassword',loading:false});
		if(!v.fake_password)
			return this.setState({errOpen:true,errMessage:'invalid emergency password',errField:'fake_password',loading:false});
		
		let vv=flatten(v);
		console.log('vv==>',vv);
		Post(this.memberType,vv,(e,r)=>{
			if(e) {
				this.setState({
					loading:false
				},()=>{
					//window.alert(((e||{}).e||{}).toString()); //console.log('e==>',e);
					this.setState({errOpen:true,errMessage:((e||{}).e||{}).toString()||'unknown error',loading:false});
				});
				return;
			}
			this.props.history.replace('/downline'+(this.me!==this.member_of?'/'+this.member_of:''));
		})
	}
	updateTitle=()=>{
		let title=this.memberType==='agent'?'Add Agent':'Add Player';
		if(this.member_of!==this.me){
			title=this.memberType==='agent'?`Add Agent to ${this.member_of}`:`Add Player to ${this.member_of}`;
		}
		appEvent.fire('titleChange',title);
	}
	configValidation=(game,k)=>{
		if(['it','bit','sit'].indexOf(k) > -1)
			return v=>{
				v=(v||'0').toString();
				if(!(v.match(/^[\d,.]+$/))) return false;
				let v2=parseFloat(v);
				if(isNaN(v2)) return false;
				return true;
			}
		return v=>{
			v=(v||'0').toString();
			if(!(v.match(/^[\d,.]+$/))) return false;
			let v2=parseFloat(v);
			if(isNaN(v2)) return false;
			let min=parseFloat(((this.state.game[game]||{})[Alias[k]+'_min'])|| 0),
				max=parseFloat(((this.state.game[game]||{})[Alias[k]+'_max'])|| 20);
			return !((v2 < min) || (v2 > max));
		}
	}
	closeErr=()=>{
		this.setState({errOpen:false,errField:''});
	}
	pushErrSubs=f=>{
		let errSubs=this.state.errSubs||[];
		errSubs.push(f);
		this.setState({errSubs});
	}
	popErrSubs=f=>{
		let errSubs=this.state.errSubs||[];
		do {
			let i=errSubs.indexOf(f);
			if(i > -1) errSubs.splice(i,1);
		} while (errSubs.indexOf(f) > -1);
		this.setState({errSubs});
	}
	componentWillMount(){
		appEvent.on('addMemberErrPush',this.pushErrSubs);
		appEvent.on('addMemberErrPop',this.popErrSubs);
		Me((e,me)=>{
			this.meChange(me);
		});
		Game((e,game)=>{
			this.setState({game});
		});
		
		Config((e,sConfig)=>{
			this.setState({
				sConfig,
				credit:((sConfig||{}).default_credit||{})[this.memberType]||'0'
			});
		})
	}
	componentDidMount(){
		this.getId();
		this.refs.name.focus();
	}
	componentWillUnmount(){
		appEvent.off('addMemberErrPush',this.pushErrSubs);
		appEvent.off('addMemberErrPop',this.popErrSubs);
	}
	render(){
		const { classes} = this.props,
			{memberType} = this,
			{id,name,credit,config,agent_config,loading,game,errOpen,errMessage,errField,errSubs} = this.state;
		return <div className={classes.root}>
			<div className={classes.content}>
				<List className={classes.sub}>
					<Typography type="subheading" gutterBottom>Profile</Typography>
					<Divider />
					<CustomInput ref={"name"} label={'Name'} value={name} c={classes} onChange={this.valueChange('name')}/>
					<ListItem className={classes.item}>
						<Grid item xs={4} className={classes.label}>{'Id'}</Grid>
						<Grid item xs={1} className={classes.label}>{":"}</Grid>
						<Grid item xs={7} className={classes.value2}>
							<input disabled={true} className={classes.input} value={id} />
							<IconButton className={classes.button} aria-label="reload" onClick={this.getId}>
								<RefreshIcon/>
							</IconButton>
						</Grid>
					</ListItem>
					<CustomInput label={'Password'} value={this.state.password} c={classes} err={errField==='password'} type={"password"} onChange={this.valueChange('password')}/>
					<CustomInput label={'Confirrm Password'} value={this.state.rePassword} err={errField==='rePassword'} c={classes} type={"password"} onChange={this.valueChange('rePassword')}/>
					<CustomInput label={'Emergency Password'} value={this.state.fake_password} c={classes} err={errField==='fake_password'} type={"password"} onChange={this.valueChange('fake_password')}/>
					<Divider />
					<CustomInput label={'Credit Limit'} value={credit} c={classes} type={"tel"} onChange={this.valueChange('credit')}/>
					{memberType!=='player' && <SwitchInput c={classes} label={'Create Intake'} checked={!!agent_config.ci} onChange={this.acChange('ci')} />}
					{memberType!=='player' && <SwitchInput c={classes} label={'Create Agent'} checked={!!agent_config.ca} onChange={this.acChange('ca')} />}
					{memberType!=='player' && <SwitchInput c={classes} label={'Place Bet'} checked={!!agent_config.pb} onChange={this.acChange('pb')} />}
					
					<Divider />
				</List>
				<List className={classes.sub}>
					<Typography type="body2" gutterBottom>Configuration</Typography>
					<Divider />
					{['4D','3D','2D','1D'].map(g=><div className={classes.sub2} key={g}>
						<Typography type="subheading" gutterBottom>{g}</Typography>
						{g!=='1D' && <CustomInput label={'Rebate'}
							value={(config[g]||{}).rb}
							def={'0'}
							c={classes}
							type={"tel"}
							cn={classes.item2}
							after={`% (${(game[g]||{})[Alias.rb+'_min']|| '0'} - ${(game[g]||{})[Alias.rb+'_max']|| '20'})`}
							onChange={this.configChange(g,'rb')}
							validation={this.configValidation(g,'rb')}/>}
						{/*!!agent_config.ci && <CustomInput label={'Intake'}
							value={(config[g]||{}).it}
							def={'0'}
							c={classes} type={"tel"}
							cn={classes.item2}
							after={`$ (${(game[g]||{})[Alias.it+'_min']|| '0'} - ${(game[g]||{})[Alias.it+'_max']|| '20'})`}
							onChange={this.configChange(g,'it')}
							validation={this.configValidation(g,'it')}/>*/}
						{/*(!!agent_config.pb && g!=='1D') && <StrikeItem classes={classes}
							after={`% (${(game[g]||{})[Alias.sc+'_min']|| '0'} - ${(game[g]||{})[Alias.sc+'_max']|| '20'})`}
							values={(config[g]||{}).sc||{}}
							game={game[g]}
							onChange={this.configChange(g,'sc')}
							validation={this.configValidation(g,'sc')}/>*/}
						{!!agent_config.ci && g==='1D' && <CustomInput label={'Intake'}
							value={(config[g]||{}).it}
							def={'0'}
							c={classes} type={"tel"}
							cn={classes.item2}
							after={`$`}
							onChange={this.configChange(g,'it')}
							validation={this.configValidation(g,'it')}/>}
						{!!agent_config.ci && g!=='1D' && <CustomInput label={'Big Intake'}
							value={(config[g]||{}).bit}
							def={'0'}
							c={classes}
							type={"tel"}
							cn={classes.item2}
							after={`$`}
							onChange={this.configChange(g,'bit')}
							validation={this.configValidation(g,'bit')}/>}
						{!!agent_config.ci && g!=='1D' && <CustomInput label={'Small Intake'}
							value={(config[g]||{}).sit}
							def={'0'}
							c={classes}
							type={"tel"}
							cn={classes.item2}
							after={`$`}
							onChange={this.configChange(g,'sit')}
							validation={this.configValidation(g,'sit')}/>}
						{(!!agent_config.pb && g!=='1D') && <CustomInput label={'Strike Commission'}
							value={(config[g]||{}).sc}
							def={'0'}
							c={classes}
							type={"tel"}
							cn={classes.item2}
							after={`% (${(game[g]||{})[Alias.sc+'_min']|| '0'} - ${(game[g]||{})[Alias.sc+'_max']|| '20'})`}
							onChange={this.configChange(g,'sc')}
							validation={this.configValidation(g,'sc')}/>}
					</div>)}
				</List>
			</div>
			<div className={classes.footer}>
				<Divider />
				<div className={classes.wrapper}>
					<Button
						raised
						disabled={loading || !!errSubs.length}
						color={"primary"}
						onClick={this.submit}>Submit</Button>
					{loading && <CircularProgress size={24} className={classes.buttonProgress} />}
				</div>
				<Button raised color={'secondary'} className={classes.button2} onClick={this.cancel}>Cancel</Button>
			</div>
			<SnackBar open={errOpen} onClose={this.closeErr} message={errMessage}/>
		</div>
	}
}

export default withRoot(withStyles(styles, { withTheme: true })(AddMember));