import {addLocaleData,IntlProvider} from 'react-intl';
import en from 'react-intl/locale-data/en';

import React from 'react';
import { render } from 'react-dom';
import Index from './pages/index';
import 'moment/locale/en-gb';
addLocaleData([...en]); //https://github.com/yahoo/react-intl/wiki#loading-locale-data

const rootElement = document.querySelector('#root');
if (rootElement) {
  render(<IntlProvider locale={'en'}>
    <Index />
  </IntlProvider>, rootElement);
}
