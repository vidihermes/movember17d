var path=require('path'),
	fs=require('fs'),
	rimraf=require('rimraf'),
	copy = require('copy'),
	cwd=process.cwd(),
	bjsPath=path.resolve(cwd,'../backend/public/static/js'),
	bViews=path.resolve(cwd,'../backend/views'),
	parentPath=path.resolve(cwd,'../'),
	cView=path.resolve(cwd,'./build/index.html');

rimraf.sync(bjsPath+'/*');
['web-admin','web-sagent','web-agent'].forEach(function(f){
	var cjsPath=path.resolve(parentPath,f,'./build/static/js');
	copy(cjsPath+'/*', bjsPath, function() {});
	console.log('-->',cjsPath);
})

console.log(cView+' -> '+bViews+'/admin.ejs');
fs.writeFileSync(bViews+'/admin.ejs',fs.readFileSync(cView,'utf8'),'utf8');
