import React,{Component} from 'react';
//import Divider from 'material-ui/Divider';
import List/*,{ ListItem}*/ from 'material-ui/List';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
//import IconButton from 'material-ui/IconButton';
//import Collapse from 'material-ui/transitions/Collapse';

//import withRoot from '../components/withRoot';
import {appEvent} from '../components/events';
import {Get} from '../components/ajax';
//import {parseNotif} from '../components/NotifMenu';

const styles = theme => ({
	root: {
		width: '100%',
		display:'flex',
		flexDirection:'column',
		flex:1,
		height:'100%',
	},
	date:{
		fontSize:'10pt',
		float:'right',
		color:'#aaa'
	},
	item:{
		padding:[10,20],
		borderBottom:[1,'solid','#ddd']
	},
	empty:{
		color:'rgba(0,0,0,0.5)',
		backgroundColor:'rgba(255,255,255,0.5)',
		padding:10,
		textAlign:'center'
	}
});

class Logs extends Component{
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}
	static contextTypes = {
		intl: PropTypes.object
	}
	state={
		members:[],
		ready:false,
	}
	ready=()=>{
		Get('mylogs',{limit:200},(e,r)=>{
			if(e) return console.error(e);
			this.setState({
				ready:true,
				members:(r.d||[]) //.filter((v,k)=>k<5)
			});
		})
	}
	componentWillMount(){
		appEvent.fire('titleChange','Logs');
		this.ready();
	}
	componentWillUnmount(){
		//appEvent.off('meChange',this.meChange);
	}
	route=uri=>{
		return (e)=>{
			if(e && e.stopPropagation) e.stopPropagation();
			console.log(uri)
			this.props.history.push(uri);
		}
	}
	formatNumb=n=>{
		return this.context.intl.formatNumber(n,{style: 'decimal',minimumFractionDigits:2,maximumFractionDigits:2});
	}
	render(){
		const { classes } = this.props,
			{members,ready} = this.state;
		return <div className={classes.root}>
			<List className={classes.sub}>
				{members.map((v,k)=><li key={v.id} k={k} className={classes.item}>
					<div className={classes.date}>{v.date}</div>
					<div>{v.description}</div>
				</li>)}
				{!members.length && ready && <div className={classes.empty} >there is no data found</div>}
			</List>
		</div>
	}
}
export default withStyles(styles, { withTheme: true })(Logs);
//export default withRoot(withStyles(styles, { withTheme: true })(Notifs));