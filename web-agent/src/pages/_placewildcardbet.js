import React,{Component} from 'react';
import PropTypes from 'prop-types';
import Autosuggest from 'react-autosuggest';
import { withStyles } from 'material-ui/styles';
import withRoot from '../components/withRoot';
import {appEvent} from '../components/events';
import IconButton from 'material-ui/IconButton';
import Button from 'material-ui/Button';
import Paper from 'material-ui/Paper';
import { MenuItem } from 'material-ui/Menu';

import PersonIcon from 'material-ui-icons/Person';
import SearchIcon from 'material-ui-icons/Search';

import {Me,Config,Game,RequestDrawDate,CurrentPrize,saveTmpBet,loadTmpBet} from '../components/game';
import {Get,Post} from '../components/ajax';
import {defBetList,enableDay,defaultDay,/*calcFactor,*/WildcardBetRow} from '../components/BetRow';
import {SnackBar} from '../components/dialog';

//import {formatNumber} from 'react-intl';

const styles = theme =>{
	window.theme=theme;
	return {
		root: {
			flexGrow: 1,
			//marginTop: theme.spacing.unit * 3,
			backgroundColor: theme.palette.background.paper,
			padding:10
		},
		hRow:{
			padding:[5,0],
			fontSize:'12pt',
			display:'flex',
			alignItems:'center'
		},
		dateItem:{
			padding:[0,5]
		},
		disable:{
			color:'rgba(0,0,0,0.5)'
		},
		//hRow1:{
		//	display:'flex',
		//	alignItems:'center'
		//},
		idInput:{
			padding:[5,10],
			fontSize:'12pt',
			margin:[0,0,0,10],
			fontWeight:'bold'
		},
		idButton:{
			height: 40,
			width: 40
		},
		hSelect:{
			fontSize:'12pt'
		},
		hErr:{
			color:'red'
		},
		number:{
			padding:[2,10],
			fontSize:18,
			width:'100%',
			boxSizing:'border-box',
		},
		numberErr:{
			borderColor:'red'
		},
		select:{
			fontSize:18,
		},
		toolbar:{
			padding:[10,0]
		},
		tblRow:{
			border:'1px solid #000',
			textAlign:'center'
		},
		incRow:{
			background:'rgba(255,255,0,0.2)',
		},
		errRow:{
			background:'rgba(255,0,0,0.2)',
		},
		validRow:{
			background:'rgba(0,255,0,0.2)',
		},
		th:{
			borderRight:'1px solid #000',
			borderBottom:'1px solid #000',
			padding:5,
			'&:last-child':{
				borderRight:[0,'solid','#000'],
			}
		},
		
		container: {
			//flexGrow: 1,
			position: 'relative',
			//height: 200,
			zIndex: 1000,
			//backgroundColor:'#fff',
		},
		suggestionsContainerOpen: {
			position: 'absolute',
			marginTop: 4,//theme.spacing.unit,
			marginBottom: theme.spacing.unit * 3,
			left: 10,
			right: 0,
			//backgroundColor:'blue'
		},
		suggestion: {
			display: 'block',
			//backgroundColor:'green'
		},
		suggestionsList: {
			margin: 0,
			padding: 0,
			listStyleType: 'none',
			//backgroundColor:'red'
		},
		sugId:{
			padding:[0,10,0,0]
		},
		sugName:{
			color:'rgba(0,0,0,0.5)',
		},
		sugHl:{
			color:'rgba(0,0,255,1)',
		},
		succesBox:{
			backgroundColor:theme.palette.primary[500]
		},
		errorBox:{
			backgroundColor:theme.palette.error[300]
		},
	}
};

//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions
//function escapeRegExpChars(string) {
//	return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
//}

//yg tengah adalah yg ketemu
function parts(s,q){
	s=s||'';
	q=q||'';
	if(!s || !q) return [s,'',''];
	let p=s.toUpperCase().search(q.toUpperCase());
	if(p<0) return [s,'',''];
	return [s.substr(0,p),s.substr(p,q.length),s.substr(p+q.length,s.length-p-q.length)]
}

const perm=s=>{
	var permArr = [],
		usedChars = [];
	
	function permute(input) {
		var i, ch;
		for (i = 0; i < input.length; i++) {
			ch = input.splice(i, 1)[0];
			usedChars.push(ch);
			if (!input.length) {
				permArr.push(usedChars.slice());
			}
			permute(input);
			input.splice(i, 0, ch);
			usedChars.pop();
		}
		return permArr
	};
	return permute(s.split('')).map(function(v){return v.join('')})
}

const uniqPermute=s=>{
	return Array.from(new Set(perm(s)));
}

class PlaceWildcardBet extends Component{
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}
	static contextTypes = {
		intl: PropTypes.object
	}
	_uPermute={}
	items={}
	ns='1 2 3 4 5'.split(' ')
	state={
		me:{},
		id:'',
		player:{},
		suggestions: [],
		searchMode:false,
		drawDate:[],
		drawDay:{},
		closeTime:'13:00',
		game:{},
		currentPrize:{},
		ready:false,
		
		clearEnable:false,
		buttonEnable:false,
		
		//drawDate:drawDate().map(v=>v.toDateString()).join(' | '),
		
		// reload saat update user
		credit:1000,
		balance:-200,
		available:800,
		cf:{},//calcFactor(),
		//\reload saat update user
		
		eItems:[], //error items
		iItems:[], //incomplete items
		big:0,
		small:0,
		day:'1',//defaultDay(),//'1',
		defBet:'4D',
		
		err:false,
		
		successOpen:false,
		errorOpen:false,
	}
	itemChange=o=>{
		this.calc();
	}
	uPermute=(numb)=>{
		if(this._uPermute[numb]) return this._uPermute[numb];
		return this._uPermute=uniqPermute(numb);
	}
	calc=()=>{
		//let {day,credit,balance,err,cf,drawDay}=this.state;
		//let big=0,
		//	small=0,
		//	eItems=[],
		//	iItems=[],
		//	dl=(drawDay[day]||[]).length;
		//
		//this.ns.forEach(k=>{
		//	let {g,n,b,s,bt,err,inc,complete}=this.items[k].state;
		//	//if(!(v.err||[]).length && !v.inc){
		//	if(complete){
		//		let ibig=parseFloat(b||'0'),
		//			ismall=parseFloat(s||'0');
		//		
		//		//if((ibig || ismall) && v.numb){
		//			let anumb=[n];
		//			if(bt==='R'){
		//				anumb=this.uPermute(n);
		//			}
		//			anumb.forEach(num=>{
		//				//let sc=((cf[g]||{}).sc||{})[num]||0; //(cf.strike_comm && cf.strike_comm[v.game] && cf.strike_comm[v.game][num]) || 0;
		//				let rb=(cf[g]||{}).rb || 0;
		//				//big += (ibig - (ibig*rb/100) - (ibig*sc/100)) * dl;
		//				//small += (ismall - (ismall*rb/100) - (ismall*sc/100)) * dl;
		//				big += (ibig - (ibig*rb/100)) * dl;
		//				small += (ismall - (ismall*rb/100)) * dl;
		//			});
		//		//}
		//	} else if(inc){
		//		iItems.push(k);
		//	} else if(Object.keys(err).length){
		//		eItems.push(k);
		//	}
		//});
		//let st={eItems,iItems,big,small};
		//if((big + small) > (credit+balance)){
		//	if(!err) st.err=true;
		//} else if(err) st.err=false;
		//this.setState(st);
	}
	dayChange=(e)=>{
		this.ns.forEach(k=>{
			this.items[k].setDay(e.target.value);
		})
		this.setState({day:e.target.value});
		setTimeout(()=>{
			this.calc();
		},1)
	}
	defBetChange=(e)=>{
		this.ns.forEach(k=>{
			let {err,inc,complete}=this.items[k].state;
			if(!(Object.keys(err)||[]).length && !inc && !complete){
				this.items[k].setBet(e.target.value);
			}
		});
		this.setState({defBet:e.target.value});
	}
	onNext=({cx,cy,x,y})=>{
		let tn=['g','n','b','s','bt'],
			rowCnt=this.ns.length,
			xTmp=x,
			yTmp=y;
		if(!!x){
			if(cx+x > tn.length-1){x=0; cx=0; y=1;}
			else if(cx+x < 0){x=0; cx=tn.length-1; y=-1}
		} else if(!!y) {
			if(cy+y > rowCnt){y=0; cy=1; x=1;}
			else if(cy+y < 1){y=0; cy=rowCnt; x=-1}
		}
		cy=((cy+y)+(rowCnt-1))%rowCnt+1;
		cx=(cx+x+tn.length)%tn.length;
		if(!this.items[cy.toString()].setFocus(tn[cx]))
			this.onNext({cx,cy,x:xTmp,y:yTmp});
	}
	//submit=()=>{
	//	this.items[4].setFocus('numb');
	//}
	searchClick=()=>{
		this.setState({searchMode:true,ready:false},()=>{
			//this.setFocus('userid');
			window.sug=this.sug;
			this.sug.input.focus();
		});
	}
	idInputChange = (event, { newValue, method }) => {
		this.setState({
			id: newValue
		});
	};
	findId=(inputValue)=>{
		Get('user',{id:inputValue||this.state.id,limit:1,can_play:true,active:true,withconfig:true},(e,r)=>{
			console.log('r==>',r);
			if(r && r.d && r.d.length){
				return this.setPlayer(r.d[0]);
			}
			this.setState({id:''});
			//console.log('r==>',r);
			//this.setState({
			//	suggestions:(r||{}).d||[]
			//});
		});
	}
	idKeyDown=(event)=>{
		const { keyCode } = event
        const input = event.target
        // if input non-empty, clear it and stop the event; otherwise let the event continue (e.g. close the modal)
        if (keyCode === 13 && input.value.length > 0) {
			console.log('enter kepencet',input.value);
			this.findId(input.value);
          //event.stopPropagation()
          //input.value = ''
          //props.onChange(event)
        }
	}
	setFocus=rf=>{
		//if(this.refs[rf].setSelectionRange) this.refs[rf].setSelectionRange(0, this.refs[rf].value.length);
		this.refs[rf].focus();
	}
	onSuggestionsFetchRequested = ({ value }) => {
		Get('user',{search:value,can_play:true,active:true},(e,r)=>{
			//console.log('r==>',r);
			this.setState({
				suggestions:(r||{}).d||[]
			});
		});
	}	
	onSuggestionsClearRequested = () => {
		this.setState({
			suggestions: []
		});
	}
	setPlayer=o=>{
		console.log('player==>',o)
		let credit=parseFloat(o.credit||'0'),
			balance=parseFloat(o.balance||'0'),
			available=credit+balance;
		this.setState({
			id:o.id,
			searchMode:false,
			player:o,
			credit,
			balance,
			available,
			cf:o.config||{},
			ready:true,
		},()=>{
			this.calc();
		});
	}
	onSuggestionSelected=(event, {suggestionValue})=>{
		let id=suggestionValue;
		Get('user',{id,limit:1,can_play:true,active:true,withconfig:true},(e,r)=>{
			//console.log('r==>',r);
			if(r && r.d && r.d.length){
				this.setPlayer(r.d[0]);
			}
		});
	}
	getSuggestionValue=(suggestion)=>{
		return suggestion.id;
	}
	renderSuggestion=(suggestion, { query, isHighlighted })=>{
		let {classes}=this.props,
			idParts=parts(suggestion.id,query),
			nameParts=parts(suggestion.name,query);
		//console.log('query==>',query);
		return <MenuItem selected={isHighlighted} component="div">
			<div className={classes.sugId}>
				{idParts.map((v,k)=>{
					if(k===1) return <span key={k} className={classes.sugHl}>{v.replace(/\s/g,'\u00A0')}</span>;
					return <span key={k}>{v.replace(/\s/g,'\u00A0')}</span>;
				})}
			</div>
			<div className={classes.sugName}>
				{nameParts.map((v,k)=>{
					if(k===1) return <span key={k} className={classes.sugHl}>{v.replace(/\s/g,'\u00A0')}</span>;
					return <span key={k}>{(v||'').replace(/\s/g,'\u00A0')}</span>;
				})}
			</div>
		</MenuItem>
	}
	renderSuggestionsContainer=({ containerProps, children })=><Paper {...containerProps} square>
		{children}
    </Paper>
	gameValidation=(g,num)=>{
		let{game}=this.state;
		this.gameValidate=this.gameValidate||{};
		this.gameValidate[g]=this.gameValidate[g]||new RegExp(game[g].validate);
		return !!(num||'').match(this.gameValidate[g]);
	}
	saveTmpBet=()=>{
		//let dt={};
		//this.ns.forEach(k=>{
		//	let {err,g,n,b,s,bt}=this.items[k].state;
		//	if(!(Object.keys(err).length)){
		//		dt[k]={g,n,b,s,bt};
		//	}
		//});
		//saveTmpBet('normal',dt);
	}
	loadTmpBet=()=>{
		//let bts=loadTmpBet('normal')||{};
		//Object.keys(bts).forEach(k=>{
		//	let v=bts[k]
		//	this.items[k].setState(v,this.items[k].checkIncomplete);
		//})
	}
	clear=()=>{
		let {defBet}=this.state;
		this.ns.forEach(k=>{
			this.items[k].setState({g:defBet,n:'',b:'',s:'',bt:'-',err:{},inc:false,complete:false});
		});
		this.setState({eItems:[],iItems:[],big:0,small:0,err:false},()=>{
			this.calc();
		})
	}
	submit=()=>{
		let {id,day}=this.state;
		let dt={place_to:id,day_id:day,form_type:'normal',detail:{}};
		let i=0;
		this.ns.forEach(k=>{
			let {g,n,b,s,bt,complete}=this.items[k].state;
			if(complete){
				dt.detail[i++]={g,n,b,s,bt};
			}
		});
		if(i > 0){
			Post('bet',dt,(e,r)=>{
				if(e){
					console.log('e==>',e);
					return;
				}
				console.log('r==>',r);
			});
		}
	}
	save=()=>{
		//return this.setState({successOpen:true,successMsg:'berhasil...'});
		let {id,day}=this.state;
		//let dt={place_to:id,day_id:day,form_type:'normal',detail:{}};
		let dt={form_type:'normal',detail:{}};
		let i=0;
		this.ns.forEach(k=>{
			let {g,n,b,s,bt,complete}=this.items[k].state;
			if(complete){
				dt.detail[i++]={g,n,b,s,bt};
			}
		});
		if(i > 0){
			Post('savebet',dt,(e,r)=>{
				if(e){
					console.log('e==>',e);
					return this.setState({errorOpen:true,errorMsg:e.toString()});
				}
				return this.setState({successOpen:true,successMsg:'Bets have been saved'});
			});
		}
	}
	componentWillMount(){
		appEvent.fire('titleChange','Place Bets');
		Game((e,game)=>{
			console.log('game==>',game);
			//this.setState({config});
			this.setState({game})
		});
		Config((e,config)=>{
			console.log('config==>',config);
			//this.setState({config});
			this.setState({
				drawDay:config.draw_day,
				closeTime:config.bet_time.close,
			},()=>{
				this.setState({day:defaultDay(this.state.drawDay,this.state.drawDate)})
			})
		});
		CurrentPrize((e,currentPrize)=>{
			console.log('currentPrize==>',currentPrize);
			//this.setState({config});
			this.setState({
				currentPrize:currentPrize,
			});
		});
		RequestDrawDate((e,drawDate)=>{
			console.log('drawDate==>',drawDate)
			this.setState({drawDate},()=>{
				this.setState({day:defaultDay(this.state.drawDay,this.state.drawDate)})
			})
		});
		Me((e,me)=>{
			this.setState({me},()=>{
				if(me && me.id && me.can_play) {
					//this.setState({id:me.id,player:me});
					this.setPlayer(me);
				} else {
					this.searchClick();
				}
			});
		});
	}
	componentDidMount(){
		
		this.loadTmpBet();
	}
	componentWillUnmount(){
		this.saveTmpBet();
	}
	formatNumb=n=>{
		return this.context.intl.formatNumber(n,{style: 'decimal',minimumFractionDigits:2,maximumFractionDigits:2});
	}
	onSuccessClose=()=>{
		this.setState({successOpen:false});
	}
	onErrorClose=()=>{
		this.setState({errorOpen:false});
	}
	render(){
		const { ready, searchMode, id, big,small,day,defBet,eItems,iItems, drawDate,drawDay, credit, balance, available, err, me,
			suggestions, currentPrize,
			successOpen, successMsg, errorOpen, errorMsg
		} = this.state;
		const formatNumber=this.formatNumb;
		//const {formatNumber} = this.context.intl;
		const { classes } = this.props;
		return <div className={classes.root}>
			<div className={classes.hRow}>
				<div>Place To</div>
				{/*<input ref={'userid'} placeholder={"UserID"} className={classes.idInput} value={id} disabled={!searchMode} onChange={this.idInputChange} />*/}
				<Autosuggest
					theme={{
						container: classes.container,
						suggestionsContainerOpen: classes.suggestionsContainerOpen,
						suggestionsList: classes.suggestionsList,
						suggestion: classes.suggestion,
					}}

					ref={e=>{this.sug=e;}}
					suggestions={suggestions}
					onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
					onSuggestionsClearRequested={this.onSuggestionsClearRequested}
					onSuggestionSelected={this.onSuggestionSelected}
					getSuggestionValue={this.getSuggestionValue}
					renderSuggestion={this.renderSuggestion}
					renderSuggestionsContainer={this.renderSuggestionsContainer}
					inputProps={{
						placeholder: "userid",
						value:id,
						disabled:!searchMode,
						className:classes.idInput,
						onKeyDown:this.idKeyDown,
						onChange: this.idInputChange
					  }} />
				{(me && me.can_play) && <IconButton aria-label="me" className={classes.idButton} onClick={()=>{
						this.setState({ready:false},()=>{
							this.setPlayer(me);
						});
					}}>
					<PersonIcon/>
				</IconButton>}
				{(me && me.type!=='player') && <IconButton aria-label="search-user" className={classes.idButton} onClick={this.searchClick}>
					<SearchIcon/>
				</IconButton>}
			</div>
			<div className={classes.hRow}>
				<div>Draw Date:</div>
				<div>
					{(drawDate||[]).map((v,k)=>
						<span key={k} className={classes.dateItem+(!v.enable?' '+classes.disable:'')}>{new Date(v.date).toDateString()}</span>)}
				</div>
			</div>
			
			<div className={classes.hRow}>
				Select Day :
				<select className={classes.hSelect} name={"day"} value={day} onChange={this.dayChange}>
					{Object.keys(drawDay).map(d=><option disabled={!enableDay(d,drawDay||{},drawDate||[])} key={d} value={d}>{d+" = <"+drawDay[d].join(', ')+"> "}</option>)}
				</select>
				&nbsp;
				Default Bet :
				<select className={classes.hSelect} name={"defBet"} value={defBet} onChange={this.defBetChange}>
					{defBetList.map(d=><option key={d} value={d}>{d}</option>)}
				</select>
			</div>
			<div className={classes.hRow}>
				Credit Limit : {!ready?'-':formatNumber(credit)} &nbsp; &nbsp; Balance : {!ready?'-':formatNumber(balance)} &nbsp; &nbsp; Available : {!ready?'-':formatNumber(available)}
			</div>
			<div className={[
					classes.hRow,
					err?classes.hErr:null
				].join(' ')}>
				Big : {formatNumber(big)} &nbsp; &nbsp; Small : {formatNumber(small)} &nbsp; &nbsp; &nbsp; &nbsp; Total : {formatNumber(big+small)}
			</div>
			<div className={classes.toolbar}>
				<Button disabled={!(!!eItems.length || !!iItems.length || big || small)} raised onClick={this.clear}>Clear</Button> &nbsp;
				<Button disabled={!(big || small)||!!eItems.length||!!iItems.length} raised onClick={this.save}>Save</Button> &nbsp;
				<Button disabled={!(big || small) || err || !ready ||!!eItems.length||!!iItems.length} raised color={"primary"} onClick={this.submit}>Submit</Button>
			</div>
			<table style={{border:'1px solid #000'}} cellSpacing={"0"} cellPadding={"1"}>
				<thead><tr style={{border:'1px solid #000', textAlign:'center', fontWeight:'bold'}}>
					<th className={classes.th}>No</th>
					<th className={classes.th}>D</th>
					<th className={classes.th}>Bet</th>
					<th className={classes.th}>Num</th>
					<th className={classes.th}>Big</th>
					<th className={classes.th}>Small</th>
				</tr></thead><tbody>
				{this.ns.map(k=><WildcardBetRow ref={e=>{this.items[k]=e}} key={k} idx={k} day={day} classes={classes} onNext={this.onNext} onChange={this.itemChange} gameValidation={this.gameValidation} currentPrize={currentPrize}/>)}
			</tbody></table>
			<SnackBar open={successOpen} onClose={this.onSuccessClose} message={successMsg} classes={{root:classes.succesBox}}/>
			<SnackBar open={errorOpen} onClose={this.onErrorClose} message={errorMsg} classes={{root:classes.errorBox}}/>
		</div>
	}
}

export default withRoot(withStyles(styles, { withTheme: true })(PlaceWildcardBet));