import React,{Component} from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import Divider from 'material-ui/Divider';
import List,{ ListItem} from 'material-ui/List';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import IconButton from 'material-ui/IconButton';
//import Button from 'material-ui/Button';
import Collapse from 'material-ui/transitions/Collapse';
//import PersonIcon from 'material-ui-icons/Person';
//import CloseIcon from 'material-ui-icons/Close';
//import ArrowForwardIcon from 'material-ui-icons/ArrowForward';

//import OpenInNewIcon from 'material-ui-icons/OpenInNew';
import ExpandLess from 'material-ui-icons/ExpandLess';
import ExpandMore from 'material-ui-icons/ExpandMore';

import withRoot from '../components/withRoot';
import {appEvent} from '../components/events';
import {/*saveTmpBet,*/Dow,Me,toDbDate} from '../components/game';
import {Get/*,Delete*/} from '../components/ajax';
import {SearchBar,TableClasses,Classes} from '../components/dialog';
import {bList,mList} from './placebet1d';

const styles = theme => ({
	...TableClasses(theme),
	...Classes(theme),
	root: {
		width: '100%',
		display:'flex',
		flexDirection:'column',
		flex:1,
		height:'100%',
	},
	sub:{
		width: '100%',
		padding:[5,20],
		boxSizing:'border-box',
		flex:1,
		overflow:'auto',
	},
	sub2:{
		width: '100%',
		paddingLeft:24,
		boxSizing:'border-box',
	},
	item:{
		display:'flex',
	},
	item2:{
		display:'flex',
		padding:[6,0,6,20]
	},
	button: {
		//margin:[-10,0],//margin: theme.spacing.unit,
		//margin:0,
		width:24,
		height:24,
	},
	button2: {
		margin: theme.spacing.unit,
	},
	collapsible:{
		margin:[0,0,0,0],
		padding:[0,0,0,0],
	},
	sItem2:{
		textAlign:'center'
	},
	sFirstItem:{
		textAlign:'left',
		padding:[5,0,20,0],
		//borderBottom:[1,'solid','#eee']
		fontSize:'14pt'
	},
	r:{
		textAlign:'right',
	}
});

function defaultDate(){
	let d=new Date();
	let dow=d.getDay();
	let dd=[3,6,0];
	if(dd.indexOf(dow)>=0) return d;
	if(dow<3) {
		d.setDate(d.getDate()+3-dow);
		return d;
	} else if(dow<6){
		d.setDate(d.getDate()+6-dow);
		return d;
	}
	return d;
}

//console.log('defaultDate()==>',defaultDate());
let Head=(p)=><thead><tr>
	<th>Account</th>
	<th>Big</th>
	<th>Small</th>
	<th>Amount</th>
</tr></thead>
let Head1D=(p)=><thead><tr>
	<th>Account</th>
	<th>Amount</th>
</tr></thead>

let Head2=(p)=><thead><tr>
	<th rowSpan={'2'}>N</th>
	<th colSpan={'3'}>Pool</th>
	<th colSpan={'3'}>Intake</th>
	<th colSpan={'3'}>Outset</th>
</tr>
<tr>
	<th>Big</th>
	<th>Small</th>
	<th>Amount</th>
	<th>Big</th>
	<th>Small</th>
	<th>Amount</th>
	<th>Big</th>
	<th>Small</th>
	<th>Amount</th>
</tr></thead>

let Head21D=(p)=><thead><tr>
	<th>N</th>
	<th>Pool</th>
	<th>Intake</th>
	<th>Outset</th>
</tr></thead>

let HRow2=(p)=><thead><tr>
	<th>&nbsp;</th>
	<th className={p.c.r}>{p.formatNumb(p.big)}</th>
	<th className={p.c.r}>{p.formatNumb(p.small)}</th>
	<th className={p.c.r}>{p.formatNumb(parseFloat(p.big)+parseFloat(p.small))}</th>
	<th className={p.c.r}>{p.formatNumb(p.bi)}</th>
	<th className={p.c.r}>{p.formatNumb(p.si)}</th>
	<th className={p.c.r}>{p.formatNumb(parseFloat(p.bi)+parseFloat(p.si))}</th>
	<th className={p.c.r}>{p.formatNumb(p.bp)}</th>
	<th className={p.c.r}>{p.formatNumb(p.sp)}</th>
	<th className={p.c.r}>{p.formatNumb(parseFloat(p.bp)+parseFloat(p.sp))}</th>
</tr></thead>

let HRow21D=(p)=><thead><tr>
	<th>&nbsp;</th>
	<th className={p.c.r}>{p.formatNumb(p.ttl)}</th>
	<th className={p.c.r}>{p.formatNumb(p.i1d)}</th>
	<th className={p.c.r}>{p.formatNumb(p.p1d)}</th>
</tr></thead>

let Row2=(p)=><tr>
	<td>{p.num}</td>
	<td className={p.c.r}>{p.formatNumb(p.big_pool)}</td>
	<td className={p.c.r}>{p.formatNumb(p.small_pool)}</td>
	<td className={p.c.r}>{p.formatNumb(parseFloat(p.big_pool)+parseFloat(p.small_pool))}</td>
	<td className={p.c.r}>{p.formatNumb(p.big_intake)}</td>
	<td className={p.c.r}>{p.formatNumb(p.small_intake)}</td>
	<td className={p.c.r}>{p.formatNumb(parseFloat(p.big_intake)+parseFloat(p.small_intake))}</td>
	<td className={p.c.r}>{p.formatNumb(p.big_pass)}</td>
	<td className={p.c.r}>{p.formatNumb(p.small_pass)}</td>
	<td className={p.c.r}>{p.formatNumb(parseFloat(p.big_pass)+parseFloat(p.small_pass))}</td>
</tr>
let Row21D=(p)=><tr>
	<td>{bList[p.num]}</td>
	<td className={p.c.r}>{p.formatNumb(p.pool_1d)}</td>
	<td className={p.c.r}>{p.formatNumb(p.intake_1d)}</td>
	<td className={p.c.r}>{p.formatNumb(p.pass_1d)}</td>
</tr>

let Head3=(p)=><thead><tr>
	<th rowSpan={'2'}>Account</th>
	<th colSpan={'3'}>Day {p.day}</th>
	<th colSpan={'3'}>Total</th>
</tr>
<tr>
	<th>Big</th>
	<th>Small</th>
	<th>Less Rebate</th>
	<th>Big</th>
	<th>Small</th>
	<th>Less Rebate</th>
</tr></thead>

let Head31D=(p)=><thead><tr>
	<th rowSpan={'2'}>Account</th>
	<th colSpan={'2'}>Day {p.day}</th>
	<th colSpan={'2'}>Total</th>
</tr>
<tr>
	<th>Amount</th>
	<th>Less Rebate</th>
	<th>Amount</th>
	<th>Less Rebate</th>
</tr></thead>

let Row3=(p)=><tr>
	<td><a href={'#'} onClick={()=>{p.setUser({id:p.account})}} >{p.account}</a></td>
	<td className={p.c.r}>{p.formatNumb(p.big)}</td>
	<td className={p.c.r}>{p.formatNumb(p.small)}</td>
	<td className={p.c.r}>{p.formatNumb(parseFloat(p.big_less_rebate)+parseFloat(p.small_less_rebate))}</td>
	<td className={p.c.r}>{p.formatNumb(p.big)}</td>
	<td className={p.c.r}>{p.formatNumb(p.small)}</td>
	<td className={p.c.r}>{p.formatNumb(parseFloat(p.big_less_rebate)+parseFloat(p.small_less_rebate))}</td>
</tr>

let Row31D=(p)=><tr>
	<td><a href={'#'} onClick={()=>{p.setUser({id:p.account})}} >{p.account}</a></td>
	<td className={p.c.r}>{p.formatNumb(p.p1d)}</td>
	<td className={p.c.r}>{p.formatNumb(parseFloat(p.big_less_rebate)+parseFloat(p.small_less_rebate)+parseFloat(p.less_rebate_1d))}</td>
	<td className={p.c.r}>{p.formatNumb(p.p1d)}</td>
	<td className={p.c.r}>{p.formatNumb(parseFloat(p.big_less_rebate)+parseFloat(p.small_less_rebate)+parseFloat(p.less_rebate_1d))}</td>
</tr>


let HRow3=(p)=><thead><tr>
	<th>Total</th>
	<th className={p.c.r}>{p.formatNumb(p.big)}</th>
	<th className={p.c.r}>{p.formatNumb(p.small)}</th>
	<th className={p.c.r}>{p.formatNumb(p.lrb)}</th>
	<th className={p.c.r}>{p.formatNumb(p.big)}</th>
	<th className={p.c.r}>{p.formatNumb(p.small)}</th>
	<th className={p.c.r}>{p.formatNumb(p.lrb)}</th>
</tr></thead>

let HRow31D=(p)=><thead><tr>
	<th>Total</th>
	<th className={p.c.r}>{p.formatNumb(p.p1d)}</th>
	<th className={p.c.r}>{p.formatNumb(p.lrb)}</th>
	<th className={p.c.r}>{p.formatNumb(p.p1d)}</th>
	<th className={p.c.r}>{p.formatNumb(p.lrb)}</th>
</tr></thead>

let Row=(p)=><tr>
	<td><a {...(p.setUser?{href:'#',onClick:()=>{p.setUser({id:p.id})}}:{})} >{p.id}</a></td>
	<td className={p.c.r}>{p.formatNumb(p.big)}</td>
	<td className={p.c.r}>{p.formatNumb(p.small)}</td>
	<td className={p.c.r}>{p.formatNumb(p.big+p.small)}</td>
</tr>

let Row1D=(p)=><tr>
	<td><a {...(p.setUser?{href:'#',onClick:()=>{p.setUser({id:p.id})}}:{})} >{p.id}</a></td>
	<td className={p.c.r}>{p.formatNumb(p.p1d)}</td>
</tr>

let HRow=(p)=><thead><tr>
	<th>Total</th>
	<th className={p.c.r}>{p.formatNumb(p.big)}</th>
	<th className={p.c.r}>{p.formatNumb(p.small)}</th>
	<th className={p.c.r}>{p.formatNumb(p.amount)}</th>
</tr></thead>

let HRow1D=(p)=><thead><tr>
	<th>Total</th>
	<th className={p.c.r}>{p.formatNumb(p.amount)}</th>
</tr></thead>

function getNDay(dt){
	let d=new Date(dt);
	return (d.getDay()+6)%7+1;
}


class SummaryBet extends Component{
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}
	static contextTypes = {
		intl: PropTypes.object
	}
	state={
		//members:[],
		ready:false,
		me:{},
		id:'',
		drawDate:null,
		data:null,
	}
	ready=()=>{
		let {id,drawDate,pageType}=this.state;
		console.log({id,drawDate,pageType});
		Get(pageType==='summarybets'?'summarybet':(pageType==='detailbets'?'summarybetdetail':'tallybet'),{user_id:id,drawdate:toDbDate(drawDate)},(e,r)=>{
			if(e) return console.error(e);
			console.log('r==>',r);
			if(r && r.d) {
				if(pageType==='summarybets'){
					let data={};
					console.log('r==>',r.d);
					Object.keys(r.d).forEach(g=>{
						data[g]={};
						data[g].own=r.d[g].filter(v=>v.id===id);
						data[g].members=r.d[g].filter(v=>v.id!==id);
					})
					return this.setState({data,ready:true});
				}
				return this.setState({data:r.d,ready:true});
			}
			this.setState({data:null,ready:true});
			//if(r && r.d) this.setState({members:r.d,ready:true});
		});
		//if(!id) return this.setState({members:[],id:'',ready:false});
		//let prm={place_to:id,limit:1000};
		//if(fromDate) prm.from_date=toDbDate(fromDate);
		//if(toDate) prm.to_date=toDbDate(toDate);
		//if(pageType==='history') prm.history=1;
		//
		//Get('ticketpage',prm,(e,r)=>{
		//	if(e) return console.error(e);
		//	if(r && r.d) this.setState({members:r.d,ready:true});
		//});
	}
	componentWillReceiveProps(np){
		this.getProps(np,()=>{
			let {id}=this.state;
			if(id) return this.ready();
		})
	}
	getProps=(p,cb)=>{
		let m=p.match;
		m=m||{};
		let pagetype;
		if(m.path==='/summarybets/:userid?/:date?') pagetype='summarybets'
		else if(m.path==='/detailbets/:userid?/:date?') pagetype='detailbets'
		else pagetype='tallybets';
		
		let {id,drawDate,pageType}=this.state;
		let {userid,date}=m;
		if(id!==userid || drawDate!==date || pageType!==pagetype){
			this.setState({
				pageType:pagetype,
				id:m.params.userid||'',
				drawDate:m.params.date||defaultDate(),
			},cb);
		}
	}
	componentWillMount(){
		this.getProps(this.props,()=>{
			let {id}=this.state;
			appEvent.fire('titleChange','Summary Bets');
			if(id) return this.ready();
		});
		Me((e,r)=>{
			this.meChange(r);
		})
	}
	meChange=me=>{
		this.setState({
			me
		},()=>{
			let {id}=this.state;
			if(!id) this.setState({
				id:me.id
			},this.ready);
		});
	}
	componentWillUnmount(){
		
	}
	route=uri=>{
		return (e)=>{
			if(e && e.stopPropagation) e.stopPropagation();
			console.log(uri)
			this.props.history.push(uri);
		}
	}
	formatNumb=n=>{
		return this.context.intl.formatNumber(n,{style: 'decimal',minimumFractionDigits:2,maximumFractionDigits:2});
	}
	setPage=(pageType)=>{
		let {drawDate,id}=this.state,
			s='';
		if(id) s='/'+id;
		if(drawDate) s+='/'+toDbDate(drawDate);
		this.route('/'+pageType+s)();
		//this.setState({id:d.id},this.ready);
	}
	setUser=(d)=>{
		let {drawDate,pageType}=this.state,
			s='';
		if(drawDate) s='/'+toDbDate(drawDate);
		this.route('/'+pageType+'/'+d.id+s)();
		//this.setState({id:d.id},this.ready);
	}
	dateChange=(value)=>{
		let {id,me,pageType}=this.state;
		id=id||(me||{}).id||'';
		this.route('/'+pageType+'/'+id+'/'+toDbDate(value))();
		//this.setState({drawDate:toDbDate(value)},this.ready);
	}
	isDrawDay=(d)=>{
		let da=moment(d).day();
		return [3,6,0].indexOf(da) >= 0;
	}
	maxDate=()=>{
		let d=new Date();
		let dow=d.getDay();
		d.setDate(d.getDate()+7-dow);
		return d;
	}
	render(){
		const { classes } = this.props,
			{me,data, ready, id,pageType,drawDate} = this.state;
			console.log('pageType==>',pageType)
		return <div className={classes.root}>
			<div className={classes.tableFooter}>
				<div className={classes.footerSub2}>
					<SearchBar label={'User Id'}
						id={id}
						me={me}
						userDetailValidate={d=>(d.active)}
						onFound={this.setUser}/>
					<div className={classes.footerSub2}>
						&nbsp; &nbsp;
						Draw Date &nbsp;
						<DatePicker className={classes.date}
							selected={moment(drawDate)}
							locale={"en-gb"}
							filterDate={this.isDrawDay}
							onChange={this.dateChange}
							maxDate={moment(this.maxDate())}
						/>
						&nbsp; &nbsp;
						{pageType !== 'summarybets' && <a className={classes.link} onClick={()=>{this.setPage('summarybets')}}>&nbsp; Summary &nbsp;</a>}
						{pageType !== 'detailbets' && <a className={classes.link} onClick={()=>{this.setPage('detailbets')}}>&nbsp; Detail &nbsp;</a>}
						{pageType !== 'tallybets' && <a className={classes.link} onClick={()=>{this.setPage('tallybets')}}>&nbsp; SummaryTally &nbsp;</a>}
					</div>
				</div>
			</div>
			<div className={classes.sub+' markdown-body'}>
				<h2>Summary Bet Account {id} for {(new Date(drawDate)).toDateString()}</h2>
				{pageType==='summarybets' && ['4D','3D','2D','1D'].map(g=>{
					let big=(((data||{})[g]||{}).members||[]).reduce((b,i)=>b+i.big,0);
					let small=(((data||{})[g]||{}).members||[]).reduce((s,i)=>s+i.small,0);
					let ttl=(((data||{})[g]||{}).members||[]).reduce((t,i)=>t+i.p1d,0);
					return <div key={g}>
						<h3>{g}</h3>
						<table>
							{!!(((data||{})[g]||{}).own||[]).length && (g!=='1D'?<Head/>:<Head1D/>)}
							{!!(((data||{})[g]||{}).own||[]).length && <tbody>
								{(((data||{})[g]||{}).own||[]).map((v,k)=>
									g!=='1D'?<Row key={k} {...v} formatNumb={this.formatNumb} c={classes}/>:<Row1D key={k} {...v} formatNumb={this.formatNumb} c={classes}/>
								)}
							</tbody>}
							{g!=='1D'?<Head/>:<Head1D/>}
							<tbody>
								{(((data||{})[g]||{}).members||[]).map((v,k)=>
									g!=='1D'?<Row key={k} {...v} formatNumb={this.formatNumb} setUser={this.setUser} c={classes} />:<Row1D key={k} {...v} formatNumb={this.formatNumb} setUser={this.setUser} c={classes} />
								)}
							</tbody>
							{g!=='1D'?<HRow {...{big,small,amount:big+small}} formatNumb={this.formatNumb} c={classes}/>:<HRow1D {...{amount:ttl}} formatNumb={this.formatNumb} c={classes}/>}
						</table>
					</div>
				})}
				{pageType==='detailbets' && ['4D','3D','2D','1D'].map(g=>{
					let big=((data||{})[g]||[]).reduce((s,i)=>s+parseFloat(i.big_pool),0),
						small=((data||{})[g]||[]).reduce((s,i)=>s+parseFloat(i.small_pool),0),
						ttl=((data||{})[g]||[]).reduce((p,i)=>p+parseFloat(i.pool_1d),0),
						bi=((data||{})[g]||[]).reduce((s,i)=>s+parseFloat(i.big_intake),0),
						si=((data||{})[g]||[]).reduce((s,i)=>s+parseFloat(i.small_intake),0),
						i1d=((data||{})[g]||[]).reduce((p,i)=>p+parseFloat(i.intake_1d),0),
						bp=((data||{})[g]||[]).reduce((s,i)=>s+parseFloat(i.big_pass),0),
						sp=((data||{})[g]||[]).reduce((s,i)=>s+parseFloat(i.small_pass),0),
						p1d=((data||{})[g]||[]).reduce((p,i)=>p+parseFloat(i.pass_1d),0);
					return <div key={g}>
						<h3>{g}</h3>
						<table>
							{g!=='1D'?<Head2/>:<Head21D/>}
							<tbody>
								{((data||{})[g]||[]).map((v,k)=>
									g!=='1D'?<Row2 key={k} {...v} formatNumb={this.formatNumb} setUser={this.setUser} c={classes} />:<Row21D key={k} {...v} formatNumb={this.formatNumb} setUser={this.setUser} c={classes} />
								)}
							</tbody>
							{g!=='1D'?<HRow2 {...{big,small,bi,si,bp,sp}} formatNumb={this.formatNumb} c={classes}/>:<HRow21D {...{ttl,i1d,p1d}} formatNumb={this.formatNumb} c={classes}/>}
						</table>
					</div>
				})}
				{pageType==='tallybets' && ['4D','3D','2D','1D'].map(g=>{
					let big=((data||{})[g]||[]).reduce((s,i)=>s+parseFloat(i.big),0),
						small=((data||{})[g]||[]).reduce((s,i)=>s+parseFloat(i.small),0),
						p1d=((data||{})[g]||[]).reduce((p,i)=>p+parseFloat(i.p1d),0),
						lrb=((data||{})[g]||[]).reduce((s,i)=>s+parseFloat(i.big_less_rebate+i.small_less_rebate+i.less_rebate_1d),0);
					return <div key={g}>
						<h3>{g}</h3>
						<table>
							{g!=='1D'?<Head3 day={getNDay(drawDate)}/>:<Head31D day={getNDay(drawDate)}/>}
							<tbody>
								{((data||{})[g]||[]).map((v,k)=>
									g!=='1D'?<Row3 key={k} {...v} formatNumb={this.formatNumb} setUser={this.setUser} c={classes} />:<Row31D key={k} {...v} formatNumb={this.formatNumb} setUser={this.setUser} c={classes} />
								)}
							</tbody>
							{g!=='1D'?<HRow3 {...{big,small,lrb}} formatNumb={this.formatNumb} c={classes}/>:<HRow31D {...{p1d,lrb}} formatNumb={this.formatNumb} c={classes}/>}
						</table>
					</div>
				})}
			</div>
		</div>
	}
}

export default withRoot(withStyles(styles, { withTheme: true })(SummaryBet));