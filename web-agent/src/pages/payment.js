import React,{Component} from 'react';
import Divider from 'material-ui/Divider';
import List,{ ListItem} from 'material-ui/List';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import IconButton from 'material-ui/IconButton';
import Button from 'material-ui/Button';
import Collapse from 'material-ui/transitions/Collapse';
//import PersonIcon from 'material-ui-icons/Person';
//import CloseIcon from 'material-ui-icons/Close';
//import ArrowForwardIcon from 'material-ui-icons/ArrowForward';

//import OpenInNewIcon from 'material-ui-icons/OpenInNew';
import ExpandLess from 'material-ui-icons/ExpandLess';
import ExpandMore from 'material-ui-icons/ExpandMore';

import withRoot from '../components/withRoot';
import {appEvent} from '../components/events';
import {/*saveTmpBet,*/Dow,Me} from '../components/game';
import {Get,Post,flatten} from '../components/ajax';
import {SearchBar,Confirm,SnackBar,TableClasses,Classes} from '../components/dialog';
import {bList,mList} from './placebet1d';

const styles = theme => ({
	...TableClasses(theme),
	...Classes(theme),
	root: {
		width: '100%',
		display:'flex',
		flexDirection:'column',
		flex:1,
		height:'100%',
	},
	sub:{
		width: '100%',
		padding:5,
		boxSizing:'border-box',
		flex:1,
		overflow:'auto',
	},
	sub2:{
		width: '100%',
		paddingLeft:24,
		boxSizing:'border-box',
	},
	item:{
		display:'flex',
	},
	item2:{
		display:'flex',
		padding:[6,0,6,20]
	},
	button: {
		//margin:[-10,0],//margin: theme.spacing.unit,
		//margin:0,
		width:24,
		height:24,
	},
	button2: {
		margin: theme.spacing.unit,
	},
	tableRow:{
		padding:[0,0],
		borderTop:[1,'solid','#eee'],
		//cursor:'pointer',
		display:'flex',
		flexDirection:'row',
		'&:hover':{
			backgroundColor:'rgba(0,0,255,0.05)'
		}
	},
	collapsible:{
		margin:[0,0,0,0],
		padding:[0,0,0,0],
	},
	sItem2:{
		textAlign:'center'
	},
	sFirstItem:{
		textAlign:'left',
		padding:[5,0,20,0],
		//borderBottom:[1,'solid','#eee']
		fontSize:'14pt'
	},
	link:{
		cursor:'pointer',
		color:'blue',
	}
});

const TicketItem =(p)=>{
		let {classes,v,k,formatNumb,onCheckChange,onPayChange,onDescChange}=p;
		return <tr>
			<td className={classes.col}>{k+1}</td>
			<td className={classes.col}>{v.user_id}</td>
			<td className={classes.col+' '+classes.col4+' '+classes.cols}>{v.name}</td>
			<td className={classes.col+' '+classes.col2}>{v.net<0?'Collect':'Payout'}</td>
			<td className={classes.col+' '+classes.col2+' '+classes.coln}>{formatNumb(Math.abs(v.net))}</td>
			<td className={classes.col+' '+classes.col2}><input value={v.desc} onChange={onDescChange(k)}/></td>
			<td className={classes.col+' '+classes.col2}><input value={v.pay} onChange={onPayChange(k)}/></td>
			<td className={classes.col+' '+classes.col2}><input type={'checkbox'} checked={v.checked||false} onChange={onCheckChange(k)}/></td>
		</tr>
	}

class Payment extends Component{
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}
	static contextTypes = {
		intl: PropTypes.object
	}
	state={
		members:[],
		ready:false,
		me:{},
		id:'',
		ca:false,
		confirmOpen:false
	}
	submit=()=>{
		let {members}=this.state;
		//let ttl='Confirm payment ?';
		this.setState({
			confirmOpen:true,
			confirmOnOK:()=>{
				let data=members.filter(v=>v.checked).map(v=>{
					return {
						user_id:v.user_id,
						description:v.desc,
						amount:parseFloat(v.pay)||0
					}
				});
				//console.log('data==>',data);
				//console.log('flatten(data)==>',flatten({data}));
				Post('payments',flatten({data}),(e,r)=>{
					if(e) {
						this.setState({
							errorOpen:true,
							errorMsg:((e||{}).e||'').toString()
						})
					}
					console.log('r==>',r);
					if(r && r.d) {
						this.setState({
							successOpen:true,
							successMsg:'payment success'
						},()=>{
							this.ready();
						})
					}
				});
				this.setState({confirmOpen:false});
			}
		});
	}
	csClose=()=>{
		this.setState({csOpen:false});
	}
	updateCheck=(idx)=>{
		return (e)=>{
			let {members}=this.state;
			members[idx].checked=e.target.checked;
			this.setState({members});
		}
	}
	caChange=(e)=>{
		let {members,ca}=this.state;
		ca=e.target.checked;
		members.forEach(v=>{
			v.checked=ca;
		});
		this.setState({ca,members});
	}
	updatePay=(idx)=>{
		return (e)=>{
			let {members}=this.state;
			members[idx].pay=e.target.value;
			this.setState({members});
		}
	}
	updateDesc=(idx)=>{
		return (e)=>{
			let {members}=this.state;
			members[idx].desc=e.target.value;
			this.setState({members});
		}
	}
	calc=()=>{
		let {members,checkeds}=this.state;
		let take=checkeds.reduce((a,v)=>(a+parseFloat(members[v].to_upline||0)), 0);
		let bring=checkeds.reduce((a,v)=>(a+parseFloat(members[v].from_upline||0)), 0);
		this.setState({take,bring});
	}
	componentWillMount(){
		appEvent.fire('titleChange','Payment');
		Me((e,r)=>{
			this.meChange(r);
		})
	}
	ready=(d)=>{
		let {ca,id}=this.state;
		Get('payments/'+id,{},(e,r)=>{
			console.log('r==>',r);
			if(e) return console.error(e);
			let members=r.d;
			members.forEach(v=>{
				v.pay=-v.net;
				v.checked=v.ca;
				v.desc=v.net<0?'Payment Received +':'Payout -';
			});
			if(r && r.d) this.setState({members:r.d,ready:true,checkeds:[]});
		});
	}
	meChange=me=>{
		this.setState({
			me,
			id:me.id,
		},this.ready);
	}
	componentWillUnmount(){
		
	}
	route=uri=>{
		return (e)=>{
			if(e && e.stopPropagation) e.stopPropagation();
			console.log(uri)
			this.props.history.push(uri);
		}
	}
	formatNumb=n=>{
		return this.context.intl.formatNumber(n,{style: 'decimal',minimumFractionDigits:2,maximumFractionDigits:2});
	}
	checkAll=()=>{
		let {members}=this.state;
		let checkeds=members.map((v,k)=>k);
		this.setState({checkeds},this.calc)
	}
	uncheckAll=()=>{
		let {members}=this.state;
		let checkeds=[];
		this.setState({checkeds},this.calc)
	}
	onSuccessClose=()=>{
		this.setState({successOpen:false});
	}
	onErrorClose=()=>{
		this.setState({errorOpen:false});
	}
	render(){
		const { classes} = this.props,
			{me,members, ready, id, searchParams,confirmOpen,confirmOnOK,
			successOpen,errorOpen,successMsg,errorMsg} = this.state;
		let totalNet=!(!!members.length && ready)?0:members.reduce((n,v)=>n+=-v.net,0),
			totalPay=!(!!members.length && ready)?0:members.reduce((n,v)=>n+=v.checked?parseFloat(v.pay):0,0);
		return <div className={classes.root}>
			<div className={classes.tableFooter}>
				<h3>{`Payments for Account under ${id}`}</h3>
				{/*<SearchBar label={'Member Id'}
					searchParams={searchParams}
					userDetailValidate={d=>d.active}
					onSearchMode={()=>{this.setState({ready:false,members:[]})}}
					onFound={this.setUser}/>*/}
			</div>
			<div className={classes.sub+' markdown-body'}>
				<table>
					<thead><tr>
						<th>#</th>
						<th>Account</th>
						<th>Name</th>
						<th colSpan={2}>Outstanding</th>
						<th>Description</th>
						<th>Amount</th>
						<th><input type={'checkbox'} checked={this.state.ca} onChange={this.caChange}/></th>
					</tr></thead>
				{(!members.length && ready || (this.props.match && this.props.match.path==='/htickets')) &&<tbody><tr><td colSpan={6}><div className={classes.empty} >there is no data found</div></td></tr></tbody>}
				{!!members.length && ready && <tbody>
					{members.map((v,k)=>
						<TicketItem key={v.user_id} k={k} classes={classes} v={v} formatNumb={this.formatNumb} onCheckChange={this.updateCheck} onPayChange={this.updatePay} onDescChange={this.updateDesc}/>)}
				</tbody>}
				{ready && !!members.length && <thead><tr>
					<th colSpan={3}>Total</th>
					<th colSpan={2}>{this.formatNumb(totalNet)}</th>
					<th colSpan={2}>{this.formatNumb(totalPay)}</th>
					<th><Button style={{margin:'5px'}} raised disabled={!totalPay} color={"primary"} onClick={this.submit}>Pay</Button></th>
				</tr></thead>}
				</table>
			</div>
			{confirmOpen && <Confirm open={confirmOpen} title={'Payment ?'} msg={'Are you sure you want to process these payments?'} onClose={()=>{
				this.setState({confirmOpen:false})
			}} onOK={confirmOnOK}/>}
			{successOpen && <SnackBar open={successOpen} onClose={this.onSuccessClose} message={successMsg} classes={{root:classes.succesBox}}/>}
			{errorOpen && <SnackBar open={errorOpen} onClose={this.onErrorClose} message={errorMsg} classes={{root:classes.errorBox}}/>}
		</div>
	}
}

export default withRoot(withStyles(styles, { withTheme: true })(Payment));