import React,{Component} from 'react';
import Divider from 'material-ui/Divider';
import List,{ ListItem} from 'material-ui/List';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import IconButton from 'material-ui/IconButton';
import Button from 'material-ui/Button';
import PersonIcon from 'material-ui-icons/Person';

import withRoot from '../components/withRoot';
import {appEvent} from '../components/events';
import {Me,UserType} from '../components/game';
import {Get} from '../components/ajax';
import {BreadCrumbs,TableClasses,Classes} from '../components/dialog';

const styles = theme => ({
	...TableClasses(theme),
	...Classes(theme),
	root: {
		width: '100%',
		display:'flex',
		flexDirection:'column',
		flex:1,
		height:'100%',
	},
	sub:{
		width: '100%',
		padding:5,
		boxSizing:'border-box',
		flex:1,
		overflow:'auto',
	},
	sub2:{
		width: '100%',
		paddingLeft:24,
		boxSizing:'border-box',
	},
	item:{
		display:'flex',
	},
	item2:{
		display:'flex',
		padding:[6,0,6,20]
	},
	button: {
		//margin:[-10,0],//margin: theme.spacing.unit,
		width:24,
		height:24,
	},
	button2: {
		margin: theme.spacing.unit,
	},
	tableRow:{
		padding:[0,0],
		borderTop:[1,'solid','#eee'],
		transition:'background .2s'
	},
	agentRow:{
		cursor:'pointer',
		'&:hover':{
			backgroundColor:theme.palette.secondary.light
		}
	},
	col:{
		flex:1,
		borderLeft:[1,'solid','#eee'],
		//borderRight:[1,'solid','rgba(0,0,0)'],
		textAlign:'center',
		padding:[5,2],
		'&:first-child':{
			borderLeft:[0,'solid','#ddd'],
		}
	},
});

class Users extends Component{
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}
	static contextTypes = {
		intl: PropTypes.object
	}
	state={
		members:[],
		me:{},
		id:this.props.match.params.id||(this.me||{}).id||'',
		//openAddAgent:this.props.match.params.action==='addAgent',
		//openAddPlayer:this.props.match.params.action==='addPlayer',
		createAgent:false,
	}
	meChange=me=>{
		this.setState({
			me,
		},()=>{
			this.ready();
		});
	}
	componentWillReceiveProps(np){
		if(np.match.params.id !== this.state.id){
			this.setState({
				id:np.match.params.id
			},()=>{
				this.ready();
			});
			//this.id=nextProps.match.params.id;
			//this.ready();
		}
	}
	ready=()=>{
		let {id,me} = this.state;
		id=id||me.id;
		this.setState({id});
		appEvent.fire('titleChange',id===me.id?'My Downline':id+' Downline');
		
		Get('user',{member_of:id,withbalance:1,withcreditallocated:1,withactivebet:1,withconfig:1},(e,r)=>{
			if(e) return;
			if(r && r.d) this.setState({members:r.d});
		});
		if(id===me.id)
			return this.setState({
				path:'',
				createAgent:(me.agent_config||{}).ca
			});
		Get('user',{id,limit:1,withconfig:true},(e,r)=>{
			if(e) return;
			//console.log('r.d[0]==>',r.d[0]);
			if(r && r.d && r.d.length)
				this.setState({
					path:r.d[0].path,
					createAgent:((r.d[0]||{}).agent_config||{}).ca
				});
		});
	}
	componentWillMount(){
		Me((e,r)=>{
			this.meChange(r);
		})
		////appEvent.on('meChange',this.meChange);
		//if(this.me) this.ready();
	}
	componentWillUnmount(){
		//appEvent.off('meChange',this.meChange);
	}
	route=uri=>{
		return (e)=>{
			if(e && e.stopPropagation) e.stopPropagation();
			console.log(uri)
			this.props.history.push(uri);
		}
	}
	formatNumb=n=>{
		return this.context.intl.formatNumber(n,{style: 'decimal',minimumFractionDigits:2,maximumFractionDigits:2});
	}
	render(){
		const { classes } = this.props,
			{members,path,createAgent,id,me} = this.state;
		return <div className={classes.root}>
			<div className={classes.tableFooter}>
				<div>
					{!!path && <BreadCrumbs data={path} route={this.route} />}
				</div>
				<div className={classes.footerButtons}>
					<div>{id===me.id?"Manage downline":"Manage "+id+" downline"}</div>
					<div className={classes.footerSub}>
						<Button raised color={'secondary'} disabled={!createAgent} className={classes.button2} onClick={()=>{
							this.route('/downline/'+id+'/addAgent')();
						}}>Add Agent</Button>
						<Button raised color={'secondary'} className={classes.button2} onClick={()=>{
							this.route('/downline/'+id+'/addPlayer')();
						}}>Add Player</Button>
					</div>
				</div>
			</div>
			<List className={classes.sub}>
				<Divider />
				<ListItem className={classes.tableTitle}>
					<div className={classes.col}>No</div>
					<div className={classes.col+' '+classes.col2}>ID</div>
					<div className={classes.col+' '+classes.col4}>Name</div>
					<div className={classes.col+' '+classes.col2}>Type</div>
					<div className={classes.col+' '+classes.col2}>Upline</div>
					<div className={classes.col+' '+classes.col3}>Credit Limit</div>
					<div className={classes.col+' '+classes.col3}>Active Bet</div>
					<div className={classes.col+' '+classes.col3}>Credit Left</div>
					<div className={classes.col+' '+classes.col}>{""}</div>
				</ListItem>
				{members.map((v,k)=><ListItem className={classes.tableRow+' '+(v.type==='agent'?classes.agentRow:'')} key={v.id} onClick={v.type==='agent'?this.route('/downline/'+v.id):null}>
					<div className={classes.col}>{k+1}</div>
					<div className={classes.col+' '+classes.col2+' '+classes.cols}>{v.id}</div>
					<div className={classes.col+' '+classes.col4+' '+classes.cols}>{v.name}</div>
					<div className={classes.col+' '+classes.col2+' '+classes.cols}>{UserType[v.type]}</div>
					<div className={classes.col+' '+classes.col2+' '+classes.cols}>{v.member_of}</div>
					<div className={classes.col+' '+classes.col3+' '+classes.coln}>{this.formatNumb(v.credit||0)}</div>
					<div className={classes.col+' '+classes.col3+' '+classes.coln}>{this.formatNumb(v.active_bet||0)}</div>
					<div className={classes.col+' '+classes.col3+' '+classes.coln}>{this.formatNumb((parseFloat(v.credit)||0)+parseFloat(v.balance||0)-parseFloat(v.credit_allocated||0)-parseFloat(v.active_bet||0))}</div>
					<div className={classes.col+' '+classes.col}><IconButton className={classes.button}
						onClick={this.route('/user/'+v.id)}
					><PersonIcon/></IconButton></div>
				</ListItem>)}
				<Divider />
			</List>
		</div>
	}
}

export default withRoot(withStyles(styles, { withTheme: true })(Users));