import React,{Component} from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import Divider from 'material-ui/Divider';
import List,{ ListItem} from 'material-ui/List';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import IconButton from 'material-ui/IconButton';
//import Button from 'material-ui/Button';
import Collapse from 'material-ui/transitions/Collapse';
//import PersonIcon from 'material-ui-icons/Person';
//import CloseIcon from 'material-ui-icons/Close';
//import ArrowForwardIcon from 'material-ui-icons/ArrowForward';

//import OpenInNewIcon from 'material-ui-icons/OpenInNew';
import ExpandLess from 'material-ui-icons/ExpandLess';
import ExpandMore from 'material-ui-icons/ExpandMore';

import withRoot from '../components/withRoot';
import {appEvent} from '../components/events';
import {/*saveTmpBet,*/Dow,Me,CompleteDrawDate,toDbDate} from '../components/game';
import {Get/*,Delete*/} from '../components/ajax';
import {SearchBar,TableClasses,Classes} from '../components/dialog';
import {bList,mList} from './placebet1d';

const styles = theme => ({
	...TableClasses(theme),
	...Classes(theme),
	root: {
		width: '100%',
		display:'flex',
		flexDirection:'column',
		flex:1,
		height:'100%',
		//padding:[20],
	},
	sub:{
		width: '100%',
		padding:[0,20],
		boxSizing:'border-box',
		flex:1,
		overflow:'auto',
	},
	sub2:{
		width: '100%',
		paddingLeft:24,
		boxSizing:'border-box',
	},
	item:{
		display:'flex',
	},
	item2:{
		display:'flex',
		padding:[6,0,6,20]
	},
	button: {
		//margin:[-10,0],//margin: theme.spacing.unit,
		//margin:0,
		width:24,
		height:24,
	},
	button2: {
		margin: theme.spacing.unit,
	},
	colA:{
		padding:[0,5]
	},
	colT:{
		flexGrow:9,
		borderLeft:[0,'solid','#eee'],
	},
	collapsible:{
		margin:[0,0,0,0],
		padding:[0,0,0,0],
	},
	sItem2:{
		textAlign:'center'
	},
	sFirstItem:{
		textAlign:'left',
		padding:[5,0,20,0],
		//borderBottom:[1,'solid','#eee']
		fontSize:'14pt'
	},
	r:{
		textAlign:'right',
	}
});

function defaultDate(){
	let d=new Date();
	let dow=d.getDay();
	let dd=[3,6,0];
	if(dd.indexOf(dow)>=0) return d;
	d.setDate(d.getDate()-dow);
	return d;
}

var ResultPerDate={};

const rankTitle={
	'1':'1st',
	'2':'2nd',
	'3':'3rd',
}

function createResultData(rslt,strike){
	let r={};
	['4','3','2'].forEach(g=>{
		r[g]={};
		['1','2','3'].forEach(p=>{
			if(!r[g][rslt[p].substr(0,parseInt(g,10))]){
				r[g][rslt[p].substr(0,parseInt(g,10))]={};
			}
		});
		rslt.s.forEach(n=>{
			if(!r[g][n.substr(0,parseInt(g,10))]){
				r[g][n.substr(0,parseInt(g,10))]={};
			}
		})
		rslt.c.forEach(n=>{
			if(!r[g][n.substr(0,parseInt(g,10))]){
				r[g][n.substr(0,parseInt(g,10))]={};
			}
		});
		(strike[g+'D']||[]).forEach(v=>{
			r[g][v.num]={
				big:v.big_strike,
				small:v.small_strike,
			}
		})
	});
	return r;
}

class StrikeReport extends Component{
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}
	static contextTypes = {
		intl: PropTypes.object
	}
	state={
		result:false,
		strikeResult:false,
		//members:{},
		resultData:{},
		ready:false,
		me:{},
		dd:[],
		id:'',
		drawDate:defaultDate()
	}
	resultPerDate=(drawdate,cb)=>{
		if(ResultPerDate[drawdate]) return cb(ResultPerDate[drawdate]);
		Get('resultbydate/'+drawdate,{},(e,r)=>{
			console.log('e==>',e);
			console.log('r==>',r);
			if(e) {
				console.error(e);
				ResultPerDate[drawdate]=false;
				return cb(false);
			}
			ResultPerDate[drawdate]=r.d;
			cb(ResultPerDate[drawdate]);
		});
	}
	ready=()=>{
		let {drawDate,id}=this.state;
		if(!(drawDate && id)) return;
		let drawdate=toDbDate(drawDate);
		console.log('id==>',id);
		this.resultPerDate(drawdate,(result)=>{
			this.setState({result},()=>{
				if(!result) return;
				Get('strikereport',{user_id:id,draw_date:drawdate},(e,r)=>{
					console.log('e==>',e);
					console.log('r==>',r);
					if(e) return console.error(e);
					let strike=r.d||[];
					this.setState({
						resultData:createResultData(result,strike),
						ready:true
					},()=>{
						console.log('result data==>',this.state.retultData);
					})
					
					//if(r && r.d) this.setState({members:r.d,ready:true});
				});
			})
		});
		//Get('strikereport',{user_id:id,draw_date:drawDate},(e,r)=>{
		//	console.log('e==>',e);
		//	console.log('r==>',r);
		//	if(e) return console.error(e);
		//	if(r && r.d) this.setState({members:r.d,ready:true});
		//});
		
		//CompleteDrawDate(dds=>{
		//	let dd=Object.values(dds).map(v=>{
		//		let d=new Date(v.draw);
		//		d.setDate(d.getDate()-7);
		//		return d;
		//	});
		//	this.setState({dd,ready:true});
		//});
		//let {me}=this.state;
		
		//Get('summarybet/'+me.id,{},(e,r)=>{
		//	if(e) return console.error(e);
		//	if(r && r.d) this.setState({members:r.d,ready:true});
		//});
	}
	componentWillMount(){
		appEvent.fire('titleChange','Strike Report');
		CompleteDrawDate(dds=>{
			let dd=Object.values(dds).map(v=>{
				let d=new Date(v.draw);
				d.setDate(d.getDate()-7);
				return d;
			});
			this.setState({dd},this.ready);
		});
		Me((e,r)=>{
			this.meChange(r);
		})
	}
	meChange=me=>{
		this.setState({
			me,
			id:me.id,
		},this.ready);
	}
	componentWillUnmount(){
		
	}
	route=uri=>{
		return (e)=>{
			if(e && e.stopPropagation) e.stopPropagation();
			console.log(uri)
			this.props.history.push(uri);
		}
	}
	formatNumb=n=>{
		return this.context.intl.formatNumber(n,{style: 'decimal',minimumFractionDigits:2,maximumFractionDigits:2});
	}
	dateChange=(value)=>{
		let {id,me}=this.state;
		id=id||(me||{}).id||'';
		//this.route('/'+pageType+'/'+id+'/'+toDbDate(value))();
		this.setState({drawDate:toDbDate(value)},this.ready);
	}
	isDrawDay=(d)=>{
		let da=moment(d).day();
		return [3,6,0].indexOf(da) >= 0;
	}
	maxDate=()=>{
		let d=new Date();
		let dow=d.getDay();
		d.setDate(d.getDate()+7-dow);
		return d;
	}
	setUser=(d)=>{
		this.setState({id:d.id},this.ready)
	}
	render(){
		const { classes } = this.props,
			{resultData, ready,me,dd,id,drawDate,result} = this.state,
			formatNumb=this.formatNumb;

		return <div className={classes.root}>
			<div className={classes.tableFooter}>
				<div className={classes.footerSub2}>
					<SearchBar label={'User Id'}
						id={id}
						me={me}
						userDetailValidate={d=>(d.active)}
						onFound={this.setUser}/>
					<div className={classes.footerSub2}>
						&nbsp; &nbsp;
						Draw Date &nbsp;
						<DatePicker className={classes.date}
							selected={moment(drawDate)}
							locale={"en-gb"}
							filterDate={this.isDrawDay}
							onChange={this.dateChange}
							maxDate={moment(this.maxDate())}
						/>
					</div>
				</div>
			</div>
			<div className={classes.sub+' markdown-body'}>
				{ready && <h2>Strike Report {me.id} for {(new Date(drawDate)).toDateString()}</h2>}
				{result && ['4','3','2'].map(g=><div key={g}>
						<h4>{g+'D'}</h4>
						<table><thead><tr>
							<th>P</th>
							<th>N</th>
							<th>Big</th>
							<th>Small</th>
							<th>Strike</th>
						</tr></thead><tbody>
							{['1','2','3'].map(p=><tr key={p}>
								<td className={classes.sItem2}>{rankTitle[p]}</td>
								<td>{result[p].substr(0,parseInt(g,10))}</td>
								<td className={classes.r}>{formatNumb(((resultData[g]||{})[result[p].substr(0,parseInt(g,10))]||{}).big||0)}</td>
								<td className={classes.r}>{formatNumb(((resultData[g]||{})[result[p].substr(0,parseInt(g,10))]||{}).small||0)}</td>
								<td className={classes.r}>{formatNumb(parseFloat(((resultData[g]||{})[result[p].substr(0,parseInt(g,10))]||{}).big||0)+ parseFloat(((resultData[g]||{})[result[p].substr(0,parseInt(g,10))]||{}).small||0))}</td>
							</tr>)}
							{g==='4' && <tr><td rowSpan={11} className={classes.sItem2}>Starter</td></tr>}
							{g==='4' && [0,1,2,3,4,5,6,7,8,9].map(i=><tr key={i}>
								<td>{result.s[parseInt(i)]}</td>
								<td className={classes.r}>{formatNumb(((resultData[g]||{})[result.s[parseInt(i)]]||{}).big||0)}</td>
								<td className={classes.r}>{formatNumb(((resultData[g]||{})[result.s[parseInt(i)]]||{}).small||0)}</td>
								<td className={classes.r}>{formatNumb(parseFloat(((resultData[g]||{})[result.s[parseInt(i)]]||{}).big||0)+ parseFloat(((resultData[g]||{})[result.s[parseInt(i)]]||{}).small||0))}</td>
							</tr>)}
							{g==='4' && <tr><td rowSpan={11} className={classes.sItem2}>Consolations</td></tr>}
							{g==='4' && [0,1,2,3,4,5,6,7,8,9].map(i=><tr key={i}>
								<td>{result.c[parseInt(i)]}</td>
								<td className={classes.r}>{formatNumb(((resultData[g]||{})[result.c[parseInt(i)]]||{}).big||0)}</td>
								<td className={classes.r}>{formatNumb(((resultData[g]||{})[result.c[parseInt(i)]]||{}).small||0)}</td>
								<td className={classes.r}>{formatNumb(parseFloat(((resultData[g]||{})[result.c[parseInt(i)]]||{}).big||0)+ parseFloat(((resultData[g]||{})[result.c[parseInt(i)]]||{}).small||0))}</td>
							</tr>)}
						</tbody></table>
					</div>)}
					
					{false && result && <div>
						<h4>1D</h4>
						<table><thead><tr>
							<th>P</th>
							<th>N</th>
							<th>Match</th>
							<th>Amount</th>
							<th>Strike</th>
						</tr></thead><tbody>
							{['1st','2nd','3rd'].map(p=><tr key={p}>
								<td>{p}</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>)}
						</tbody></table>
					</div>}
					
				</div>
		</div>
	}
}

export default withRoot(withStyles(styles, { withTheme: true })(StrikeReport));