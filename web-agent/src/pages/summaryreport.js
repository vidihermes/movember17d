import React,{Component} from 'react';
import Divider from 'material-ui/Divider';
import List,{ ListItem} from 'material-ui/List';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import IconButton from 'material-ui/IconButton';
//import Button from 'material-ui/Button';
import Collapse from 'material-ui/transitions/Collapse';
//import PersonIcon from 'material-ui-icons/Person';
//import CloseIcon from 'material-ui-icons/Close';
//import ArrowForwardIcon from 'material-ui-icons/ArrowForward';

//import OpenInNewIcon from 'material-ui-icons/OpenInNew';
import ExpandLess from 'material-ui-icons/ExpandLess';
import ExpandMore from 'material-ui-icons/ExpandMore';

import withRoot from '../components/withRoot';
import {appEvent} from '../components/events';
import {/*saveTmpBet,*/Dow,Me} from '../components/game';
import {Get/*,Delete*/} from '../components/ajax';
//import {SnackBar,Confirm} from '../components/dialog';
import {bList,mList} from './placebet1d';

const styles = theme => ({
	root: {
		width: '100%',
		display:'flex',
		flexDirection:'column',
		flex:1,
		height:'100%',
		padding:[20],
	},
	sub:{
		width: '100%',
		padding:5,
		boxSizing:'border-box',
		flex:1,
		overflow:'auto',
	},
	sub2:{
		width: '100%',
		paddingLeft:24,
		boxSizing:'border-box',
	},
	item:{
		display:'flex',
	},
	item2:{
		display:'flex',
		padding:[6,0,6,20]
	},
	button: {
		//margin:[-10,0],//margin: theme.spacing.unit,
		//margin:0,
		width:24,
		height:24,
	},
	button2: {
		margin: theme.spacing.unit,
	},
	tableTitle:{
		padding:[0,0],
		borderBottom:[2,'solid','#000'],
		fontWeight:'bold',
	},
	tableRow:{
		padding:[0,0],
		borderTop:[1,'solid','#eee'],
		cursor:'pointer',
		display:'flex',
		flexDirection:'row',
		'&:hover':{
			backgroundColor:'rgba(0,0,255,0.1)'
		}
	},
	tableFooter:{
		padding:[0,10],
		borderTop:[1,'solid','#eee'],
		justifyContent:'space-between',
		display:'flex',
		alignItems:'center',
		[theme.breakpoints.down('sm')]: {
			display:'block'
		},
	},
	footerButtons:{
		display:'flex',
		alignItems:'center',
		justifyContent:'center',
		flexWrap:'wrap'
	},
	footerSub:{
		[theme.breakpoints.down('sm')]: {
			display:'flex',
			justifyContent:'space-around'
		},
	},
	col:{
		flex:1,
		borderLeft:[1,'solid','#eee'],
		//borderRight:[1,'solid','rgba(0,0,0)'],
		textAlign:'center',
		padding:[10,5],
		//height:36,
		'&:first-child':{
			borderLeft:[0,'solid','#ddd'],
		}
	},
	col2:{
		flexGrow:2,
	},
	col3:{
		flexGrow:3,
	},
	col4:{
		flexGrow:4,
	},
	cols:{
		textAlign:'left',
	},
	coln:{
		textAlign:'right',
	},
	colA:{
		padding:[0,5]
	},
	colT:{
		flexGrow:9,
		borderLeft:[0,'solid','#eee'],
	},
	collapsible:{
		margin:[0,0,0,0],
		padding:[0,0,0,0],
	},
	sItem2:{
		textAlign:'center'
	},
	sFirstItem:{
		textAlign:'left',
		padding:[5,0,20,0],
		//borderBottom:[1,'solid','#eee']
		fontSize:'14pt'
	},
	succesBox:{
		backgroundColor:theme.palette.primary[500]
	},
	errorBox:{
		backgroundColor:theme.palette.error[300]
	},
	empty:{
		color:'rgba(0,0,0,0.5)',
		backgroundColor:'rgba(255,255,255,0.5)',
		padding:10,
		textAlign:'center'
	},
});

class SummaryBets extends Component{
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}
	static contextTypes = {
		intl: PropTypes.object
	}
	state={
		members:{},
		ready:false,
		me:{},
	}
	ready=()=>{
		//let {me}=this.state;
		
		//Get('summarybet/'+me.id,{},(e,r)=>{
		//	if(e) return console.error(e);
		//	if(r && r.d) this.setState({members:r.d,ready:true});
		//});
	}
	componentWillMount(){
		appEvent.fire('titleChange','Summary Report');
		Me((e,r)=>{
			this.meChange(r);
		})
	}
	meChange=me=>{
		this.setState({
			me,
		},()=>{
			this.ready();
		});
	}
	componentWillUnmount(){
		
	}
	route=uri=>{
		return (e)=>{
			if(e && e.stopPropagation) e.stopPropagation();
			console.log(uri)
			this.props.history.push(uri);
		}
	}
	formatNumb=n=>{
		return this.context.intl.formatNumber(n,{style: 'decimal',minimumFractionDigits:2,maximumFractionDigits:2});
	}
	render(){
		const { classes } = this.props,
			{members, ready,me} = this.state;
		return <div className={classes.root}>
			<div className={'markdown-body'}>
				<h3>Summary Report for {me.id} at {(new Date()).toDateString()}</h3>
				<table><thead><tr>
					<th>Transaction Date</th>
					<th>Description</th>
					<th>Amount</th>
					<th>Rebate</th>
					<th>Less Rebate</th>
					<th>Strike</th>
					<th>Strike Comm</th>
					<th>Net</th>
				</tr></thead><tbody>
				<tr><td className={classes.empty} colSpan={8}>there is no data found</td></tr>
				<tr><th colSpan={7}>Net Balance</th><th className={classes.coln}>-</th></tr>
				</tbody></table>
			</div>
			{!Object.keys(members).length && ready  && <div className={classes.empty} >there is no data found</div>}
		</div>
	}
}

export default withRoot(withStyles(styles, { withTheme: true })(SummaryBets));