import React,{Component} from 'react';
import Divider from 'material-ui/Divider';
import List,{ ListItem} from 'material-ui/List';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import IconButton from 'material-ui/IconButton';
import Button from 'material-ui/Button';
import Collapse from 'material-ui/transitions/Collapse';
//import PersonIcon from 'material-ui-icons/Person';
//import CloseIcon from 'material-ui-icons/Close';
//import ArrowForwardIcon from 'material-ui-icons/ArrowForward';

//import OpenInNewIcon from 'material-ui-icons/OpenInNew';
import ExpandLess from 'material-ui-icons/ExpandLess';
import ExpandMore from 'material-ui-icons/ExpandMore';

import withRoot from '../components/withRoot';
import {appEvent} from '../components/events';
import {/*saveTmpBet,*/Dow,Me} from '../components/game';
import {Get,Post,flatten} from '../components/ajax';
import {SearchBar,ChangeStatus,SnackBar,TableClasses,Classes} from '../components/dialog';
import {bList,mList} from './placebet1d';

const styles = theme => ({
	...TableClasses(theme),
	...Classes(theme),
	root: {
		width: '100%',
		display:'flex',
		flexDirection:'column',
		flex:1,
		height:'100%',
	},
	sub:{
		width: '100%',
		padding:5,
		boxSizing:'border-box',
		flex:1,
		overflow:'auto',
	},
	sub2:{
		width: '100%',
		paddingLeft:24,
		boxSizing:'border-box',
	},
	item:{
		display:'flex',
	},
	item2:{
		display:'flex',
		padding:[6,0,6,20]
	},
	button: {
		//margin:[-10,0],//margin: theme.spacing.unit,
		//margin:0,
		width:24,
		height:24,
	},
	button2: {
		margin: theme.spacing.unit,
	},
	tableRow:{
		padding:[0,0],
		borderTop:[1,'solid','#eee'],
		//cursor:'pointer',
		display:'flex',
		flexDirection:'row',
		'&:hover':{
			backgroundColor:'rgba(0,0,255,0.05)'
		}
	},
	collapsible:{
		margin:[0,0,0,0],
		padding:[0,0,0,0],
	},
	sItem2:{
		textAlign:'center'
	},
	sFirstItem:{
		textAlign:'left',
		padding:[5,0,20,0],
		//borderBottom:[1,'solid','#eee']
		fontSize:'14pt'
	},
	link:{
		cursor:'pointer',
		color:'blue',
	}
});

const TicketItem =(p)=>{
		let {classes,v,k,formatNumb,checked,onCheckChange}=p;
		return <ListItem className={classes.tableRow} >
				<div className={classes.col}>{v.id}</div>
				<div className={classes.col+' '+classes.col4+' '+classes.cols}>{(new Date(v.date)).toLocaleString()}</div>
				<div className={classes.col+' '+classes.col2+' '+classes.cols}>{v.type}</div>
				<div className={classes.col+' '+classes.col2}>{v.ticket_id}</div>
				<div className={classes.col+' '+classes.col2+' '+classes.coln}>{formatNumb(v.to_upline)}</div>
				<div className={classes.col+' '+classes.col2+' '+classes.coln}>{formatNumb(v.from_upline)}</div>
				<div className={classes.col+' '+classes.col2}><input type={'checkbox'} checked={checked} onChange={onCheckChange}/></div>
		</ListItem>
	}

class Payment extends Component{
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}
	static contextTypes = {
		intl: PropTypes.object
	}
	state={
		members:[],
		ready:false,
		me:{},
		id:'',
		checkeds:[],
		take:0,
		bring:0
	}
	submit=()=>{
		let {take,bring,id,checkeds,members}=this.state;
		let ttl='Confirm payment '+((take||0)<(bring||0)?'to':'from')+' '+id+' : '+this.formatNumb(Math.abs((take||0)-(bring||0)));
		this.setState({
			csOpen:true,
			csTitle:ttl,
			csOK:({reason})=>{
				let amount=(take||0)-(bring||0),
					balances=checkeds.map(i=>members[i].id);
					console.log({
						amount,
						balances,
						reason,
						id
					});
				Post('paybalance/'+id,flatten({balances,description:reason}),(e,r)=>{
					if(e) {
						this.setState({
							errorOpen:true,
							errorMsg:((e||{}).e||'').toString()
						})
					}
					if(r && r.d) {
						this.setState({
							successOpen:true,
							successMsg:'payment success'
						},()=>{
							this.setUser({id});
						})
					}
				});
				this.csClose();
			}
		})
	}
	csClose=()=>{
		this.setState({csOpen:false});
	}
	updateCheck=(idx)=>{
		return (e)=>{
			let checked=e.target.checked;
			let {checkeds}=this.state;
			let k=checkeds.indexOf(idx);
			if(checked && k<0){
				checkeds.push(idx);
				this.setState({checkeds},this.calc)
			}
			if(!checked && k > -1){
				checkeds.splice(k,1);
				this.setState({checkeds},this.calc)
			}
		}
	}
	calc=()=>{
		let {members,checkeds}=this.state;
		let take=checkeds.reduce((a,v)=>(a+parseFloat(members[v].to_upline||0)), 0);
		let bring=checkeds.reduce((a,v)=>(a+parseFloat(members[v].from_upline||0)), 0);
		this.setState({take,bring});
	}
	componentWillMount(){
		appEvent.fire('titleChange','Payment');
		Me((e,r)=>{
			this.meChange(r);
		})
	}
	meChange=me=>{
		this.setState({
			me,
			searchParams:{
				member_of:me.id,
				active:true,
			}
		});
	}
	componentWillUnmount(){
		
	}
	route=uri=>{
		return (e)=>{
			if(e && e.stopPropagation) e.stopPropagation();
			console.log(uri)
			this.props.history.push(uri);
		}
	}
	formatNumb=n=>{
		return this.context.intl.formatNumber(n,{style: 'decimal',minimumFractionDigits:2,maximumFractionDigits:2});
	}
	setUser=(d)=>{
		Get('balance',{limit:1000,paid:false,user_id:d.id},(e,r)=>{
			if(e) return console.error(e);
			if(r && r.d) this.setState({members:r.d,id:d.id,ready:true,checkeds:[]});
		});
	}
	checkAll=()=>{
		let {members}=this.state;
		let checkeds=members.map((v,k)=>k);
		this.setState({checkeds},this.calc)
	}
	uncheckAll=()=>{
		let {members}=this.state;
		let checkeds=[];
		this.setState({checkeds},this.calc)
	}
	onSuccessClose=()=>{
		this.setState({successOpen:false});
	}
	onErrorClose=()=>{
		this.setState({errorOpen:false});
	}
	render(){
		const { classes} = this.props,
			{me,members, ready, id, searchParams,take,bring,checkeds,csOpen,csTitle,csOK,
			successOpen,errorOpen,successMsg,errorMsg} = this.state;
		return <div className={classes.root}>
			<div className={classes.tableFooter}>
				<SearchBar label={'Member Id'}
					searchParams={searchParams}
					userDetailValidate={d=>d.active}
					onSearchMode={()=>{this.setState({ready:false,members:[]})}}
					onFound={this.setUser}/>
			</div>
			<List className={classes.sub}>
				<Divider />
				<ListItem className={classes.tableTitle}>
					<div className={classes.col}>ID</div>
					<div className={classes.col+' '+classes.col4}>Date</div>
					<div className={classes.col+' '+classes.col2}>Type</div>
					<div className={classes.col+' '+classes.col2}>TicketId</div>
					<div className={classes.col+' '+classes.col2}>Pay</div>
					<div className={classes.col+' '+classes.col2}>To be Paid</div>
					<div className={classes.col+' '+classes.col2}><a onClick={this.checkAll} className={classes.link}>Check</a><br/><a onClick={this.uncheckAll} className={classes.link}>Uncheck</a></div>
				</ListItem>
				{(!members.length && ready || (this.props.match && this.props.match.path==='/htickets')) && <div className={classes.empty} >there is no data found</div>}
				{members.map((v,k)=>
					<TicketItem key={v.id} k={k} classes={classes} v={v} formatNumb={this.formatNumb} checked={checkeds.indexOf(k)>-1 } onCheckChange={this.updateCheck(k)}/>)}
				<Divider />
				{ready && !!members.length && <div style={{textAlign:'right'}}>
					From {id}: <b>{this.formatNumb(take||0)}</b>, &nbsp; To {id}: <b>{this.formatNumb(bring||0)}</b>, Your Amount: <b>{this.formatNumb((take||0)-(bring||0))}</b>
					<Button style={{margin:'5px'}} raised disabled={!checkeds.length} color={"primary"} onClick={this.submit}>Pay</Button>
				</div>}
			</List>
			{csOpen && <ChangeStatus open={csOpen} title={csTitle} label={'Note :'} onClose={this.csClose} onOK={csOK}/>}
			{successOpen && <SnackBar open={successOpen} onClose={this.onSuccessClose} message={successMsg} classes={{root:classes.succesBox}}/>}
			{errorOpen && <SnackBar open={errorOpen} onClose={this.onErrorClose} message={errorMsg} classes={{root:classes.errorBox}}/>}
		</div>
	}
}

export default withRoot(withStyles(styles, { withTheme: true })(Payment));