const EventEmitter = require('events');

class AppEvent extends EventEmitter {
	off=this.removeListener;
	fire=this.emit;
}

export const appEvent = new AppEvent();
