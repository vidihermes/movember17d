import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import List, { ListItem, ListItemText } from 'material-ui/List';
import { MenuItem } from 'material-ui/Menu';
import Collapse from 'material-ui/transitions/Collapse';
import ExpandLess from 'material-ui-icons/ExpandLess';
import ExpandMore from 'material-ui-icons/ExpandMore';
import Divider from 'material-ui/Divider';
import withRoot from './withRoot';
import {appEvent} from './events';
import {Me} from './game';


const $=window.$;

const styles = theme => ({
	root: {
		width: '100%',
		maxWidth: 360,
		//background: theme.palette.background.paper,
	},
	nested: {
		paddingLeft: theme.spacing.unit * 4,
	},
});

let sty={
	font:{
		fontFamily: `"Roboto", "Helvetica", "Arial", sans-serif`,
	}
};
sty={
	...sty,
	label:{...sty.font,
		color:'rgba(0,0,0,0.54)',
		padding:'5px 16px',
		fontSize:'0.875rem'
	},
	value:{...sty.font,
		padding:'2px 16px 5px 32px',
		fontSize:'1rem'
	}
}

class LeftMenu extends React.Component {
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}
	static contextTypes = {
		intl: PropTypes.object
	}
	state = {
		open: true,
		credit:0,
		balance:0,
		me:null
	}
	sm=false
	handleClick=k=>{
		return ()=>{
			this.setState({[k]:!this.state[k]});
		}
	}
	route=uri=>{
		return ()=>{
			this.props.history.push(uri);
			if(this.sm) appEvent.fire('drawerToggle');
		}
	}
	meChange=me=>{
		let {credit,balance,credit_allocated,active_bet}=me||{};
		this.setState({
			me,
			credit:parseFloat(credit||'0'),
			balance:parseFloat(balance||'0'),
			credit_allocated:parseFloat(credit_allocated||'0'),
			active_bet:parseFloat(active_bet||'0'),
		})
	}
	getSm=()=>{
		this.sm=$(window).width() < 960;
	}
	componentWillMount(){
		appEvent.on('meChange',this.meChange);
		this.getSm();
		$(window).on('resize',this.getSm);
		let {me}=this.state;
		if(me) this.meChange(me)
		else {
			Me((e,me)=>{
				this.meChange(me);
			});
		}
	}
	componentWillUnmount(){
		appEvent.off('meChange',this.meChange)
		$(window).off('resize',this.getSm);
	}
	formatNumb=n=>{
		return this.context.intl.formatNumber(n,{style: 'decimal',minimumFractionDigits:2,maximumFractionDigits:2});
	}
	render() {
		const { classes, location} = this.props,
			loc=location.pathname||'',
			{me,credit,balance,credit_allocated,active_bet} = this.state;
		
		return (
			<div>
				<List>
					<li style={sty.label}>Credit Limit :</li>
					<li style={sty.value}>{this.formatNumb(credit||0)}</li>
	
					<li style={sty.label}>Account Balance :</li>
					<li style={sty.value}>{this.formatNumb(balance||0)}</li>
	
					<li style={sty.label}>Credit Left :</li>
					<li style={sty.value}>{this.formatNumb((parseFloat(credit)||0)+(parseFloat(balance)||0)-(parseFloat(credit_allocated)||0)-(parseFloat(active_bet)||0))}</li>
				</List>
				<Divider />
				<List className={classes.root}>
					<ListItem button onClick={this.handleClick('adminMenu')}>
						<ListItemText primary="Admin" />
						{this.state.adminMenu ? <ExpandLess /> : <ExpandMore />}
					</ListItem>
					<Collapse component="li" in={this.state.adminMenu} timeout="auto" unmountOnExit>
						<List disablePadding>
							<MenuItem selected={!!loc.match(/^\/me/)} button className={classes.nested} onClick={this.route('/me')}>
								<ListItemText primary="Profile" />
							</MenuItem>
							{(me||{}).type==='agent' && <MenuItem button className={classes.nested} selected={!!loc.match(/^\/downline/)} onClick={this.route('/downline')}>
								<ListItemText primary="Downline" />
							</MenuItem>}
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/payment/)} onClick={this.route('/payment')}>
								<ListItemText primary="Payment" />
							</MenuItem>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/hpayment/)} onClick={this.route('/hpayment')}>
								<ListItemText primary="Payment History" />
							</MenuItem>
						</List>
					</Collapse>
	
					<ListItem button onClick={this.handleClick('betsMenu')}>
						<ListItemText primary="Bets" />
						{this.state.betsMenu ? <ExpandLess /> : <ExpandMore />}
					</ListItem>
					<Collapse component="li" in={this.state.betsMenu} timeout="auto" unmountOnExit>
						<List disablePadding>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/placebet$/)} onClick={this.route('/placebet')}>
								<ListItemText primary="Place Bet" />
							</MenuItem>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/placebet1d$/)} onClick={this.route('/placebet1d')}>
								<ListItemText primary="Place Bet 1D" />
							</MenuItem>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/placemassbet$/)} onClick={this.route('/placemassbet')}>
								<ListItemText primary="Mass Bet" />
							</MenuItem>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/placewildcardbet/)} onClick={this.route('/placewildcardbet')}>
								<ListItemText primary="Wildcard Bet" />
							</MenuItem>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/savedbets/)} onClick={this.route('/savedbets')}>
								<ListItemText primary="Saved Bets" />
							</MenuItem>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/placefixbet/) || !!loc.match(/^\/viewfixbet/)} onClick={this.route('/placefixbet')}>
								<ListItemText primary="Fixed Bets" />
							</MenuItem>
							{(me||{}).type==='agent' && <MenuItem button className={classes.nested} selected={!!loc.match(/^\/summarybets/) || !!loc.match(/^\/detailbets/) || !!loc.match(/^\/tallybets/)} onClick={this.route('/summarybets')}>
								<ListItemText primary="Summary Bets" />
							</MenuItem>}
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/tickets/)} onClick={this.route('/tickets')}>
								<ListItemText primary="Tickets" />
							</MenuItem>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/htickets/)} onClick={this.route('/htickets')}>
								<ListItemText primary="History Tickets" />
							</MenuItem>
						</List>
					</Collapse>
					
					<ListItem button onClick={this.handleClick('reportsMenu')}>
						<ListItemText primary="Reports" />
						{this.state.reportsMenu ? <ExpandLess /> : <ExpandMore />}
					</ListItem>
					<Collapse component="li" in={this.state.reportsMenu} timeout="auto" unmountOnExit>
						<List disablePadding>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/fullreport/)} onClick={this.route('/fullreport')}>
								<ListItemText primary="Full Reports" />
							</MenuItem>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/strikereport/)} onClick={this.route('/strikereport')}>
								<ListItemText primary="Strike Reports" />
							</MenuItem>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/summaryreport/)} onClick={this.route('/summaryreport')}>
								<ListItemText primary="Summary Reports" />
							</MenuItem>
						</List>
					</Collapse>
					<ListItem button onClick={this.handleClick('optionsMenu')}>
						<ListItemText primary="Options" />
						{this.state.optionsMenu ? <ExpandLess /> : <ExpandMore />}
					</ListItem>
					<Collapse component="li" in={this.state.optionsMenu} timeout="auto" unmountOnExit>
						<List disablePadding>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/logs/)} onClick={this.route('/logs')}>
								<ListItemText primary="Logs" />
							</MenuItem>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/notif/)} onClick={this.route('/notif')}>
								<ListItemText primary="Notification" />
							</MenuItem>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/announcement/) || !!loc.match(/^\/$/)} onClick={this.route('/announcement')}>
								<ListItemText primary="Announcement" />
							</MenuItem>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/prize/)} onClick={this.route('/prize')}>
								<ListItemText primary="Regulations" />
							</MenuItem>
							<MenuItem button className={classes.nested} selected={!!loc.match(/^\/result/)} onClick={this.route('/result')}>
								<ListItemText primary="Results" />
							</MenuItem>
						</List>
					</Collapse>
					<Divider />
					<ListItem button selected={!!loc.match(/^\/help/)} onClick={this.route('/help')}>
						<ListItemText primary="Help" />
					</ListItem>
				</List>
				<div style={{height:'100px'}} />
			</div>
		);
	}
}

//export default withStyles(styles)(LeftMenu);
export default withRoot(withStyles(styles, { withTheme: true })(LeftMenu));