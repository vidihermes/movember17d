import React,{Component} from 'react';
//import Intl from 'intl';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Autosuggest from 'react-autosuggest';
import Snackbar from 'material-ui/Snackbar';
import IconButton from 'material-ui/IconButton';
import Modal from 'material-ui/Modal';
import Button from 'material-ui/Button';
import Paper from 'material-ui/Paper';
import { MenuItem } from 'material-ui/Menu';
import Divider from 'material-ui/Divider';
import Typography from 'material-ui/Typography';
import { CircularProgress } from 'material-ui/Progress';
import { ListItem} from 'material-ui/List';
import Grid from 'material-ui/Grid';
import Dialog, {
	DialogActions,
	DialogContent,
	DialogContentText,
	DialogTitle,
} from 'material-ui/Dialog';

import SearchIcon from 'material-ui-icons/Search';
import PersonIcon from 'material-ui-icons/Person';
import CloseIcon from 'material-ui-icons/Close';
import ChevronRight from 'material-ui-icons/ChevronRight';
import SaveIcon from 'material-ui-icons/Save';
import AddIcon from 'material-ui-icons/Add';
import RemoveIcon from 'material-ui-icons/Remove';
import ModeEditIcon from 'material-ui-icons/ModeEdit';

import {Get} from './ajax'

//export const formatNumb=intl=>{
//	return n=>intl.formatNumber(n,{ style: 'decimal',minimumFractionDigits:2,maximumFractionDigits:2});
//}
//export const formatNumber=new Intl.NumberFormat('en-IN', { style: 'decimal',minimumFractionDigits:2,maximumFractionDigits:2}).format;
//
function getCpModalStyle() {
	const top = 50;// + rand();
	const left = 50;// + rand();
	
	return {
		position: 'absolute',
		width: '98%',
		maxWidth:'500px',
		height:'80%',
		maxHeight:'180px',
		top: `${top}%`,
		left: `${left}%`,
		transform: `translate(-${top}%, -${left}%)`,
		border: '1px solid #e5e5e5',
		backgroundColor: '#fff',
		boxShadow: '0 5px 15px rgba(0, 0, 0, .5)',
		padding: 0,
		boxSizing: 'border-box',
		//overflowY:'auto'
		fontFamily: `"Roboto", "Helvetica", "Arial", sans-serif`,
	};
}

function getBrModalStyle() {
	return {...getCpModalStyle(),
		minHeight:'180px',
		maxHeight:'380px',
		display:'flex',
		flexDirection:'column'
	}
}

function getCcModalStyle() {
	return {...getCpModalStyle(),
		maxHeight:'200px',
		display:'flex',
		flexDirection:'column'
	}
}

export const SnackBar=p=>{
	//let {classes} = p;
	return <Snackbar
		anchorOrigin={{
			vertical: 'bottom',
			horizontal: 'left',
		}}
		open={p.open}
		autoHideDuration={p.duration||2000}
		onClose={p.onClose}
		SnackbarContentProps={{
			'aria-describedby': 'message-id',
			classes:p.classes||{}
		}}
		message={<span id="message-id" className={p.msgClass||''}>{p.message}</span>}
		action={[
			<IconButton
				key="close"
				aria-label="Close"
				color="inherit"
				onClick={p.onClose}>
			<CloseIcon />
			</IconButton>,
		]}
		className={p.className||''}/>
}

const sty={
	fld:{
		display:'flex',
		flexDirection:'row',
		alignItems:'center',
		margin:'5px 10px'
	},
	fldItem:{
		flex:1,
	},
	sep:{
		width:'10px',
	},
	header:{
		padding:'10px 10px 0 10px'
	},
	footer:{
		textAlign:'right'
	},
	input:{
		padding:'5px 10px',
		fontSize:'12pt',
		width:'100%',
		boxSizing:'border-box'
	},
	button:{
		margin:'8px'
	},
	
	bcItem:{
		display:'inline-block',
		lineHeight:'24px',
		height:24,
		verticalAlign:'middle',
	},
	bcSub:{
		display:'inline-block',
		lineHeight:'24px',
		height:24,
		verticalAlign:'middle',
		cursor:'default',
	},
	bcLink:{
		color:'blue',
		cursor:'pointer',
	},
}

export class ChangePassword extends Component{
	state={
		password:'',
		confirm:''
	}
	change=(k)=>{
		return e=>{
			this.setState({[k]:e.target.value})
		}
	}
	ok=()=>{
		this.props.onOK(this.state);
	}
	render(){
		let p=this.props;
		return <Modal
			aria-labelledby="simple-modal-title"
			aria-describedby="simple-modal-description"
			open={p.open}
			onClose={p.onClose}
			>
			<div style={getCpModalStyle()}>
				<Typography style={sty.header} type="title" gutterBottom>{p.title||'Change Password'}</Typography>
				<Divider />
				<div>
					<div style={sty.fld}>
						<div style={sty.fldItem}>{p.label||'New Password'}</div>
						<div style={sty.sep}>:</div>
						<div style={sty.fldItem}><input type={"password"} style={sty.input} autoFocus={true} value={this.state.password} onChange={this.change('password')} /></div>
					</div>
					<div style={sty.fld}>
						<div style={sty.fldItem}>{p.confirmLabel||'Confirm New Password'}</div>
						<div style={sty.sep}>:</div>
						<div style={sty.fldItem}><input type={"password"} style={sty.input} value={this.state.confim}  onChange={this.change('confirm')} /></div>
					</div>
				</div>
				<Divider />
				<div style={sty.footer}>
					<Button raised onClick={this.ok} color={"primary"} style={sty.button}>OK</Button>
					<Button raised onClick={p.onClose} color={"secondary"} style={sty.button}>Cancel</Button>
				</div>
			</div>
		</Modal>
	}
}

export class ChangeStatus extends Component{
	state={
		reason:''
	}
	change=(k)=>{
		return e=>{
			this.setState({[k]:e.target.value})
		}
	}
	ok=()=>{
		this.props.onOK(this.state);
	}
	render(){
		let p=this.props;
		return <Modal
			aria-labelledby="simple-modal-title"
			aria-describedby="simple-modal-description"
			open={p.open}
			onClose={p.onClose}
			>
			<div style={getCpModalStyle()}>
				<Typography style={sty.header} type="title" gutterBottom>{p.title}</Typography>
				<Divider />
				<div>
					<div style={sty.fld}>{p.label||'Reason'}</div>
					<div style={sty.fld}>
						<input style={sty.input} value={this.state.confim} autoFocus={true} onChange={this.change('reason')} />
					</div>
				</div>
				<Divider />
				<div style={sty.footer}>
					<Button raised onClick={this.ok} color={"primary"} style={sty.button}>OK</Button>
					<Button raised onClick={p.onClose} color={"secondary"} style={sty.button}>Cancel</Button>
				</div>
			</div>
		</Modal>
	}
}

export class ChangeCredit extends Component{
	state={
		amount:'',
		reason:''
	}
	change=(k)=>{
		return e=>{
			this.setState({[k]:e.target.value})
		}
	}
	ok=()=>{
		this.props.onOK(this.state);
	}
	render(){
		let p=this.props;
		return <Modal
			aria-labelledby="simple-modal-title"
			aria-describedby="simple-modal-description"
			open={p.open}
			onClose={p.onClose}
			>
			<div style={getCcModalStyle()}>
				<Typography style={sty.header} type="title" gutterBottom>{p.title}</Typography>
				<Divider />
				<div style={sty.fld}>
					<div style={sty.fldItem}>{'Amount'}</div>
					<div style={sty.sep}>:</div>
					<div style={sty.fldItem}><input type={"tel"} style={sty.input} autoFocus={true} value={this.state.amount} onChange={this.change('amount')} /></div>
				</div>
				<div>
					<div style={sty.fld}>{'Reason'||p.label}</div>
					<div style={sty.fld}>
						<input style={sty.input} value={this.state.confim} onChange={this.change('reason')} />
					</div>
				</div>
				<Divider />
				<div style={sty.footer}>
					<Button raised onClick={this.ok} color={"primary"} style={sty.button}>OK</Button>
					<Button raised onClick={p.onClose} color={"secondary"} style={sty.button}>Cancel</Button>
				</div>
			</div>
		</Modal>
	}
}

export const BetResult=p=>{
	return <Modal
		aria-labelledby="bet-result"
		aria-describedby="this-is-the-bet-result-window"
		open={p.open}
		onClose={p.onClose}
		>
		<div style={getBrModalStyle()}>
			<Typography style={sty.header} type="title" gutterBottom>{p.title||'Bet Place Successful'}</Typography>
			<Divider />
			<div style={{flex:1,overflow:'auto',padding:'0 20px'}}>
				{p.children}
			</div>
			<Divider />
			<div style={sty.footer}>
				<Button raised onClick={p.onClose} color={"primary"} style={sty.button}>OK</Button>
			</div>
		</div>
	</Modal>
}

export const BreadCrumbs=p=><div className={p.className||''}>
	{p.data.map((v,k)=>{
		let lnk=k===0?p.route(p.first||'/downline'):(k<p.data.length-1?p.route((p.prefix||'/downline/')+v):null);
		return <span key={v} style={lnk?sty.bcItem:{}}>
			{k > 0 && <ChevronRight style={sty.bcItem} />}
			<a onClick={lnk} style={{...sty.bcSub,...(lnk?sty.bcLink:{})}}>{v}</a>
		</span>})}
</div>

export class EditableItem extends Component{
	state={
		editMode:false,
		value:this.props.value||this.props.def||'',
		after:this.props.after||'',
		err:this.props.err||false,
		loading:false,
	}
	change=e=>{
		let {upperCase,lowerCase}=this.props,
			value=e.target.value||'';
		if(upperCase){
			value=value.toUpperCase();
		} else if(lowerCase){
			value=value.toLowerCase();
		}
		this.setState({value});
	}
	click=k=>{
		return ()=>{
			if(k==='save'){
				if(this.props.onSave){
					this.setState({loading:true,editMode:false},()=>{
						this.props.onSave(this.state.value,(e,r)=>{
							if(e) {
								//this.setState({err:true});
								console.log('e==>',e);
							}
							this.setState({loading:false},()=>{
								this.blur();
							});
						})
					});
				} else if(this.props.onChange){
					this.props.onChange(this.state.value);
					this.setState({editMode:false},()=>{
						this.blur();
					});
				}
			} else if(k==='cancel'){
				this.setState({value:this.props.value||this.props.def||'',editMode:false},()=>{
					this.blur();
				});
			} else {
				this.setState({editMode:true},()=>{
					this.setFocus();
				});
			}
		}
	}
	setFocus=()=>{
		if(this.refs.input.setSelectionRange) this.refs.input.setSelectionRange(0, this.refs.input.value.length);
		this.refs.input.focus();
	}
	blur=()=>{
		window.document.getSelection().empty();
	}
	componentWillReceiveProps(np){
		if(np.err !== this.state.err)
			this.setState({err:np.err});
		if(np.value !== this.state.value)
			this.setState({value:np.value||np.def||''})
		if(np.after !== this.state.after)
			this.setState({after:np.after})
	}
	render(){
		let {editMode,value,after,err,loading}=this.state;
		let p=this.props;
		let valid=p.validation?p.validation(value):true;
		return <ListItem className={p.c[p.rootClass||'item2']}>
			<Grid item xs={4} className={p.c.label}>{p.label||'Intake'}</Grid>
			<Grid item xs={1} className={p.c.label}>{":"}</Grid>
			<Grid item xs={7} className={p.c.value2}>
				<input ref={"input"}
					type={p.type||"tel"}
					disabled={!editMode}
					className={p.c.input+(!valid||err?' '+p.c.err:'')}
					onChange={this.change}
					value={value} />
				<div className={p.c.after}>{after||''}</div>
				{!!editMode && <div className={p.c.wrapper}>
					<IconButton disabled={loading || !valid} className={p.c.button} aria-label="Save" color="primary" onClick={this.click('save')} >
						<SaveIcon/>
					</IconButton>
					{loading && <CircularProgress size={24} className={p.c.buttonProgress} />}
					</div>}
				{!!editMode && <IconButton className={p.c.button} aria-label="Cancel" onClick={this.click('cancel')} >
					<CloseIcon/>
				</IconButton>}
				{!editMode && <IconButton className={p.c.button} aria-label="Edit" color="primary" onClick={this.click('edit')} >
					<ModeEditIcon/>
				</IconButton>}
			</Grid>
		</ListItem>
	}
}

export const Confirm=p=><Dialog
	open={p.open}
	onClose={p.onClose}
	aria-labelledby="alert-dialog-title"
	aria-describedby="alert-dialog-description">
	<DialogTitle id="alert-dialog-title">{p.title}</DialogTitle>
	<DialogContent>
		<DialogContentText id="alert-dialog-description">
			{p.msg}
		</DialogContentText>
	</DialogContent>
	<DialogActions>
		<Button onClick={p.onOK} color="primary" autoFocus raised>
			{p.okCaption||'OK'}
		</Button>
		<Button onClick={p.onClose} raised>
			{p.cancelCaption||'Cancel'}
		</Button>
	</DialogActions>
</Dialog>

//export const CreditEditable=p=><ListItem className={p.c.item}>
//	<Grid item xs={4} className={p.c.label}>{p.label}</Grid>
//	<Grid item xs={1} className={p.c.label}>{":"}</Grid>
//	<Grid item xs={7} className={p.c.value+(p.valueCn?' '+p.valueCn:'')}>
//		<div style={{flex:1}}>{p.value}</div>
//		<IconButton color={'secondary'} className={p.c.button} onClick={p.onPlus} >
//			<AddIcon/>
//		</IconButton>
//		<IconButton color={'secondary'} className={p.c.button} onClick={p.onMin} >
//			<RemoveIcon/>
//		</IconButton>
//	</Grid>
//</ListItem>

export const CreditEditable=p=><ListItem className={p.c.item}>
	<Grid item xs={4} className={p.c.label}>{p.label}</Grid>
	<Grid item xs={1} className={p.c.label}>{":"}</Grid>
	<Grid item xs={7} className={p.c.value+(p.valueCn?' '+p.valueCn:'')}>
		<div style={{flex:1}}>{p.value}</div>
		<IconButton color={'secondary'} className={p.c.button} onClick={p.onEdit} >
			<ModeEditIcon/>
		</IconButton>
	</Grid>
</ListItem>

//yg tengah adalah yg ketemu
function parts(s,q){
	s=s||'';
	q=q||'';
	if(!s || !q) return [s,'',''];
	let p=s.toUpperCase().search(q.toUpperCase());
	if(p<0) return [s,'',''];
	return [s.substr(0,p),s.substr(p,q.length),s.substr(p+q.length,s.length-p-q.length)]
}

const sbStyles = theme => ({
	container: {
		//flexGrow: 1,
		position: 'relative',
		//height: 200,
		zIndex: 1000,
		//backgroundColor:'#fff',
	},
	suggestionsContainerOpen: {
		position: 'absolute',
		marginTop: 4,//theme.spacing.unit,
		marginBottom: theme.spacing.unit * 3,
		left: 10,
		right: 0,
		//backgroundColor:'blue'
	},
	suggestion: {
		display: 'block',
		//backgroundColor:'green'
	},
	suggestionsList: {
		margin: 0,
		padding: 0,
		listStyleType: 'none',
		//backgroundColor:'red'
	},
	sugId:{
		padding:[0,10,0,0]
	},
	sugName:{
		color:'rgba(0,0,0,0.5)',
	},
	sugHl:{
		color:'rgba(0,0,255,1)',
	},
	hRow:{
		padding:[5,0],
		fontSize:'12pt',
		display:'flex',
		alignItems:'center'
	},
	idInput:{
		padding:[5,10],
		fontSize:'12pt',
		margin:[0,0,0,10],
		fontWeight:'bold'
	},
	idButton:{
		height: 40,
		width: 40
	},
});

class SB extends Component{
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}
	state={
		id:'',
		searchMode:true,
		me:null,
		allowSearch:true,
		suggestions: [],
		searchParams:this.props.searchParams||null
	}
	onSuggestionsFetchRequested = ({ value }) => {
		let {searchParams}=this.state;
		//,can_play:true,active:true
		
		//console.log('searchParams==>',searchParams);
		
		Get('user',{...{search:value,limit:25},...(searchParams||{})},(e,r)=>{
			//console.log('r==>',r);
			this.setState({
				suggestions:(r||{}).d||[]
			});
		});
	}
	componentWillReceiveProps(np){
		let {id,searchMode,me,allowSearch,searchParams}=this.state;
		let st={};
		if(np.hasOwnProperty('id') && np.id !== id) st.id=np.id;
		if(np.hasOwnProperty('searchMode') && np.searchMode !== searchMode) st.searchMode=np.searchMode;
		if(np.hasOwnProperty('me') && np.me !== me) st.me=np.me;
		if(np.hasOwnProperty('allowSearch') && np.allowSearch !== allowSearch) st.allowSearch=np.allowSearch;
		if(np.hasOwnProperty('searchParams') && np.searchParams !== searchParams) st.searchParams=np.searchParams;		
		if(Object.keys(st).length) this.setState(st);
	}
	onSuggestionsClearRequested = () => {
		this.setState({
			suggestions: []
		});
	}
	onSuggestionBlur=e=>{
		this.findId(e.target.value)
	}
	onSuggestionSelected=(event, {suggestionValue})=>{
		this.findId(suggestionValue)
	}
	getSuggestionValue=(suggestion)=>{
		return suggestion.id;
	}
	searchClick=()=>{
		this.setState({searchMode:true},()=>{
			//this.setFocus('userid');
			window.sug=this.sug;
			this.sug.input.focus();
			if(this.props.onSearchMode) this.props.onSearchMode();
		});
	}
	idInputChange = (event, { newValue, method }) => {
		this.setState({
			id: newValue
		});
	};
	meClick=()=>{
		let {me}=this.state;
		this.findId(me.id);
	}
	findId=(inputValue)=>{
		let {userDetailParams,userDetailValidate,onFound}=this.props;
		if(!inputValue) return this.setState({id:''});
		Get('user/'+inputValue||this.state.id,userDetailParams||{},(e,r)=>{
			let d=(r||{}).d;
			if(d && userDetailValidate(d)){
				return this.setState({
					id:d.id,
					searchMode:false,
				},()=>{
					return onFound(d);
				})
			}
			this.setState({id:''});
		});
	}
	idKeyDown=(event)=>{
		const { keyCode } = event
        const input = event.target
        // if input non-empty, clear it and stop the event; otherwise let the event continue (e.g. close the modal)
        if (keyCode === 13 && input.value.length > 0) {
			console.log('enter kepencet',input.value);
			this.findId(input.value);
        }
	}
	renderSuggestion=(suggestion, { query, isHighlighted })=>{
		let {classes}=this.props,
			idParts=parts(suggestion.id,query),
			nameParts=parts(suggestion.name,query);
		//console.log('query==>',query);
		return <MenuItem selected={isHighlighted} component="div">
			<div className={classes.sugId}>
				{idParts.map((v,k)=>{
					if(k===1) return <span key={k} className={classes.sugHl}>{v.replace(/\s/g,'\u00A0')}</span>;
					return <span key={k}>{v.replace(/\s/g,'\u00A0')}</span>;
				})}
			</div>
			<div className={classes.sugName}>
				{nameParts.map((v,k)=>{
					if(k===1) return <span key={k} className={classes.sugHl}>{v.replace(/\s/g,'\u00A0')}</span>;
					return <span key={k}>{(v||'').replace(/\s/g,'\u00A0')}</span>;
				})}
			</div>
		</MenuItem>
	}
	renderSuggestionsContainer=({ containerProps, children })=><Paper {...containerProps} square>
		{children}
    </Paper>
	render(){
		let {classes,label,getRef}=this.props,
			{suggestions,id,searchMode,me,allowSearch}=this.state;
		return <div ref={(e)=>{
					if(getRef) getRef(this);
				}} className={classes.hRow} style={{}}>
			<div>{label}</div>
			<Autosuggest
				theme={{
					container: classes.container,
					suggestionsContainerOpen: classes.suggestionsContainerOpen,
					suggestionsList: classes.suggestionsList,
					suggestion: classes.suggestion,
				}}
		
				ref={e=>{this.sug=e}}
				suggestions={suggestions}
				onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
				onSuggestionsClearRequested={this.onSuggestionsClearRequested}
				onSuggestionSelected={this.onSuggestionSelected}
				getSuggestionValue={this.getSuggestionValue}
				renderSuggestion={this.renderSuggestion}
				renderSuggestionsContainer={this.renderSuggestionsContainer}
				inputProps={{
					placeholder: "userid",
					value:id,
					disabled:!searchMode,
					className:classes.idInput,
					onKeyDown:this.idKeyDown,
					onChange: this.idInputChange,
					onBlur: this.onSuggestionBlur,
				  }} />
			{me && <IconButton aria-label="me" className={classes.idButton} onClick={this.meClick}>
				<PersonIcon/>
			</IconButton>}
			{allowSearch && <IconButton aria-label="search-user" className={classes.idButton} onClick={this.searchClick}>
				<SearchIcon/>
			</IconButton>}
		</div>
	}
}

export const Classes=(theme)=>({
	succesBox:{
		backgroundColor:theme.palette.primary.main,
		color:theme.palette.text.primary
	},
	errorBox:{
		backgroundColor:theme.palette.error.main
	},
	empty:{
		color:'rgba(0,0,0,0.5)',
		backgroundColor:'rgba(255,255,255,0.5)',
		padding:10,
		textAlign:'center'
	},
	link:{
		cursor:'pointer',
		color:'blue',
		'&:hover':{
			textDecoration:'underline'
		}
	}
});

export const TableClasses=(theme)=>({
	tableTitle:{
		padding:[0,0],
		borderBottom:[2,'solid','#000'],
		fontWeight:'bold',
	},
	tableRow:{
		padding:[0,0],
		borderTop:[1,'solid','#eee'],
		cursor:'pointer',
		display:'flex',
		flexDirection:'row',
		'&:hover':{
			backgroundColor:'rgba(0,0,255,0.1)'
		}
	},
	tableFooter:{
		padding:[0,10],
		borderTop:[1,'solid','#eee'],
		justifyContent:'space-between',
		display:'flex',
		flexDirectoon:'row',
		alignItems:'center',
		[theme.breakpoints.down('sm')]: {
			display:'block'
		},
	},
	footerButtons:{
		display:'flex',
		alignItems:'center',
		justifyContent:'center',
		flexWrap:'wrap'
	},
	footerSub:{
		[theme.breakpoints.down('sm')]: {
			display:'flex',
			justifyContent:'space-around'
		},
	},
	footerSub2:{
		display:'flex',
		flexDirectoon:'row',
		alignItems:'center',
		justifyContent:'center',
	},
	date:{
		padding:[5,10],
		fontSize:'12pt'
	},
	col:{
		flex:1,
		borderLeft:[1,'solid','#eee'],
		//borderRight:[1,'solid','rgba(0,0,0)'],
		textAlign:'center',
		padding:[10,5],
		//height:36,
		'&:first-child':{
			borderLeft:[0,'solid','#ddd'],
		}
	},
	col2:{
		flexGrow:2,
	},
	col3:{
		flexGrow:3,
	},
	col4:{
		flexGrow:4,
	},
	cols:{
		textAlign:'left',
	},
	coln:{
		textAlign:'right',
	},
	colA:{
		padding:[0,5]
	},
	colT:{
		flexGrow:9,
		borderLeft:[0,'solid','#eee'],
	},
})

export const BetClasses=(theme)=>({
	root: {
		flexGrow: 1,
		backgroundColor: theme.palette.background.paper,
		display:'flex',
		flexDirection:'column',
		height:'100%'
	},
	hRow:{
		padding:[5,0],
		fontSize:'12pt',
		display:'flex',
		alignItems:'center'
	},
	dateItem:{
		padding:[0,5]
	},
	disable:{
		color:'rgba(0,0,0,0.5)'
	},

	hSelect:{
		fontSize:'12pt'
	},
	hErr:{
		color:'red'
	},
	number:{
		padding:[2,10],
		fontSize:18,
		width:'100%',
		boxSizing:'border-box',
	},
	numberErr:{
		borderColor:'red'
	},
	select:{
		fontSize:18,
	},
	toolbar:{
		padding:[10,0]
	},
	tblRow:{
		border:'1px solid #000',
		textAlign:'center'
	},
	incRow:{
		background:'rgba(255,255,0,0.2)',
	},
	errRow:{
		background:'rgba(255,0,0,0.2)',
	},
	validRow:{
		background:'rgba(0,255,0,0.2)',
	},
	th:{
		borderRight:'1px solid #000',
		borderBottom:'1px solid #000',
		padding:5,
		'&:last-child':{
			borderRight:[0,'solid','#000'],
		}
	},
	wrapper: {
		margin: theme.spacing.unit,
		position: 'relative',
		display:'inline-block'
	},
	buttonProgress: {
		color: 'teal',//green[500],
		position: 'absolute',
		top: '50%',
		left: '50%',
		marginTop: -12,
		marginLeft: -12,
	},
	sItem2:{
		textAlign:'center'
	},
	sFirstItem:{
		textAlign:'left',
		padding:[10,0],
		//borderBottom:[1,'solid','#eee']
		fontSize:'12pt'
	},
	col:{},
	colT:{},
	sub:{
		flex:1,
		overflow:'auto',
		padding:[0,10]
	},
	footer:{
		padding:[5,10],
		borderTop:[1,'solid','#aaa']
	}
});

export const SearchBar=withStyles(sbStyles, { withTheme: true })(SB);