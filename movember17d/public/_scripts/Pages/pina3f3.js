﻿$(function () {
    $('.a-pin').click(function () {
        var code = $(this).text();
        if (code.length == 0) return;

        var txtPin = $('.txt-pin');
        var pin = txtPin.val();
        txtPin.val(pin + code);

        $('[id$=hiPin]').val(txtPin.val());
    });

    $('.back-space').click(function () {       
        var txtPin = $('.txt-pin');
        var pin = txtPin.val();
        if (pin.length == 0) return;

        pin = pin.substring(0, pin.length - 1);

        txtPin.val(pin);
        $('[id$=hiPin]').val(txtPin.val());
    });
});

function showDialog() {
    var options =
    {
        title: 'Continue login with PIN',
        width: 300,
        closable: true,
        height: 300,
        closable: true,
        onClose: function () {
        }
    };

    $('#pinDialog').dialog(options);
    $('#pinDialog').dialog('open');
}

function showSetup() {
    var options =
    {
        title: 'PIN',
        width: 300,
        closable: true,
        height: 300,
        closable: true,
        onClose: function () {
        }
    };

    $('#pinSetup').dialog(options);
    $('#pinSetup').dialog('open');
}

function checkInputPin() {
    var pin1 = $('[id$=txtInputPin]').val();
    var pin2 = $('[id$=txtConfirmPin]').val();
    var pattern = /^[0-9]*$/;

    if (pin1.length != 6 || pin2.length != 6 || !pattern.test(pin1)) {
        $('[id$=lblPinMsg]').text('Invalid PIN!');
        return false;
    }

    if (pin1 != pin2) {
        $('[id$=lblPinMsg]').text('Two PIN are not the same!');
        return false;
    }

    $('[id$=hiPin1]').val(pin1);

    return true;
}
