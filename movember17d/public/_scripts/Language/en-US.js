﻿// JScript document
function LanguageString() 
{
    //betting
    this.Betting_Delete_Page_Confirm = "Are you sure to delete this page? Note: Delete of betting page will only affect per draw date. please ensure page delete correctly. Thank you.";
    this.Betting_Red_Number = " is Red Number by Admin";
    this.Betting_Big_Maxinum = "Big amount should <= 999";
    this.Betting_Small_Mininum = "Small amount should <= 900";
    this.Betting_Select_Number = 'Please select account';
    this.Betting_Enter_Number = 'Please enter number details for betting!';
    this.Betting_Confirm_Submit = 'Are you sure to submit the entries for '+AppGameName+' betting?';
    this.Betting_Confirm_Reset = 'do you wish to reset the form?';
    this.Betting_Recalculate = 'do you wish to Recalculate?';
    this.Betting_Submit_Empty = "Please don't submit an empty row.";
    this.Betting_4_Digit = "Please enter 4 digit number!";
    this.Betting_Number_Wildcard = "Please enter Number with Wildcard!";
    this.Betting_Select_Page = 'Please select Page to delete';
    this.Betting_Delete_Fixed = 'Are you sure to delete the entries for Fixed Entry?';
    this.Betting_ARA_Load = "Are you sure to load this item?";
    this.Betting_Confirm_Delete = "Are you sure to delete this item?";
    this.Setup_Confirm_Reset = "Are you sure to reset the credit now?";
    this.Setup_Recaculate_Confirm = "Are you sure to Recalculate Intake And PlaceOut?";
    this.Setup_Fixed_Confirm = "Are you sure to submit bet the entries from Fixed Entry?";
    this.Setup_Submit_Item = "Are you sure to submit this item?";
    this.Betting_Confirm_Save = 'Are you sure to save the entries for betting?';
    this.Betting_Confirm_Submit_Fixed = 'Are you sure to save the fixed entries for betting?';

    //share
    this.ShareString_Date = "Date";
    this.ShareString_Name = "Name";
    this.ShareString_Description = "Descriptions";
    this.ShareString_Actions = "Actions";
    this.ShareString_Perform_By = "Perform By";
    this.ShareString_UserID = "UserID";
    this.ShareString_Role = "Role";
    this.ShareString_Status = "Status";
    this.ShareString_Credit = "Credit";
    this.ShareString_Balance = "Credit Used";
    this.ShareString_Available = "Credit Avail";
    this.ShareString_Details = "Details";
    this.ShareString_AccountID = "Account ID";
    this.ShareString_AgentID = "Agent ID";
    this.ShareString_MemberID = "Member ID";
    this.ShareString_ManagerID = "Manager ID";
    this.ShareString_UplineID = "Upline ID";
    this.ShareString_No_Access_Right = "No access right!";
    this.ShareString_Revieved_Time = "Recieved Time";
    this.ShareString_Main_Account = "Main Account";
    this.ShareString_Strike_Amount = "Strike";
    //report
    this.Report_Log_Type = "Category";
}

var GlobalString = new LanguageString();

//test
//alert(GlobalString.Common_FailMassage);

