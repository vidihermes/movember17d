exports.done=function(e){
	if(!e) console.log('done!!')
	else console.error(new Error(e));
	console.log('');
	process.exit();
};

exports.print=function(desc,subs){
	desc=Array.isArray(desc)?desc:[desc];
	subs=subs||{};
	desc.forEach(d=>{
		console.log(d);
	});
	Object.keys(subs).forEach(k=>{
		console.log('\t'+k+'\t'+subs[k]);
	});
	console.log('');
};