process.env.TZ = 'Asia/Singapore';

require('json5/lib/require');
require('prototypes');

let {print}=require('./print'),
	arg=process.argv.splice(2),
	cmd=arg.shift(),
	cmdU=(cmd||'').toLowerCase();
	
function help(){
	print('syntax "command <argument>", where command is one of :',{
		redis:'redis related command',
		db:'database related command',
		tmp:'temporary command',
	});
}

if(!cmd) {
	help();
} else if(cmdU === 'redis'){
	require('./redis')(arg.shift());
} else if(cmdU === 'db'){
	require('./db')(arg.shift(),arg);
} else if(cmdU === 'tmp'){
	require('./tmp')(arg.shift());
} else {
	help();
}

