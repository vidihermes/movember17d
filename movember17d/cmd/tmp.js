var pg=require('pg'),
	init=require('../lib/init'),
	dateUtil=require('../lib/date'),
	Game=require('../routes/game').model;
let {print,done}=require('./print'),
	dbconfig=require('../config/db.json5');

function help(){
	print('command (tmp <argument>), where argument is one of:',{
		alternotif:'create t_notif_access if not exists',
	});
}

//DROP TABLE IF EXISTS t_payment;
//
//CREATE TABLE IF NOT EXISTS t_payment
//(
//  id bigserial NOT NULL,
//  date timestamp without time zone DEFAULT now(),
//  user_id character varying(50),
//  from_user character varying(50),
//  amount numeric DEFAULT 0,
//  description character varying(255),
//  type character varying(100),
//  detail json DEFAULT '{}'::json,
//  CONSTRAINT t_payment_pkey PRIMARY KEY (id)
//);

function alterNotif(){
//	let q=`
//-- update t_active_ticket set day_left=day_list, day_length=day_count;
//delete from t_current_balance;
//delete from t_active_ticket;
//delete from t_payment;
//delete from t_ticket_page;
//
//`;
//let q=`
//update m_game set validate='^\\d{2}$' where id='2D';
//update m_game set validate='^\\d{3}$' where id='3D';
//update m_game set validate='^\\d{4}$' where id='4D';
//`;

//let q=`
//update m_game set rebate_max='15' where id='2D';
//update m_game set rebate_max='15' where id='3D';
//`;

//let q=`
//update m_game set strike_comm_max='5';
//`;

//let q=`
//CREATE OR REPLACE FUNCTION get_from(needle anyelement, haystack anyarray)
//  RETURNS anyarray AS
//$BODY$
//	with
//		a as (select $2 isi),
//		i as (select generate_subscripts(a.isi, 1) idx from a),
//		p as (SELECT i.idx,a.isi[i.idx] from a,i where a.isi[i.idx] = $1)
//	select array(select a.isi[i.idx] from a,i,p where i.idx >= p.idx);
//$BODY$
//  LANGUAGE sql STABLE;
//
//CREATE TABLE t_strike
//(
//  id bigserial NOT NULL,
//  ticket_id bigint,
//  num character varying(10),
//  date timestamp without time zone DEFAULT now(),
//  draw_date date,
//  rank character varying(10),
//  big_prize numeric DEFAULT 0,
//  small_prize numeric DEFAULT 0,
//  CONSTRAINT t_strike_pkey PRIMARY KEY (id)
//);`;

//let q=`
//delete from t_current_balance;
//delete from t_active_ticket;
//delete from t_strike;
//delete from t_payment;
//delete from t_ticket_page;
//`;

//let q=`DROP TABLE IF EXISTS m_fix_ticket;
//
//CREATE TABLE m_fix_ticket
//(
//  id bigserial NOT NULL,
//  date timestamp without time zone DEFAULT now(),
//  place_to character varying(50),
//  place_by character varying(50),
//  game_id character varying(50),
//  num character varying(50),
//  big numeric DEFAULT 0,
//  small numeric DEFAULT 0,
//  bt character varying(50),
//  page_id bigint,
//  permute_length integer DEFAULT 1,
//  CONSTRAINT m_fix_ticket_pkey PRIMARY KEY (id)
//);
//
//DROP TABLE IF EXISTS m_fix_ticket_page;
//
//CREATE TABLE m_fix_ticket_page
//(
//  id bigserial NOT NULL,
//  date timestamp without time zone DEFAULT now(),
//  form_type character varying(50),
//  place_to character varying(50),
//  place_by character varying(50),
//  active boolean DEFAULT true,
//  CONSTRAINT m_fix_ticket_page_pkey PRIMARY KEY (id)
//);
//
//`;

let q=`CREATE OR REPLACE FUNCTION v_balance(user_id_ text)
  RETURNS double precision AS
$BODY$

WITH
recursive
usrr(date,user_id,member_of,members,game_id,num,big_intake,small_intake,intake_1d,big_bet,small_bet,bet_1d,big_pool,small_pool,pool_1d,big_pass,small_pass,pass_1d,d)as(select
		date,user_id,member_of,members,game_id,num,big_intake,small_intake,intake_1d,big_bet,small_bet,bet_1d,big_pool,small_pool,pool_1d,big_pass,small_pass,pass_1d,
		0::float
		from tuallp t,cfg
		where user_id=cfg.uid
	union select c.date,c.user_id,c.member_of,
		array_rm(tm.members,c.members),
		c.game_id,c.num,
		c.big_intake,c.small_intake,c.intake_1d,
		c.big_bet,c.small_bet,c.bet_1d,
		case when (COALESCE(array_length(c.members,1),0)<1) then c.big_pool
			else c.big_pool+COALESCE(tm.big_pass,0) end,
		case when (COALESCE(array_length(c.members,1),0)<1) then c.small_pool 
			else c.small_pool+COALESCE(tm.small_pass,0) end,
		case when (COALESCE(array_length(c.members,1),0)<1) then c.pool_1d
			else c.pool_1d+COALESCE(tm.pass_1d,0) end,
		case when (COALESCE(array_length(c.members,1),0)<1) then c.big_pass
			when COALESCE(array_length(array_rm(tm.members,c.members),1),0)>0 then null
			when (c.big_bet+c.big_pool+COALESCE(tm.big_pass,0)-c.big_intake) < 0 then 0
			else c.big_bet+c.big_pool+COALESCE(tm.big_pass,0)-c.big_intake end,
		case when (COALESCE(array_length(c.members,1),0)<1) then c.small_pass
			when COALESCE(array_length(array_rm(tm.members,c.members),1),0)>0 then null
			when (c.small_bet+c.small_pool+COALESCE(tm.small_pass,0)-c.small_intake) < 0 then 0
			else c.small_bet+c.small_pool+COALESCE(tm.small_pass,0)-c.small_intake end,
		case when (COALESCE(array_length(c.members,1),0)<1) then c.pass_1d
			when COALESCE(array_length(array_rm(tm.members,c.members),1),0)>0 then null
			when (c.bet_1d+c.pool_1d+COALESCE(tm.pass_1d,0)-c.intake_1d) < 0 then 0
			else c.bet_1d+c.pool_1d+COALESCE(tm.pass_1d,0)-c.intake_1d end,
		d+1
	from tuallp c
	left join tm on tm.date=c.date and tm.user_id=c.user_id and tm.game_id=c.game_id and tm.num=c.num
	inner join usrr u on c.date=u.date and c.user_id=any(u.members) and c.game_id=u.game_id and c.num=u.num),
um as (select 
	date,
	user_id,
	member_of,
	game_id,
	num,
	array(select t.user_id::text from usrr t where t.date=c.date and t.member_of=c.user_id and t.game_id=c.game_id and t.num=c.num and COALESCE(array_length(t.members,1),0)<1) members,
	(select sum(big_pass) from usrr t where t.date=c.date and t.member_of=c.user_id and t.game_id=c.game_id and t.num=c.num and COALESCE(array_length(t.members,1),0)<1) big_pass,
	(select sum(small_pass) from usrr t where t.date=c.date and t.member_of=c.user_id and t.game_id=c.game_id and t.num=c.num and COALESCE(array_length(t.members,1),0)<1) small_pass,
	(select sum(pass_1d) from usrr t where t.date=c.date and t.member_of=c.user_id and t.game_id=c.game_id and t.num=c.num and COALESCE(array_length(t.members,1),0)<1) pass_1d
	from usrr c
),
usrrup (date,user_id,member_of,members,game_id,num,big_intake,small_intake,intake_1d,big_bet,small_bet,bet_1d,big_pool,small_pool,pool_1d,big_pass,small_pass,pass_1d,depth)as(select
		c.date,c.user_id,c.member_of,
		array_rm(um.members,c.members),
		c.game_id,c.num,
		c.big_intake,c.small_intake,c.intake_1d,
		c.big_bet,c.small_bet,c.bet_1d,
		c.big_pool+COALESCE(um.big_pass,0),
		c.small_pool+COALESCE(um.small_pass,0),
		c.pool_1d+COALESCE(um.pass_1d,0),
		case when (COALESCE(array_length(c.members,1),0)<1) then c.big_pass
			when COALESCE(array_length(array_rm(um.members,c.members),1),0)>0 then null
			when (c.big_bet+c.big_pool+
			COALESCE(um.big_pass,0)-c.big_intake) < 0 then 0
			else c.big_bet+c.big_pool+COALESCE(um.big_pass,0)-c.big_intake end,
		case when (COALESCE(array_length(c.members,1),0)<1) then c.small_pass
			when COALESCE(array_length(array_rm(um.members,c.members),1),0)>0 then null
			when (c.small_bet+c.small_pool+COALESCE(um.small_pass,0)-c.small_intake) < 0 then 0
			else c.small_bet+c.small_pool+COALESCE(um.small_pass,0)-c.small_intake end,						
		case when (COALESCE(array_length(c.members,1),0)<1) then c.pass_1d
			when COALESCE(array_length(array_rm(um.members,c.members),1),0)>0 then null
			when (c.bet_1d+c.pool_1d+COALESCE(um.pass_1d,0)-c.intake_1d) < 0 then 0
			else c.bet_1d+c.pool_1d+COALESCE(um.pass_1d,0)-c.intake_1d end,
		0::float
	from usrr c
		left join um on um.date=c.date and um.user_id=c.user_id and um.game_id=c.game_id and um.num=c.num
		where COALESCE(array_length(c.members,1),0) > 0
	union select 
		u.date,
		u.user_id,
		u.member_of,
		array_rm(
			ARRAY[c.user_id::text]||array(select t.user_id::text from usrr t where t.date=c.date and t.member_of=u.user_id and t.game_id=u.game_id and t.num=u.num and COALESCE(array_length(t.members,1),0)<1)
		,u.members),
		u.game_id,u.num,
		u.big_intake,u.small_intake,u.intake_1d,
		u.big_bet,u.small_bet,u.bet_1d,
		u.big_pool+c.big_pass + COALESCE((select sum(big_pass) from usrr t where t.date=c.date and t.member_of=u.user_id and t.game_id=u.game_id and t.num=u.num and COALESCE(array_length(t.members,1),0)<1),0),
		u.small_pool+c.small_pass + COALESCE((select sum(small_pass) from usrr t where t.date=c.date and t.member_of=u.user_id and t.game_id=u.game_id and t.num=u.num and COALESCE(array_length(t.members,1),0)<1),0),
		u.pool_1d+c.pass_1d + COALESCE((select sum(pass_1d) from usrr t where t.date=c.date and t.member_of=u.user_id and t.game_id=u.game_id and t.num=u.num and COALESCE(array_length(t.members,1),0)<1),0),

		case when u.big_pool+ COALESCE((select sum(big_pass) from usrr t where t.date=c.date and t.member_of=u.user_id and t.game_id=u.game_id and t.num=u.num and COALESCE(array_length(t.members,1),0)<1),0) +
			c.big_pass+u.big_bet < u.big_intake then 0 else u.big_pool+ COALESCE((select sum(big_pass) from usrr t where t.date=c.date and t.member_of=u.user_id and t.game_id=u.game_id and t.num=u.num and COALESCE(array_length(t.members,1),0)<1),0)+
			c.big_pass+u.big_bet - u.big_intake end,
		case when u.small_pool+ COALESCE((select sum(small_pass) from usrr t where t.date=c.date and t.member_of=u.user_id and t.game_id=u.game_id and t.num=u.num and COALESCE(array_length(t.members,1),0)<1),0) +
			c.small_pass+u.small_bet < u.small_intake then 0 else u.small_pool+ COALESCE((select sum(small_pass) from usrr t where t.date=c.date and t.member_of=u.user_id and t.game_id=u.game_id and t.num=u.num and COALESCE(array_length(t.members,1),0)<1),0)+
			c.small_pass+u.small_bet - u.small_intake end,
		case when u.pool_1d+ COALESCE((select sum(pass_1d) from usrr t where t.date=c.date and t.member_of=u.user_id and t.game_id=u.game_id and t.num=u.num and COALESCE(array_length(t.members,1),0)<1),0) +
			c.pass_1d+u.bet_1d < u.intake_1d then 0 else u.pool_1d+ COALESCE((select sum(pass_1d) from usrr t where t.date=c.date and t.member_of=u.user_id and t.game_id=u.game_id and t.num=u.num and COALESCE(array_length(t.members,1),0)<1),0)+
			c.pass_1d+u.bet_1d - u.intake_1d end,
		depth+1
	from usrrup c, usrr u
		where c.date=u.date and c.member_of=u.user_id and u.game_id=c.game_id and u.num=c.num 
		and COALESCE(array_length(c.members,1),0) < 1
),
cfg as (select $1::text uid),
--cfg as (select 'AAG1001'::text uid),
mbr as (select id,create_date dt from m_user,cfg where id=cfg.uid),
fd as (select GREATEST(min(t.date),max(mbr.dt)) d from t_active_ticket t,mbr,cfg where mbr.id=cfg.uid),
_dr as(SELECT date_trunc('day', dd)::date dd
	FROM fd,generate_series (
	fd.d::timestamp,
	now()::timestamp,
	'1 day'::interval) dd),
dr as (select dd,cast(EXTRACT(ISODOW FROM dd) as int) d from _dr where EXTRACT(ISODOW FROM _dr.dd) = any(ARRAY[3,6,7])),
nums as (select dr.dd date,t.id ticket_id,game_id,UNNEST(numbers) num 
	from t_active_ticket t,dr,mbr m
	where t.draw_days @> ARRAY[dr.dd] and t.users @> ARRAY[m.id] and not (t.day_left @> ARRAY[dr.d])),
--ng as (select num from nums group by num),
ng as (select game_id,num from nums group by game_id,num),
prz as (select 
		s.draw_date,
		--t.id ticket_id,
		t.game_id,
		s.num,
		s.big_prize,
		s.small_prize,
		s.prize_1d
	from t_strike s
	--left join ng on s.num=ng.num
	left join t_active_ticket t on t.id = s.ticket_id
	inner join dr on s.draw_date=dr.dd-- and not (t.day_left @> ARRAY[dr.d])
	--inner join mbr m on t.users @> ARRAY[m.id]
	group by s.draw_date,
		t.game_id,
		s.num,
		s.big_prize,
		s.small_prize,
		s.prize_1d
	order by draw_date,game_id,num),
usr as (select 
		dr.dd date,
		t.id ticket_id,
		t.game_id,
		UNNEST(get_from(m.id,users)) user_id
	from t_active_ticket t
	inner join dr on t.draw_days @> ARRAY[dr.dd] and not (t.day_left @> ARRAY[dr.d])
	inner join mbr m on t.users @> ARRAY[m.id]
	order by date,ticket_id),
usr3 as(select 
	user_id,
	member_of
	from usr
	left join m_user u on usr.user_id=u.id
	group by user_id,member_of),
it as (select
		u.date,
		t.game_id game_id,
		u.user_id,
		max(COALESCE(cast(t.user_configs #>> ARRAY[u.user_id,'bit'] as float),0)) big_intake,
		max(COALESCE(cast(t.user_configs #>> ARRAY[u.user_id,'sit'] as float),0)) small_intake,
		max(COALESCE(cast(t.user_configs #>> ARRAY[u.user_id,'it'] as float),0)) intake_1D,
		max(COALESCE(cast(t.user_configs #>> ARRAY[u.user_id,'rb'] as float),0)) rebate,
		max(COALESCE(cast(t.user_configs #>> ARRAY[u.user_id,'sc'] as float),0)) sc
	from usr u
	inner join t_active_ticket t on u.ticket_id=t.id
	group by u.date,t.game_id,u.user_id
	order by u.date,t.game_id,u.user_id),
tu as (select 
		n.date,
		t.place_to user_id,
		t.game_id,
		n.ticket_id,
		n.num,
		COALESCE(cast(bets #>> ARRAY[n.num,'b'] as float),0) big_bet,
		COALESCE(cast(bets #>> ARRAY[n.num,'s'] as float),0) small_bet,
		COALESCE(cast(bets #>> ARRAY[n.num,'1D'] as float),0) bet_1D
	from nums n
	left join t_active_ticket t on t.id=n.ticket_id
	left join t_strike s on s.ticket_id=t.id and s.num=n.num and s.draw_date=n.date
	order by n.date,num),
tug as (select date,
	user_id,
	game_id, num,
	sum(big_bet) big_bet,
	sum(small_bet) small_bet,
	sum(bet_1d) bet_1d--,
	from tu
	group by date,user_id, game_id, num),
tuall as(select 
		usr.date,
		usr.user_id,
		u.member_of,
		array(select user_id::text from usr3 where member_of=usr.user_id) members,
		usr.game_id,
		ng.num,
		it.big_intake,
		it.small_intake,
		it.intake_1d,
		COALESCE(big_bet,0) big_bet,
		COALESCE(small_bet,0) small_bet,
		COALESCE(bet_1d,0) bet_1d,
		0::float big_pool,
		0::float small_pool,
		0::float pool_1d
	from usr
--	cross join ng
--	cross join cfg
--	left join m_user u on usr.user_id=u.id
--	left join it on it.user_id=usr.user_id and it.game_id=usr.game_id
--	left join tug t on t.user_id=usr.user_id and t.user_id=usr.user_id and t.num=ng.num
	inner join ng on usr.game_id=ng.game_id
	left join m_user u on usr.user_id=u.id
	left join it on it.date=usr.date and it.user_id=usr.user_id and it.game_id=usr.game_id and it.game_id=ng.game_id
	left join tug t on t.date=usr.date and t.user_id=usr.user_id and t.game_id=usr.game_id and t.num=ng.num and t.game_id=ng.game_id
	group by usr.date,usr.user_id,u.member_of,usr.game_id,ng.num,it.big_intake,it.small_intake,it.intake_1d,big_bet,small_bet,bet_1d),
tuallp as(select date,user_id,member_of,members,game_id,num,big_intake,small_intake,intake_1d,big_bet,small_bet,bet_1d,big_pool,small_pool,pool_1d,
	case when COALESCE(array_length(members,1),0)>=1 then null when big_bet<big_intake then 0 else big_bet-big_intake end big_pass,
	case when COALESCE(array_length(members,1),0)>=1 then null when small_bet<small_intake then 0 else small_bet-small_intake end small_pass,
	case when COALESCE(array_length(members,1),0)>=1 then null when bet_1d<intake_1d then 0 else bet_1d-intake_1d end pass_1d
	from tuall
),
tm as (select date,game_id,num,user_id,
	array(select user_id::text from tuallp t where t.date=c.date and t.user_id=any(c.members) and t.game_id=c.game_id and t.num=c.num and COALESCE(array_length(t.members,1),0)<1) members,
	(select sum(big_pass) from tuallp t where t.date=c.date and t.user_id=any(c.members) and t.game_id=c.game_id and t.num=c.num and COALESCE(array_length(t.members,1),0)<1) big_pass,
	(select sum(small_pass) from tuallp t where t.date=c.date and t.user_id=any(c.members) and t.game_id=c.game_id and t.num=c.num and COALESCE(array_length(t.members,1),0)<1) small_pass,
	(select sum(pass_1d) from tuallp t where t.date=c.date and t.user_id=any(c.members) and t.game_id=c.game_id and t.num=c.num and COALESCE(array_length(t.members,1),0)<1) pass_1d
from tuallp c),

summa as (select up.date,up.user_id,up.game_id,up.num,rebate,
		big_pass,small_pass,pass_1d,
		big_prize,small_prize,prize_1d,
		big_pass*COALESCE(big_prize,0) big_strike,
		small_pass*COALESCE(small_prize,0) small_strike,
		pass_1d*COALESCE(prize_1d,0) strike_1d,
		big_pass*COALESCE(big_prize,0)*sc/100 big_sc,
		small_pass*COALESCE(small_prize,0)*sc/100 small_sc,
		pass_1d*COALESCE(prize_1d,0)*sc/100 sc_1d
	from usrrup up
		inner join cfg on up.user_id=cfg.uid or up.member_of=cfg.uid
		inner join it on it.date=up.date and it.game_id=up.game_id and it.user_id=up.user_id
		left join prz p on p.draw_date=up.date and p.game_id=up.game_id and p.num=up.num
		where COALESCE(array_length(members,1),0)<1
	union select up.date,up.user_id,up.game_id,up.num,rebate,
		big_pass,small_pass,pass_1d,
		big_prize,small_prize,prize_1d,
		big_pass*COALESCE(big_prize,0) big_strike,
		small_pass*COALESCE(small_prize,0) small_strike,
		pass_1d*COALESCE(prize_1d,0) strike_1d,
		big_pass*COALESCE(big_prize,0)*sc/100 big_sc,
		small_pass*COALESCE(small_prize,0)*sc/100 small_sc,
		pass_1d*COALESCE(prize_1d,0)*sc/100 sc_1d
	from usrr up
		inner join cfg on up.user_id=cfg.uid or up.member_of=cfg.uid
		inner join it on it.date=up.date and it.game_id=up.game_id and it.user_id=up.user_id
		left join prz p on p.draw_date=up.date and p.game_id=up.game_id and p.num=up.num
		where COALESCE(array_length(members,1),0)<1),

bydate as (select date,user_id,game_id,rebate,
	sum(big_pass) big_pass,sum(small_pass) small_pass,sum(pass_1d) pass_1d,

	sum(big_pass)*rebate/100 big_rebate,
	sum(small_pass) * rebate/100 small_rebate,
	sum(pass_1d)*rebate/100 rebate_1d,
	
	sum(big_pass)-sum(big_pass) * rebate/100 big_less_rebate,
	sum(small_pass) - sum(small_pass) * rebate/100 small_less_rebate,
	sum(pass_1d)-sum(pass_1d)*rebate/100 less_rebate_1d,

	sum(big_strike) big_strike,sum(small_strike) small_strike, sum(strike_1d) strike_1d,
	sum(big_sc) big_sc,sum(small_sc) small_sc, sum(sc_1d) sc_1d
	from summa group by date,user_id,game_id,rebate),
pu as(select user_id,
		sum(big_pass) big_pass,sum(small_pass) small_pass,sum(pass_1d) pass_1d,
		sum(big_rebate) big_rebate,
		sum(small_rebate) small_rebate,
		sum(rebate_1d) rebate_1d,
		sum(big_less_rebate) big_less_rebate, sum(small_less_rebate) small_less_rebate, sum(less_rebate_1d) less_rebate_1d,
		sum(big_strike) big_strike,sum(small_strike) small_strike, sum(strike_1d) strike_1d,
		sum(big_sc) big_sc,sum(small_sc) small_sc, sum(sc_1d) sc_1d
	from bydate group by user_id order by user_id),
pm as(select p.user_id,sum(amount) amount 
	from t_payment p,m_user u, cfg
	where p.user_id=u.id and (u.id=cfg.uid or u.member_of=cfg.uid)
	group by user_id)
select ((select COALESCE(sum((big_less_rebate+small_less_rebate+less_rebate_1d)-(big_strike+small_strike+strike_1d-big_sc-small_sc-sc_1d)),0) from pu,cfg where user_id <>cfg.uid) - 
	(select COALESCE(sum((big_less_rebate+small_less_rebate+less_rebate_1d)-(big_strike+small_strike+strike_1d-big_sc-small_sc-sc_1d)),0) from pu,cfg where user_id =cfg.uid)) +
	COALESCE((select sum(amount) from pm,cfg where user_id=cfg.uid),0) -
	COALESCE((select sum(amount) from pm,cfg where user_id<>cfg.uid),0)

$BODY$
  LANGUAGE sql STABLE`;

	let client = new pg.Client(dbconfig);
	client.connect(function(e){
        if(e) return done(e);

		init.afterDbConnect(client,()=>{
			console.log('db connected');
			//client.query(`ALTER TABLE m_user ADD COLUMN login_id character varying(50)`,(e)=>{
			//	console.log('user...');
			//	done(e);
			//});
			//client.query(`ALTER TABLE t_payment ADD COLUMN by_user character varying(50)`,(e)=>{
			//	console.log('payment...');
			//	done(e);
			//});
			
			client.query(q,(e,r)=>{
				console.log('v balance...');
				done(e);
			});
			
			//client.query('update m_config set value=$1 where id=$2 returning *',[{"close":"18:15","draw":"21:00"},'bet_time'],(e,r)=>{
			//	r.rows.forEach(v=>{
			//		console.log(v.id+':'+v.value);
			//	});
			//	done(e);
			//});
		
			//client.query(`ALTER TABLE t_strike ADD COLUMN prize_1d numeric DEFAULT 0`,(e)=>{
			//	console.log('t_strike...');
			//	done(e);
			//});
			
			//let change=[];
			//client.query('select id,config from m_user where type=$1',['agent'],(e,r)=>{
			//	r.rows.forEach(v=>{
			//		//console.log(JSON.stringify(v));
			//		let cfg=JSON.parse(JSON.stringify(v.config));
			//		['4D','3D','2D'].forEach(g=>{
			//			cfg[g].bit=cfg[g].it;
			//			cfg[g].sit=cfg[g].it;
			//		});
			//		change.push({
			//			id:v.id,
			//			cfg
			//		});
			//	});
			//	//console.log('s=>',change);
			//	change.forEach(v=>{
			//		client.query('update m_user set config=$2 where id=$1',[v.id,v.cfg],(e)=>{
			//			if(e) console.log(new Error(e));
			//		});
			//	});
			//	done(e);
			//});
			
			
			//client.query('select * from t_active_ticket',(e,r)=>{
			//	r.rows.forEach(v=>{
			//		let {id,date,day_list}=v,
			//			ldate=new Date(date);
			//		//	console.log(ldate.getDay());
			//		Game.getDrawDate((e,dd)=>{
			//			let draw_days=day_list.map(sd=>{
			//				return dateUtil.toDbDate(new Date(dd[sd.toString()].close));
			//			});
			//			
			//			client.query('update t_active_ticket set draw_days=$2 where id=$1',[id,draw_days],()=>{});
			//			//console.log('dd==>',dd);
			//			console.log('dds=>',draw_days);
			//			console.log({id,date,day_list});
			//			
			//		},client,ldate);
			//	});
			//	
			//	
			//	client.query('select b.id,b.draw_day,t.date from t_current_balance b left join t_active_ticket t on t.id=b.ticket_id',(e,r)=>{
			//		r.rows.forEach(v=>{
			//			let {id,date,draw_day}=v,
			//				ldate=new Date(date);
			//			//	console.log(ldate.getDay());
			//			Game.getDrawDate((e,dd)=>{
			//				//let draw_days=day_list.map(sd=>{
			//				//	return dateUtil.toDbDate(new Date(dd[sd.toString()].close));
			//				//});
			//				let draw_date=dateUtil.toDbDate(new Date(dd[draw_day.toString()].close));
			//				client.query('update t_current_balance set draw_date=$2 where id=$1',[id,draw_date],()=>{});
			//				//console.log('dd==>',dd);
			//				console.log('dds=>',draw_date);
			//				console.log({id,date,draw_day});
			//				
			//			},client,ldate);
			//		});
			//		//done(e);
			//	});
			//	
			//});
			//let change=[];
			//client.query(`select id,config from m_user where type='sagent' or type='agent' or type='player'`,[],(e,r)=>{
			//	r.rows.forEach(v=>{
			//		console.log(JSON.stringify(v));
			//		let cfg=JSON.parse(JSON.stringify(v.config));
			//		['4D','3D','2D'].forEach(g=>{
			//			if(cfg[g]){
			//				cfg[g].it=parseFloat(cfg[g].it||0);
			//				cfg[g].bit=parseFloat(cfg[g].it||0);
			//				cfg[g].sit=parseFloat(cfg[g].it||0);
			//				cfg[g].rb=parseFloat(cfg[g].rb||0);
			//				cfg[g].sc=parseFloat(0);
			//			}
			//		});
			//		change.push({
			//			id:v.id,
			//			cfg
			//		});
			//	});
			//	//console.log('s=>',change);
			//	change.forEach(v=>{
			//		client.query('update m_user set config=$2 where id=$1',[v.id,v.cfg],(e)=>{
			//			if(e) console.log(new Error(e));
			//		});
			//	});
			//	//done(e);
			//});
			
			//let prize={"4D":{"normal":{"big":{"1":2000,"2":1000,"3":490,"s":250,"c":60},"small":{"1":3000,"2":2000,"3":800}},"i":{"big":{"4":{"1":500,"2":250,"3":127,"s":62,"c":15},"6":{"1":335,"2":168,"3":85,"s":41,"c":10},"12":{"1":166,"2":83,"3":40,"s":20,"c":6},"24":{"1":83,"2":41,"3":20,"s":10,"c":3}},"small":{"4":{"1":750,"2":500,"3":200},"6":{"1":500,"2":333,"3":133},"12":{"1":250,"2":167,"3":66},"24":{"1":125,"2":83,"3":33}}}},"3D":{"normal":{"big":{"1":400,"2":200,"3":100},"small":{"1":700}}},"2D":{"normal":{"big":{"1":40,"2":20,"3":10},"small":{"1":70}}},"1D":{"2":1.95}};
			//client.query('update m_prize set prize=$1 where id=$2 returning *',[prize,1],(e,r)=>{
			//	console.log(r.rows[0]);
			//	done(e);
			//});
		});

		//client.query(q,(e,r)=>{
		//	console.log('bersih2...');
		//	done(e);
		//});
		
		//client.query('ALTER TABLE t_current_balance ADD COLUMN draw_date date',(e)=>{
		//	if(e) return console.log(e);
		//	client.query('ALTER TABLE t_active_ticket ADD COLUMN draw_days date[]',(e)=>{
		//		if(e) return console.log(e);
		//	});
		//});


		//client.query(q, [],e=>{
		//	if(e) return done(e);
		//	console.log('game');
		//	client.query('select * from m_game',(e,r)=>{
		//		r.rows.forEach(v=>{
		//			console.log(v.id+':'+v.validate);
		//		});
		//		done(e);
		//	});
		//});
		
		//client.query('select * from m_game',(e,r)=>{
		//	r.rows.forEach(v=>{
		//		console.log(v.id+':'+v.validate);
		//	});
		//	done(e);
		//});
    
		//client.query(q, [],e=>{
		//	if(e) return done(e);
		//	//console.log('game');
		//	client.query('update m_config set value=$1 where id=$2 returning *',[{"close":"17:45","draw":"18:00"},'bet_time'],(e,r)=>{
		//		r.rows.forEach(v=>{
		//			console.log(v.id+':'+v.value);
		//		});
		//		done(e);
		//	});
		//});

	
//  client.query('ALTER TABLE t_current_balance ADD COLUMN payment_id bigint',(e)=>{
//      if(e) return console.log(e);
//			console.log('t_current_balance sudah nambah kolom');
//		});
//	});
	//client.query(`update t_active_ticket set permute_length=1 where game_id='1D'`,(e)=>{
	//	if(e) return console.log(e);
	//		  console.log('permute_length updated');
	//	});
	
	});
}

module.exports=function(sub){
	if(!sub) return help();
	if(sub=='alternotif'){
		alterNotif();
	} else {
		return help();
	}
};