let {print,done}=require('./print');

function help(){
	print('command (redis <argument>), where argument is one of:',{
		clearuser:'remove all user in redis',
		clearsession:'remove all session',
		clearprize:'remove all prize',
	});
}

module.exports=function(sub){
	if(!sub) return help();
	if(sub=='clearuser'){
		let redisUser=require('../lib/redis').user;
		return redisUser.flushdb(e=>{
			if(e) return done(e);
			done();
		});
	} else if(sub=='clearsession'){
		let _redisSession=require('../lib/redis')._session;
		return _redisSession.flushdb(e=>{
			if(e) return done(e);
			done();
		});
	} else if(sub=='clearprize'){
		let redisPrize=require('../lib/redis').prize;
		return redisPrize.flushdb(e=>{
			if(e) return done(e);
			done();
		});
	} else {
		return help();
	}
};