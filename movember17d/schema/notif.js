let n={};
n.DOWNLINE_FAKE_LOGIN={
	type:'object',
	properties:{
		user_id:{type:'string'},
		by_system:{
			type:'boolean',
			default:true
		},
		params:{
			type:'object',
			properties:{
				downline_id:{type:'string'}
			},
			required: ['downline_id']
		}
	},
	required: ['user_id','params']
};

n.MEMBER_INVALID_PASSWORD={
	type:'object',
	properties:{
		user_id:{type:'string'},
		by_system:{
			type:'boolean',
			default:true
		},
		params:{
			type:'object',
			properties:{
				member_id:{type:'string'}
			},
			required: ['member_id']
		}
	},
	required: ['user_id','params']
};

n.PROFILE_UPDATE={
	type:'object',
	properties:{
		user_id:{type:'string'},
		by_id:{type:'string'},
		by_system:{type:'boolean',default:false},
		params:{
			type:'object',
		}
	},
	required: ['user_id','by_id','params']
};

n.MEMBER_PROFILE_UPDATE={
	type:'object',
	properties:{
		user_id:{type:'string'},
		by_id:{type:'string'},
		by_system:{type:'boolean',default:false},
		params:{
			type:'object',
			properties:{
				member_id:{type:'string'},
				change:{type:'object'}
			},
			required: ['member_id','change']
		}
	},
	required: ['user_id','by_id','params']
};

n.PLACE_BET={
	type:'object',
	properties:{
		user_id:{type:'string'},
		by_id:{type:'string'},
		by_system:{type:'boolean',default:false},
		params:{
			type:'object',
			properties:{
				ticket_id:{type:'string'}
			},
			required: ['ticket_id']
		}
	},
	required: ['user_id','by_id','params']
};

n.PLACE_FIX_BET={
	type:'object',
	properties:{
		user_id:{type:'string'},
		by_system:{type:'boolean',default:true},
		params:{
			type:'object',
			properties:{
				ticket_id:{type:'string'}
			},
			required: ['ticket_id']
		}
	},
	required: ['user_id','params']
};

n.ADD_MEMBER={
	type:'object',
	properties:{
		user_id:{type:'string'},
		by_id:{type:'string'},
		by_system:{type:'boolean',default:false},
		params:{
			type:'object',
			properties:{
				id:{type:'string'},
			},
			required: ['id']
		}
	},
	required: ['user_id','by_id','params']
};

n.STRIKE={
	type:'object',
	properties:{
		user_id:{type:'string'},
		by_system:{type:'boolean',default:true},
		params:{
			type:'object',
			properties:{
				ticket_id:{type:'string'},
			},
			required: ['ticket_id']
		}
	},
	required: ['user_id','params']
};

n.DOWNLINE_STRIKE={
	type:'object',
	properties:{
		user_id:{type:'string'},
		by_system:{type:'boolean',default:true},
		params:{
			type:'object',
			properties:{
				ticket_id:{type:'string'},
				downline_id:{type:'string'},
			},
			required: ['ticket_id','downline_id']
		}
	},
	required: ['user_id','params']
};

n.STATUS_UPDATE={
	type:'object',
	properties:{
		user_id:{type:'string'},
		by_id:{type:'string'},
		by_system:{type:'boolean',default:false},
		description:{type:'string'},
		params:{
			type:'object',
			properties:{
				status:{type:'string'},
			},
			required: ['status']
		}
	},
	required: ['user_id','by_id','params']
};

n.MEMBER_STATUS_UPDATE={
	type:'object',
	properties:{
		user_id:{type:'string'},
		by_id:{type:'string'},
		description:{type:'string'},
		by_system:{type:'boolean',default:false},
		params:{
			type:'object',
			properties:{
				member_id:{type:'string'},
				status:{type:'string'},
			},
			required: ['status','member_id']
		}
	},
	required: ['user_id','by_id','params']
};

n.DOWNLINE_STATUS_UPDATE={
	type:'object',
	properties:{
		user_id:{type:'string'},
		by_id:{type:'string'},
		description:{type:'string'},
		by_system:{type:'boolean',default:false},
		params:{
			type:'object',
			properties:{
				downline_id:{type:'string'},
				status:{type:'string'},
			},
			required: ['status','downline_id']
		}
	},
	required: ['user_id','by_id','params']
};

n.CREDIT_UPDATE={
	type:'object',
	properties:{
		user_id:{type:'string'},
		by_id:{type:'string'},
		by_system:{type:'boolean',default:false},
		description:{type:'string'},
		params:{
			type:'object',
			properties:{
				type:{type:'string',enum:['increase','decrease','edit']},
				amount:{type:'number'}
			},
			required: ['type','amount']
		}
	},
	required: ['user_id','by_id','params']
};

n.MEMBER_CREDIT_UPDATE={
	type:'object',
	properties:{
		user_id:{type:'string'},
		by_id:{type:'string'},
		description:{type:'string'},
		by_system:{type:'boolean',default:false},
		params:{
			type:'object',
			properties:{
				member_id:{type:'string'},
				type:{type:'string',enum:['increase','decrease','edit']},
				amount:{type:'number'}
			},
			required: ['type','member_id','amount']
		}
	},
	required: ['user_id','by_id','params']
};

n.DOWNLINE_CREDIT_UPDATE={
	type:'object',
	properties:{
		user_id:{type:'string'},
		by_id:{type:'string'},
		description:{type:'string'},
		by_system:{type:'boolean',default:false},
		params:{
			type:'object',
			properties:{
				downline_id:{type:'string'},
				type:{type:'string',enum:['increase','decrease','edit']},
				amount:{type:'number'}
			},
			required: ['type','downline_id','amount']
		}
	},
	required: ['user_id','by_id','params']
};

module.exports=n;