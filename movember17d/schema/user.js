var user_game_config={
	type:'object',
	default:{},
	properties:{
		rb:{
			type:'number',
			default:0
		},
		it:{
			type:'number',
			default:0
		},
		bit:{
			type:'number',
			default:0
		},
		sit:{
			type:'number',
			default:0
		},
		//sc:{
		//	type:'object',
		//	'patternProperties': {
		//		"^[0-9]{2,4}$": {
		//		  type:'number',
		//		  default:0
		//		}
		//	},
		//	additionalProperties:false
		//},
		sc:{
			type:'number',
			default:0
		},
	}
};

var user_game1d_config={
	type:'object',
	default:{},
	properties:{
		it:{
			type:'number',
			default:0
		},
	}
};

var user_config={
	type:'object',
	default:{},
	properties:{
		'4D':user_game_config,
		'3D':user_game_config,
		'2D':user_game_config,
		'1D':user_game1d_config,
	},
	additionalProperties: false
};

var agent_config={
	type:'object',
	default:{},
	properties:{
		'ci':{type:'boolean',default: false},
		'ca':{type:'boolean',default: false},
		'pb':{type:'boolean',default: false},
	}
};

var def_props={
	type:'object',
	properties:{
		id:{type:'string',maxLength: 50},
		name:{type:'string',maxLength: 100},
		password:{type:'string',minLength: 3,maxLength: 100},
		fake_password:{type:'string',minLength: 3,maxLength: 100},
		type:{type:'string','enum': ["dev", "sadmin", "admin","sagent","agent","player"]},
		member_of:{type:'string',maxLength: 50},
		active:{type:'boolean',default: true},
		status:{type:'string','enum': ["normal", "suspended", "locked","terminated"],default:'normal'},
		credit:{type:'number',default:0},
		can_play:{type:'boolean',default:false},
		agent_config:agent_config,
		config:user_config,
		create_by:{type:'string',maxLength: 50},
	},
	required:['id','password','fake_password','type'],
	additionalProperties: false
};

var user=def_props,
	agent=Object.assign({},def_props,{
		required:['id','password','fake_password','type','member_of'],
	}),
	player=Object.assign({},def_props,{
		properties:Object.assign({},def_props.properties,{
			can_play:{type:'boolean',default:true},
		}),
		required:['id','password','fake_password','type','member_of'],
	});

//console.log(JSON.stringify(user));

module.exports={
	user,
	agent,
	player,
	agent_config,
	user_config,	
};
