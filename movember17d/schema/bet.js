let n={};
n['1D']={
	type:'object',
	properties:{
		day_id:{type:'string',pattern: '^[1-7]$'},
		form_type: {type:'string',enum:['1D']},
		detail:{
			type:'array',
			items: {
				type: 'object',
				properties:{
					n:{
						type:'string',
						pattern: '^[oebs]$'
					},
					m:{
						type:'string',
						pattern: '^[123]$'
					},
					a:{type:'number'}
				},
				additionalProperties: false,
				minProperties: 3
			},
			minItems:1
		}
	},
	required: ['day_id','form_type','detail']
};


n.normal={
	type:'object',
	properties:{
		place_to: {type:'string'},
		form_type: {type:'string',enum:['normal','mass','wildcard']},
		day_id:{type:'string',pattern: '^[1-7]$'},
		detail:{
			type:'array',
			minItems:1,
			items: {
				type: 'object',
				properties:{
					g:{type:'string',enum:['4D','3D','2D']},
					n:{type:'string'/*,pattern: '^[0-9]{2,4}$' */},
					b:{type:'number',default:0},
					s:{type:'number',default:0},
					bt:{type:'string',enum:['-','R','I','*']},
				},
				additionalProperties: false,
				minProperties: 3
			},
		}
	},
	required: ['day_id','form_type','detail']
};

n.fixed=JSON.parse(JSON.stringify(n.normal));
delete n.fixed.properties.day_id;
n.fixed.properties.form_type={type:'string',enum:['fixed']},
n.fixed.required=['form_type','detail'];

var item=n.normal.properties.detail.items;

n['2Ditem']=JSON.parse(JSON.stringify(item));
n['2Ditem'].properties.n.pattern='^[0-9]{2}$';

n['3Ditem']=JSON.parse(JSON.stringify(item));
n['3Ditem'].properties.n.pattern='^[0-9]{3}$';

n['4Ditem']=JSON.parse(JSON.stringify(item));
n['4Ditem'].properties.n.pattern='^[0-9]{4}$';

n['2D*item']=JSON.parse(JSON.stringify(item));
n['2D*item'].properties.n={
	type:'string',
	pattern:'^\\d*\\*\\d*$',
	minLengt: 2,
	maxLength: 2
};

n['3D*item']=JSON.parse(JSON.stringify(item));
n['3D*item'].properties.n={
	type:'string',
	pattern:'^\\d*\\*\\d*$',
	minLengt: 3,
	maxLength: 3
};

n['4D*item']=JSON.parse(JSON.stringify(item));
n['4D*item'].properties.n={
	type:'string',
	pattern:'^\\d*\\*\\d*$',
	minLengt: 4,
	maxLength: 4
};

module.exports=n;