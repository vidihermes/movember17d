require('json5/lib/require');
require('prototypes');
var express = require('express'),
  path = require('path'),
  favicon = require('serve-favicon'),
  logger = require('morgan'),
  passport = require('passport'),
  //cookieParser = require('cookie-parser'),
  cors = require('cors'),
  bodyParser = require('body-parser'),
  routes = require('./routes/index'),
  devAddress = require('./config/dev.json5').devAddress,
  sessionMiddleware = require('./lib/session');

var app = express();
//app.disable('etag');
app.set('view engine', 'ejs');
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json({type: '*/*'}));
app.use(cors());
app.use(logger('dev'));
//app.use(cookieParser());
app.use(sessionMiddleware);
app.use(passport.initialize());
app.use(passport.session());
app.use('/api', routes);
app.use('/',
  function (req, res, next) {
    if (req.isAuthenticated()) return next();
    return res.render('smokescreen', {});
  },
  function (req, res, next) {
    if (req.user) {
      if (['developer', 'sadmin', 'admin'].indexOf(req.user.type) >= 0) return next();
      if (!req.session.agree) {
        var md = require('markdown-it')({
          html: true,
          linkify: true,
          typographer: true
        });
        var fs = require('fs');
        return res.render('agreement', {
          heading: 'Welcome ' + req.user.id,
          type: 'html',
          description: 'You must agree to the following rules and regulations in order to proceed to our site',
          content: fs.readFileSync(__dirname + '/msg/agreement/en.html', 'utf8')
        });
        //return res.render('agreement', {
        //	heading:'Welcome '+req.user.id,
        //	description:'You must agree to the following rules and regulations in order to proceed to our site',
        //	content:md.render(fs.readFileSync(__dirname+'/msg/agreement/en.md','utf8'))
        //});
      }
      return next();
    }
    return res.status(401).json({s: 0, e: 'user not found'});
  },
  function (req, res) {
    var isdevmod = process.env.dev || false;
    console.log(isdevmod);
    if (req.user.type === 'agent' || req.user.type === 'player') {
      //USER_ID=239482 USER_KEY=foobar node app.js
      if (isdevmod) return res.redirect(devAddress + ':3002');
      return res.render('agent', {});
    } else if (['developer', 'sadmin', 'admin'].indexOf(req.user.type) >= 0) {
      if (isdevmod) return res.redirect(devAddress + ':3003');
      return res.render('admin', {});
    } else if (req.user.type === 'sagent') {
      if (isdevmod) return res.redirect(devAddress + ':3004');
      return res.render('sagent', {});
    }
    res.send({t: req.user.type});
  }
);

app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

if (app.get('env') === 'development') {
  app.use(function (err, req, res) {
    res.status(err.status || 500);
    console.log('error=>', err);
    res.send({
      s: 0,
      err: err.message
    });
  });
}
app.use(function (err, req, res) {
  res.status(err.status || 500);
  res.send({
    s: 0,
    err: err.message
  });
});

module.exports = app;