## Regulations

**movember17d system payout**

| Prizes	       | 1 Big	       | 1 Small   |
| ---------------- | ------------- | --------- |
| 1st Prize *      | \$4000.00     | \$3000.00 |
| 2nd Prize *      | \$2000.00     | \$2000.00 |
| 3rd Prize *      | \$1000.00     | \$1000.00 |
| 10 Starters *    | \$500.00 each |	       |
| 10 Consolations *| \$150.00 each |	       |

**i-bet system payout**
<table>
<thead>
<tr><th>Prizes</th><th colspan=4>1 Big</th><th colspan=4>1 Small</th></tr>
<tr><th></th><th>24 Roll</th><th>12 Roll</th><th>6 Roll</th><th>4 Roll</th><th>24 Roll</th><th>12 Roll</th><th>6 Roll</th><th>4 Roll</th></tr>
</thead>
<tbody>
<tr><td>1st Prize*</td><td>$166.66</td><td>$333.33</td><td>$666.66</td><td>$1,000.00</td><td>$125.00</td><td>$250.00</td><td>$500.00</td><td>$750.00</td></tr>
<tr><td>2nd Prize*</td><td>$83.33</td><td>$166.66</td><td>$333.33</td><td>$500.00</td><td>$83.33</td><td>$166.66</td><td>$333.33</td><td>$500.00</td></tr>
<tr><td>3rd Prize*</td><td>$41.66</td><td>$83.33</td><td>$166.66</td><td>$250.00</td><td>$41.66</td><td>$83.33</td><td>$166.66</td><td>$250.00</td></tr>
<tr><td>Starter*</td><td>$20.83</td><td>$41.66</td><td>$83.33</td><td>$125.00</td><td></td><td></td><td></td><td></td></tr>
<tr><td>Consolation*</td><td>$6.25</td><td>$12.50</td><td>$25.00</td><td>$37.50</td><td></td><td></td><td></td><td></td></tr>
</tbody>
</table>

\* *commissions not deducted*

### General Rules

Players must be at least 18 years of age. movember17d is not responsible for any players using our website. movember17d absolutely holds no responsibility for any national laws, federal laws or international laws infringed by any players using our website. Players are responsible for their own action for that matter and to bind by their own countries laws, whether it involved with any states laws or legal issues. Any attempts of fraud will render your account void. movember17d will not pay the prizes or winnings to which a player might otherwise be entitled if their payment is dishonored in anyway and for whatsoever reason. We reserve the right to cancel all outstanding entries for such players and prevent them from playing at our own discretion. We also reserve the right to refuse entries. All Agents are responsible for their respective members accounts and have to honor all bets submitted by them. Agents are not partner of movember17d. Any discrepancies that arise between Players and Agents are not the responsibility of movember17d. movember17d will not responsible for any stolen passwords due to players or agents own negligence or any other party involvement. All players are responsible for their own accounts and any transactions exchanged through their accounts. By way of log-in and placing your bet, you hereby agreed with our Rules and Regulations and declared that you are of legal age to participate in the betting and also agreed that movember17d will not hold any responsibilities or any liabilities with any legal issues that you as a Player have gotten involved into.

### Gaming

Red Numbers are numbers that are full in our data base and are paid acconding to market rates. movember17d do not guaranteed any minimum  payout for Red Number. movember17d has the final decision and it will be deem final. There shall be no further claims from either parties thereafter. We do not entertain any change of bets, requests for refund of bets or any other objections that may arise due to Players negligence or errors during the course of submission. This website shall not be construed to form any part, whether express or implied, of the terms and condition of the Games.

### Limits and Liabilities

The decision of movember17d will be final and binding in all matters and no correspondence will be entered into. movember17d will not be liable for any loss of whatever nature arising from the cancellation of any matches or draws, including the loss for whatsoever reason whether the chance to participate in a bet. Without prejudice to the generality of the foregoing, movember17d will not be liable to any person in the event of force majeure, and or for the events of failure or damage or destruction to movember17d central computer system or records or any part thereof, or delays, losses, errors, omissions or any others thereof resulting from failure of any telecommunications or any other data transmission system, or any delay resulting in non receipt of any entry for a particular bet or draw. movember17d will not be liable for any loss as a result of an act of God, in an outbreak of hostilities, riot, civil unrest, act of terrorism, act of any government or authority (including refusal or revocation of any license or consent), fire explosion, flood, theft, malicious damage, strike, lock-out and industrial action of any kind. The Parties shall not commit nor purport to commit the other to honor any obligation other than is specifically provided for by these Rules and Regulations. movember17d reserves the right to amend these Rules and Regulations from time to time and vary any of them without prior notice.

### Discrepancy

In the event there is discrepancy between the bets that the Player claimed and the data base of movember17d, those in the data base maintained by movember17d will prevail and considered final and valid. The responsibility for entries, submit, confirmed and acceptance for any game bets lay in the hand of the Players at all time. Bets submitted after the closing time will be deem null and void unless specified. These Rules and Regulations constitute the entire agreement and understanding between the parties. If there is any discrepancy between English language version of these Rules and Regulations and any other language version the English language version supercede the other languages and is deem correct and valid.

### Disclaimer

Whilst every effort is made to ensure the accuracy of all information published at this site. movember17d asserts that it would not be responsible for whatsoever cause by any inaccuracies of it published results, errors, omissions, misleading, out of date information or any others distortion. movember17d is not in any way associated to any government body or private companies, committees, institutes or organizations dueling with lottery draw. 