var pg=require('pg'),
    config={
        user: 'postgres',
        host: '35.201.173.228',
        database: 'postgres',
        password: 'kkFrgk2uO48046w6',
    },
    client = new pg.Client(config),
    client2 = new pg.Client(Object.assign({},config,{database: 'movember17d'}));

function createDb(cb){
    client.connect(function(e){
        if(e) return console.error(new Error(e));
        client.query('CREATE DATABASE movember17d', [], function(e){
            if(e) return console.error(new Error(e));
            cb();
        });
    });    
}

function clearTable(cb){
    client2.query('SELECT * FROM pg_catalog.pg_tables where schemaname = $1;', ['public'], function(e,r){
        if(e) return console.error(new Error(e));
        if(r && r.rows){
            var s='';
            r.rows.forEach(function(t){
                console.log(t.tablename);
                s+='DROP TABLE IF EXISTS '+t.tablename+';';
            });
            client2.query(s,[],function(e2){
                if(e2) return console.error(new Error(e2));
                cb();
            });
        }
    });
}

function initTable(){
    client2.connect(function(e){
        if(e) return console.error(new Error(e));
        clearTable(createTable);
    });    
}

function go(create){
    if(create) createDb(initTable)
    else initTable();
}

function createTable(){
    var q=`
CREATE TABLE m_config
(
  id character varying(50) NOT NULL,
  value json,
  last_update timestamp without time zone,
  update_by character varying(50),
  CONSTRAINT m_config_pkey PRIMARY KEY (id)
);

CREATE TABLE m_game
(
  id character varying(50) NOT NULL,
  name character varying(50),
  validate text,
  rebate_min numeric DEFAULT 0,
  rebate_max numeric DEFAULT 0,
  intake_min numeric DEFAULT 0,
  intake_max numeric DEFAULT 0,
  rebate_default numeric DEFAULT 0,
  intake_default numeric DEFAULT 0,
  strike_comm_min numeric DEFAULT 0,
  strike_comm_max numeric DEFAULT 0,
  strike_comm_default numeric DEFAULT 0,
  CONSTRAINT m_game_pkey PRIMARY KEY (id)
);

CREATE TABLE m_id
(
  _id bigserial NOT NULL,
  id bigint,
  created timestamp without time zone DEFAULT now(),
  prefix character varying(10),
  CONSTRAINT m_id_pkey PRIMARY KEY (_id)
);

CREATE TABLE m_prize
(
  id bigserial NOT NULL,
  create_date timestamp without time zone DEFAULT now(),
  apply_date timestamp without time zone,
  prize json,
  CONSTRAINT m_prize_pkey PRIMARY KEY (id)
);

CREATE TABLE m_user
(
  id character varying(50) NOT NULL,
  name character varying(100),
  type character varying(20),
  member_of character varying(50),
  active boolean DEFAULT true,
  status character varying(20),
  pass_salt character varying(100),
  pass_sha character varying(255),
  fake_pass_salt character varying(100),
  fake_pass_sha character varying(255),
  create_date timestamp without time zone DEFAULT now(),
  create_by character varying(50),
  last_login timestamp without time zone,
  update_by character varying(50),
  last_update timestamp without time zone,
  login_ip character varying(50),
  login_err smallint DEFAULT 0,
  can_play boolean DEFAULT true,
  credit numeric DEFAULT 0,
  agent_config json,
  config json DEFAULT '{}'::json,
  CONSTRAINT pk_user_id PRIMARY KEY (id)
);

CREATE TABLE t_balance
(
  id bigserial NOT NULL,
  user_id character varying(50),
  date timestamp without time zone DEFAULT now(),
  amount numeric DEFAULT 0,
  type character varying(50), -- bet, downline_bet, strike,downline_strike
  ticket_id bigint,
  detail json DEFAULT '{}'::json,
  paid boolean DEFAULT false,
  CONSTRAINT t_balance_pkey PRIMARY KEY (id)
);

CREATE TABLE t_ticket
(
  id bigserial NOT NULL,
  date timestamp without time zone DEFAULT now(),
  place_to character varying(50),
  place_by character varying(50),
  day_id character varying(50),
  form_type character varying(50) DEFAULT 'normal'::character varying, -- normal,mass,wildcard,1D
  fix_id bigint,
  day_left smallint[],
  detail json DEFAULT '{}'::json, -- normal detail :...
  prize_id bigint,
  users character varying[],
  numbers character varying[], -- all bet and uniq permutation bet number
  user_configs json DEFAULT '{}'::json,
  CONSTRAINT t_ticket_pkey PRIMARY KEY (id)
);

CREATE TABLE t_user_notif
(
  id serial NOT NULL,
  user_id character varying(50),
  by_id character varying(50),
  by_system boolean DEFAULT false,
  date timestamp without time zone DEFAULT now(),
  type character varying(50),
  description character varying(255),
  params json,
  CONSTRAINT pk_user_notif PRIMARY KEY (id)
);

-- DATA
-- m_config
insert into m_config (id,value) values
    ('bet_time','{"close":"14:20","draw":"15:00"}'::json),
    ('default_credit','{"agent":10000,"player":10000}'::json),
    ('draw_day','{"1":["Wed","Sat","Sun"],"2":["Sat","Sun"],"3":["Wed"],"6":["Sat"],"7":["Sun"]}'::json);

-- m_game
insert into m_game(id,validate,rebate_min,rebate_max,rebate_default,intake_min,intake_max,intake_default) values
    ('1D','^(\\d|ODD|EVEN|BIG|SMALL)$',0,10,0,0,5,0),
    ('2D','^\\d{2}',0,10,0,0,5,0),
    ('3D','^\\d{3}',0,10,0,0,5,0),
    ('4D','^\\d{4}',0,20,0,0,5,0);

-- m_prize
insert into m_prize(apply_date,prize) values
    (now(),'{"4D":{"normal":{"big":{"1":2000,"2":1000,"3":490,"s":250,"c":60},"small":{"1st":3000,"2nd":2000,"3rd":800}},"i":{"big":{"4":{"1":500,"2":250,"3":127,"s":62,"c":15},"6":{"1":335,"2":168,"3":85,"s":41,"c":10},"12":{"1":166,"2":83,"3":40,"s":20,"c":6},"24":{"1":83,"2":41,"3":20,"s":10,"c":3}},"small":{"4":{"1":750,"2":500,"3":200},"6":{"1":500,"2":333,"3":133},"12":{"1":250,"2":167,"3":66},"24":{"1":125,"2":83,"3":33}}}},"3D":{"normal":{"big":{"1":400,"2":200,"3":100}}},"2D":{"normal":{"big":{"1":40,"2":20,"3":10}}},"1D":{"2":1.95}}'::json);

`;
        
        client2.query(q, [], function(e, r){
            if (e) return console.log('e==>',e);
			console.log('selesai')
        });
}

go();