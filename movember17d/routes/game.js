var fs = require('fs'),
	dbconnect = require('../lib/dbconnect'),
	sql = require('../lib/sql'),
	//IO = require('../sio/'),
	redisPrize = require('../lib/redis').prize,
	dateUtil = require('../lib/date'),
	gameConfig=null,
	serverConfig=null,
	dateDiff=null;

function getPrize(id,cb,dbclient){
	redisPrize.getJSON(id,function(e,r){
		if(!e && r) {
			console.log('dapat prize dari redis...');
			return cb(null, r);
		}
		dbclient=dbclient||dbconnect;
		dbclient.query('select prize from m_prize where id=$1', [id], function(err, r2){
			if (err) {
				console.log(err.stack);
				return cb(err);
			}
			if(r2 && r2.rows && r2.rows.length && r2.rows[0].prize){
				var d=r2.rows[0].prize;
				redisPrize.setJSON(id,d);
				console.log('dapat prize dari db...');
				return cb(null,d);
			}
			cb('no prize \"'+id+'\" found');
		});
	});
}

function currentPrizeId(cb){
	dbconnect.query('select id from m_prize\n'+
		'where apply_date=(select max(apply_date) from m_prize where apply_date < now())', [], function(e, r){
		if (e) {
			console.log(e.stack);
			return cb(e);
		}
		if(r && r.rows && r.rows.length && r.rows[0].id){
			return cb(null,r.rows[0].id);
		}
		cb('no currentPrize found');
	});
}

//getPrizes
//select * from m_prize
//where apply_date=(select max(apply_date) from m_prize where apply_date < now())


function getDate(cb,dbclient){
	dbclient=dbclient||dbconnect;
	if(dateDiff===null){
		//return dbclient.query('select now() date',function(e,r){
		//	if(e) return cb(e);
		//	if(!(r && r.rows && r.rows.length)) return cb('can not get dbdate');
			//dateDiff=(new Date(r.rows[0].date)).getTime() - ((new Date()).getTime());
			//console.log('dateDiff==>',dateDiff);
			dateDiff=dateUtil.diffTimeZone()*3600*1000;
			return cb(null,{
				date:(new Date()).getTime(), //.getTime()+dateDiff,
				TZ:dateUtil.dbTZ
			});
			
			//console.log('(new Date()).getTime()==>',(new Date()).getTime());
			//console.log('dateDiff==>',dateDiff);
			
			//diffTimeZone()
			
		//});
	}
	cb(null,{
		date:(new Date()).getTime(), //.getTime()+dateDiff,
		TZ:dateUtil.dbTZ
	});
}

function getServerConfig(cb,dbclient){
	dbclient=dbclient||dbconnect;
	if(!serverConfig){
		return dbclient.query('select * from m_config',function(e,r){
			if(e) return cb(e);
			if(!(r && r.rows && r.rows.length)) return cb('no config data found');
			serverConfig={};
			r.rows.forEach(function(v){
				serverConfig[v.id]=v.value||{};
			});
			cb(null,serverConfig);
		});
	}
	cb(null,serverConfig);
}

function getGameConfig(cb){
	if(!gameConfig){
		return dbconnect.query('select * from m_game',function(e,r){
			if(e) return cb(e);
			if(!(r && r.rows && r.rows.length)) return cb('no game data found');
			gameConfig={};
			r.rows.forEach(function(v){
				gameConfig[v.id]=v;
			});
			cb(null,gameConfig);
		});
	}
	cb(null,gameConfig);
}

function validate(game_id,numb,cb){
	getGameConfig(function(e,cfg){
		if(e) return cb(null,false);
		cb(null,!!numb.match(new RegExp(cfg[game_id].validate)));
	});
}

function wildCharValidate(game_id,numb){
	var l=parseInt(game_id.replace(/[^\d+]/g,''));
	return numb.length!=l?false:!!numb.match(/^\d*\*\d*$/);
}

function getDrawDate(cb,dbclient,st){
	let drawDays=[3,6,7];
	getDate(function(e,sdt){
		if(e) {
			console.error(new Error(e));
			return cb(e);
		}
		let dt=sdt.date;
		st=st||new Date(dt);
		//console.log('dateDiff===>',dateDiff);
		//console.log('st===>',st.toLocaleString());
		getServerConfig(function(e,cfg){
			if(e) {
				console.error(new Error(e));
				return cb(e);
			}
			
			let closingTime=((cfg||{}).bet_time||{}).close,//||'01:00',
				drawTime=((cfg||{}).bet_time||{}).draw,//||'02:00',
				stClose=setTimes(st,closingTime),
				stDraw=setTimes(st,drawTime);
			console.log('closingTime==>',closingTime);
			console.log('stClose==>',stClose.toLocaleString());
			if(st.getDay() < 1){
				if(st < setTimes(st,closingTime)) stClose=addDays(stClose, -7);
				if(st < setTimes(st,drawTime)) stDraw=addDays(stDraw, -7);
			}
			let dds={};
			drawDays.forEach(v=>{
				let ddClose=addDays(stClose, v - st.getDay()),
					ddDraw=addDays(stDraw, v - st.getDay());
				dds[v]={
					close:ddClose.getTime(),
					draw:ddDraw.getTime(),
					closed:ddClose.getTime() < st.getTime(),
					drawn:ddDraw.getTime() < st.getTime(),
				};
			});
			cb(null,dds);
		},dbclient);
	},dbclient);
}

exports.config=function(req,res){
	getServerConfig(function(e,r){
		if(e) return res.status(500).json({ s:0,e:e.toString()});
		res.status(200).send({ s:1,d:r});
	});
};

exports.game=function(req,res){
	getGameConfig(function(e,r){
		if(e) return res.status(500).json({ s:0,e:e.toString()});
		res.status(200).send({ s:1,d:r});
	});
};

exports.date=function(req,res){
	getDate(function(e,r){
		if(e) return res.status(500).json({ s:0,e:e.toString()});
		res.status(200).send({ s:1,d:r});
	});
};

function addDays(date, days){
	var result = new Date(date);
	result.setDate(result.getDate() + days);
	return result;
}

function setTimes(date, times){
	let t=Array.isArray(times)?times:times.split(':'),
		h=parseInt(t[0],10),
		m=parseInt(t[1],10),
		s=parseInt(t[2]||'0',10),
		d=new Date(date);
	d.setHours(h);
	d.setMinutes(m);
	d.setSeconds(s);
	return d;
}

exports.drawDate=function(req,res){
	let drawDays=[3,6,7];
	getDate(function(e,dt){
		if(e) return res.status(500).json({ s:0,e:e.toString()});
		let st=new Date(dt);
		getServerConfig(function(e,cfg){
			if(e) return res.status(500).json({ s:0,e:e.toString()});
			let closingTime=((cfg||{}).bet_time||{}).close||'01:00',
				st2=setTimes(st,closingTime);
			
			if((st.getDay() < 1) && (st < setTimes(st,closingTime))) st2=addDays(st2, -7);
			let dds=drawDays.map(function(v){
				let dd=addDays(st2, v - st.getDay());
				return {
					date:dd.getTime(),
					enable:dd > st,
				};
			});
			res.status(200).send({ s:1,d:dds});
		});
	});
};

exports.getCurrentPrize=function(req,res){
	currentPrizeId(function(e,id){
		if(e) return res.status(500).json({ s:0,e:e.toString()});
		getPrize(id,function(err,r){
			if(err) return res.status(500).json({ s:0,e:err.toString()});
			res.status(200).send({ s:1,d:r,id});
		});
	});
};

exports.updateRunningText=function(req,res){
	let body=req.body||{},
		text=body.text;

	let q=`with upd as (update m_config set value=$1,last_update=now(),update_by=$2 where id='running_text' returning *)
	insert into m_config(id,value,last_update,update_by) select 'running_text',$1,now(),$2 where NOT EXISTS (SELECT * FROM upd)`;
	dbconnect.query(q, [{text},req.user.id], function(e, r){
		if(e){
			console.error(new Error(e));
			return res.status(500).json({ s:0,e:e.toString()});
		}
		if(r){
			if(serverConfig){
				serverConfig=Object.assign({},serverConfig,{running_text:{text}});
			}
			require('../sio/').broadcast('runningText',{text});
			return res.status(200).send({ s:1,d:{'runningText':{text}}});
		}
		return res.status(500).json({ s:0,e:'data not found'});
	});
};

exports.updateMsgFile=function(type,lang){
	lang=lang||'en';
	if(['announcement','agreement','help'].indexOf(type) < 0) {
		return (req,res)=>{
			res.status(500).json({ s:0,e:{msg:'file '+type+' not found'}});
		}
	}
	return (req,res)=>{
		let body=req.body||{},
			content=body.content;
		try{
			fs.writeFileSync(__dirname+'/../msg/'+type+'/'+lang+'.html',content,'utf8')
		} catch (e){
			return res.status(500).json({ s:0,e:e.toString()});
		}
		res.status(200).send({ s:1,d:{
			type:'html',
			content
		}});
	};
}

exports.getMsgFile=function(type,lang){
	lang=lang||'en';
	if(['announcement','agreement','help'].indexOf(type) < 0) {
		return (req,res)=>{
			res.status(500).json({ s:0,e:{msg:'file '+type+' not found'}});
		}
	}
	return function(req,res){
		res.status(200).json({s:1,d:{
				type:'html',
				content:fs.readFileSync(__dirname+'/../msg/'+type+'/'+lang+'.html','utf8')
			}
		});
	}
}

exports.getCompleteDrawDate=(req,res)=>{
	getDrawDate((e,r)=>{
		if(e) return res.status(500).json({ s:0,e:e.toString()});
		res.status(200).send({ s:1,d:r});
	})
}

exports.model={
	getGameConfig:getGameConfig,
	getServerConfig:getServerConfig,
	setTimes:setTimes,
	getDate:getDate,
	getDrawDate:getDrawDate,
	validate:validate,
	wildCharValidate:wildCharValidate,
	getPrize:getPrize,
	currentPrizeId:currentPrizeId,
};