var Normal=require('./normal'),
	{processBet,dow}=Normal,
	betSchema=require('../../schema/bet'),
	Intake = require('../intake').model,
	Ajv = require('ajv'),
	ajv = new Ajv({useDefaults:true,coerceTypes:true});

const validate=ajv.compile(betSchema['1D']); 

const _permute={
	o:'1 3 5 7 9'.split(' '),
	e:'0 2 4 6 8'.split(' '),
	b:'5 6 7 8 9'.split(' '),
	s:'0 1 2 3 4'.split(' '),
}

function balanceCalculation({users,userDetail,bets,dows},cb){
	if(users.length <= 2) return cb('First level agent (player) can not place in 1D');

	var _uPermute={};
	
	let balance={},
		rusers=JSON.parse(JSON.stringify(users)).reverse(),
		player=rusers.shift(),
		firstAgent=rusers.shift();

	function playerBalance({player}){
		let pcf={},
			pa=0;
			bets2=[];
		bets.forEach(v=>{
			let {n,m,a}=v;
			pa+=a;
			bets2.push({n,m,a});
			if(!_uPermute[n]) _uPermute[n]=_permute[n];
		});
		let betPerDay={};
		dows.forEach(d=>{
			betPerDay[d]=JSON.parse(JSON.stringify(bets2));
		});
		//let bet=(pa)*dows.length,
		let passUp=(pa)*dows.length;
		return {
			passUp,
			amount:-passUp,
			detail:{
				passUp,
				ref:{},
				betPerDay,
			},
			bets:betPerDay
		};
	}
	function agentBalance({agent,take,bets,memberId,its,isFirstAgent,playerRef}){
		let cf=userDetail[agent].config||{},
			ac=userDetail[agent].agent_config||{},
			acf={},
			passUp=0,
			bets2={},
			gac=0;
		Object.keys(bets).forEach(day=>{
			let dbets=bets[day];
			bets2[day]=[];
			dbets.forEach(v=>{
				let {n,m,a}=v,
					bets2i={n,m,a,gac:0,da:a},
					it=ac.ci?(cf['1D']||{}).it || 0:0,
					ait=0,
					gaci=0;//,
					//pa=a;
				a=a||0;
				if(it){
					acf['1D']=acf['1D']||{};
					acf['1D'].it=it;
					
					let iid=[day,agent,'1D',n].join(';');
					let left=it-its[iid]; //10-3
					
					if(a && (left > 0)){
						ait=left > a?a:left; //7
						its[iid] += ait; //10
						left=it-its[iid];
					}
				}
				if(isFirstAgent){
					bets2i.gac=ait+(0.02 * (a-ait)); //first agent get actual cash
				} else {
					bets2i.gac=ait*(ait<a?0.98:1); //non first agent get actual cash
				}
				bets2i.it=ait;
				bets2i.a=a-bets2i.gac;
				gac+=bets2i.gac;
				passUp+=bets2i.a;
				bets2[day].push(bets2i);
			});
		});
		
		return {
			//bet,
			passUp,
			amount:gac,
			detail:{
				//bet,
				passUp,
				ref:acf,
				betPerDay:bets2,
				memberId
			},
			bets:bets2,
			its,
		};
	};
	
	
	function superAgentBalance({agent,take,bets,memberId,its,isFirstAgent,playerRef}){
		let cf=userDetail[agent].config||{},
			ac=userDetail[agent].agent_config||{},
			acf={},
			passUp=0,
			bets2={},
			gac=0;
		try {
			Object.keys(bets).forEach(day=>{
				let dbets=bets[day];
				bets2[day]=[];
				dbets.forEach((v,idx)=>{
					let {n,m,a}=v,
						bets2i={n,m,a,gac:0,da:a},
						it=ac.ci?(cf['1D']||{}).it || 1000:1000,
						ait=0,
						gaci=0;//,
						//pa=a;
					a=a||0;

						acf['1D']=acf['1D']||{};
						acf['1D'].it=it;
						
						let iid=[day,agent,'1D',n].join(';');
						let left=it-its[iid]; //10-3
						
						if(left < a) {
							throw {
								maxBet:it,
								idx,
								overflow:a-left,
								day,
								g:'1D',
								a
							};
						}
						
						if(a && (left > 0)){
							ait=left > a?a:left; //7
							its[iid] += ait; //10
							left=it-its[iid];
						}
						
					if(isFirstAgent){
						bets2i.gac=ait+(0.02 * (a-ait)); //first agent get actual cash
					} else {
						bets2i.gac=ait*(ait<a?0.98:1); //non first agent get actual cash
					}
					bets2i.it=ait;
					bets2i.a=a-bets2i.gac;
					gac+=bets2i.gac;
					passUp+=bets2i.a;
					bets2[day].push(bets2i);
				});
			});
		} catch (error) {
			return {e:error};
		}
		
		return {
			//bet,
			passUp,
			amount:gac,
			detail:{
				//bet,
				passUp,
				ref:acf,
				betPerDay:bets2,
				memberId
			},
			bets:bets2,
			its,
		};
	};
	
	
	let configs={},
		finish=false;
	let pb=playerBalance({player});
	balance[player]={user_id:player,type:'bet',amount:pb.amount,detail:pb.detail};
	configs[player]=(pb.detail||{}).ref||{};
	Intake.getBulkIntake1D({betPerDay:pb.bets,userDetail,exclude:player},(e,its2)=>{
		if(e){
			console.error(new Error(e));
			return cb(e);
		}
		//let fx=agentBalance;
		//if(!rusers.length) {
		//	finish=true;
		//	fx=superAgentBalance;
		//}
		let _a=agentBalance({
			agent:firstAgent,
			take:pb.passUp,
			bets:pb.bets,
			memberId:player,
			isFirstAgent:true,
			its:its2,
			playerRef:(pb.detail||{}).ref||{}});
		//if(_a.e){
		//	return cb(_a.e);
		//}
		
		balance[firstAgent]={user_id:firstAgent,type:'member_bet',amount:_a.amount,detail:_a.detail};
		configs[firstAgent]=(_a.detail||{}).ref||{};
		
		let memberId=firstAgent
		
		while(_a.passUp && !finish){
			let agent=rusers.shift();
			if(!rusers.length) {
				finish=true;
				fx=superAgentBalance;
			}
			let {passUp,its}=_a;
			_a=fx({
				agent,
				take:passUp,
				bets:_a.bets,
				memberId,
				its,
				isFirstAgent:false,
				playerRef:(pb.detail||{}).ref||{}});
			if(_a.e){
				return cb(_a.e);
			}
			balance[agent]={user_id:agent,type:'downline_bet',amount:_a.amount,detail:_a.detail};
			configs[agent]=(_a.detail||{}).ref||{};
			memberId=agent;
		}
		
		if(_a.passUp){
			console.log('aneh===> :P ');
		}
		
		let allPermute=[];
		Object.values(_uPermute).forEach(function(v){
			allPermute.push.apply(allPermute,v);
		});
		
		cb(null,{
			numbers:Array.from(new Set(allPermute)),
			balance,
			configs,
			its:_a.its
		});
		
	});
}

exports.placeBet=(req,res)=>{
	let {body}=req;
	if(!validate(body)){
		return res.status(500).json({s:0,e:validate.errors});
	}
	//body.day_left=dows;
	body.detail={bets:body.detail};
	processBet(body,req,res,balanceCalculation);	
}
