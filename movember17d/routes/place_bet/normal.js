User = require('../user').model,
	Game = require('../game').model,
	Balance = require('../balance').model,
	Ticket = require('../ticket').model,
	Logs = require('../logs').model,
	Intake = require('../intake').model,
	redisBalance = require('../../lib/redis').balance;
	

var dow={
	'1':'Mon',
	'2':'Tue',
	'3':'Wed',
	'4':'Thu',
	'5':'Fri',
	'6':'Sat',
	'7':'Sun',
};

function getDow(d){
	d=Array.isArray(d)?d:[d];
	var dowV=Object.values(dow).map(function(v){
		return v.toUpperCase();
	});
	return d.map(function(v){
		var i=dowV.indexOf(v.toUpperCase());
		return parseInt(Object.keys(dow)[i]);
	});
}

function perm(s){
	var permArr = [],
		usedChars = [];
	
	function permute(input) {
		var i, ch;
		for (i = 0; i < input.length; i++) {
			ch = input.splice(i, 1)[0];
			usedChars.push(ch);
			if (input.length == 0) {
				permArr.push(usedChars.slice());
			}
			permute(input);
			input.splice(i, 0, ch);
			usedChars.pop();
		}
		return permArr;
	}
	return permute(s.split('')).map(function(v){return v.join('');});
}

function uniqPermute(s){
	return Array.from(new Set(perm(s)));
}

function wildChar(s){
	return ('0 1 2 3 4 5 6 7 8 9'.split(' ')).map(function(v){
		return s.replace(/\*/,v);
	});
}

function balanceCalculation({users,userDetail,bets,dows},cb){
	var nonPermute=[];
	var _uPermute={};
	function uPermute(numb){
		if(_uPermute[numb]) return _uPermute[numb];
		_uPermute[numb]=uniqPermute(numb);
		return _uPermute[numb];
	}
	var _wildcard={};
	function Wildcard(numb){
		if(_wildcard[numb]) return _wildcard[numb];
		_wildcard[numb]=wildChar(numb);
		return _wildcard[numb];
	}
	
	let balance={},
		rusers=JSON.parse(JSON.stringify(users)).reverse(),
		player=rusers.shift(),
		firstAgent=rusers.shift();

	function playerBalance({player}){
		let cf=userDetail[player].config||{},
			pcf={},
			bd={
				b:0,
				s:0,
			},
			pd={
				b:0,
				s:0
			},
			bets2=[];
		bets.forEach(v=>{
			let {g,n,b,s,bt}=v,
				bets2i={g,n,bt,rb:0,ns:{bd:{},pd:{}}},
				anumb=[n];
			if(bt==='R'){
				anumb=uPermute(n);
			} else if(bt==='*'){
				anumb=Wildcard(n);
			} else if(bt==='I'){
				uPermute(n);
			} else {
				nonPermute.push(n);
			}
			let rb=(cf[g]||{}).rb || 0;
			bets2i.rb=rb;
			if(rb) {
				pcf[g]=pcf[g]||{};
				pcf[g].rb=rb;
			}
			bets2i.ns={bd:{},pd:{}};
			anumb.forEach(num=>{
				let sc=((cf[g]||{}).sc||{})[num]||0;
				bets2i.ns.bd[num]={};
				if(b) bets2i.ns.bd[num].b=b;
				if(s) bets2i.ns.bd[num].s=s;
				if(sc){
					pcf[g]=pcf[g]||{};
					pcf[g].sc=pcf[g].sc||{};
					pcf[g].sc[num]=((cf[g]||{}).sc||{})[num];
				}
				bets2i.ns.pd[num]={};
				let db=b - (b*rb/100),
					ds=s - (s*rb/100);
				if(db) bets2i.ns.pd[num].b=db;
				if(ds) bets2i.ns.pd[num].s=ds;
				bd.b += b;
				bd.s += s;
				pd.b += db;
				pd.s += ds;
			});
			bets2.push(bets2i);
		});
		
		let betPerDay={};
		dows.forEach(d=>{
			betPerDay[d]=JSON.parse(JSON.stringify(bets2));
		});
		
		let bet=(bd.b+bd.s)*dows.length,
			passUp=(pd.b+pd.s)*dows.length;
		return {
			bet,
			passUp,
			amount:-passUp,
			detail:{
				bet,
				passUp,
				ref:pcf,
				betPerDay,
			},
			bets:betPerDay
		};
	}
	
	function agentBalance({agent,take,bets,memberId,its,isFirstAgent,playerRef}){
		let cf=userDetail[agent].config||{},
			ac=userDetail[agent].agent_config||{},
			acf={},
			bd={
				b:0,
				s:0,
			},
			pd={
				b:0,
				s:0
			},
			bets2={};
		Object.keys(bets).forEach(day=>{
			let dbets=bets[day];
			bets2[day]=[];
			dbets.forEach(v=>{
				let {g,n,bt,ns}=v,
					bets2i={g,n,bt,rb:0,ns:{bd:{},pd:{}}},
					rb=(cf[g]||{}).rb || 0,
					it=ac.ci?(cf[g]||{}).it || 0:0; //nanti ini diganti;
				if(rb){
					acf[g]=acf[g]||{};
					acf[g].rb=rb;
					bets2i.rb=rb;
				}
				if(it){
					acf[g]=acf[g]||{};
					acf[g].it=it;
				}
				if(isFirstAgent && playerRef[g] && playerRef[g].sc){
					acf[g]=acf[g]||{};
					acf[g]._sc=playerRef[g].sc;
				}
				Object.keys(ns.bd||{}).forEach(num=>{
					let {b,s}=ns.bd[num],
						bit=0,
						sit=0;
						
					b=b||0; //20
					s=s||0;
					if(it){
						let iid=[day,agent,g,num].join(';');
						let left=it-its[iid]; //10-3
						if(b && (left > 0)){
							bit=left > b?b:left; //7
							b=b-bit; //13
							its[iid] += bit; //10
							left=it-its[iid];
						}
						if(s && (left > 0)){
							sit=left > s?s:left; //7
							s=s-sit; //13
							its[iid] += sit; //10
						}
					}
					
					bets2i.ns.bd[num]={};
					bets2i.ns.pd[num]={};
					if(b || bit) {
						bets2i.ns.bd[num].b=b;
						bets2i.ns.bd[num].bit=bit;
					}
					if(s || sit) {
						bets2i.ns.bd[num].s=s;
						bets2i.ns.bd[num].sit=sit;
					}
										
					let pb=b-(b*(rb/100)),
						ps=s-(s*(rb/100));
						
					if(pb) bets2i.ns.pd[num].b=pb;
					if(ps) bets2i.ns.pd[num].s=ps;
					
					bd.b += b;
					bd.s += s;
					pd.b += pb;
					pd.s += ps;
			
				});
				bets2[day].push(bets2i);
			});
		});
		
		let bet=(bd.b+bd.s),
			passUp=(pd.b+pd.s);
		return {
			bet,
			passUp,
			amount:take-passUp,
			detail:{
				bet,
				passUp,
				ref:acf,
				betPerDay:bets2,
				memberId
			},
			bets:bets2,
			its,
		};
	}
	
	function superAgentBalance({agent,take,bets,memberId,its,isFirstAgent,playerRef}){
		let cf=userDetail[agent].config||{},
			ac=userDetail[agent].agent_config||{},
			acf={},
			bd={
				b:0,
				s:0,
			},
			pd={
				b:0,
				s:0
			},
			bets2={};
		try {
			Object.keys(bets).forEach(day=>{
				let dbets=bets[day];
				bets2[day]=[];
				dbets.forEach((v,idx)=>{
					let {g,n,bt,ns}=v,
						bets2i={g,n,bt,rb:0,ns:{bd:{},pd:{}}},
						rb=(cf[g]||{}).rb || 0,
						it=ac.ci?(cf[g]||{}).it || 1000:1000;
					if(isFirstAgent && playerRef[g] && playerRef[g].sc){
						acf[g]=acf[g]||{};
						acf[g]._sc=playerRef[g].sc;
					}
					Object.keys(ns.bd||{}).forEach(num=>{
						let {b,s}=ns.bd[num],
							bit=0,
							sit=0;
							
						b=b||0; //20
						s=s||0;
						//if(it){
						let iid=[day,agent,g,num].join(';'),
							left=it-its[iid];
						
						if(left < (b + s)) {
							throw {
								maxBet:it,
								idx,
								overflow:(b + s)-left,
								day,
								g,
								num
							};
						}
						
						if(b){
							bit=left > b?b:left; //7
							b=b-bit; //13
							its[iid] += bit; //10
							left=it-its[iid];
						}
						
						if(s){
							sit=left > s?s:left; //7
							s=s-sit; //13
							its[iid] += sit; //10
							left=it-its[iid];
						}
						
						bets2i.ns.bd[num]={};
						bets2i.ns.pd[num]={};
						if(b || bit) {
							bets2i.ns.bd[num].b=b;
							bets2i.ns.bd[num].bit=bit;
						}
						if(s || sit) {
							bets2i.ns.bd[num].s=s;
							bets2i.ns.bd[num].sit=sit;
						}
											
						let pb=b-(b*(rb/100)),
							ps=s-(s*(rb/100));
							
						if(pb) bets2i.ns.pd[num].b=pb;
						if(ps) bets2i.ns.pd[num].s=ps;
						
						bd.b += b;
						bd.s += s;
						pd.b += pb;
						pd.s += ps;
				
					});
					bets2[day].push(bets2i);
				});
			});
		} catch (error) {
			return {e:error};
		}
		let bet=(bd.b+bd.s),
			passUp=(pd.b+pd.s);
		return {
			bet,
			passUp,
			amount:take-passUp,
			detail:{
				bet,
				passUp,
				ref:acf,
				betPerDay:bets2,
				memberId
			},
			bets:bets2,
			its,
		};
	}
	
	
	let configs={},
		finish=false;
	let pb=playerBalance({player});
	balance[player]={user_id:player,type:'bet',amount:pb.amount,detail:pb.detail};
	configs[player]=(pb.detail||{}).ref||{};
	
	Intake.getBulkIntake({betPerDay:pb.bets,userDetail,exclude:player},(e,its2)=>{
		if(e){
			console.error(new Error(e));
			return cb(e);
		}
		let fx=agentBalance;
		if(!rusers.length) {
			finish=true;
			fx=superAgentBalance;
		}
		let _a=fx({
			agent:firstAgent,
			take:pb.passUp,
			bets:pb.bets,
			memberId:player,
			isFirstAgent:true,
			its:its2,
			playerRef:(pb.detail||{}).ref||{}});
		if(_a.e){
			return cb(_a.e);
		}
		balance[firstAgent]={user_id:firstAgent,type:'member_bet',amount:_a.amount,detail:_a.detail};
		configs[firstAgent]=(_a.detail||{}).ref||{};
		let memberId=firstAgent;
		while(_a.passUp && !finish){
			let agent=rusers.shift();
			if(!rusers.length) {
				finish=true;
				fx=superAgentBalance;
			}
			let {passUp,its}=_a;
			_a=fx({
				agent,
				take:passUp,
				bets:_a.bets,
				memberId,
				its,
				isFirstAgent:false,
				playerRef:(pb.detail||{}).ref||{}});
			if(_a.e){
				return cb(_a.e);
			}
			balance[agent]={user_id:agent,type:'downline_bet',amount:_a.amount,detail:_a.detail};
			configs[agent]=(_a.detail||{}).ref||{};
			memberId=agent;
		}
		
		if(_a.passUp){
			console.log('aneh===> :P ');
		}
		
		let allPermute=[];
		Object.values(_uPermute).forEach(function(v){
			allPermute.push.apply(allPermute,v);
		});
		allPermute.push.apply(allPermute,nonPermute);
		cb(null,{
			numbers:Array.from(new Set(allPermute)),
			balance,
			configs,
			its:_a.its
		});
	});
}

//exports.betDataCollect=function(body,req,res,next){
//	Game.getServerConfig(function(e,config){ //get dow
//		let dows=getDow(config.draw_day[body.day_id]);
//		body.day_left=dows;
//		User.getUserParents(body.place_to,function(e,parents){ //get parents
//			if(e) return res.status(500).json({s:0,e:e.toString()});
//			body.users=parents;
//			User.getAnyUsers(parents||[],function(e,parentDetail){ //get parent detail
//				if(e) return res.status(500).json({s:0,e:e.toString()});
//				Game.currentPrizeId(function(e,prizeId){ //get current prize
//					if(e) return res.status(500).json({ s:0,e:e.toString()});
//					body.prize_id=prizeId;
//					Balance.getBalance(parents,(e,currentBalance)=>{ //get parents balance
//						if(e) return res.status(500).json({ s:0,e:e.toString()});
//						if((currentBalance[body.place_to]||0) + (parentDetail[body.place_to].credit||0) <= 0){
//							return res.status(500).json({ s:0,e:{
//								msg:'credit balance not enaught',
//								credit:parentDetail[body.place_to].credit||0,
//								balance:currentBalance[body.place_to]||0
//							}});
//						}
//						next({config,dows,parents,parentDetail,prizeId,currentBalance});
//					})
//				});
//			});
//		});
//	});
//};

exports.processBet=function(body,req,res,balanceCalc){
	Game.getServerConfig(function(e,config){ //get dow
		let dows=getDow(config.draw_day[body.day_id]);
		body.day_left=dows;
		User.getUserParents(body.place_to,function(e,parents){ //get parents
			if(e) return res.status(500).json({s:0,e:e.toString()});
			body.users=parents;
			User.getAnyUsers(parents||[],function(e,parentDetail){ //get parent detail
				if(e) return res.status(500).json({s:0,e:e.toString()});
				Game.currentPrizeId(function(e,prizeId){ //get current prize
					if(e) return res.status(500).json({ s:0,e:e.toString()});
					body.prize_id=prizeId;
					Balance.getBalance(parents,(e,currentBalance)=>{ //get parents balance
						if(e) return res.status(500).json({ s:0,e:e.toString()});
						if(parseFloat(currentBalance[body.place_to]||'0') + parseFloat(parentDetail[body.place_to].credit||'0') < 0){
							return res.status(500).json({ s:0,e:{
								msg:'credit balance not enaught',
								credit:parentDetail[body.place_to].credit||0,
								balance:currentBalance[body.place_to]||0
							}});
						}
						//next({config,dows,parents,parentDetail,prizeId,currentBalance});
						balanceCalc({ //get calculate balance
							users:parents,
							userDetail:parentDetail,
							bets:body.detail.bets,
							dows
						},(e,r)=>{
							if(e) {
								if(e && (e||{}).maxBet){
									return res.status(500).json({ s:0,e:e});
								}
								return res.status(500).json({ s:0,e:e.toString()});
							}
							let {numbers,balance,configs,its}=r;
							if(parseFloat(currentBalance[body.place_to]||'0') + parseFloat(parentDetail[body.place_to].credit||'0') + balance[body.place_to].amount < 0){
								return res.status(500).json({ s:0,e:{
									msg:'credit balance not enaught',
									credit:parentDetail[body.place_to].credit||0,
									balance:currentBalance[body.place_to]||0,
											amount:balance[body.place_to].amount||0
								}});
							}
							
							body.numbers=numbers;
							body.user_configs=configs;
							
							//res.send({s:1,d:{
							//	body,
							//	balance,
							//	its
							//}});
							
							Ticket.addTicket(body,function(e,r){ //save ticket
								if(e) {
									console.error(new Error(e));
									return res.status(500).json({ s:0,e:e.toString()});
								}
								Logs.addLog({user_id:req.user.id,type:'place_bet',description:'place bet success',params:r});
								Object.values(balance).forEach(function(v){
									v.ticket_id=r.id;
									Balance.addBalance(v,function(e){ //save all balance
										if(e) console.error(new Error(e));
									});
									redisBalance.del(v.user_id,e=>{
										if(e) console.error(new Error(e));
									});
								});
								
								Intake.setBulkIntake(its,e=>{ //update intake redis
									if(e) console.error(new Error(e));
								});
								
								Ticket.ticketList({id:r.id,user_id:body.place_to,withbalancedetail:1},(e,rTiket)=>{
									if(e) return res.status(500).json({ s:0,e:e.toString()});
									res.status(200).send({ s:1,d:rTiket[0]});
								});
							});
							
						});
					})
				});
			});
		});
	});
};

exports.placeBet=function(req,res){
	let {body}=req;
	body.detail={bets:JSON.parse(JSON.stringify(body.detail||[]),function(key,value){
		if(key==='b'||key==='s') return parseFloat(value||'0');
		if(key==='bt') return (value||'-').toUpperCase();
		return value;
	})};
	if(!body.day_id) return res.status(500).json({s:0,e:'invalid day_id parameter'});

	Game.getServerConfig(function(e,config){ //get dow
		let dows=getDow(config.draw_day[body.day_id]);
		body.day_left=dows;
		
		User.getUserParents(body.place_to,function(e,parents){ //get parents
			if(e) return res.status(500).json({s:0,e:e.toString()});
			
			body.users=parents;
			User.getAnyUsers(parents||[],function(e,parentDetail){ //get parent detail
				if(e) return res.status(500).json({s:0,e:e.toString()});
				
				Game.currentPrizeId(function(e,prizeId){ //get current prize
					if(e) return res.status(500).json({ s:0,e:e.toString()});
					body.prize_id=prizeId;
					
					Balance.getBalance(parents,(e,currentBalance)=>{ //get parents balance
						if(e) return res.status(500).json({ s:0,e:e.toString()});
						if(parseFloat(currentBalance[body.place_to]||'0') + parseFloat(parentDetail[body.place_to].credit||'0') <= 0){
							return res.status(500).json({ s:0,e:{
								msg:'credit balance not enaught...',
								credit:parentDetail[body.place_to].credit||0,
								balance:currentBalance[body.place_to]||0
							}});
						}
						
						balanceCalculation({ //get calculate balance
							users:parents,
							userDetail:parentDetail,
							bets:body.detail.bets,
							dows
						},(e,r)=>{
							if(e) {
								if(e && (e||{}).maxBet){
									return res.status(500).json({ s:0,e:e});
								}
								return res.status(500).json({ s:0,e:e.toString()});
							}
							let {numbers,balance,configs,its}=r;
							if(parseFloat(currentBalance[body.place_to]||'0') + parseFloat(parentDetail[body.place_to].credit||'0') + balance[body.place_to].amount < 0){
								return res.status(500).json({ s:0,e:{
									msg:'credit balance not enaught..',
									credit:parentDetail[body.place_to].credit||0,
									balance:currentBalance[body.place_to]||0,
									amount:balance[body.place_to].amount||0
								}});
							}
							
							body.numbers=numbers;
							body.user_configs=configs;
							
							Ticket.addTicket(body,function(e,r){ //save ticket
								if(e) {
									console.error(new Error(e));
									return res.status(500).json({ s:0,e:e.toString()});
								}
								Logs.addLog({user_id:req.user.id,type:'place_bet',description:'place bet success',params:r});
								Object.values(balance).forEach(function(v){
									v.ticket_id=r.id;
									Balance.addBalance(v,function(e){ //save all balance
										if(e) console.error(new Error(e));
									});
									redisBalance.del(v.user_id,e=>{
										if(e) console.error(new Error(e));
									});
								});
								
								Intake.setBulkIntake(its,e=>{ //update intake redis
									if(e) console.error(new Error(e));
								});
								
								//res.status(200).send({ s:1,d:r});
								Ticket.ticketList({id:r.id,user_id:body.place_to,withbalancedetail:1},(e,rTiket)=>{
									if(e) return res.status(500).json({ s:0,e:e.toString()});
									res.status(200).send({ s:1,d:rTiket[0]});
								});
							});
						});
					});
				});
			});
		});
	});
}