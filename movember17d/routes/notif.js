var dbconnect = require('../lib/dbconnect'),
	sql = require('../lib/sql'),
	notifSchema=require('../schema/notif'),
	Ajv = require('ajv'),
	ajv = new Ajv({useDefaults:true,coerceTypes:true});	

//notification only for update by other user or by system
var notifType={
	DOWNLINE_FAKE_LOGIN:'downline_fake_login',
	MEMBER_INVALID_PASSWORD:'member_invalid_password',
	PROFILE_UPDATE:'profile_update',
	MEMBER_PROFILE_UPDATE:'member_profile_update',
	PLACE_BET:'place_bet',
	PLACE_FIX_BET:'place_fix_bet',
	ADD_MEMBER:'member_add',
	STRIKE:'strike',
	DOWNLINE_STRIKE:'downline_strike',
	STATUS_UPDATE:'status_update',
	MEMBER_STATUS_UPDATE:'member_status_update',
	DOWNLINE_STATUS_UPDATE:'downline_status_update',
	CREDIT_UPDATE:'credit_update',
	MEMBER_CREDIT_UPDATE:'member_credit_update',
	DOWNLINE_CREDIT_UPDATE:'downline_credit_update',
};

var cSchema={};

function validate(type,data){
	if(!cSchema[type]){ cSchema[type]=ajv.compile(notifSchema[type]); }
	let r=cSchema[type](data);
	if(!r) console.error(new Error(cSchema[type].errors));
	return r;
}

function sendNotif(type,to,params,by_id,by_system,cb){
	sql.insertQuery('t_user_notif',{
		user_id:to,
		type:type,
		by_id:by_id?by_id:null,
		by_system:by_system?by_system:false,
		params:params||{}
	},{},function(q,v){
		dbconnect.query(q, v, function(e, r){
			if(e){
				console.error(new Error(e));
				return cb(e);
			}
			if(!(r && r.rows)) return cb('insert notification failed');
			return cb(null,r.rows[0]);
		});
	});
}

function sendNotification(type,data){
	if(validate(type,data)){
		data.type=notifType[type];
			sql.insertQuery('t_user_notif',data,{},function(q,v){
			dbconnect.query(q, v, function(e, r){
				if(e){
					console.error(new Error(e));
					return cb(e);
				}
				if(!(r && r.rows && r.rows.length)) {
					console.log('insert notification failed');
					//if(cb) return cb('insert notification failed');
				}
				let d=r.rows[0];
				//require('../sio/')
				require('../sio/').Push('U:'+d.user_id,'notif',d);
				//if(cb) cb(null,d);
			});
		});
	} else {
		console.log('gagal');
	}
}

function notifView(user_id,cb){
	let q=`with upd as (update t_notif_access set last_view=now(), last_read=now() where user_id=$1 returning *)
	insert into t_notif_access(user_id,last_view,last_read) select $1,now(),now() where NOT EXISTS (SELECT * FROM upd)`;

	dbconnect.query(q,[user_id], function(e, r){
		if(e) return cb(e);
		unView(user_id,(r)=>{
			cb(null,r);
		});
	})
	//select last_view from t_notif_access where user_id='AGENT1'
}

function unView(user_id,cb){
	let q=`select count(id) as cnt from t_user_notif where user_id=$1 and date > (
	case when NOT EXISTS (select last_view from t_notif_access where user_id=$1)
		then to_date('1900-01-01','YYYY-MM-DD')
		else (select last_view from t_notif_access where user_id=$1) 
	end)`;
	
	//dbconnect.query('select last_view from t_notif_access where user_id=$1',[user_id], function(e, r){
	//	if(r && r.rows && r.rows.length){
	//		let d=r.rows[0].last_view;
	//		return dbconnect.query('select count(id) as cnt from t_user_notif where user_id=$1 and date > $2',[user_id,d],(e,r2)=>{
	//			if(r2 && r2.rows && r2.rows.length) return cb(r2.rows[0].cnt);
	//			cb(0);
	//		})
	//	}
		dbconnect.query(q,[user_id],(e,r2)=>{
			if(r2 && r2.rows && r2.rows.length) return cb(r2.rows[0].cnt);
			cb(0);
		})
	//})
	//select last_view from t_notif_access where user_id='AGENT1'
}

exports.getMyNotif=function(req,res){
	let query=req.query||{};
	var qo={},
		limit=parseInt(query.limit||'25'),
		page=parseInt(query.page||'1');
	if(query.orderby) qo[query.orderby]=query.order||'DESC';
	if(query.orderby != 'date') qo.date='DESC';
	query.user_id=req.user.id;
	
	let {q,v}=sql.selectQuery('t_user_notif',query,qo,['description'],['id','user_id','by_id','by_system','type']);
	dbconnect.query(q,v, function(e, r){
		if(e){
			console.error(new Error(e));
			return res.status(500).json({s:0,e:e.toString()});
		}
		unView(req.user.id,(unviewed)=>{
			res.status(200).json({s:1,d:(r||{}).rows||[],unviewed});
		});
	});
}

exports.model={
	sendNotif:sendNotif,
	sendNotification:sendNotification,
	notifView:notifView,
	type:notifType
};