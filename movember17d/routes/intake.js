var redisIntake = require('../lib/redis').intake,
	Game = require('./game').model;

// berubah
// jangan ubah intake per nomor tapi catet bet yang masuk.

function getBulkIntake({betPerDay,userDetail,exclude},cb){
	//Game.getDate((e,r)=>{
	//	if(e){
	//		console.error(new Error(e));
	//		return cb(e);
	//	}
	//	let dateDiff=(new Date(r.date)).getTime() - ((new Date()).getTime());
	//	Game.getDrawDate((e,dd)=>{
	//		if(e){
	//			console.error(new Error(e));
	//			return cb(e);
	//		}
			let agents=JSON.parse(JSON.stringify(userDetail));
			if(exclude) delete agents[exclude];
			let ids=[],
				rs={};//,
				//dds={};
			Object.keys(betPerDay).forEach(day=>{
				betPerDay[day].forEach(bi=>{
					let {g,ns}=bi;
					Object.keys(agents).forEach(agentId=>{
						//let ac=userDetail[agentId].agent_config||{},
						//	cf=userDetail[agentId].config||{};
						Object.keys((ns||{}).bd||{}).forEach(n=>{
							let id=[day,agentId,g,n].join(';');
							ids.push(id);
							rs[id]=0;//ac.ci?(cf[g]||{}).it||0:0;
							//dds[id]=day;
						});
					});
				});
			});
			redisIntake.getMulti(ids,function(e,d){
				console.log('===>e==>',e);
				console.log('===>d==>',d);
				if(e){
					console.error(new Error(e));
					if(cb) return cb(e);
				}
				//rs=Object.assign(rs);
				Object.keys(d).forEach(k=>{
					rs[k]=parseFloat(d[k]);
				});
				//ids.forEach((id,k)=>{
				//	if(d[k]!==null) {
				//		rs[id]=parseFloat(d[k]);
				//		//redisIntake.set(id,rs[id],e=>{
				//		//	if(e) console.error(new Error(e));
				//		//});
				//		//redisIntake.expire(id,(dd[dds[id]].draw + dateDiff)/1000,e=>{
				//		//	if(e) console.error(new Error(e));
				//		//});
				//	}
				//	//else {
				//	//	rs[id]=parseFloat(d[k]);
				//	//}
				//});
				console.log('rs==>',rs);
				if(cb) cb(null,rs);
			});
			
	//	});
	//});
}

function getBulkIntake1D({betPerDay,userDetail,exclude},cb){
	let agents=JSON.parse(JSON.stringify(userDetail));
	if(exclude) delete agents[exclude];
	let ids=[],
		rs={};//,
		//dds={};
	Object.keys(betPerDay).forEach(day=>{
		betPerDay[day].forEach(bi=>{
			let {n}=bi;
			Object.keys(agents).forEach(agentId=>{
				let id=[day,agentId,'1D',n].join(';');
				ids.push(id);
				rs[id]=0;//ac.ci?(cf[g]||{}).it||0:0;
			});
		});
	});
	redisIntake.getMulti(ids,function(e,d){
		console.log('===>e==>',e);
		console.log('===>d==>',d);
		if(e){
			console.error(new Error(e));
			if(cb) return cb(e);
		}
		//rs=Object.assign(rs);
		Object.keys(d).forEach(k=>{
			rs[k]=parseFloat(d[k]);
		});
		console.log('rs==>',rs);
		if(cb) cb(null,rs);
	});
}

function setBulkIntake(its,cb){
	//console.log('set bulk intake 1');
	let ids={};
	Object.keys(its).forEach(it=>{
		if(its[it]) ids[it]=its[it];
	});
	//console.log('ids==>',ids);
	Game.getDate((e,r)=>{
		if(e){
			console.error(new Error(e));
			return cb(e);
		}
		//console.log('set bulk intake 2');
		let dateDiff=(new Date(r)).getTime() - ((new Date()).getTime());
		Game.getDrawDate((e,dd)=>{
			if(e){
				console.error(new Error(e));
				return cb(e);
			}
			
			//console.log('set bulk intake 3');
			let idsK=Object.keys(ids),
				ee={};
			//console.log('idsK==>',idsK);
			idsK.forEach((id,k)=>{
				let idi=id.split(';'),
					day=idi[0];
				
				//console.log('day=>',day);
				//console.log('dd=>',dd);
				//console.log('dd[day].draw=>',dd[day].draw);
				//console.log('dateDiff=>',dateDiff);
				//console.log(id,'==',ids[id],'==>', (dd[day].draw + dateDiff)/1000);
				redisIntake.set(id,ids[id],'EX', Math.floor((dd[day].draw + dateDiff)/1000),e=>{
					if(e) {
						console.error(new Error(e));
						ee[id]=e;
					}
					if(k >= idsK - 1){
						if(Objects.key(ee).length){
							return cb('banyak error');
						}
						cb(null);
					}
				});
			});
		});
	});
}

exports.model={
	getBulkIntake:getBulkIntake,
	getBulkIntake1D:getBulkIntake1D,
	setBulkIntake:setBulkIntake,
};