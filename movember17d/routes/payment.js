var dbconnect = require('../lib/dbconnect'),
	sql = require('../lib/sql'),
	Logs =  require('./logs').model,
	error = require('../lib/error');

const qtagihan=`-- tagihan

WITH
cfg as (select $1::varchar uid),
mbr as (select id,name,create_date dt from m_user,cfg where member_of=cfg.uid),
fd as (select GREATEST(min(t.date),min(mbr.dt)) d from t_active_ticket t,mbr),
_dr as(SELECT date_trunc('day', dd)::date dd
	FROM fd,generate_series (
	fd.d::timestamp,
	now()::timestamp,
	'1 day'::interval) dd),
dr as (select dd,cast(EXTRACT(ISODOW FROM dd) as int) d from _dr where EXTRACT(ISODOW FROM _dr.dd) = any(ARRAY[3,6,7])),
nums as (select id ticket_id,UNNEST(numbers) num from t_active_ticket),
usr as (select 
		dr.dd date,
		mbr.id root,
		t.id ticket_id,
		UNNEST(get_from(mbr.id,users)) user_id
	from t_active_ticket t
	inner join dr on t.draw_days @> ARRAY[dr.dd] and not (t.day_left @> ARRAY[dr.d])
	inner join mbr on t.users @> ARRAY[mbr.id]),
it as (select
		u.date,
		t.game_id game_id,
		u.user_id,
		max(COALESCE(cast(t.user_configs #>> ARRAY[u.user_id,'bit'] as float),0)) big_intake,
		max(COALESCE(cast(t.user_configs #>> ARRAY[u.user_id,'sit'] as float),0)) small_intake,
		max(COALESCE(cast(t.user_configs #>> ARRAY[u.user_id,'it'] as float),0)) intake_1D
	from usr u
	inner join t_active_ticket t on u.ticket_id=t.id
	group by u.date,t.game_id,u.user_id
	order by u.date,t.game_id,u.user_id),
tt as (select 
		u.date,
		u.root,
		u.user_id,
		u.ticket_id,
		n.num
	from usr u
	inner join nums n on n.ticket_id=u.ticket_id),
tu as (select 
		u.date,
		u.root,
		u.user_id,
		t.place_to,
		t.game_id,
		u.ticket_id,
		u.num,
		COALESCE(it.big_intake,0) big_intake,
		COALESCE(it.small_intake,0) small_intake,
		COALESCE(cast(t.user_configs #>> ARRAY[u.root,'rb'] as float),0) rebate,
		COALESCE(im.big_intake,0) member_big_intake,
		COALESCE(im.small_intake,0) member_small_intake,
		CASE WHEN t.game_id<>'1D' THEN 0
			WHEN t.place_to<>u.user_id THEN 0
			ELSE COALESCE(it.intake_1D,0) END intake_1D,
		CASE WHEN t.game_id<>'1D' THEN 0
			WHEN t.place_to=u.user_id THEN 0
			ELSE COALESCE(im.intake_1D,0) END member_intake_1D,
		CASE WHEN t.place_to=u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'b'] as float),0) ELSE 0 END big_bet,
		CASE WHEN t.place_to=u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'s'] as float),0) ELSE 0 END small_bet,
		CASE WHEN t.place_to<>u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'b'] as float),0) ELSE 0 END big_pool,
		CASE WHEN t.place_to<>u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'s'] as float),0) ELSE 0 END small_pool,
		CASE WHEN t.place_to=u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'1D'] as float),0) ELSE 0 END bet_1D,
		CASE WHEN t.place_to<>u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'1D'] as float),0) ELSE 0 END pool_1D,

		COALESCE(s.big_prize,0) big_prize,
		COALESCE(s.small_prize,0) small_prize,
		COALESCE(s.prize_1d,0) prize_1d,
		COALESCE(cast(t.user_configs #>> ARRAY[u.root,'sc'] as float),0) sc
	from tt u
	left join t_active_ticket t on u.ticket_id=t.id
	left join it on it.date=u.date and it.game_id=t.game_id and it.user_id=u.root
	left join it im on im.date=u.date and im.game_id=t.game_id and im.user_id=u.user_id and im.user_id<>u.root
	left join t_strike s on s.ticket_id=t.id and s.num=u.num and s.draw_date=u.date order by u.date,num),
byDateRootNNum as (select 
		u.date,
		root,
		num,
		sum(big_bet) big_bet,
		sum(small_bet) small_bet,
		sum(big_pool) big_pool,
		sum(small_pool) small_pool,
		sum(bet_1D) bet_1D,
		sum(pool_1D) pool_1D,
		u.big_intake big_intake,
		u.small_intake small_intake,
		u.rebate,
		u.big_prize,
		u.small_prize,
		u.prize_1d,
		u.sc,
		u.member_big_intake member_big_intake,
		u.member_small_intake member_small_intake,
		u.intake_1D intake_1D,
		u.member_intake_1D member_intake_1D
	from tu u
	group by u.date,root,num,u.big_intake,u.small_intake,u.rebate,intake_1D,member_intake_1D,u.big_prize,u.small_prize,u.prize_1d,u.sc,u.member_big_intake,u.member_small_intake order by u.date,num),
drn2 as (select
		date,
		root,
		num,
		--sum(big_bet) big_bet,
		--sum(small_bet) small_bet,
		--sum(big_pool) big_pool,
		--sum(small_pool) small_pool,
		--sum(member_big_intake) member_big_intake,
		--sum(member_small_intake) member_small_intake,
		big_prize,
		small_prize,
		prize_1d,
		sc,
		rebate,
		CASE WHEN sum(big_bet)+sum(big_pool)-sum(member_big_intake)-big_intake < 0 THEN 0 ELSE sum(big_bet)+sum(big_pool)-sum(member_big_intake)-big_intake END big_pass,
		CASE WHEN sum(small_bet)+sum(small_pool)-sum(member_small_intake)-small_intake < 0 THEN 0 ELSE sum(small_bet)+sum(small_pool)-sum(member_small_intake)-small_intake END small_pass,
		CASE WHEN sum(bet_1D)+sum(pool_1D)-sum(member_intake_1D)-intake_1D < 0 THEN 0 ELSE sum(bet_1D)+sum(pool_1D)-sum(member_intake_1D)-intake_1D END pass_1D
	from byDateRootNNum u
	group by date,root,num,u.big_intake,small_intake,intake_1D,rebate,big_prize,small_prize,prize_1d,sc order by date,num
),
byDateNRoot as (select
		date,
		root,
		sum(big_pass) big_pass,
		sum(small_pass) small_pass,
		sum(pass_1D) pass_1D,
		sum(big_pass*rebate/100) big_rebate,
		sum(small_pass*rebate/100) small_rebate,
		sum(pass_1D*rebate/100) rebate_1D,
		sum(big_prize * big_pass) big_strike,
		sum(small_prize * small_pass) small_strike,
		sum(prize_1d * pass_1d) strike_1d,
		sum(big_prize * big_pass * sc/100) big_strike_comm,
		sum(small_prize * small_pass * sc/100) small_strike_comm
	from drn2 drn
	group by date,root order by date,root),
byroot as (select
		root user_id,
		mbr.name,
		(sum(big_strike)+sum(small_strike)+sum(strike_1d)-sum(big_strike_comm)-sum(small_strike_comm))-
		(sum(big_pass)+sum(small_pass)+sum(pass_1D)-sum(big_rebate)-sum(small_rebate)-sum(rebate_1D)) net
	from byDateNRoot
	left join mbr on mbr.id=root
	group by root,name),
p as (select r.user_id,r.net+COALESCE(sum(p.amount),0) net from byroot r
	left join t_payment p on p.user_id=r.user_id
	group by r.user_id,r.net)
select * from p where net<>0`;

const qTagihan=`select * from v_tagihan($1)`;

exports.payments=(req,res)=>{
	let params=req.params||{},
		userid=params.id||req.user.id;
	
	dbconnect.query(qTagihan,[userid], function(e, r){
		if(e) {
			if(e.code) return res.status(500).json(error.db(e));
			return res.status(500).json({s:0,e:e.toString()});
		}
		return res.status(200).json({s:1,d:r.rows});
	});
};

exports.pay=(req,res)=>{
	let params=req.body||{},
		body=req.body||{},
		from_user=params.id||req.user.id,
		by_user=req.user.id;
		data=body.data||[];
	data.forEach(v=>{
		v.from_user=from_user;
		v.by_user=by_user;
		v.type='payment';
	});
	sql.insertQuery('t_payment',data,{},(q,v)=>{
		dbconnect.query(q,v, function(e, r){
			if(e) {
				if(e.code) return res.status(500).json(error.db(e));
				return res.status(500).json({s:0,e:e.toString()});
			}
			return res.status(200).json({s:1,d:r.rows});
		});
	});
};

exports.paymentHistory=(req,res)=>{
	let params=req.params||{},
		query=req.query||{},
		fromdate=query.fromdate,
		todate=query.todate,
		fromuser=params.id||req.user.id,
		customFilter=[{
			f:`u.member_of`,
			v:fromuser
		}],
		vd=/^\d{4}\-\d{2}\-\d{2}$/;
	if(fromdate && fromdate.match(vd)){
		customFilter.push({
			f:`date_trunc('day', p.date)`,
			o:'>=',
			v:fromdate
		});
	}
	if(todate && todate.match(vd)){
		customFilter.push({
			f:`date_trunc('day', p.date)`,
			o:'<=',
			v:todate
		});
	}
	sql.ooSelectQuery('t_payment',{
		query,
		alias:'p',
		filterFields:['user_id'],
		orders:{'p.date':'desc'},
		searchFields:['id','name'],
		addTables:['m_user u'],
		addFields:['u.name as name'],
		valFilter:['u.id=p.user_id'],
		customFilter,
		//full:true
	},(e,{q,v})=>{
		dbconnect.query(q,v, function(e, r){
			if(e) {
				if(e.code) return res.status(500).json(error.db(e));
				return res.status(500).json({s:0,e:e.toString()});
			}
			return res.status(200).json({s:1,d:r.rows});
		});
	});
};