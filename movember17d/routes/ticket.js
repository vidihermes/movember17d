var dbconnect = require('../lib/dbconnect'),
	sql = require('../lib/sql'),
	Logs = require('./logs').model,
	error = require('../lib/error'),
	User = require('./user').model,
	Game = require('./game').model;

const isMyDescendant=(req,res,kid,cb)=>{
	if(['developer','sadmin','admin'].indexOf(req.user.type) > -1) return cb();
	User.isMyDescendant(req.user.id,kid,(e,yes)=>{
		if(e) {
			if(e.code) return res.status(500).json(error.db(e));
			return res.status(500).json({s:0,e:e.toString()});
		}
		if(yes) return cb();
		res.status(500).json({s:0,e:'unauthorized'});
	});
};

function addSavedTicket(data,cb,valData){
	var vdata=Object.assign({},data);
	var iq=sql.insertQuery('t_saved_ticket',vdata,valData);
	//console.log('iq==>',iq);
	dbconnect.query(iq.q, iq.v, function(e, r){
		if (e) {
			console.error(new Error(e));
			return cb(e);
		}
		if(!(r && r.rows && r.rows.length)) return cb('insert saved_ticket failed');
		return cb(null,r.rows[0]);
	});
}

exports.saveTicket=function(req, res){
	var body=Object.assign({},req.body||{}),
		f='user_id form_type detail'.split(' ');
	for(var k in body){ if(f.indexOf(k) < 0) delete body[k]; }
	
	body.user_id=body.user_id||req.user.id;
	body.create_by=req.user.id;
	body.form_type=body.form_type||'normal';
	if(body.form_type==='normal'){
		body.detail={bets:JSON.parse(JSON.stringify(body.detail||[]),(key,value)=>{
			if(key==='b'||key==='s') return parseFloat(value||'0');
			if(key==='bt') return (value||'-').toUpperCase();
			return value;
		})};
	}
	addSavedTicket(body,function(e,r){ //save ticket
		//console.log('e==>',e);
		//console.log('r==>',r);
		if(e) {
			console.error(new Error(e));
			return res.status(500).json({ s:0,e:e.toString()});
		}
		Logs.addLog({user_id:req.user.id,type:'add_saved_ticket',description:'add saved ticket',params:r});
		res.status(200).send({ s:1,d:r});
	});
}
function listSavedTicket(query,cb){
	query=query||{};
	var qo={};
	if(query.orderby) qo[query.orderby]=query.order||'DESC';
	if(query.orderby != 'date') qo.date='DESC';
	let {q,v}=sql.selectQuery('t_saved_ticket',query,qo,['id'],['user_id','form_type']);
	//console.log('qu==>',qu);
	dbconnect.query(q,v, function(e, r){
		if(e){
			console.error(new Error(e));
			return cb(e);
		}
		return cb(null,r.rows);
	});
}
function deleteSavedTicket(id,cb){
	dbconnect.query('delete from t_saved_ticket where id=$1',[id], function(e, r){
		if(e){
			console.error(new Error(e));
			return cb(e);
		}
		return cb(null,r.rows);
	});
}


exports.savedTicketList=function(req, res){
	req.query=req.query||{};
	req.query.user_id=req.user.id;
	listSavedTicket(req.query,function(e,r){
		if(e) return res.status(500).json({ s:0,e:e.toString()});
		res.status(200).send({ s:1,d:r});
	});
};

exports.savedTicketDelete=function(req, res){
	let id=(req.params||{}).id;
	if(!id) return res.status(500).json({ s:0,e:{msg:'id required'}});
	listSavedTicket({id,limit:1},(e,r)=>{
		if(e) return res.status(500).json({ s:0,e:e.toString()});
		if(!(r && r.length)) return res.status(500).json({ s:0,e:{msg:'saved bet not fount'}});
		let d=r[0];
		if(d.user_id != req.user.id) return res.status(500).json({ s:0,e:{msg:'not authorized'}});
		deleteSavedTicket(id,e=>{
			if(e) return res.status(500).json({ s:0,e:e.toString()});
			res.status(200).send({ s:1,d:{id,msg:'deleted'}});
			Logs.addLog({user_id:req.user.id,type:'delete_saved_ticket',description:'delete saved ticket success',params:{ticket_id:id}});
		});
	});
};

function activeTicketList(query,cb,res,history){
	history=history||false;
	query=query||{};
	let qo={};
	query.limit=query.limit||'100';
	query.page=query.page||'1';
	if(query.orderby) qo[query.orderby]=query.order||'DESC';
	if(query.orderby != 'date') qo.date='DESC';
	
	let addFields=['((big+small+amount)*permute_length*'+(history?'(day_count-day_length)':'day_length')+') as bet_amount'];

	sql.ooSelectQuery('t_active_ticket',{
		query,
		orders:qo,
		searchFields:['num','place_to','game'],
		filterFields:['id','place_to','game','place_by','page_id'],
		addFields,
		customFilter:[history?{f:'day_length',o:'<',vv:'day_count'}:{f:'day_length',o:'>',vv:0}],
		//full:true
	},(e,{q,v})=>{
		dbconnect.query(q,v, function(e, r){
			if(res){
				if(e) {
					if(e.code) return res.status(500).json(error.db(e));
					return res.status(500).json({s:0,e:e.toString()});
				}
				return res.status(200).json({s:1,d:r.rows});
			}
			if(e){
				console.error(new Error(e));
				if(cb) return cb(e);
				return;
			}
			if(cb) return cb(null,r.rows);
		});
	});
}

exports.listActiveTicket=function(req, res){
	let query=req.query||{},
	history=query.history;
	activeTicketList(query,null,res,history);
};

//t_ticket_page active
//select * from t_ticket_page where id in (select distinct page_id from t_active_ticket where day_length > 0)

//t_ticket_page history
//select * from t_ticket_page where id in (select distinct page_id from t_active_ticket where day_length <= 0)
function ticketPageList(query,cb,res,history){
	history=history||false;
	query=query||{},
	from_date=query.from_date,
	to_date=query.to_date;
	let qo={};
		
	query.limit=query.limit||'100';
	query.page=query.page||'1';
	if(query.orderby) qo[query.orderby]=query.order||'DESC';
	if(query.orderby != 'date') qo.date='DESC';
	//query.place_to=query.place_to||req.user.id;
	
	let addFields=[];
	let customFilter=[];
	if(history){
		addFields.push('(select sum((big+small+amount)*permute_length*(day_count-day_length)) from t_active_ticket t where page_id=p.id) as page_amount');
		customFilter.push({f:'p.id',o:'in',vv:'(select distinct page_id from t_active_ticket where day_length < day_count)'});
	} else {
		addFields.push('(select sum((big+small+amount)*permute_length*day_length) from t_active_ticket t where page_id=p.id) as page_amount');
		customFilter.push({f:'p.id',o:'in',vv:'(select distinct page_id from t_active_ticket where day_length > 0)'});
	}
	
	if(from_date) customFilter.push({f:`date_trunc('day',p.date)`,o:'>=',v:from_date});
	if(to_date) customFilter.push({f:`date_trunc('day',p.date)`,o:'<=',v:to_date});
	
	sql.ooSelectQuery('t_ticket_page',{
		query,
		alias:'p',
		orders:qo,
		searchFields:['place_to','place_by','form_type'],
		filterFields:['id','place_to','place_by','form_type'],
		addFields,
		customFilter,
		//full:true
	},(e,{q,v})=>{
		dbconnect.query(q,v, function(e, r){
			if(res){
				if(e) {
					if(e.code) return res.status(500).json(error.db(e));
					return res.status(500).json({s:0,e:e.toString()});
				}
				return res.status(200).json({s:1,d:r.rows});
			}
			if(e){
				console.error(new Error(e));
				if(cb) return cb(e);
				return;
			}
			if(cb) return cb(null,r.rows);
		});
	});
}

//select sum(big*permute_length) as big,sum(small*permute_length) as small,sum(amount) as amount from t_active_ticket where game_id='2D' and place_to='AG10024' and day_left @> ARRAY[3]

exports.summaryBets=function(req,res){
	//console.log('kepanggil...');
	let placeto=req.params.placeto||req.user.id;
	let q='select game_id, '+
		'sum(big*permute_length) as big,'+
		'sum(small*permute_length) as small,'+
		'sum(amount) as amount '+
		'from t_active_ticket where place_to=$1 and day_left @> $2 group by game_id';
	Game.getDrawDate((e,dds)=>{
		if(e) return res.status(500).json({s:0,e:'error while get draw day'});
		let rslt={},
			cnt=0,
			left=0;
		//console.log(dds);
		Object.keys(dds).forEach((k)=>{
			if(!dds[k].drawn){
				cnt++;
				rslt[k]={
					day:dds[k],
					bets:{},
				};
			}
		});
		Object.keys(rslt).forEach((k)=>{
			dbconnect.query(q,[placeto,[parseInt(k,10)]],(e, r)=>{
				if(e) console.log('e==>',e);
				//console.log('r==>',r);
				left++;
				if(!e && r){
					(r.rows||[]).forEach(g=>{
						let {big,small,amount}=g;
						rslt[k].bets[g.game_id]={big,small,amount};
					});
				}
				if(left >= cnt){
					return res.status(200).json({s:1,d:rslt});
				}
			});
		});
	});
};

exports.voidTicket=function(req, res){
	let ticketid=(req.params||{}).ticketid;
	dbconnect.query('select t.*,(select count(id) from t_active_ticket t2 where t2.page_id=t.page_id) cnt from t_active_ticket t where id=$1',[ticketid],(e,r)=>{
		if(e) return res.status(500).json({s:0,e:e.toString()});
		if(!(r && r.rows && r.rows.length)) return res.status(500).json({s:0,e:'ticket not found'});
		let t=r.rows[0];
		isMyDescendant(req,res,t.place_to,()=>{
			if(t.day_length!==t.day_count) return res.status(500).json({s:0,e:'ticket has been drawn'});
			dbconnect.query('delete from t_active_ticket t where id=$1 returning *',[t.id],(e)=>{
				if(e) {
					console.log(new Error(e));
					return res.status(500).json({s:0,e:e.toString()});
				}
				if(parseInt(t.cnt,10) > 1) return res.status(200).json({s:1,d:t});
				dbconnect.query('delete from t_ticket_page p where id=$1',[t.page_id],()=>{
					return res.status(200).json({s:1,d:t});
				});
			});
		});
	});
};

exports.listTicketPage=function(req, res){
	let query=req.query||{},
		history=query.history;
	ticketPageList(query,null,res,history);
};

exports.listActiveTicket=function(req, res){
	let query=req.query||{},
	history=query.history;
	activeTicketList(query,null,res,history);
};

exports.model={
	addSavedTicket,
	deleteSavedTicket,
	//ticketList:ticketList,
	listSavedTicket,
	activeTicketList,
	ticketPageList,
};