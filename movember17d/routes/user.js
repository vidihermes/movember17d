var passport = require('passport'),
	CryptoJS = require('crypto-js'),
	redisUser = require('../lib/redis').user,
	dbconnect = require('../lib/dbconnect'),
	sql = require('../lib/sql'),
	Id = require('./id').model,
	Balance = require('./balance').model,
	Notif =  require('./notif').model,
	Logs =  require('./logs').model,
	ajv = require('../lib/ajv'),
	schema = require('../schema/user'),
	userValidate=ajv(schema.user),
	agentValidate=ajv(schema.agent),
	playerValidate=ajv(schema.player),
	error = require('../lib/error');
	

//console.log('Notif==>',Notif);

var userType={
	DEVELOPER:'developer',
	SUPER_ADMIN:'sadmin',
	ADMIN:'sadmin',
	SUPER_AGENT:'sagent',
	AGENT:'agent',
	PLAYER:'player'
};


function addUser(data,cb,valData){
	var vdata=Object.assign({},data);
	if(vdata.password){
		vdata.pass_salt=CryptoJS.lib.WordArray.random(16).toString();
		vdata.pass_sha=CryptoJS.SHA256(vdata.password+vdata.pass_salt).toString();
		delete vdata.password;
	}
	if(vdata.fake_password){
		vdata.fake_pass_salt=CryptoJS.lib.WordArray.random(16).toString();
		vdata.fake_pass_sha=CryptoJS.SHA256(vdata.fake_password+vdata.fake_pass_salt).toString();
		delete vdata.fake_password;
	}
	//var iq=sql.insertQuery('m_user',vdata,valData,false,['config','agent_config']);
	var iq=sql.insertQuery('m_user',vdata,valData);
	//console.log('iq==>',iq);
	dbconnect.query(iq.q, iq.v, function(e, r){
		if(e){
			//console.error(new Error(e));
			return cb(e);
		}
		if(!(r && r.rows && r.rows.length)) return cb('insert user failed');
		return cb(null,r.rows[0]);
	});
}

function isMyDescendant(me,kid,cb){
	var q='WITH RECURSIVE users_cte(id, name, member_of, create_date, type, status, active, depth, path) AS (\n'+
		' SELECT tn.id, tn.name, tn.member_of, tn.create_date, tn.type, tn.status, tn.active, 1::INT AS depth, ARRAY[]::varchar[] || ARRAY[tn.id]::varchar[] AS path  \n'+
		' FROM m_user AS tn \n'+
		' WHERE tn.id = $1\n'+
		'UNION ALL \n'+
		' SELECT c.id, c.name, c.member_of, c.create_date, c.type, c.status, c.active, p.depth + 1 AS depth,\n'+
		'	(p.path || ARRAY[c.id])\n'+
		' FROM users_cte AS p, m_user AS c\n'+
		' WHERE c.member_of = p.id\n'+
		')\n'+
		'SELECT * FROM users_cte AS n where n.id=$2;\n';

	// console.log('q==>',q);
	dbconnect.query(q,[me,kid],function(e, r){
		if(e){
			console.error(new Error(e));
			return cb(e);
		}
		if(r && r.rows && r.rows.length) return cb(null,r.rows[0]);
		return cb(null,false);
	});
}

function selectDescendantQuery(me,query,orders,searchFields,filterFields,incldeConfig){
	orders=orders||{};
	var search=query.search,
		limit=parseInt(query.limit||'25'),
		page=parseInt(query.page||'1'),
		v=[me],
		i=1,
		q='WITH RECURSIVE users_cte(id, name, login_id, member_of, create_date, type, status, active,credit,can_play,'+(incldeConfig?' agent_config, config,':'')+' depth, path) AS (\n'+
		' SELECT tn.id, tn.name, tn.login_id, tn.member_of, tn.create_date, tn.type, tn.status, tn.active,tn.credit,tn.can_play,'+(incldeConfig?' tn.agent_config, tn.config,':'')+' 1::INT AS depth, ARRAY[]::varchar[] || ARRAY[tn.id]::varchar[] AS path  \n'+
		' FROM m_user AS tn \n'+
		' WHERE tn.id = $1\n'+
		'UNION ALL \n'+
		' SELECT c.id, c.name, c.login_id, c.member_of, c.create_date, c.type, c.status, c.active,c.credit,c.can_play,'+(incldeConfig?' c.agent_config, c.config,':'')+' p.depth + 1 AS depth,\n'+
		'	(p.path || ARRAY[c.id])\n'+
		' FROM users_cte AS p, m_user AS c\n'+
		' WHERE c.member_of = p.id\n'+
		')\n'+
		'SELECT * FROM users_cte AS n \n',
		aorder=[],
		awhere=[];
		
	if(search && searchFields && (searchFields||[]).length){
		v.push('%'+search+'%');
		var ii=++i;
		var aqs=searchFields.map(function(vs){
			return 'n.'+vs+' ilike $'+ii;
		});
		awhere.push('('+aqs.join(' OR ')+')');
	}
	if(filterFields && filterFields.length){
		filterFields.forEach(function(vf){
			if(query.hasOwnProperty(vf)){
				v.push(query[vf]);
				awhere.push('n.'+vf+' = $'+(++i));
			}
		});
	}
	if(awhere.length) q+=' WHERE '+awhere.join(' AND ');
	for(var k in orders){
		if(orders.hasOwnProperty(k)) aorder.push('n.'+k+' '+orders[k]);
	}
	if(aorder.length) q+=' ORDER BY '+aorder.join(',');
	q+=' LIMIT $'+(++i)+' OFFSET $'+(++i);
	v.push(limit,limit*(page-1));
	return {
		q:q,
		v:v
	};
}

function withBalance(rows,cb){
	rows=rows||[];
	let ids=rows.map(v=>{return v.id;});
	Balance.getBalance(ids,(e,balance)=>{
		if(e){
			console.error(new Error(e));
			return cb(e);
		}
		cb(null,rows.map(v=>{
			v.balance=balance[v.id]||0;
			return v;
		}));
	});
}

function selectMyDescendant(me,query,cb){
	query=query||{};
	var qo={};
	if(query.orderby) qo[query.orderby]=query.order||'DESC';
	if(query.orderby != 'create_date') qo.create_date='DESC';
	
	var qu=selectDescendantQuery(me,query,qo,['id','name'],['id','type','active','status','member_of','can_play'],!!query.withconfig);
	dbconnect.query(qu.q,qu.v, function(e, r){
		if(e){
			console.error(new Error(e));
			return cb(e);
		}
		if(query.withconfig){
			return withBalance(r.rows,cb);
			//let ids=r.rows.map(v=>{return v.id;});
			//return Balance.getBalance(ids,(e,balance)=>{
			//	if(e){
			//		console.error(new Error(e));
			//		return cb(e);
			//	}
			//	cb(null,r.rows.map(v=>{
			//		v.balance=balance[v.id]||0;
			//		return v;
			//	}));
			//});
		}
		return cb(null,r.rows);
	});
}

function getUserParents(id,cb,to){
	to=to||false;
	let v=to?[id,to]:[id];
	var q='WITH RECURSIVE users_cte(id, member_of, depth, path) AS (\n'+
		' SELECT tn.id, tn.member_of, 1::INT AS depth, ARRAY[]::varchar[] || ARRAY[tn.id]::varchar[] AS path  \n'+
		' FROM m_user AS tn \n'+
		' WHERE '+(to?'tn.id=$2':'tn.member_of is null')+'\n'+
		'UNION ALL \n'+
		' SELECT c.id, c.member_of, p.depth + 1 AS depth,\n'+
		'	(p.path || ARRAY[c.id])\n'+
		' FROM users_cte AS p, m_user AS c\n'+
		' WHERE c.member_of = p.id\n'+
		')\n'+
		'SELECT * FROM users_cte AS n where n.id=$1\n';
	dbconnect.query(q,v, function(e, r){
		if(e){
			console.error(new Error(e));
			return cb(e);
		}
		if(r && r.rows && r.rows.length && r.rows[0].path) return cb(null,r.rows[0].path);
		cb(null,[]);
	});
}

function getUserRoot(id,cb){
	let q=`WITH RECURSIVE uc AS (SELECT u.id,u.member_of FROM m_user u WHERE id=$1
	UNION ALL SELECT u.id,u.member_of FROM m_user u, uc WHERE uc.member_of=u.id)
	SELECT * FROM uc where member_of is null`;
	dbconnect.query(q,[id], function(e, r){
		if(e){
			console.error(new Error(e));
			return cb(e);
		}
		if(r && r.rows && r.rows.length && r.rows[0].id) return cb(null,r.rows[0].id);
		cb(null,false);
	});
}

function getAnyUsers(ids,cb){
	redisUser.getMultiJSON(ids,function(e,r){
		if(e) return cb(e);
		let ids2=[];
		ids.forEach(function(id){
			if(!r[id]) ids2.push(id);
		});
		if(!ids2.length) {
			console.log('dari redis semua');
			return cb(null,r);
		}
		
		var q='select * from m_user where id = ANY($1)';
		dbconnect.query(q,[ids2], function(e2, r2){
			if(e2){
				console.error(new Error(e2));
				return cb(e2);
			}
			if(r2 && r2.rows){
				if(!r2.rows.length) return cb(null,r);
				r2.rows.forEach(function(v){
					r[v.id]=v;
					redisUser.setJSON(v.id,v);
				});
				return cb(null,r);
			}
			cb(null,{});
		});
	});
}

function getUserConfigs(ids,cb,dbclient){
	dbclient=dbclient||dbconnect;
	var q='select id, type, status, active, config, agent_config from m_user where id = ANY($1)',
		v=[ids];
	dbclient.query(q,v, function(e, r){
		if(e){
			console.error(new Error(e));
			return cb(e);
		}
		if(r && r.rows && r.rows.length) {
			let rs={};
			r.rows.forEach(function(v){
				rs[v.id]={
					type:v.type,
					status:v.status,
					active:v.active,
					config:v.config,
					agent_config:v.agent_config
				};
			});
			return cb(null,rs);
		}
		cb(null,{});
	});
}

function fakeLogin(id,cb){
	selectMyDescendant(id,{limit:5000},function(e,r){
		var i=[],
			l=r.map(function(vi,k){
				i.push('$'+(k+3));
				return vi.id;
			});
		
		redisUser.del(l);
		
		var v=['locked','__system__'];
		v.push.apply(v,l);
		
		var q='UPDATE m_user SET status=$1, last_update=now(), update_by=$2 WHERE id IN ('+i.join(', ')+')';
		dbconnect.query(q,v, function(e, r){
			if(e){
				console.error(new Error(e));
				return cb(e);
			}
			return cb(null,r.rows);
		});
	});
}

function getCalcFiedls(id,{withbalance,withcreditallocated,withactivebet},cb){
	let q='select ',
		qa=[];
	if(withbalance) qa.push(Balance.balanceField('$1'));
	if(withcreditallocated) qa.push('(select sum(credit) from m_user where member_of=$1) as credit_allocated');
	if(withactivebet) qa.push('(select sum((big+small+amount)*permute_length*day_length) from t_active_ticket where place_to=$1) as active_bet');
	if(qa.length){
		return dbconnect.query(q+qa.join(','),[id], function(e, r){
			if(e) return cb(e);
			if(r && r.rows && r.rows.length) return cb(null,r.rows[0]);
			cb(null,{});
		});
	}
	cb(null,{});
}

function listUser(query,cb,res){
	query=query||{};
	var qo={};
	if(query.orderby) qo[query.orderby]=query.order||'DESC';
	if(query.orderby != 'create_date') qo.create_date='DESC';

	let addFields=[];
	//if(query.withbalance) addFields.push('(select sum(to_upline)-sum(from_upline) from t_current_balance where paid=\'false\' and user_id in (select id from m_user where member_of=u.id)) as balance');
	if(query.withbalance) addFields.push(Balance.balanceField('u.id'));
	//if(query.withbalance) addFields.push('(select sum(amount) from t_current_balance where user_id=u.id and paid=\'false\') as balance');
	if(query.withcreditallocated) addFields.push('(select sum(credit) from m_user where member_of=u.id) as credit_allocated');
	if(query.withactivebet) addFields.push('(select sum((big+small+amount)*permute_length*day_length) from t_active_ticket where place_to=u.id) as active_bet');
	sql.ooSelectQuery('m_user',{
		query,
		alias:'u',
		orders:{'u.create_date':'desc'},
		searchFields:['id','name'],
		filterFields:['id','type','active','status','member_of','can_play'],
		addFields,
		//full:true
	},(e,{q,v})=>{
		dbconnect.query(q,v, function(e, r){
			if(res){
				if(e) {
					if(e.code) return res.status(500).json(error.db(e));
					return res.status(500).json({s:0,e:e.toString()});
				}
				return res.status(200).json({s:1,d:r.rows});
			}
			if(e){
				console.error(new Error(e));
				if(cb) return cb(e);
				return;
			}
			if(cb) return cb(null,r.rows);
		});
	});
}

function getUser(id,cb,includeBalance){
	includeBalance=includeBalance||false;
	function wbCb(e,u){
		withBalance([u],(e,r)=>{
			if(e) return cb(e);
			cb(null,r[0])
		});
	}
	redisUser.getJSON(id,function(e,r){
		if(!e && r) {
			return includeBalance?wbCb(null, r):cb(null, r);
		}
		dbconnect.query('select * from m_user where id=$1', [id], function(err, res2){
			if (err) {
				console.error(new Error(err));
				return cb(err);
			}
			if(res2.rows && res2.rows.length){
				var u=res2.rows[0];
				redisUser.setJSON(id,u);
				return includeBalance?wbCb(null, u):cb(null, u);
			}
			return cb(null, null);
		});
	});
	//dbconnect.query('select * from m_user where id=$1',[id], function(e, r){
	//	if(e){
	//		console.error(new Error(e));
	//		return cb(e);
	//	}
	//	if(!(r && r.rows && r.rows.length)) return cb('user not found');
	//	return cb(null,r.rows[0]);
	//});
}

function updateUser(data,condition,cb,valData){
	var vdata=Object.assign({},data);
	if(vdata.password){
		vdata.pass_salt=CryptoJS.lib.WordArray.random(16).toString();
		vdata.pass_sha=CryptoJS.SHA256(vdata.password+vdata.pass_salt).toString();
		delete vdata.password;
	}
	if(vdata.fake_password){
		vdata.fake_pass_salt=CryptoJS.lib.WordArray.random(16).toString();
		vdata.fake_pass_sha=CryptoJS.SHA256(vdata.fake_password+vdata.fake_pass_salt).toString();
		delete vdata.fake_password;
	}
	//var iq=sql.insertQuery('m_user',vdata,valData);
	var uq=sql.updateQuery('m_user',vdata,condition,valData);
	//console.log('uq==>',uq);
	dbconnect.query(uq.q, uq.v, function(e, r){
		if(e){
			console.error(new Error(e));
			return cb(e);
		}
		if(!(r && r.rows && r.rows.length)) return cb(null,null);
		return cb(null,r.rows[0]);
	});
}

exports.login=function(req,res,next){
	var body=req.body,
		username=body.username,
		password=body.password;
	username=(username||'').toUpperCase();
	if(!(username && password)) return res.status(400).send({s:0,e:'username and password required'});
	
	return passport.authenticate('local', function (err, account) {
		//console.log('account==>',account);
		req.logIn(account, function() {
			if(!err){
				return sql.updateQuery('m_user',{login_err:0,login_ip:req.connection.remoteAddress},{id:username},{last_login:'now()'},function(q,v){
					dbconnect.query(q, v, function(){
						res.status(200).json({s:1,d:account});
					});
				});
				Logs.addLog({user_id:username,type:'login',description:'login success'});
			} else {
				Logs.addLog({user_id:username,type:'login_error',description:'login error'});
			}
			if(err && err.e){
				if(err.e=='fake_login' && err.id) fakeLogin(err.id,function(){});
				return res.status(422).send({s:0,e:err.e.toString()});
			}
			res.status(err ? 422 : 200).send(err ? {s:0,e:err.toString()} : {s:1,d:account});
		});
	})(req,res,next);
};

exports.logout=function(req, res) {
	if(req && req.user && req.user.id){
		let user_id=req.user.id;
		if(user_id){
			Logs.addLog({user_id:user_id,type:'logout',description:'logout'});
		}
	}
	req.logout();
	req.session.destroy(function() {});
	res.send({s:1,d:{}});
};

exports.list=function(req, res){
	if(req.user.type=='sagent' || req.user.type=='agent' || req.user.type == 'player'){
		if((req.query||{}).member_of){
			return isMyDescendant(req.user.id,(req.query||{}).member_of,(e,yes)=>{
				if(e) {
					if(e.code) return res.status(500).json(error.db(e));
					return res.status(500).json({s:0,e:e.toString()});
				}
				if(yes) return listUser(req.query,null,res);
				res.status(500).json({s:0,e:'unauthorized'});
			});
		}
		return selectMyDescendant(req.user.id,req.query,function(e,r){
			if(e) return res.status(500).json({s:0,e:e.toString()});
			res.status(200).send({ s:1,d:r});
		});
	}
	listUser(req.query,null,res);
};

//exports.add=function(req, res){
//	var body=Object.assign({},req.body||{}),
//		f='id name type member_of active status password fake_password create_by'.split(' ');
//	for(var k in body){
//		if(f.indexOf(k) < 0) delete body[k];
//	}
//	addUser(body,function(e,r){
//		if(e) return res.status(500).json({s:0,e:e.toString()});
//		res.status(200).json({s:1,d:r});
//		Logs.addLog({user_id:req.user.id,type:'add_user',description:'add user success',params:r});
//	});
//};

function boolInput(b){
	if(typeof b === 'undefined') return null;
	if(['Y','T','TRUE','YES','1'].indexOf(b.toUpperCase()) > -1) return true;
	return false;
}

exports.addNewUser=function(type){
	return function(req, res){
		var body=Object.assign({},req.body||{});
		//	f='id name active password fake_password'.split(' ');
		//if(type=='sagent') f.push('agent_config','config');
		//for(var k in body){ if(f.indexOf(k) < 0) delete body[k]; }
		
		body.create_by=req.user.id;
		body.type=type;
		delete body.rePassword;
		//if(type=='sagent'){
		//	body.agent_config=convAC(body.agent_config||{});
		//	body.can_play=!!body.agent_config.pb;
		//} else {
		//	body.can_play=false;
		//}
		
		//body.config=convCfg(body.config||{});
		if(type==='admin'){
			if(!(req.user.type == 'sadmin' || req.user.type == 'developer')){
				return res.status(500).json({s:0,e:'unauthorized'});
			}
		} else if(type==='sadmin'){
			if(req.user.type !== 'developer'){
				return res.status(500).json({s:0,e:'unauthorized'});
			}
		}
		if(!userValidate(body)) return res.status(500).json(error.schema(userValidate));
		if(body.password===body.fake_password) return res.status(500).json({s:0,e:'password and emegency password with same value'});
		
		addUser(body,function(e,r){
			if(e) {
				if(e.code){
					if(e.code===23505) Id.useId(body.id,Id.idPrefix[body.type]);
					return res.status(500).json(error.db(e));
				}
				return res.status(500).json({s:0,e:e.toString()});
			}
			if(body.type!=='sagent'){
				Id.useId(body.id,Id.idPrefix[body.type]);
			}
			res.status(200).json({s:1,d:r});
		},{last_update:'now()'});
	};
};

//function convAC(cfg){
//	var ac={};
//	Object.keys(cfg).forEach(function(k){
//		ac[k]=JSON.parse(cfg[k])
//	});
//	return ac;
//}

//function convCfg(cfg){
//	var cf={};
//	Object.keys(cfg).forEach(function(g){
//		cf[g]={};
//		cfg[g]=cfg[g]||{};
//		Object.keys(cfg[g]).forEach(function(gv){
//			if(gv!=='sc') cf[g][gv]=JSON.parse(cfg[g][gv]||'0')
//			else {
//				cf[g].sc={};
//				cfg[g].sc=cfg[g].sc||{};
//				Object.keys(cfg[g].sc).forEach(function(sk){
//					cf[g].sc[sk]=JSON.parse(cfg[g].sc[sk]);
//				});
//			}
//			//cf[k][kk]={};
//		});
//	});
//	return cf;
//}

exports.addNestedUser=function(type){
	return [
		function(req, res, next){
			var body=req.body||{};
			if(req.user.type == 'player') return res.status(500).json({s:0,e:'unauthorized'});
			if(req.user.type == 'developer' || req.user.type == 'sadmin' || req.user.type == 'admin') return next();
			if(req.user.type == 'agent' || req.user.type == 'sagent'){
				body.member_of=body.member_of||req.user.id;
				if(body.member_of == req.user.id) return next();
			}
			isMyDescendant(req.user.id,body.member_of,function(e,r){
				if(e) {
					if(e.code) return res.status(500).json(error.db(e));
					return res.status(500).json({s:0,e:e.toString()});
				}
				if(!r) return res.status(500).json({s:0,e:'unauthorized'});
				if(!(r.type=='agent' || r.type=='sagent')) return res.status(500).json({s:0,e:'can not add descendant to '+r.type});
				next();
			});
		},
		function(req, res){
			var body=Object.assign({},req.body||{}),
				validate=agentValidate;
			//	f='id name credit active password fake_password member_of config'.split(' ');
			//if(type=='agent') f.push('agent_config');
			//for(var k in body){ if(f.indexOf(k) < 0) delete body[k]; }
			
			body.create_by=req.user.id;
			body.type=type;
			delete body.rePassword;
			
			if(type=='player'){
				body.can_play=true;
				validate=playerValidate;
			}
			
			if(!validate(body)) return res.status(500).json(error.schema(validate));
			if(body.password===body.fake_password) return res.status(500).json({s:0,e:'password and emegency password with same value'});
			
			//if(type=='player') {
			//	body.can_play=true;
			//} else {
			//	body.agent_config=convAC(body.agent_config||{});
			//	body.can_play=!!body.agent_config.pb;
			//}
			//body.config=convCfg(body.config||{});
			////else body.can_play=boolInput(body.can_play);
			if(type==='agent'){
				body.can_play=!!body.agent_config.pb;
			}
			
			listUser({id:body.member_of,withcreditallocated:1},(e,rAgent)=>{
				if(e) {
					if(e.code) return res.status(500).json(error.db(e));
					return res.status(500).json({s:0,e:e.toString()});
				}
				if(!rAgent.length) return res.status(500).json({s:0,e:'upline user not found'});
				let tAgent=rAgent[0];
				if(parseFloat(tAgent.credit||'0')-parseFloat(tAgent.credit_allocated||'0') < body.credit){
					return res.status(500).json({s:0,e:'upline credit limit not enaught'});
				}
				
				addUser(body,function(e,r){
					if(e) {
						if(e.code){
							if(e.code===23505) Id.useId(body.id,Id.idPrefix[body.type]);
							return res.status(500).json(error.db(e));
						}
						return res.status(500).json({s:0,e:e.toString()});
					}
					Id.useId(body.id,Id.idPrefix[body.type]);
					res.status(200).json({s:1,d:r});
					Logs.addLog({user_id:req.user.id,type:'add_user',description:'add user success',params:r});
				},{last_update:'now()'});
			});
		}
	];
};

var updateMe=function(req, res){
	getUser(req.user.id,function(e,r){
		if(e) return res.status(500).json({s:0,e:e.toString()});
		if(!r) return res.status(500).json({s:0,e:'user not found'});
		
		var body=Object.assign({},req.body||{}),
			f='name login_id password fake_password'.split(' ');
		if(r.type=='agent' || r.type=='sagent') f.push('config');
		
		for(var k in body){ if(f.indexOf(k) < 0) delete body[k]; }
		
		//if((r.type=='agent' || r.type=='sagent') && body.config && (r.agent_config||{}).ci){
		//	let bc=JSON.parse(JSON.stringify(body.config||{})),
		//		rc=r.config||{};
		//	
		//	['4D','3D','2D','1D'].forEach(function(g){
		//		rc[g]=rc[g]||{};
		//		bc[g]=bc[g]||{};
		//		if(bc[g].hasOwnProperty('it')){
		//			let v2=parseFloat(bc[g].it);
		//			if(isNaN(v2)) v2=0;
		//			rc[g].it=v2;
		//		}
		//	});
		//	body.config=rc;
		//}

		body.update_by=req.user.id;
		redisUser.del(req.user.id);
		
		let update=()=>{
			updateUser(body,{id:req.user.id},function(e,r){
				if(e) return res.status(500).json({s:0,e:e.toString()});
				res.status(200).json({s:1,d:r});
				Logs.addLog({user_id:req.user.id,type:'update_profile',description:'update profile',params:r});
			},{last_update:'now()'});
		};
		
		if(body.login_id){
			let v=body.login_id.toUpperCase();
			//body.login_id=body.login_id.toUpperCase();
			if(!(v.match(/^\w+$/) && !(v.match(/^[A-Z]+PL\d+$/) || v.match(/^[A-Z]+AG\d+$/)))) return res.status(500).json({s:0,e:'invalid login_id format'});
			body.login_id=v;
			dbconnect.query('select * from m_user where id<>$1 and (login_id=$2 or id=$2)',[req.user.id,body.login_id], function(e, r){
				if(e) return res.status(500).json({s:0,e:e.toString()});
				if(r && r.rows && r.rows.length) return res.status(500).json({s:0,e:'login_id already in use'});
				update();
			});
		} else {
			update();
		}
	});
};

exports.updateMe=updateMe;

exports.update=[
	function(req, res, next){
		if(req.params.id == req.user.id){ return updateMe(req,res); }
		if(req.user.type == 'player' || req.user.type == 'agent' || req.user.type == 'sagent') {
			return isMyDescendant(req.user.id,req.params.id,function(e,r){
				if(e) return res.status(500).json({s:0,e:e.toString()});
				if(!r) return res.status(500).json({s:0,e:'unauthorized'});
				next();
			});
		}
		next();
	},
	function (req,res,next){
		getUser(req.params.id,function(e,r){
			if(e) return res.status(500).json({s:0,e:e.toString()});
			if(!r) return res.status(500).json({s:0,e:'user not found'});
			req.reqType=r.type;
			if(req.user.type == 'player' || req.user.type == 'agent' || req.user.type == 'sagent') {
				req.userReq=r;
				return next();
			}
			if((r.type == 'sagent' && (req.user.type == 'admin' || req.user.type == 'sadmin' || req.user.type == 'developer')) ||
			   (r.type == 'admin' && (req.user.type == 'sadmin' || req.user.type == 'developer')) ||
			   (r.type == 'sadmin' && req.user.type == 'developer')) {
				req.userReq=r;
				return next();
			}
			if((r.type == 'player' || r.type == 'agent') && (req.user.type == 'admin' || req.user.type == 'sadmin' || req.user.type == 'developer')){
				req.userReq=r;
				return next();
			}
			res.status(401).json({s:0,e:'unauthorized'});
		});	
	},
	function(req, res){
		var body=Object.assign({},req.body||{}),
			f='name password fake_password config'.split(' ');
		if(req.reqType=='agent' || req.reqType=='sagent') f.push('agent_config');
		for(var k in body){ if(f.indexOf(k) < 0) delete body[k]; }
		
		if(req.reqType=='sagent' || req.reqType=='agent' || req.reqType=='player'){
			if(req.reqType=='sagent' || req.reqType=='agent'){
				if(body.hasOwnProperty('agent_config')){
					var ac=req.userReq.agent_config||{};
					if(body.agent_config.hasOwnProperty('pb')) {
						ac.pb=JSON.parse(body.agent_config.pb);
						body.can_play=!!ac.pb;
					}
					if(body.agent_config.hasOwnProperty('ci')) ac.ci=JSON.parse(body.agent_config.ci);
					if(body.agent_config.hasOwnProperty('ca')) ac.ca=JSON.parse(body.agent_config.ca);
					body.agent_config=ac;
				}
			}
			if(body.hasOwnProperty('config')){
				let bc=JSON.parse(JSON.stringify(body.config||{})),
					rc=req.userReq.config||{};
				['4D','3D','2D','1D'].forEach(function(g){
					rc[g]=rc[g]||{};
					bc[g]=bc[g]||{};
					//if(bc[g].hasOwnProperty('it')){
					//	let v2=parseFloat(bc[g].it);
					//	if(isNaN(v2)) v2=0;
					//	rc[g].it=v2;
					//}
					if(g!=='1D'){
						if(bc[g].hasOwnProperty('bit')){
							let v2=parseFloat(bc[g].bit);
							if(isNaN(v2)) v2=0;
							rc[g].bit=v2;
						}
						if(bc[g].hasOwnProperty('sit')){
							let v2=parseFloat(bc[g].sit);
							if(isNaN(v2)) v2=0;
							rc[g].sit=v2;
						}
					} else {
						if(bc[g].hasOwnProperty('it')){
							let v2=parseFloat(bc[g].it);
							if(isNaN(v2)) v2=0;
							rc[g].it=v2;
						}
					}
					if(bc[g].hasOwnProperty('rb')){
						let v2=parseFloat(bc[g].rb);
						if(isNaN(v2)) v2=0;
						rc[g].rb=v2;
					}
					if(bc[g].hasOwnProperty('sc')){
						let v2=parseFloat(bc[g].sc);
						if(isNaN(v2)) v2=0;
						rc[g].sc=v2;
					}
					//if(bc[g].hasOwnProperty('sc')){
					//	rc[g].sc={};
					//	for(let i in bc[g].sc){
					//		if(bc[g].sc.hasOwnProperty(i)){
					//			let v2=parseFloat(bc[g].sc[i]);
					//			if(isNaN(v2)) v2=0;
					//			rc[g].sc[i]=v2;
					//		}
					//	}
					//}
				});
				body.config=rc;
			}
		}

		body.update_by=req.user.id;
		redisUser.del(req.params.id);
		updateUser(body,{id:req.params.id},function(e,r){
			if(e) return res.status(500).json({s:0,e:e.toString()});
			res.status(200).json({s:1,d:r});
			Notif.sendNotification('PROFILE_UPDATE',{
				user_id:req.params.id,
				by_id:req.user.id,
				params:body
			});
			Logs.addLog({user_id:req.user.id,type:'update_user_profile',description:'update user profile',params:r});
		},{last_update:'now()'});
	}
];

exports.updateStatus=[
	(req, res, next)=>{
		let body=req.body||{},
			why=body.why,
			status=body.status;
		console.log('status=>',status);
		if(['suspend','lock','terminate','unsuspend','unlock'].indexOf(status) < 0){
			return res.status(500).json({s:0,e:'invalid status params'});
		}
		req.notifStatus=status;
		if(status==='unsuspend' || status==='unlock'){
			req.newStatus='normal';
		} else {
			req.newStatus=(status==='lock'?'locked':(status==='suspend'?'suspended':status));
		}
		console.log('req.newStatus==>',req.newStatus);
		req.why=why;
		if(req.params.id == req.user.id){ return res.status(500).json({s:0,e:'unauthorized'}); }
		if(req.user.type == 'player' || req.user.type == 'agent' || req.user.type == 'sagent') {
			return isMyDescendant(req.user.id,req.params.id,function(e,r){
				if(e) return res.status(500).json({s:0,e:e.toString()});
				if(!r) return res.status(500).json({s:0,e:'unauthorized'});
				next();
			});
		}
		next();
	},
	(req, res)=>{
		let data={
			update_by:req.user.id
		};
		if(req.newStatus==='terminate') data.active=false;
		data.status=req.newStatus;
		data.login_err=0;
		
		console.log('data==>',data);
		redisUser.del(req.params.id);
		updateUser(data,{id:req.params.id},function(e,r){
			if(e) return res.status(500).json({s:0,e:e.toString()});
			res.status(200).json({s:1,d:r});
			getUserParents(req.params.id,(e,parents)=>{
				let victim=(parents||[]).pop()||null,
					agent=(parents||[]).pop()||null,
					nextAgent=(parents||[]).pop()||null;
				if(victim) Notif.sendNotification('STATUS_UPDATE',{
					user_id:victim,
					by_id:req.user.id,
					description:req.why,
					params:{
						status:req.notifStatus,
						newStatus:data.status,
					}
				});
				if(agent) Notif.sendNotification('MEMBER_STATUS_UPDATE',{
					user_id:agent,
					by_id:req.user.id,
					description:req.why,
					params:{
						status:req.notifStatus,
						newStatus:data.status,
						member_id:victim
					}
				});
				if(nextAgent) {
					Notif.sendNotification('DOWNLINE_STATUS_UPDATE',{
						user_id:nextAgent,
						by_id:req.user.id,
						description:req.why,
						params:{
							status:req.notifStatus,
							newStatus:data.status,
							downline_id:victim
						}
					});
				}
				if(data.status==='terminate'){
					while ((parents||[]).length){
						Notif.sendNotification('DOWNLINE_STATUS_UPDATE',{
							user_id:(parents||[]).pop(),
							by_id:req.user.id,
							description:req.why,
							params:{
								status:req.notifStatus,
								newStatus:data.status,
								downline_id:victim
							}
						});
					}
				}
				Logs.addLog({user_id:req.user.id,type:'update_user_status',description:'update user status',params:r});
			});
		},{last_update:'now()'});
	}
];

exports.updateCredit=[
	(req, res, next)=>{
		let body=req.body||{},
			why=body.why,
			type=body.type,
			amount=body.amount;
		if(['increase','decrease','edit'].indexOf(type) < 0){
			return res.status(500).json({s:0,e:'invalid type params'});
		}
		if(!amount || isNaN(parseFloat(amount))){
			return res.status(500).json({s:0,e:'invalid amount params'});
		}
		req.amount=parseFloat(amount);
		req.updateType=type;
		req.why=why;
		if(req.params.id == req.user.id){ return res.status(500).json({s:0,e:'unauthorized'}); }
		if(req.user.type == 'player' || req.user.type == 'agent' || req.user.type == 'sagent') {
			return isMyDescendant(req.user.id,req.params.id,function(e,r){
				if(e) return res.status(500).json({s:0,e:e.toString()});
				if(!r) return res.status(500).json({s:0,e:'unauthorized'});
				next();
			});
		}
		next();
	},
	(req, res, next)=>{
		listUser({id:req.params.id,withcreditallocated:1},(e,rUser)=>{
			if(e) {
				if(e.code) return res.status(500).json(error.db(e));
				return res.status(500).json({s:0,e:e.toString()});
			}
			if(!rUser.length) return res.status(500).json({s:0,e:'user not found'});
			
			let tUser=rUser[0];
			req.tUser=tUser;
			req.currentCredit=parseFloat(tUser.credit||'0');
			if(req.updateType==='decrease'){
				if(req.currentCredit - parseFloat(tUser.credit_allocated||0) < req.amount){
					return res.status(500).json({s:0,e:'credit limit not enaught'});
				}
				return next();
			} else if(req.updateType==='edit' && req.amount < req.currentCredit ){
				if(req.amount - parseFloat(tUser.credit_allocated||0) < 0){
					return res.status(500).json({s:0,e:'credit limit not enaught'});
				}
				return next();
			}
			if(tUser.member_of){
				return listUser({id:tUser.member_of,withcreditallocated:1},(e,rAgent)=>{
					if(e) {
						if(e.code) return res.status(500).json(error.db(e));
						return res.status(500).json({s:0,e:e.toString()});
					}
					if(!rAgent.length) return res.status(500).json({s:0,e:'user agent not found'});
					let tAgent=rAgent[0];
					req.tAgent=tAgent;
					let amt=req.updateType==='edit'?req.amount-req.currentCredit:req.amount;
					if(parseFloat(tAgent.credit||0) - parseFloat(tAgent.credit_allocated||0) < amt){
						return res.status(500).json({s:0,e:'agent credit limit not enaught'});
					}
					return next();
				});
			}
			return next();
		});
	},
	(req, res)=>{
		let data={
			update_by:req.user.id,
			credit:req.updateType==='decrease'?req.currentCredit-req.amount:(req.updateType==='increase'?req.currentCredit+req.amount:req.amount)
		};
		console.log('data==>',data);
		redisUser.del(req.params.id);
		updateUser(data,{id:req.params.id},function(e,r){
			if(e) {
				if(e.code) return res.status(500).json(error.db(e));
				return res.status(500).json({s:0,e:e.toString()});
			}
			res.status(200).json({s:1,d:r});
			
			let nextAgent=(req.tAgent||{}).member_of;
			if(req.tUser.id) Notif.sendNotification('CREDIT_UPDATE',{
				user_id:req.tUser.id,
				by_id:req.user.id,
				description:req.why,
				params:{
					type:req.updateType,
					amount:req.amount,
				}
			});
			if(req.tAgent) Notif.sendNotification('MEMBER_CREDIT_UPDATE',{
				user_id:req.tAgent.id,
				by_id:req.user.id,
				description:req.why,
				params:{
					type:req.updateType,
					amount:req.amount,
					member_id:req.tUser.id,
				}
			});
			if(nextAgent) Notif.sendNotification('DOWNLINE_CREDIT_UPDATE',{
				user_id:nextAgent,
				by_id:req.user.id,
				description:req.why,
				params:{
					type:req.updateType,
					amount:req.amount,
					downline_id:req.tUser.id,
				}
			});
			Logs.addLog({user_id:req.user.id,type:'update_user_credit',description:`${req.updateType} ${req.tUser.id} credit ${req.amount}`,params:{
				amount:req.amount,
				type:req.updateType,
				reason:req.why
			}});
		},{last_update:'now()'});
	}
];

//function getBalanceAndMemberCredit(id,cb){
//	let q='select '+
//		'(select sum(amount) from t_balance where user_id=$1 and paid=\'false\') as balance,'+
//		'(select sum(credit) from m_user where member_of=$1) as credit_allocated';
//	return dbconnect.query(q,[id], function(e, r){
//		if(e) return cb(e);
//		let rd=((r||{}).rows||[])[0];
//		cb(null,parseFloat(rd.balance||'0'))
//		if()
//		r=Object.assign(r,(ra.rows||[])[0]||{});
//		return getWithPath(r);
//		//return res.status(200).json({s:1,d:r.rows});
//	});
//}

exports.me=function(req, res){
	getUser(req.user.id,function(e,r){
		if(e) return res.status(500).json({s:0,e:e.toString()});
		getCalcFiedls(r.id,{withbalance:1,withcreditallocated:1,withactivebet:1},(e,cf)=>{
			if(e) {
				if(e.code) return res.status(500).json(error.db(e));
				return res.status(500).json({s:0,e:e.toString()});
			}
			r=Object.assign(r,cf);
			res.status(200).json({s:1,d:r});
		});
	},true);
};

exports.detail=[
	function(req, res, next){
		if(req.params.id == req.user.id){ return next(); }
		if(req.user.type == 'admin' || req.user.type == 'sadmin' || req.user.type == 'developer') return next();
		if(req.user.type == 'player' || req.user.type == 'agent' || req.user.type == 'sagent') {
			return isMyDescendant(req.user.id,req.params.id,function(e,r){
				if(e) return res.status(500).json({s:0,e:e.toString()});
				if(!r) return res.status(500).json({s:0,e:'unauthorized'});
				next();
			});
		}
		res.status(401).json({s:0,e:'unauthorized'});
	},
	function(req, res){
		let query=req.query,
			{withbalance,withcreditallocated,withactivebet,withpath}=query;
			
		function getWithPath(r){
			if(!withpath) return res.status(200).json({s:1,d:r});
			getUserParents(r.id,(e,rpath)=>{
				if(e) {
					if(e.code) return res.status(500).json(error.db(e));
					return res.status(500).json({s:0,e:e.toString()});
				}
				r.path=rpath;
				res.status(200).json({s:1,d:r});
			},(req.user.type == 'admin' || req.user.type == 'sadmin' || req.user.type == 'developer')?null:req.user.id);
		}
		
		getUser(req.params.id,function(e,r){
			if(e) return res.status(500).json({s:0,e:e.toString()});
			if(!r) return res.status(500).json({s:0,e:'user not found'});
			
			if(withbalance || withcreditallocated || withactivebet){
				let q='select ',
					qa=[];
				//if(withbalance) qa.push('(select sum(to_upline)-sum(from_upline) from t_current_balance where paid=\'false\' and user_id in (select id from m_user where member_of=$1)) as balance');
				if(withbalance) qa.push(Balance.balanceField('$1'));
				//if(withbalance) qa.push('(select sum(amount) from t_current_balance where user_id=$1 and paid=\'false\') as balance');
				if(withcreditallocated) qa.push('(select sum(credit) from m_user where member_of=$1) as credit_allocated');
				if(withactivebet) qa.push('(select sum((big+small+amount)*permute_length*day_length) from t_active_ticket where place_to=$1) as active_bet');
				
				return dbconnect.query(q+qa.join(','),[req.params.id], function(e, ra){
					if(e) {
						if(e.code) return res.status(500).json(error.db(e));
						return res.status(500).json({s:0,e:e.toString()});
					}
					r=Object.assign(r,(ra.rows||[])[0]||{});
					return getWithPath(r);
					//return res.status(200).json({s:1,d:r.rows});
				});      				
			}
			return getWithPath(r);
		});
	}
];

exports.model={
	type:userType,
	addUser,
	getUserParents,
	getUserRoot,
	getAnyUsers,
	getUserConfigs,
	getUser,
	isMyDescendant
};