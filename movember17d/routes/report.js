var dbconnect = require('../lib/dbconnect'),
	sql = require('../lib/sql'),
	error = require('../lib/error'),
	User = require('./user').model;

const isMyDescendant=(req,res,kid,cb)=>{
	if(['developer','sadmin','admin'].indexOf(req.user.type) > -1) return cb();
	User.isMyDescendant(req.user.id,kid,(e,yes)=>{
		if(e) {
			if(e.code) return res.status(500).json(error.db(e));
			return res.status(500).json({s:0,e:e.toString()});
		}
		if(yes) return cb();
		res.status(500).json({s:0,e:'unauthorized'});
	});
};

const summaryBetDetailq=`-- summary detail

WITH
cfg as (select $1::text gid,$2::text uid,$3::date dd),
-- cfg as (select '1D'::text gid,'AAG1001'::text uid,'2018-03-24'::date dd),
mbr as (select id from m_user,cfg where id=cfg.uid),
nums as (select id ticket_id,UNNEST(numbers) num from t_active_ticket),
usr as (select 
		m.id root,
		t.id ticket_id,
		UNNEST(get_from(m.id,users)) user_id
	from t_active_ticket t
	inner join cfg on t.draw_days @> ARRAY[cfg.dd] and t.game_id=cfg.gid
	inner join mbr m on t.users @> ARRAY[m.id]),
it as (select
		t.game_id game_id,
		u.user_id,
		max(COALESCE(cast(t.user_configs #>> ARRAY[u.user_id,'bit'] as float),0)) big_intake,
		max(COALESCE(cast(t.user_configs #>> ARRAY[u.user_id,'sit'] as float),0)) small_intake,
		max(COALESCE(cast(t.user_configs #>> ARRAY[u.user_id,'it'] as float),0)) intake_1D
	from usr u
	inner join t_active_ticket t on u.ticket_id=t.id
	group by t.game_id,u.user_id
	order by t.game_id,u.user_id),
tt as (select 
		u.root,
		u.user_id,
		u.ticket_id,
		us.type,
		us.member_of,
		n.num
	from usr u
	inner join nums n on n.ticket_id=u.ticket_id
	left join m_user us on us.id=u.user_id),
tu as (select 
		u.root,
		u.user_id,
		t.place_to,
		t.game_id,
		u.ticket_id,
		u.num,
		COALESCE(it.big_intake,0) big_intake,
		COALESCE(it.small_intake,0) small_intake,
		COALESCE(im.big_intake,0) member_big_intake,
		COALESCE(im.small_intake,0) member_small_intake,
		COALESCE(it.intake_1D,0) intake_1D,
		COALESCE(im.intake_1D,0) member_intake_1D,
		CASE WHEN t.place_to=u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'b'] as float),0) ELSE 0 END big_bet,
		CASE WHEN t.place_to=u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'s'] as float),0) ELSE 0 END small_bet,
		CASE WHEN t.place_to=u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'1D'] as float),0) ELSE 0 END bet_1D,
		CASE WHEN t.place_to<>u.root and t.place_to=u.user_id and u.member_of=u.root and u.type='player' THEN COALESCE(cast(bets #>> ARRAY[u.num,'b'] as float),0) ELSE 0 END big_pool_player,
		CASE WHEN t.place_to<>u.root and t.place_to=u.user_id and u.member_of=u.root and u.type='player' THEN COALESCE(cast(bets #>> ARRAY[u.num,'s'] as float),0) ELSE 0 END small_pool_player,
		CASE WHEN t.place_to<>u.root and t.place_to=u.user_id and u.member_of=u.root and u.type='player' THEN COALESCE(cast(bets #>> ARRAY[u.num,'1D'] as float),0) ELSE 0 END pool_1D_player,
		CASE WHEN t.place_to<>u.root and t.place_to=u.user_id and not(u.member_of=u.root and u.type='player') THEN COALESCE(cast(bets #>> ARRAY[u.num,'b'] as float),0) ELSE 0 END big_pool_agent,
		CASE WHEN t.place_to<>u.root and t.place_to=u.user_id and not(u.member_of=u.root and u.type='player') THEN COALESCE(cast(bets #>> ARRAY[u.num,'s'] as float),0) ELSE 0 END small_pool_agent,
		CASE WHEN t.place_to<>u.root and t.place_to=u.user_id and not(u.member_of=u.root and u.type='player') THEN COALESCE(cast(bets #>> ARRAY[u.num,'1D'] as float),0) ELSE 0 END pool_1D_agent
	from tt u
	left join t_active_ticket t on u.ticket_id=t.id
	left join it on it.game_id=t.game_id and it.user_id=u.root
	left join it im on im.game_id=t.game_id and im.user_id=u.user_id and im.user_id<>u.root
	order by num),
rnu as (select 
		root,
		num,
		sum(big_bet) big_bet,
		sum(small_bet) small_bet,
		sum(bet_1D) bet_1D,
		sum(big_pool_agent) big_pool_agent,
		sum(small_pool_agent) small_pool_agent,
		sum(pool_1D_agent) pool_1D_agent,
		sum(big_pool_player) big_pool_player,
		sum(small_pool_player) small_pool_player,
		sum(pool_1D_player) pool_1D_player,
		big_intake,
		small_intake,
		intake_1D,
		member_big_intake,
		member_small_intake,
		member_intake_1D
	from tu u
	group by root,num,user_id,big_intake,small_intake,intake_1D,member_big_intake,member_small_intake,member_intake_1D order by num),
byRootNNum as (select 
		root,
		num,
		sum(big_bet) big_bet,
		sum(small_bet) small_bet,
		sum(bet_1D) bet_1D,
		CASE WHEN sum(big_pool_agent)-sum(member_big_intake)<0 THEN sum(big_pool_player) ELSE sum(big_pool_agent)-sum(member_big_intake)+sum(big_pool_player) END big_pool,
		CASE WHEN sum(small_pool_agent)-sum(member_small_intake)<0 THEN sum(small_pool_player) ELSE sum(small_pool_agent)-sum(member_small_intake)+sum(small_pool_player) END small_pool,
		CASE WHEN sum(pool_1D_agent)-sum(member_intake_1D)<0 THEN sum(pool_1D_player) ELSE sum(pool_1D_agent)-sum(member_intake_1D)+sum(pool_1D_player) END pool_1D,
		u.big_intake big_intake,
		u.small_intake small_intake,
		u.intake_1D intake_1D
	from rnu u
	group by root,num,u.big_intake,u.small_intake,intake_1D order by num),
rnintake as (select
		root,
		num,
		sum(big_bet) big_bet,
		sum(small_bet) small_bet,
		sum(bet_1D) bet_1D,
		sum(big_pool) big_pool,
		sum(small_pool) small_pool,
		sum(pool_1D) pool_1D,
		CASE WHEN sum(big_bet)+sum(big_pool) > big_intake THEN big_intake ELSE sum(big_bet)+sum(big_pool) END big_intake,
		CASE WHEN sum(small_bet)+sum(small_pool) > small_intake THEN small_intake ELSE sum(small_bet)+sum(small_pool) END small_intake,
		CASE WHEN sum(bet_1D)+sum(pool_1D) > intake_1D THEN intake_1D ELSE sum(bet_1D)+sum(pool_1D) END intake_1D
	from byRootNNum u
	group by root,num,u.big_intake,small_intake,intake_1D order by num
),
npass as (select
		num,
		sum(big_bet) big_bet,
		sum(small_bet) small_bet,
		sum(bet_1D) bet_1D,
		sum(big_pool) big_pool,
		sum(small_pool) small_pool,
		sum(pool_1d) pool_1d,
		sum(big_intake) big_intake,
		sum(small_intake) small_intake,
		sum(intake_1D) intake_1D,
		CASE WHEN sum(big_pool)+sum(big_bet)-sum(big_intake) < 0 THEN 0 ELSE sum(big_pool)+sum(big_bet)-sum(big_intake) END big_pass,
		CASE WHEN sum(small_pool)+sum(small_bet)-sum(small_intake) < 0 THEN 0 ELSE sum(small_pool)+sum(small_bet)-sum(small_intake) END small_pass,
		CASE WHEN sum(pool_1d)+sum(bet_1D)-sum(intake_1D) < 0 THEN 0 ELSE sum(pool_1d)+sum(bet_1D)-sum(intake_1D) END pass_1D
	from rnintake
	group by num)
select * from npass`;

const summaryBetDetailQ='select * from v_summary_detail($1,$2,$3)';
//const summaryBetGeneralQ=`-- summary bet
//
//WITH
//cfg as (select $1::text gid,$2::text uid,$3::date dd),
//-- cfg as (select '1D'::text gid,'AG10024'::text uid,'2018-03-17'::date dd),
//mbr as (select id from m_user,cfg where member_of=cfg.uid or id=cfg.uid),
//nums as (select id ticket_id,UNNEST(numbers) num from t_active_ticket),
//usr as (select 
//		mbr.id root,
//		t.id ticket_id,
//		UNNEST(get_from(mbr.id,users)) user_id
//	from t_active_ticket t
//	inner join cfg on t.draw_days @> ARRAY[cfg.dd] and t.game_id=cfg.gid
//	inner join mbr on t.users @> ARRAY[mbr.id]),
//it as (select
//		t.game_id game_id,
//		u.user_id,
//		max(COALESCE(cast(t.user_configs #>> ARRAY[u.user_id,'bit'] as float),0)) big_intake,
//		max(COALESCE(cast(t.user_configs #>> ARRAY[u.user_id,'sit'] as float),0)) small_intake,
//		max(COALESCE(cast(t.user_configs #>> ARRAY[u.user_id,'it'] as float),0)) intake_1D
//	from usr u
//	inner join t_active_ticket t on u.ticket_id=t.id
//	group by t.game_id,u.user_id
//	order by t.game_id,u.user_id),
//tt as (select 
//		u.root,
//		u.user_id,
//		u.ticket_id,
//		n.num
//	from usr u
//	inner join nums n on n.ticket_id=u.ticket_id),
//tu as (select 
//		u.root,
//		u.user_id,
//		t.place_to,
//		t.game_id,
//		u.ticket_id,
//		u.num,
//		COALESCE(it.big_intake,0) big_intake,
//		COALESCE(it.small_intake,0) small_intake,
//		COALESCE(im.big_intake,0) member_big_intake,
//		COALESCE(im.small_intake,0) member_small_intake,
//		CASE WHEN t.game_id<>'1D' THEN 0
//			WHEN t.place_to<>u.user_id THEN 0
//			ELSE COALESCE(it.intake_1D,0) END intake_1D,
//		CASE WHEN t.game_id<>'1D' THEN 0
//			WHEN t.place_to=u.user_id THEN 0
//			ELSE COALESCE(im.intake_1D,0) END member_intake_1D,
//		CASE WHEN t.place_to=u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'b'] as float),0) ELSE 0 END big_bet,
//		CASE WHEN t.place_to=u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'s'] as float),0) ELSE 0 END small_bet,
//		CASE WHEN t.place_to<>u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'b'] as float),0) ELSE 0 END big_pool,
//		CASE WHEN t.place_to<>u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'s'] as float),0) ELSE 0 END small_pool,
//
//		CASE WHEN t.place_to=u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'1D'] as float),0) ELSE 0 END bet_1D,
//		CASE WHEN t.place_to<>u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'1D'] as float),0) ELSE 0 END pool_1D
//	from tt u
//	left join t_active_ticket t on u.ticket_id=t.id
//	left join it on it.game_id=t.game_id and it.user_id=u.root
//	left join it im on im.game_id=t.game_id and im.user_id=u.user_id and im.user_id<>u.root
//	order by num),
//byRootNNum as (select 
//		root,
//		num,
//		sum(big_bet) big_bet,
//		sum(small_bet) small_bet,
//		sum(big_pool) big_pool,
//		sum(small_pool) small_pool,
//		sum(bet_1D) bet_1D,
//		sum(pool_1D) pool_1D,
//		u.big_intake big_intake,
//		u.small_intake small_intake,
//		u.member_big_intake member_big_intake,
//		u.member_small_intake member_small_intake,
//		u.intake_1D intake_1D,
//		u.member_intake_1D member_intake_1D
//	from tu u
//	group by root,num,u.big_intake,u.small_intake,u.member_big_intake,u.member_small_intake,intake_1D,member_intake_1D order by num),
//rn2 as (select
//		root,
//		num,
//		CASE WHEN sum(big_bet)+sum(big_pool)-sum(member_big_intake)-big_intake < 0 THEN 0 ELSE sum(big_bet)+sum(big_pool)-sum(member_big_intake)-big_intake END big_pass,
//		CASE WHEN sum(small_bet)+sum(small_pool)-sum(member_small_intake)-small_intake < 0 THEN 0 ELSE sum(small_bet)+sum(small_pool)-sum(member_small_intake)-small_intake END small_pass,
//		CASE WHEN sum(bet_1D)+sum(pool_1D)-sum(member_intake_1D)-intake_1D < 0 THEN 0 ELSE sum(bet_1D)+sum(pool_1D)-sum(member_intake_1D)-intake_1D END pass_1D
//	from byRootNNum u
//	group by root,num,u.big_intake,small_intake,intake_1D order by num
//),
//byroot as (select
//		root id,
//		sum(big_pass) big,
//		sum(small_pass) small,
//		sum(pass_1D) p1d
//	from rn2
//	group by root)
//select * from byroot`;

const summaryBetGeneralq=`-- summary bet

WITH
cfg as (select $1::text gid,$2::text uid,$3::date dd),
-- cfg as (select '1D'::text gid,'AAG1002'::text uid,'2018-03-24'::date dd),
mbr as (select id from m_user,cfg where member_of=cfg.uid or id=cfg.uid),
nums as (select id ticket_id,UNNEST(numbers) num from t_active_ticket),
usr as (select 
		m.id root,
		t.id ticket_id,
		UNNEST(get_from(m.id,users)) user_id
	from t_active_ticket t
	inner join cfg on t.draw_days @> ARRAY[cfg.dd] and t.game_id=cfg.gid
	inner join mbr m on t.users @> ARRAY[m.id]),
it as (select
		t.game_id game_id,
		u.user_id,
		max(COALESCE(cast(t.user_configs #>> ARRAY[u.user_id,'bit'] as float),0)) big_intake,
		max(COALESCE(cast(t.user_configs #>> ARRAY[u.user_id,'sit'] as float),0)) small_intake,
		max(COALESCE(cast(t.user_configs #>> ARRAY[u.user_id,'it'] as float),0)) intake_1D
	from usr u
	inner join t_active_ticket t on u.ticket_id=t.id
	group by t.game_id,u.user_id
	order by t.game_id,u.user_id),
tt as (select 
		u.root,
		u.user_id,
		u.ticket_id,
		us.type,
		us.member_of,
		n.num
	from usr u
	inner join nums n on n.ticket_id=u.ticket_id
	left join m_user us on us.id=u.user_id),
tu as (select 
		u.root,
		u.user_id,
		t.place_to,
		t.game_id,
		u.ticket_id,
		u.num,
		COALESCE(it.big_intake,0) big_intake,
		COALESCE(it.small_intake,0) small_intake,
		COALESCE(im.big_intake,0) member_big_intake,
		COALESCE(im.small_intake,0) member_small_intake,
		COALESCE(it.intake_1D,0) intake_1D,
		COALESCE(im.intake_1D,0) member_intake_1D,
		CASE WHEN t.place_to=u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'b'] as float),0) ELSE 0 END big_bet,
		CASE WHEN t.place_to=u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'s'] as float),0) ELSE 0 END small_bet,
		CASE WHEN t.place_to=u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'1D'] as float),0) ELSE 0 END bet_1D,
		CASE WHEN t.place_to<>u.root and t.place_to=u.user_id and u.member_of=u.root and u.type='player' THEN COALESCE(cast(bets #>> ARRAY[u.num,'b'] as float),0) ELSE 0 END big_pool_player,
		CASE WHEN t.place_to<>u.root and t.place_to=u.user_id and u.member_of=u.root and u.type='player' THEN COALESCE(cast(bets #>> ARRAY[u.num,'s'] as float),0) ELSE 0 END small_pool_player,
		CASE WHEN t.place_to<>u.root and t.place_to=u.user_id and u.member_of=u.root and u.type='player' THEN COALESCE(cast(bets #>> ARRAY[u.num,'1D'] as float),0) ELSE 0 END pool_1D_player,
		CASE WHEN t.place_to<>u.root and t.place_to=u.user_id and not(u.member_of=u.root and u.type='player') THEN COALESCE(cast(bets #>> ARRAY[u.num,'b'] as float),0) ELSE 0 END big_pool_agent,
		CASE WHEN t.place_to<>u.root and t.place_to=u.user_id and not(u.member_of=u.root and u.type='player') THEN COALESCE(cast(bets #>> ARRAY[u.num,'s'] as float),0) ELSE 0 END small_pool_agent,
		CASE WHEN t.place_to<>u.root and t.place_to=u.user_id and not(u.member_of=u.root and u.type='player') THEN COALESCE(cast(bets #>> ARRAY[u.num,'1D'] as float),0) ELSE 0 END pool_1D_agent
	from tt u
	left join t_active_ticket t on u.ticket_id=t.id
	left join it on it.game_id=t.game_id and it.user_id=u.root
	left join it im on im.game_id=t.game_id and im.user_id=u.user_id and im.user_id<>u.root
	order by num),
rnu as (select 
		root,
		num,
		sum(big_bet) big_bet,
		sum(small_bet) small_bet,
		sum(bet_1D) bet_1D,
		sum(big_pool_agent) big_pool_agent,
		sum(small_pool_agent) small_pool_agent,
		sum(pool_1D_agent) pool_1D_agent,
		sum(big_pool_player) big_pool_player,
		sum(small_pool_player) small_pool_player,
		sum(pool_1D_player) pool_1D_player,
		big_intake,
		small_intake,
		intake_1D,
		member_big_intake,
		member_small_intake,
		member_intake_1D
	from tu u
	group by root,num,user_id,big_intake,small_intake,intake_1D,member_big_intake,member_small_intake,member_intake_1D order by num),
byRootNNum as (select 
		root,
		num,
		sum(big_bet) big_bet,
		sum(small_bet) small_bet,
		sum(bet_1D) bet_1D,
		CASE WHEN sum(big_pool_agent)-sum(member_big_intake)<0 THEN sum(big_pool_player) ELSE sum(big_pool_agent)-sum(member_big_intake)+sum(big_pool_player) END big_pool,
		CASE WHEN sum(small_pool_agent)-sum(member_small_intake)<0 THEN sum(small_pool_player) ELSE sum(small_pool_agent)-sum(member_small_intake)+sum(small_pool_player) END small_pool,
		CASE WHEN sum(pool_1D_agent)-sum(member_intake_1D)<0 THEN sum(pool_1D_player) ELSE sum(pool_1D_agent)-sum(member_intake_1D)+sum(pool_1D_player) END pool_1D,
		u.big_intake big_intake,
		u.small_intake small_intake,
		u.intake_1D intake_1D
	from rnu u
	group by root,num,u.big_intake,u.small_intake,intake_1D order by num),
rnintake as (select
		root,
		num,
		sum(big_bet) big_bet,
		sum(small_bet) small_bet,
		sum(bet_1D) bet_1D,
		sum(big_pool) big_pool,
		sum(small_pool) small_pool,
		sum(pool_1D) pool_1D,
		CASE WHEN sum(big_bet)+sum(big_pool) > big_intake THEN big_intake ELSE sum(big_bet)+sum(big_pool) END big_intake,
		CASE WHEN sum(small_bet)+sum(small_pool) > small_intake THEN small_intake ELSE sum(small_bet)+sum(small_pool) END small_intake,
		CASE WHEN sum(bet_1D)+sum(pool_1D) > intake_1D THEN intake_1D ELSE sum(bet_1D)+sum(pool_1D) END intake_1D
	from byRootNNum u
	group by root,num,u.big_intake,small_intake,intake_1D order by num
),
rpass as (select
		root id,
		CASE WHEN sum(big_pool)+sum(big_bet)-sum(big_intake) < 0 THEN 0 ELSE sum(big_pool)+sum(big_bet)-sum(big_intake) END big,
		CASE WHEN sum(small_pool)+sum(small_bet)-sum(small_intake) < 0 THEN 0 ELSE sum(small_pool)+sum(small_bet)-sum(small_intake) END small,
		CASE WHEN sum(pool_1d)+sum(bet_1D)-sum(intake_1D) < 0 THEN 0 ELSE sum(pool_1d)+sum(bet_1D)-sum(intake_1D) END p1d
	from rnintake
	group by root)
select * from rpass`;

const summaryBetGeneralQ='select user_id id,big_pass big, small_pass small, pass_1d p1d from v_summary_bet($1,$2,$3)';

function summaryBetGeneral(game_id){
	return (d)=>{
		//console.log('d==>',d);
		return new Promise((resolve,reject)=>{
			let {user_id,drawdate}=d;
			//d[game_id]=d[game_id]||{};
			dbconnect.query(summaryBetGeneralQ,[game_id,user_id,drawdate],(e,result)=>{
				if(e) return reject(e);
				d[game_id]=result.rows;
				resolve(d);
				//dbconnect.query(summaryBetGeneralOwnQ,[game_id,user_id,drawdate],(e,result2)=>{
				//	if(e) return reject(e);
				//	d[game_id].push.apply(d[game_id],result2.rows);
				//	//d[game_id]=result2.rows;
				//	resolve(d);
				//});
				//resolve(d);
			});
		});
	};
}
function summaryBetDetail(game_id){
	return (d)=>{
		//console.log('d==>',d);
		return new Promise((resolve,reject)=>{
			let {user_id,drawdate}=d;
			//d[game_id]=d[game_id]||{};
			dbconnect.query(summaryBetDetailQ,[game_id,user_id,drawdate],(e,result)=>{
				if(e) return reject(e);
				d[game_id]=result.rows;
				resolve(d);
			});
		});
	};
}

exports.summaryBet=function(req, res){
	let query=req.query,
		user_id=query.user_id,
		drawdate=query.drawdate,
		d={user_id,drawdate};
		//q=generalQown('4D'),
		//qm=generalQmember('4D');
	isMyDescendant(req,res,user_id,()=>{
		let x=Promise.resolve(d);
		['4D','3D','2D','1D'].forEach(g=>{
			x=x.then(summaryBetGeneral(g));
			//x=x.then(MemberG(g));
		});
		x.then(d=>{
			let data={
				'4D':d['4D'],
				'3D':d['3D'],
				'2D':d['2D'],
				'1D':d['1D'],
			};
			res.status(200).json({s:1,d:data});
		}).catch(e=>{
			console.log('e==>',e);
			//if(e) {
				if(e.code) return res.status(500).json(error.db(e));
				return res.status(500).json({s:0,e:e.toString()});
			//}
		});
	});
};


exports.summaryBetDetail=function(req, res){
	let query=req.query,
		user_id=query.user_id,
		drawdate=query.drawdate,
		d={user_id,drawdate};
	isMyDescendant(req,res,user_id,()=>{
		let x=Promise.resolve(d);
		['4D','3D','2D','1D'].forEach(g=>{
			x=x.then(summaryBetDetail(g));
		});
		x.then(d=>{
			let data={
				'4D':d['4D'],
				'3D':d['3D'],
				'2D':d['2D'],
				'1D':d['1D'],
			};
			res.status(200).json({s:1,d:data});
		}).catch(e=>{
			console.log('e==>',e);
			if(e.code) return res.status(500).json(error.db(e));
			return res.status(500).json({s:0,e:e.toString()});
		});
	});
};

const SummaryTallyq=`-- summary tally

WITH
cfg as (select $1::text gid,$2::text uid,$3::date dd),
-- cfg as (select '1D'::text gid,'AG10024'::text uid,'2018-03-17'::date dd),
mbr as (select id from m_user,cfg where member_of=cfg.uid),
nums as (select id ticket_id,UNNEST(numbers) num from t_active_ticket),
usr as (select 
		mbr.id root,
		t.id ticket_id,
		UNNEST(get_from(mbr.id,users)) user_id
	from t_active_ticket t
	inner join cfg on t.draw_days @> ARRAY[cfg.dd] and t.game_id=cfg.gid
	inner join mbr on t.users @> ARRAY[mbr.id]),
it as (select
		t.game_id game_id,
		u.user_id,
		max(COALESCE(cast(t.user_configs #>> ARRAY[u.user_id,'bit'] as float),0)) big_intake,
		max(COALESCE(cast(t.user_configs #>> ARRAY[u.user_id,'sit'] as float),0)) small_intake,
		max(COALESCE(cast(t.user_configs #>> ARRAY[u.user_id,'it'] as float),0)) intake_1D
	from usr u
	inner join t_active_ticket t on u.ticket_id=t.id
	group by t.game_id,u.user_id
	order by t.game_id,u.user_id),
tt as (select 
		u.root,
		u.user_id,
		u.ticket_id,
		n.num
	from usr u
	inner join nums n on n.ticket_id=u.ticket_id),
tu as (select 
		u.root,
		u.user_id,
		t.place_to,
		t.game_id,
		u.ticket_id,
		u.num,
		COALESCE(it.big_intake,0) big_intake,
		COALESCE(it.small_intake,0) small_intake,
		COALESCE(cast(t.user_configs #>> ARRAY[u.root,'rb'] as float),0) rebate,
		COALESCE(im.big_intake,0) member_big_intake,
		COALESCE(im.small_intake,0) member_small_intake,
		CASE WHEN t.game_id<>'1D' THEN 0
			WHEN t.place_to<>u.user_id THEN 0
			ELSE COALESCE(it.intake_1D,0) END intake_1D,
		CASE WHEN t.game_id<>'1D' THEN 0
			WHEN t.place_to=u.user_id THEN 0
			ELSE COALESCE(im.intake_1D,0) END member_intake_1D,
		CASE WHEN t.place_to=u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'b'] as float),0) ELSE 0 END big_bet,
		CASE WHEN t.place_to=u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'s'] as float),0) ELSE 0 END small_bet,
		CASE WHEN t.place_to<>u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'b'] as float),0) ELSE 0 END big_pool,
		CASE WHEN t.place_to<>u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'s'] as float),0) ELSE 0 END small_pool,

		CASE WHEN t.place_to=u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'1D'] as float),0) ELSE 0 END bet_1D,
		CASE WHEN t.place_to<>u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'1D'] as float),0) ELSE 0 END pool_1D
	from tt u
	left join t_active_ticket t on u.ticket_id=t.id
	left join it on it.game_id=t.game_id and it.user_id=u.root
	left join it im on im.game_id=t.game_id and im.user_id=u.user_id and im.user_id<>u.root
	order by num),
byRootNNum as (select 
		root,
		num,
		sum(big_bet) big_bet,
		sum(small_bet) small_bet,
		sum(big_pool) big_pool,
		sum(small_pool) small_pool,
		sum(bet_1D) bet_1D,
		sum(pool_1D) pool_1D,
		u.big_intake big_intake,
		u.small_intake small_intake,
		u.rebate,
		u.member_big_intake member_big_intake,
		u.member_small_intake member_small_intake,
		u.intake_1D intake_1D,
		u.member_intake_1D member_intake_1D
	from tu u
	group by root,num,u.big_intake,u.small_intake,u.rebate,u.member_big_intake,u.member_small_intake,intake_1D,member_intake_1D order by num),
dit as (select
		root,
		num,
		sum(big_bet) big_bet,
		sum(small_bet) small_bet,
		sum(bet_1D) bet_1D,
		sum(big_pool) big_pool,
		sum(small_pool) small_pool,
		sum(pool_1D) pool_1D,
		--big_intake big_intake,
		--small_intake small_intake,
		rebate,
		sum(member_big_intake) member_big_intake,
		sum(member_small_intake) member_small_intake,
		sum(member_intake_1D) member_intake_1D,
		CASE
			WHEN sum(big_bet)+sum(big_pool)-sum(member_big_intake) > big_intake THEN big_intake
			WHEN sum(big_bet)+sum(big_pool)-sum(member_big_intake) > 0 THEN sum(big_bet)+sum(big_pool)-sum(member_big_intake)
			ELSE 0
		END big_intake,
		CASE
			WHEN sum(small_bet)+sum(small_pool)-sum(member_small_intake) > small_intake THEN small_intake
			WHEN sum(small_bet)+sum(small_pool)-sum(member_small_intake) > 0 THEN sum(small_bet)+sum(small_pool)-sum(member_small_intake)
			ELSE 0
		END small_intake,
		CASE
			WHEN sum(bet_1D)+sum(pool_1D)-sum(member_intake_1D) > intake_1D THEN intake_1D
			WHEN sum(bet_1D)+sum(pool_1D)-sum(member_intake_1D) > 0 THEN sum(bet_1D)+sum(pool_1D)-sum(member_intake_1D)
			ELSE 0
		END intake_1D
	from byRootNNum u
	group by root,num,u.big_intake,small_intake,intake_1D,rebate order by num
),
dpool as (select
		root,
		num,
		rebate,
		sum(big_bet) big_bet,
		sum(small_bet) small_bet,
		sum(bet_1D) bet_1D,
		sum(big_intake) big_intake,
		sum(small_intake) small_intake,
		sum(intake_1D) intake_1D,
		CASE WHEN sum(big_pool)-sum(member_big_intake) < 0 THEN 0 ELSE sum(big_pool)-sum(member_big_intake) END big_pool,
		CASE WHEN sum(small_pool)-sum(member_small_intake) < 0 THEN 0 ELSE sum(small_pool)-sum(member_small_intake) END small_pool,
		CASE WHEN sum(pool_1D)-sum(member_intake_1D) < 0 THEN 0 ELSE sum(pool_1D)-sum(member_intake_1D) END pool_1d
	from dit
	group by root,num,rebate),
dpass as (select
		root,
		num,
		rebate,
		CASE WHEN sum(big_pool)+sum(big_bet)-sum(big_intake) < 0 THEN 0 ELSE sum(big_pool)+sum(big_bet)-sum(big_intake) END big_pass,
		CASE WHEN sum(small_pool)+sum(small_bet)-sum(small_intake) < 0 THEN 0 ELSE sum(small_pool)+sum(small_bet)-sum(small_intake) END small_pass,
		CASE WHEN sum(pool_1d)+sum(bet_1D)-sum(intake_1D) < 0 THEN 0 ELSE sum(pool_1d)+sum(bet_1D)-sum(intake_1D) END pass_1d
	from dpool
	group by root,num,rebate),
byNRoot as (select
		root,
		sum(big_pass) big_pass,
		sum(small_pass) small_pass,
		sum(pass_1D) pass_1D,
		sum(big_pass*rebate/100) big_rebate,
		sum(small_pass*rebate/100) small_rebate,
		sum(pass_1D*rebate/100) rebate_1D
	from dpass
	group by root order by root),
byroot as (select
		root account,
		sum(big_pass) big,
		sum(small_pass) small,
		sum(pass_1D) p1d,
		sum(big_rebate) big_rebate,
		sum(small_rebate) small_rebate,
		sum(rebate_1D) rebate_1D,
		sum(big_pass) - sum(big_rebate) big_less_rebate,
		sum(small_pass) - sum(small_rebate) small_less_rebate,
		sum(pass_1D) - sum(rebate_1D) less_rebate_1D
	from byNRoot
	group by root)
select * from byroot`;

const SummaryTallyQ='select user_id account,big_pass big,small_pass small,pass_1d p1d,big_less_rebate,small_less_rebate,less_rebate_1d from v_summary_tally($1,$2,$3)';

function summaryBetTally(game_id){
	return (d)=>{
		//console.log('d==>',d);
		return new Promise((resolve,reject)=>{
			let {user_id,drawdate}=d;
			//d[game_id]=d[game_id]||{};
			dbconnect.query(SummaryTallyQ,[game_id,user_id,drawdate],(e,result)=>{
				if(e) return reject(e);
				d[game_id]=result.rows;
				resolve(d);
			});
		});
	};
}

exports.summaryTally=function(req, res){
	let query=req.query,
		user_id=query.user_id,
		drawdate=query.drawdate,
		d={user_id,drawdate};
	let x=Promise.resolve(d);
	isMyDescendant(req,res,user_id,()=>{
		['4D','3D','2D','1D'].forEach(g=>{
			x=x.then(summaryBetTally(g));
		});
		x.then(d=>{
			let data={
				'4D':d['4D'],
				'3D':d['3D'],
				'2D':d['2D'],
				'1D':d['1D'],
			};
			res.status(200).json({s:1,d:data});
		}).catch(e=>{
			console.log('e==>',e);
			if(e.code) return res.status(500).json(error.db(e));
			return res.status(500).json({s:0,e:e.toString()});
		});
	});	
};

const FrBetq=`-- full report atas

WITH
mbr as (select id from m_user where member_of=$1),
_dr as(SELECT date_trunc('day', dd)::date dd
	FROM generate_series (
	$2::timestamp,
	$3::timestamp,
	'1 day'::interval) dd),
dr as (select dd,cast(EXTRACT(ISODOW FROM dd) as int) d from _dr where EXTRACT(ISODOW FROM _dr.dd) = any(ARRAY[3,6,7])),
nums as (select id ticket_id,UNNEST(numbers) num from t_active_ticket),
usr as (select 
		dr.dd date,
		mbr.id root,
		t.id ticket_id,
		UNNEST(get_from(mbr.id,users)) user_id
	from t_active_ticket t
	inner join dr on t.draw_days @> ARRAY[dr.dd] and not (t.day_left @> ARRAY[dr.d])
	inner join mbr on t.users @> ARRAY[mbr.id]),
rb as (select
		u.ticket_id,
		u.user_id,
		COALESCE(cast(t.user_configs #>> ARRAY[u.user_id,'rb'] as float),0) rebate
	from usr u
	left join t_active_ticket t on t.id=u.ticket_id
	group by ticket_id,user_id,rebate),
it as (select
		u.date,
		t.game_id game_id,
		u.user_id,
		max(COALESCE(cast(t.user_configs #>> ARRAY[u.user_id,'bit'] as float),0)) big_intake,
		max(COALESCE(cast(t.user_configs #>> ARRAY[u.user_id,'sit'] as float),0)) small_intake,
		max(COALESCE(cast(t.user_configs #>> ARRAY[u.user_id,'it'] as float),0)) intake_1D
	from usr u
	inner join t_active_ticket t on u.ticket_id=t.id
	group by u.date,t.game_id,u.user_id
	order by u.date,t.game_id,u.user_id),
tt as (select 
		u.date,
		u.root,
		u.user_id,
		u.ticket_id,
		n.num
	from usr u
	inner join nums n on n.ticket_id=u.ticket_id),
tu as (select 
		u.date,
		u.root,
		u.user_id,
		t.place_to,
		t.game_id,
		u.ticket_id,
		u.num,
		COALESCE(it.big_intake,0) big_intake,
		COALESCE(it.small_intake,0) small_intake,
		COALESCE(rb.rebate,0) rebate,
		COALESCE(im.big_intake,0) member_big_intake,
		COALESCE(im.small_intake,0) member_small_intake,
		CASE WHEN t.game_id<>'1D' THEN 0
			WHEN t.place_to<>u.user_id THEN 0
			ELSE COALESCE(it.intake_1D,0) END intake_1D,
		CASE WHEN t.game_id<>'1D' THEN 0
			WHEN t.place_to=u.user_id THEN 0
			ELSE COALESCE(im.intake_1D,0) END member_intake_1D,
		CASE WHEN t.place_to=u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'b'] as float),0) ELSE 0 END big_bet,
		CASE WHEN t.place_to=u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'s'] as float),0) ELSE 0 END small_bet,
		CASE WHEN t.place_to<>u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'b'] as float),0) ELSE 0 END big_pool,
		CASE WHEN t.place_to<>u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'s'] as float),0) ELSE 0 END small_pool,
		CASE WHEN t.place_to=u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'1D'] as float),0) ELSE 0 END bet_1D,
		CASE WHEN t.place_to<>u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'1D'] as float),0) ELSE 0 END pool_1D,

		COALESCE(s.big_prize,0) big_prize,
		COALESCE(s.small_prize,0) small_prize,
		COALESCE(s.prize_1d,0) prize_1d,
		COALESCE(cast(t.user_configs #>> ARRAY[u.root,'sc'] as float),0) sc
	from tt u
	left join t_active_ticket t on u.ticket_id=t.id
	left join rb on u.ticket_id=rb.ticket_id and u.root=rb.user_id
	left join it on it.date=u.date and it.game_id=t.game_id and it.user_id=u.root
	left join it im on im.date=u.date and im.game_id=t.game_id and im.user_id=u.user_id and im.user_id<>u.root
	left join t_strike s on s.ticket_id=t.id and s.num=u.num and s.draw_date=u.date order by u.date,num),
byDateRootNNum as (select 
		u.date,
		root,
		num,
		sum(big_bet) big_bet,
		sum(small_bet) small_bet,
		sum(big_pool) big_pool,
		sum(small_pool) small_pool,
		sum(bet_1D) bet_1D,
		sum(pool_1D) pool_1D,
		u.big_intake big_intake,
		u.small_intake small_intake,
		u.rebate,
		u.big_prize,
		u.small_prize,
		u.prize_1d,
		u.sc,
		u.member_big_intake member_big_intake,
		u.member_small_intake member_small_intake,
		u.intake_1D intake_1D,
		u.member_intake_1D member_intake_1D
	from tu u
	group by u.date,root,num,u.big_intake,u.small_intake,u.rebate,intake_1D,member_intake_1D,u.big_prize,u.small_prize,u.prize_1d,u.sc,u.member_big_intake,u.member_small_intake order by u.date,num),
drn2 as (select
		date,
		root,
		num,
		--sum(big_bet) big_bet,
		--sum(small_bet) small_bet,
		--sum(big_pool) big_pool,
		--sum(small_pool) small_pool,
		--big_intake big_intake,
		--small_intake small_intake,
		--sum(member_big_intake) member_big_intake,
		--sum(member_small_intake) member_small_intake,
		big_prize,
		small_prize,
		prize_1d,
		sc,
		rebate,
		CASE WHEN sum(big_bet)+sum(big_pool)-sum(member_big_intake)-big_intake < 0 THEN 0 ELSE sum(big_bet)+sum(big_pool)-sum(member_big_intake)-big_intake END big_pass,
		CASE WHEN sum(small_bet)+sum(small_pool)-sum(member_small_intake)-small_intake < 0 THEN 0 ELSE sum(small_bet)+sum(small_pool)-sum(member_small_intake)-small_intake END small_pass,
		CASE WHEN sum(bet_1D)+sum(pool_1D)-sum(member_intake_1D)-intake_1D < 0 THEN 0 ELSE sum(bet_1D)+sum(pool_1D)-sum(member_intake_1D)-intake_1D END pass_1D
		--CASE WHEN sum(big_bet)+sum(big_pool)-sum(member_big_intake)-big_intake < 0 THEN 0 ELSE (sum(big_bet)+sum(big_pool)-sum(member_big_intake)-big_intake)*rebate/100 END big_rebate,
		--CASE WHEN sum(small_bet)+sum(small_pool)-sum(member_small_intake)-small_intake < 0 THEN 0 ELSE (sum(small_bet)+sum(small_pool)-sum(member_small_intake)-small_intake)*rebate/100 END small_rebate,
		--CASE WHEN sum(big_bet)+sum(big_pool)-sum(member_big_intake)-big_intake < 0 THEN 0 ELSE (sum(big_bet)+sum(big_pool)-sum(member_big_intake)-big_intake) * big_prize END big_strike,
		--CASE WHEN sum(small_bet)+sum(small_pool)-sum(member_small_intake)-small_intake < 0 THEN 0 ELSE (sum(small_bet)+sum(small_pool)-sum(member_small_intake)-small_intake) * small_prize END small_strike--,
	from byDateRootNNum u
	group by date,root,num,u.big_intake,small_intake,intake_1D,rebate,big_prize,small_prize,prize_1d,sc order by date,num
),
byDateNRoot as (select
		date,
		root,
		sum(big_pass) big_pass,
		sum(small_pass) small_pass,
		sum(pass_1D) pass_1D,
		sum(big_pass*rebate/100) big_rebate,
		sum(small_pass*rebate/100) small_rebate,
		sum(pass_1D*rebate/100) rebate_1D,
		sum(big_prize * big_pass) big_strike,
		sum(small_prize * small_pass) small_strike,
		sum(prize_1d * pass_1d) strike_1d,
		sum(big_prize * big_pass * sc/100) big_strike_comm,
		sum(small_prize * small_pass * sc/100) small_strike_comm
	from drn2 drn
	group by date,root order by date,root),
byroot as (select
		root user_id,
		sum(big_pass) big,
		sum(small_pass) small,
		sum(pass_1D) p1d,
		sum(big_rebate) big_rebate,
		sum(small_rebate) small_rebate,
		sum(rebate_1D) rebate_1D,
		sum(big_strike) big_strike,
		sum(small_strike) small_strike,
		sum(strike_1D) strike_1D,
		sum(big_strike_comm) big_strike_comm,
		sum(small_strike_comm) small_strike_comm
	from byDateNRoot
	group by root)
select * from byroot`;

const FrBetQ=`select user_id,
big_pass big,small_pass small,pass_1d p1d,
big_rebate, small_rebate, rebate_1d,
big_strike, small_strike, strike_1d,
big_sc big_strike_comm, small_sc small_strike_comm, sc_1d strike_comm_1d
from v_fr_atas($1,$2,$3)`;

const frPersonalTicketQ=`-- full report personal ticket

WITH
mbr as (select id from m_user where id=$1),
_dr as(SELECT date_trunc('day', dd)::date dd
	FROM generate_series (
	$2::timestamp,
	$3::timestamp,
	'1 day'::interval) dd),
dr as (select dd,cast(EXTRACT(ISODOW FROM dd) as int) d from _dr where EXTRACT(ISODOW FROM _dr.dd) = any(ARRAY[3,6,7])),
nums as (select id ticket_id,UNNEST(numbers) num from t_active_ticket),
tu as (select 
		t.place_to user_id,
		sum(COALESCE(cast(bets #>> ARRAY[n.num,'b'] as float),0)) big,
		sum(COALESCE(cast(bets #>> ARRAY[n.num,'s'] as float),0)) small,
		sum(COALESCE(cast(bets #>> ARRAY[n.num,'1D'] as float),0)) p1d
	from t_active_ticket t, mbr, nums n,dr
	where t.draw_days @> ARRAY[dr.dd] and t.place_to=mbr.id and n.ticket_id=t.id --and not (t.day_left @> ARRAY[dr.d]) 
	group by t.place_to)
select * from tu`;

const frIntakeq=`-- full report intake
WITH
mbr as (select id from m_user where id=$1),
_dr as(SELECT date_trunc('day', dd)::date dd
	FROM generate_series (
	$2::timestamp,
	$3::timestamp,
	'1 day'::interval) dd),
dr as (select dd,cast(EXTRACT(ISODOW FROM dd) as int) d from _dr where EXTRACT(ISODOW FROM _dr.dd) = any(ARRAY[3,6,7])),
nums as (select id ticket_id,UNNEST(numbers) num from t_active_ticket),
usr as (select 
		dr.dd date,
		mbr.id root,
		t.id ticket_id,
		UNNEST(get_from(mbr.id,users)) user_id
	from t_active_ticket t
	inner join dr on t.draw_days @> ARRAY[dr.dd] and not (t.day_left @> ARRAY[dr.d])
	inner join mbr on t.users @> ARRAY[mbr.id]),
it as (select
		u.date,
		t.game_id game_id,
		u.user_id,
		max(COALESCE(cast(t.user_configs #>> ARRAY[u.user_id,'bit'] as float),0)) big_intake,
		max(COALESCE(cast(t.user_configs #>> ARRAY[u.user_id,'sit'] as float),0)) small_intake,
		max(COALESCE(cast(t.user_configs #>> ARRAY[u.user_id,'it'] as float),0)) intake_1D
	from usr u
	inner join t_active_ticket t on u.ticket_id=t.id
	group by u.date,t.game_id,u.user_id
	order by u.date,t.game_id,u.user_id),
tt as (select 
		u.date,
		u.root,
		u.user_id,
		u.ticket_id,
		n.num
	from usr u
	inner join nums n on n.ticket_id=u.ticket_id),
tu as (select 
		u.date,
		u.root,
		u.user_id,
		t.place_to,
		t.game_id,
		u.ticket_id,
		u.num,
		COALESCE(it.big_intake,0) big_intake,
		COALESCE(it.small_intake,0) small_intake,
		COALESCE(cast(t.user_configs #>> ARRAY[u.root,'rb'] as float),0) rebate,
		COALESCE(im.big_intake,0) member_big_intake,
		COALESCE(im.small_intake,0) member_small_intake,
		CASE WHEN t.game_id<>'1D' THEN 0
			WHEN t.place_to<>u.user_id THEN 0
			ELSE COALESCE(it.intake_1D,0) END intake_1D,
		CASE WHEN t.game_id<>'1D' THEN 0
			WHEN t.place_to=u.user_id THEN 0
			ELSE COALESCE(im.intake_1D,0) END member_intake_1D,
		CASE WHEN t.place_to=u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'b'] as float),0) ELSE 0 END big_bet,
		CASE WHEN t.place_to=u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'s'] as float),0) ELSE 0 END small_bet,
		CASE WHEN t.place_to<>u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'b'] as float),0) ELSE 0 END big_pool,
		CASE WHEN t.place_to<>u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'s'] as float),0) ELSE 0 END small_pool,
		CASE WHEN t.place_to=u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'1D'] as float),0) ELSE 0 END bet_1D,
		CASE WHEN t.place_to<>u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'1D'] as float),0) ELSE 0 END pool_1D,

		COALESCE(s.big_prize,0) big_prize,
		COALESCE(s.small_prize,0) small_prize,
		COALESCE(s.prize_1d,0) prize_1d,
		COALESCE(cast(t.user_configs #>> ARRAY[u.root,'sc'] as float),0) sc
	from tt u
	left join t_active_ticket t on u.ticket_id=t.id
	left join it on it.date=u.date and it.game_id=t.game_id and it.user_id=u.root
	left join it im on im.date=u.date and im.game_id=t.game_id and im.user_id=u.user_id and im.user_id<>u.root
	left join t_strike s on s.ticket_id=t.id and s.num=u.num and s.draw_date=u.date order by u.date,num),
byDateRootNNum as (select 
		u.date,
		root,
		num,
		sum(big_bet) big_bet,
		sum(small_bet) small_bet,
		sum(big_pool) big_pool,
		sum(small_pool) small_pool,
		sum(bet_1D) bet_1D,
		sum(pool_1D) pool_1D,
		u.big_intake big_intake,
		u.small_intake small_intake,
		u.rebate,
		u.big_prize,
		u.small_prize,
		u.prize_1d,
		u.sc,
		u.member_big_intake member_big_intake,
		u.member_small_intake member_small_intake,
		u.intake_1D intake_1D,
		u.member_intake_1D member_intake_1D
	from tu u
	group by u.date,root,num,u.big_intake,u.small_intake,u.rebate,intake_1D,member_intake_1D,u.big_prize,u.small_prize,u.prize_1d,u.sc,u.member_big_intake,u.member_small_intake order by u.date,num),
drn2 as (select
		date,
		root,
		num,
		--sum(big_bet) big_bet,
		--sum(small_bet) small_bet,
		--sum(big_pool) big_pool,
		--sum(small_pool) small_pool,
		--sum(member_big_intake) member_big_intake,
		--sum(member_small_intake) member_small_intake,
		big_prize,
		small_prize,
		prize_1d,
		sc,
		rebate,
		CASE
			WHEN sum(big_bet)+sum(big_pool)-sum(member_big_intake) > big_intake THEN big_intake
			WHEN sum(big_bet)+sum(big_pool)-sum(member_big_intake) > 0 THEN sum(big_bet)+sum(big_pool)-sum(member_big_intake)
			ELSE 0
		END big_intake,
		CASE
			WHEN sum(small_bet)+sum(small_pool)-sum(member_small_intake) > small_intake THEN small_intake
			WHEN sum(small_bet)+sum(small_pool)-sum(member_small_intake) > 0 THEN sum(small_bet)+sum(small_pool)-sum(member_small_intake)
			ELSE 0
		END small_intake,
		CASE
			WHEN sum(bet_1D)+sum(pool_1D)-sum(member_intake_1D) > intake_1D THEN intake_1D
			WHEN sum(bet_1D)+sum(pool_1D)-sum(member_intake_1D) > 0 THEN sum(bet_1D)+sum(pool_1D)-sum(member_intake_1D)
			ELSE 0
		END intake_1D
	from byDateRootNNum u
	group by date,root,num,u.big_intake,small_intake,intake_1D,rebate,big_prize,small_prize,prize_1d,sc order by date,num
),
byDateNRoot as (select
		date,
		root,
		sum(big_intake) big_intake,
		sum(small_intake) small_intake,
		sum(intake_1D) intake_1D,
		sum(big_intake*rebate/100) big_rebate,
		sum(small_intake*rebate/100) small_rebate,
		sum(intake_1D*rebate/100) rebate_1D,
		sum(big_prize * big_intake) big_strike,
		sum(small_prize * small_intake) small_strike,
		sum(prize_1D * intake_1D) strike_1D,
		sum(big_prize * big_intake * sc/100) big_strike_comm,
		sum(small_prize * small_intake * sc/100) small_strike_comm
	from drn2 drn
	group by date,root order by date,root),
byroot as (select
		root user_id,
		sum(big_intake) big,
		sum(small_intake) small,
		sum(intake_1D) p1d, 
		sum(big_rebate) big_rebate,
		sum(small_rebate) small_rebate,
		sum(rebate_1D) rebate_1D,
		sum(big_strike) big_strike,
		sum(small_strike) small_strike,
		sum(strike_1D) strike_1D,
		sum(big_strike_comm) big_strike_comm,
		sum(small_strike_comm) small_strike_comm
	from byDateNRoot
	group by root)
select * from byroot`;

const frIntakeQ=`select user_id,big_intake big,small_intake small,intake_1d p1d,big_rebate,small_rebate,rebate_1d,big_strike,small_strike,strike_1d,big_sc big_strike_comm,small_sc small_strike_comm,sc_1d strike_comm_1d from v_fr_intake($1,$2,$3)`;

const frPbcq=`-- full report pbc

WITH
mbr as (select id from m_user where id=$1),
_dr as(SELECT date_trunc('day', dd)::date dd
	FROM generate_series (
	$2::timestamp,
	$3::timestamp,
	'1 day'::interval) dd),
dr as (select dd,cast(EXTRACT(ISODOW FROM dd) as int) d from _dr where EXTRACT(ISODOW FROM _dr.dd) = any(ARRAY[3,6,7])),
nums as (select id ticket_id,UNNEST(numbers) num from t_active_ticket),
usr as (select 
		dr.dd date,
		mbr.id root,
		t.id ticket_id,
		UNNEST(get_from(mbr.id,users)) user_id
	from t_active_ticket t
	inner join dr on t.draw_days @> ARRAY[dr.dd] and not (t.day_left @> ARRAY[dr.d])
	inner join mbr on t.users @> ARRAY[mbr.id]),
rb as (select
		u.ticket_id,
		u.user_id,
		COALESCE(cast(t.user_configs #>> ARRAY[u.user_id,'rb'] as float),0) rebate
	from usr u
	left join t_active_ticket t on t.id=u.ticket_id
	group by ticket_id,user_id,rebate),
it as (select
		u.date,
		t.game_id game_id,
		u.user_id,
		max(COALESCE(cast(t.user_configs #>> ARRAY[u.user_id,'bit'] as float),0)) big_intake,
		max(COALESCE(cast(t.user_configs #>> ARRAY[u.user_id,'sit'] as float),0)) small_intake,
		max(COALESCE(cast(t.user_configs #>> ARRAY[u.user_id,'it'] as float),0)) intake_1D
	from usr u
	inner join t_active_ticket t on u.ticket_id=t.id
	group by u.date,t.game_id,u.user_id
	order by u.date,t.game_id,u.user_id),
tt as (select 
		u.date,
		u.root,
		u.user_id,
		u.ticket_id,
		n.num
	from usr u
	inner join nums n on n.ticket_id=u.ticket_id),
tu as (select 
		u.date,
		u.root,
		u.user_id,
		t.place_to,
		t.game_id,
		u.ticket_id,
		u.num,
		COALESCE(it.big_intake,0) big_intake,
		COALESCE(it.small_intake,0) small_intake,
		COALESCE(rb.rebate,0) rebate,
		COALESCE(im.big_intake,0) member_big_intake,
		COALESCE(im.small_intake,0) member_small_intake,
		CASE WHEN t.game_id<>'1D' THEN 0
			WHEN t.place_to<>u.user_id THEN 0
			ELSE COALESCE(it.intake_1D,0) END intake_1D,
		CASE WHEN t.game_id<>'1D' THEN 0
			WHEN t.place_to=u.user_id THEN 0
			ELSE COALESCE(im.intake_1D,0) END member_intake_1D,
		CASE WHEN t.place_to=u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'b'] as float),0) ELSE 0 END big_bet,
		CASE WHEN t.place_to=u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'s'] as float),0) ELSE 0 END small_bet,
		CASE WHEN t.place_to<>u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'b'] as float),0) ELSE 0 END big_pool,
		CASE WHEN t.place_to<>u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'s'] as float),0) ELSE 0 END small_pool,
		CASE WHEN t.place_to=u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'1D'] as float),0) ELSE 0 END bet_1D,
		CASE WHEN t.place_to<>u.root and t.place_to=u.user_id THEN COALESCE(cast(bets #>> ARRAY[u.num,'1D'] as float),0) ELSE 0 END pool_1D,

		COALESCE(s.big_prize,0) big_prize,
		COALESCE(s.small_prize,0) small_prize,
		COALESCE(s.prize_1d,0) prize_1d,
		COALESCE(cast(t.user_configs #>> ARRAY[u.root,'sc'] as float),0) sc
	from tt u
	left join t_active_ticket t on u.ticket_id=t.id
	left join rb on u.ticket_id=rb.ticket_id and u.root=rb.user_id
	left join it on it.date=u.date and it.game_id=t.game_id and it.user_id=u.root
	left join it im on im.date=u.date and im.game_id=t.game_id and im.user_id=u.user_id and im.user_id<>u.root
	left join t_strike s on s.ticket_id=t.id and s.num=u.num and s.draw_date=u.date order by u.date,num),
byDateRootNNum as (select 
		u.date,
		root,
		num,
		sum(big_bet) big_bet,
		sum(small_bet) small_bet,
		sum(big_pool) big_pool,
		sum(small_pool) small_pool,
		sum(bet_1D) bet_1D,
		sum(pool_1D) pool_1D,
		u.big_intake big_intake,
		u.small_intake small_intake,
		u.rebate,
		u.big_prize,
		u.small_prize,
		u.prize_1d,
		u.sc,
		u.member_big_intake member_big_intake,
		u.member_small_intake member_small_intake,
		u.intake_1D intake_1D,
		u.member_intake_1D member_intake_1D
	from tu u
	group by u.date,root,num,u.big_intake,u.small_intake,u.rebate,intake_1D,member_intake_1D,u.big_prize,u.small_prize,u.prize_1d,u.sc,u.member_big_intake,u.member_small_intake order by u.date,num),
drn2 as (select
		date,
		root,
		num,
		--sum(big_bet) big_bet,
		--sum(small_bet) small_bet,
		--sum(big_pool) big_pool,
		--sum(small_pool) small_pool,
		--big_intake big_intake,
		--small_intake small_intake,
		--sum(member_big_intake) member_big_intake,
		--sum(member_small_intake) member_small_intake,
		big_prize,
		small_prize,
		prize_1d,
		sc,
		rebate,
		CASE WHEN sum(big_bet)+sum(big_pool)-sum(member_big_intake)-big_intake < 0 THEN 0 ELSE sum(big_bet)+sum(big_pool)-sum(member_big_intake)-big_intake END big_pass,
		CASE WHEN sum(small_bet)+sum(small_pool)-sum(member_small_intake)-small_intake < 0 THEN 0 ELSE sum(small_bet)+sum(small_pool)-sum(member_small_intake)-small_intake END small_pass,
		CASE WHEN sum(bet_1D)+sum(pool_1D)-sum(member_intake_1D)-intake_1D < 0 THEN 0 ELSE sum(bet_1D)+sum(pool_1D)-sum(member_intake_1D)-intake_1D END pass_1D
		--CASE WHEN sum(big_bet)+sum(big_pool)-sum(member_big_intake)-big_intake < 0 THEN 0 ELSE (sum(big_bet)+sum(big_pool)-sum(member_big_intake)-big_intake)*rebate/100 END big_rebate,
		--CASE WHEN sum(small_bet)+sum(small_pool)-sum(member_small_intake)-small_intake < 0 THEN 0 ELSE (sum(small_bet)+sum(small_pool)-sum(member_small_intake)-small_intake)*rebate/100 END small_rebate,
		--CASE WHEN sum(big_bet)+sum(big_pool)-sum(member_big_intake)-big_intake < 0 THEN 0 ELSE (sum(big_bet)+sum(big_pool)-sum(member_big_intake)-big_intake) * big_prize END big_strike,
		--CASE WHEN sum(small_bet)+sum(small_pool)-sum(member_small_intake)-small_intake < 0 THEN 0 ELSE (sum(small_bet)+sum(small_pool)-sum(member_small_intake)-small_intake) * small_prize END small_strike--,
	from byDateRootNNum u
	group by date,root,num,u.big_intake,small_intake,intake_1D,rebate,big_prize,small_prize,prize_1d,sc order by date,num
),
byDateNRoot as (select
		date,
		root,
		sum(big_pass) big_pass,
		sum(small_pass) small_pass,
		sum(pass_1D) pass_1D,
		sum(big_pass*rebate/100) big_rebate,
		sum(small_pass*rebate/100) small_rebate,
		sum(pass_1D*rebate/100) rebate_1D,
		sum(big_prize * big_pass) big_strike,
		sum(small_prize * small_pass) small_strike,
		sum(prize_1d * pass_1d) strike_1d,
		sum(big_prize * big_pass * sc/100) big_strike_comm,
		sum(small_prize * small_pass * sc/100) small_strike_comm
	from drn2 drn
	group by date,root order by date,root),
byroot as (select
		root user_id,
		sum(big_pass) big,
		sum(small_pass) small,
		sum(pass_1D) p1d,
		sum(big_rebate) big_rebate,
		sum(small_rebate) small_rebate,
		sum(rebate_1D) rebate_1D,
		sum(big_strike) big_strike,
		sum(small_strike) small_strike,
		sum(strike_1D) strike_1D,
		sum(big_strike_comm) big_strike_comm,
		sum(small_strike_comm) small_strike_comm
	from byDateNRoot
	group by root)
select * from byroot`;

frPbcQ=`select user_id,
big_pass big,small_pass small,pass_1d p1d,
big_rebate, small_rebate, rebate_1d,
big_strike, small_strike, strike_1d,
big_sc big_strike_comm, small_sc small_strike_comm, sc_1d strike_comm_1d
from v_fr_pbc($1,$2,$3)`;

function getReport(key,q){
	return (d)=>{
		return new Promise((resolve,reject)=>{
			let {user_id,fromDate,toDate}=d;
			dbconnect.query(q,[user_id,fromDate,toDate],(e,result)=>{
				if(e) return reject(e);
				d[key]=result.rows;
				resolve(d);
			});
		});
	};
}

exports.fullReport=(req,res)=>{
	let query=req.query,
		user_id=query.user_id,
		fromdate=query.fromdate,
		todate=query.todate,
		d={user_id};
		if(fromdate) d.fromDate=fromdate;
		if(todate) d.toDate=todate;
	isMyDescendant(req,res,user_id,()=>{
		Promise.resolve(d)
			.then(getReport('bet',FrBetQ))
			.then(getReport('personalTicket',frPersonalTicketQ))
			.then(getReport('intake',frIntakeQ))
			.then(getReport('pbc',frPbcQ))
			.then(d=>{
				let {bet,personalTicket,intake,pbc}=d;
				res.status(200).json({s:1,d:{bet,personalTicket,intake,pbc}});
			}).catch(e=>{
				console.log('e==>',e);
				if(e.code) return res.status(500).json(error.db(e));
				return res.status(500).json({s:0,e:e.toString()});
			});	
	});
};

function strikeReportQ(user_id,game_id,draw_date){
let q=`with 
cfg as (select $1::text gid,$2::text uid,$3::date dd),
nums as (select id ticket_id,UNNEST(numbers) num from t_active_ticket)
select 
	s.num,
	sum(COALESCE(cast(bets #>> ARRAY[n.num,'b'] as float),0) * s.big_prize) big_strike,
	sum(COALESCE(cast(bets #>> ARRAY[n.num,'s'] as float),0) * s.small_prize) small_strike
from t_strike s, t_active_ticket t, nums n, cfg
where s.ticket_id=t.id 
	and n.ticket_id=s.ticket_id
	and n.num=s.num
	and t.place_to=cfg.uid
	and t.game_id=cfg.gid
	and s.draw_date = cfg.dd
group by s.num`;
	return (d)=>{
		return new Promise((resolve,reject)=>{
			dbconnect.query(q,[game_id,user_id,draw_date],(e,result)=>{
				if(e) return reject(e);
				d[game_id]=result.rows;
				resolve(d);
			});
		});
	};
}

exports.strikeReport=(req,res)=>{
	let query=req.query,
		user_id=query.user_id,
		draw_date=query.draw_date;
		d={};
	if(!(user_id && draw_date)) return res.status(500).json({s:0,e:'invalid parameters'});
	if(!((draw_date||'').match(/^\d{4}-\d{2}-\d{2}$/))) return res.status(500).json({s:0,e:'invalid fromat of draw_date parameters'});
	let x=Promise.resolve(d);
	['4D','3D','2D'].forEach(game_id=>{
		x=x.then(strikeReportQ(user_id,game_id,draw_date));
	});
	x.then(d=>{
		res.status(200).json({s:1,d});
	}).catch(e=>{
		console.log(new Error(e));
		if(e.code) return res.status(500).json(error.db(e));
		return res.status(500).json({s:0,e:e.toString()});
	});	
};

exports.testReport=(req,res)=>{
	let user_id=req.query.user_id;
let q=`select
	b.user_id,
	b.id,
	b.ticket_id,
	t.game_id,
	t.num,
	CASE WHEN b.type='strike' THEN 0 ELSE b.big*t.permute_length END big,
	CASE WHEN b.type='strike' THEN 0 ELSE COALESCE(b.passup,b.small)*t.permute_length END small,
	CASE WHEN b.type='strike' THEN 0 ELSE (b.big+COALESCE(b.passup,b.small))*t.permute_length*b.rebate/100 END rebate,
	b.from_upline strike,
	b.pass,
	b.draw_date
from t_current_balance b
left join t_active_ticket t on b.ticket_id=t.id
left join m_user u on u.id=b.user_id
where u.id=$1 and (b.type='place_bet' or b.type='strike')`;
	dbconnect.query(q,[user_id],(e,r)=>{
		console.log(new Error(e));
		if(e) return res.status(500).json(error.db(e));
		res.status(200).json({s:1,d:r.rows});
	});
};
