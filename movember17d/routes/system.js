var dbconnect = require('../lib/dbconnect');

function dbVersion(cb){
	dbconnect.query('select version() as v',function(e, r){
		if(e){
			console.error(new Error(e));
			return cb(e);
		}
		if(r && r.rows && r.rows.length) return cb(null,r.rows[0]);
		return cb(null,false);
	});
}

exports.model={
	dbVersion:dbVersion
}