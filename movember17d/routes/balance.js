var dbconnect = require('../lib/dbconnect'),
	sql = require('../lib/sql'),
	redisBalance = require('../lib/redis').balance,
	Logs =  require('./logs').model,
	error = require('../lib/error');
	
//function addBalance(data,cb,valData){
//	var vdata=Object.assign({},data);
//	var iq=sql.insertQuery('t_balance',vdata,valData);
//	//console.log('iq==>',iq);
//	dbconnect.query(iq.q, iq.v, function(e, r){
//		if (e) {
//			console.error(new Error(e));
//			return cb(e);
//		}
//		if(!(r && r.rows && r.rows.length)) return cb('insert balance failed');
//		return cb(null,r.rows[0]);
//	});
//}

function getBalance(ids,cb){
	ids=Array.isArray(ids)?ids:[ids];
	redisBalance.getMulti(ids||[],(e,d)=>{
		if(e){
			console.error(new Error(e));
			return cb(e);
		}
		let id2=ids.filter(id=>{return !d.hasOwnProperty(id);}),
			q='select user_id,sum(amount) as balance from t_current_balance where user_id=ANY($1) and amount != $2 and paid != $3 group by user_id',
			v=[id2,NaN,true];
		dbconnect.query(q,v,(e,r)=>{
			if(e){
				console.error(new Error(e));
				return cb(e);
			}
			r.rows.forEach(i=>{
				d[i.user_id]=i.balance;
				redisBalance.set(i.user_id,i.balance,e=>{
					if(e) console.error(new Error(e));
				});
			});
			cb(null,d);
		});
	});
}

exports.balanceDetail=function(req, res){
	let params=req.params,
		id=params.id;
	dbconnect.query('select * from t_current_balance where id=$1',[id], function(e, r){
		if(e){
			console.error(new Error(e));
			return res.status(500).json({ s:0,e:{msg:e.toString()}});
		}
		if(!(r && r.rows && r.rows.length)){
			return res.status(500).json({ s:0,e:{msg:'balance not found'}});
		}
		res.status(200).send({ s:1,d:r.rows[0]});
	});
};

//select date_trunc('day', b.date) as date,t.num,t.match, sum(b.amount) from t_current_balance b left join t_active_ticket t on t.id=b.ticket_id where game_id='1D' and b.user_id='AG10028' and b.draw_day = 3 group by date_trunc('day', b.date),t.match, t.num

exports.balanceList=function(req, res){
	let query=req.query;
	sql.ooSelectQuery('t_current_balance',{
		query,
		orders:{date:'desc'},
		filterFields:['ticket_id','type','user_id','paid']},
		(e,{q,v})=>{
			if(e) {
				console.error(new Error(e));
				return res.status(500).json({ s:0,e:{msg:e.toString()}});
			}
			dbconnect.query(q,v,(e, r)=>{
				if(e){
					console.log('q==>',q);
					console.error(new Error(e));
					return res.status(500).json({ s:0,e:{msg:e.toString()}});
				}
				res.status(200).send({ s:1,d:r.rows});
			});
		});
};

exports.payBalance=function(req, res){
	let body=req.body||{},
		from_user=(req.params||{}).userid,
		user_id=req.user.id;
		//amount=parseFloat(body.amount),
		description=body.description,
		type='balance',
		balances=(body.balances||[]).map(id=>{return parseInt(id,10);});
		//detail={balance:balances};
	//select (sum(to_upline)-sum(from_upline)) as amount from t_current_balance where paid='false' and id = ANY($1)
	dbconnect.query(`select (sum(to_upline)-sum(from_upline)) as amount from t_current_balance where paid='false' and id = ANY($1)`,[balances],(e,r)=>{
		if(e){
			console.error(new Error(e));
			return res.status(500).json({ s:0,e:{msg:e.toString()}});
		}
		let [d]=r.rows||[],
			{amount}=d||{};
		sql.insertQuery('t_payment',{user_id,from_user,amount,type,description},{},(q,v)=>{
			dbconnect.query(q,v,(e, r)=>{
				if(e){
					console.error(new Error(e));
					return res.status(500).json({ s:0,e:{msg:e.toString()}});
				}
				let [d]=r.rows||[],{id}=d||{};
				dbconnect.query('update t_current_balance set paid=\'true\',payment_id=$2 where id = ANY($1)',[balances,id],(e)=>{
					if(e){
						console.error(new Error(e));
						return res.status(500).json({ s:0,e:{msg:e.toString()}});
					}
					res.status(200).send({ s:1,d:r.rows[0]});
				});
				Logs.addLog({user_id:req.user.id,type:'balance_payment',description:`balance payment from ${user_id} amount ${amount}`,params:{
					payment_id:id,
					from_user,
					amount:req.amount,
					type,
					description,
					balances
				}});
			});
		});	
	});	
};

//function balanceField(useridValue){
//	return `(select
//	COALESCE((select sum(from_upline)-sum(to_upline) from t_current_balance where paid='false' and user_id = ${useridValue}),0) +
//	COALESCE((select sum(to_upline)-sum(from_upline) from t_current_balance where paid='false' and user_id in (select id from m_user where member_of=${useridValue})),0)
//) as balance`;
//}

function balanceField(useridValue){
	return `(v_balance(${useridValue})) as balance`;
}

exports.paymentList=function(req, res){
	let query=req.query||{},
		qo={};
	if(query.orderby) qo[query.orderby]=query.order||'DESC';
	if(query.orderby != 'date') qo.date='DESC';
	query.user_id=req.user.id;
	let addFields=[];
	sql.ooSelectQuery('t_payment',{
		query,
		orders:qo, //{'date':'desc'},
		searchFields:['from_user','description'],
		filterFields:['id','type','user_id','from_user'],
		addFields,
		//full:true
	},(e,{q,v})=>{
		dbconnect.query(q,v, function(e, r){
			if(e) {
				if(e.code) return res.status(500).json(error.db(e));
				return res.status(500).json({s:0,e:e.toString()});
			}
			return res.status(200).json({s:1,d:r.rows});
		});
	});
};

exports.model={
	//addBalance:addBalance,
	getBalance,
	balanceField,
};
