var express = require('express'),
	router = express.Router(),
	auth = require('../lib/auth'),
	id = require('./id'),
	user = require('./user'),
	bet = require('./bet'),
	fixbet = require('./fixbet'),
	ticket = require('./ticket'),
	balance = require('./balance'),
	payment = require('./payment'),
	game = require('./game'),
	notif = require('./notif'),
	logs = require('./logs'),
	result = require('./result'),
	report = require('./report'),
	System = require('./system').model;

router.get('/',function(req, res) {
	var d={api:'movember17d api v0.0.1'};
	if(req.user){
		d.userid=req.user.id;
		if(req.user.type == 'developer'){
			d.node=process.version;
			return System.dbVersion(function(e,r){
				if(e) return res.status(422).json({s:0,e:e.toString()});
				d.db=r;
				res.status(200).json({s:1,d:d});
			});
		}
	}
	res.status(200).json({s:1,d:d});
});

router.post('/_init_test',function(req,res){
	var dbconnect = require('../lib/dbconnect'),
		User = require('./user').model;
	dbconnect.query('delete from m_user;'+
					//'delete from m_agent_detail;'+
					//'delete from m_player_detail;'+
					//'delete from t_user_history;'+
					'delete from t_user_notif;',[],function(e){
		if(e){
			console.error(new Error(e));
			return res.status(500).json({s:0,e:e.toString()});
		}
		User.addUser({
			id:'DEV1',
			type:User.type.DEVELOPER,
			password:'dev1',
			fake_password:'mydev1',
		},function(e,r){
			if(e) return res.status(500).json({s:0,e:e.toString()});
			res.status(200).json({s:1,d:r});
		});
	});
});

router.get('/announcement',auth.isAuthenticated,game.getMsgFile('announcement'));
router.get('/agreement',auth.isAuthenticated,game.getMsgFile('agreement'));
router.get('/help',auth.isAuthenticated,game.getMsgFile('help'));

router.put('/announcement',auth.isAdminOrSuper,game.updateMsgFile('announcement'));
router.put('/agreement',auth.isAdminOrSuper,game.updateMsgFile('agreement'));
router.put('/help',auth.isAdminOrSuper,game.updateMsgFile('help'));

router.put('/runningtext',auth.isAdminOrSuper,game.updateRunningText);

router.post('/agree',auth.isAuthenticated,function(req,res){
	req.session.agree=true;
	res.status(200).json({s:1,d:{}});
});
router.post('/login',user.login);
router.get('/logout',user.logout);
router.get('/id',auth.isAuthenticated,id.getId);
router.get('/sagentid',auth.isAdminOrSuper,id.sagentId);
router.get('/nestedid',auth.isAuthenticated,id.nestedId);
router.get('/game',auth.isAuthenticated,game.game);
router.get('/config',auth.isAuthenticated,game.config);
router.get('/date',auth.isAuthenticated,game.date);
router.get('/drawdate',auth.isAuthenticated,game.drawDate);
router.get('/completedrawdate',auth.isAuthenticated,game.getCompleteDrawDate);
router.get('/currentprize',auth.isAuthenticated,game.getCurrentPrize);

router.get('/me',auth.isAuthenticated,user.me);
router.put('/me',auth.isAuthenticated,user.updateMe);
router.get('/user',auth.isAuthenticated,user.list);
router.get('/user/:id',auth.isAuthenticated,user.detail);

router.put('/user/:id',auth.isAuthenticated,user.update);
router.put('/status/:id',auth.isAuthenticated,user.updateStatus);
router.put('/credit/:id',auth.isAuthenticated,user.updateCredit);

router.post('/sadmin',auth.isDeveloper,user.addNewUser('sadmin'));
router.post('/admin',auth.isSuperAdminOrDeveloper,user.addNewUser('admin'));
router.post('/sagent',auth.isAdminOrSuper,user.addNewUser('sagent'));

router.post('/agent',auth.isAuthenticated,user.addNestedUser('agent'));
router.post('/player',auth.isAuthenticated,user.addNestedUser('player'));

router.post('/bet',auth.isAuthenticated,bet.placeBet);
router.delete('/bet/:ticketid',auth.isAuthenticated,ticket.voidTicket);
router.post('/savebet',auth.isAuthenticated,ticket.saveTicket);
router.get('/savebet',auth.isAuthenticated,ticket.savedTicketList);
router.delete('/savebet/:id',auth.isAuthenticated,ticket.savedTicketDelete);
router.get('/fixbet/:pageid',auth.isAuthenticated,fixbet.fixBetByPage);
router.get('/fixbetpage',auth.isAuthenticated,fixbet.listFixBetPage);
router.put('/fixbetpage/:id',auth.isAuthenticated,fixbet.updateFixBet);
router.post('/fixbet',auth.isAuthenticated,fixbet.saveFixBet);

router.get('/ticket',auth.isAuthenticated,ticket.listActiveTicket);
router.get('/ticketpage',auth.isAuthenticated,ticket.listTicketPage);
router.get('/summarybet/:placeto',auth.isAuthenticated,ticket.summaryBets);


router.get('/balance',auth.isAuthenticated,balance.balanceList);
router.get('/balance/:id',auth.isAuthenticated,balance.balanceDetail);
router.post('/paybalance/:userid',auth.isAuthenticated,balance.payBalance);
//router.get('/payment',auth.isAuthenticated,balance.paymentList);
router.get('/payments/',auth.isAuthenticated,payment.payments);
router.get('/payments/:id',auth.isAuthenticated,payment.payments);
router.get('/paymenthistory/',auth.isAuthenticated,payment.paymentHistory);
router.get('/paymenthistory/:id',auth.isAuthenticated,payment.paymentHistory);
router.post('/payments/',auth.isAuthenticated,payment.pay);
router.post('/payments/:id',auth.isAuthenticated,payment.pay);

router.get('/mynotif',auth.isAuthenticated,notif.getMyNotif);
router.get('/mylogs',auth.isAuthenticated,logs.getMyLog);

router.get('/getcreateresult',auth.isAdminOrSuper,result.getCreateResult);
router.put('/result/:id',auth.isAdminOrSuper,result.updateResult);
router.post('/drawresult/:id',auth.isAdminOrSuper,result.drawResult);
router.get('/result',auth.isAuthenticated,result.lastDrawResult);
router.get('/resultbydate/:drawdate',auth.isAuthenticated,result.resultByDrawDate);


router.get('/summarybet',auth.isAuthenticated,report.summaryBet);
router.get('/summarybetdetail',auth.isAuthenticated,report.summaryBetDetail);
router.get('/tallybet',auth.isAuthenticated,report.summaryTally);
router.get('/fullreport',auth.isAuthenticated,report.fullReport);
router.get('/strikereport',auth.isAuthenticated,report.strikeReport);
router.get('/testreport',auth.isAuthenticated,report.testReport);

router.get('*',function(req, res){
	res.status(404).send({s:0,e:'invalid API URL'});
});


module.exports = router;