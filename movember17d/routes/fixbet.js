var dbconnect = require('../lib/dbconnect'),
    sql = require('../lib/sql'),
	ajv = require('../lib/ajv'),
	schema = require('../schema/bet'),
	validateFixed=ajv(schema.fixed),
    validate2Ditem=ajv(schema['2Ditem']),
    validate3Ditem=ajv(schema['3Ditem']),
    validate4Ditem=ajv(schema['4Ditem']),
    validate2DitemW=ajv(schema['2D*item']),
    validate3DitemW=ajv(schema['3D*item']),
    validate4DitemW=ajv(schema['4D*item']),
	error = require('../lib/error');

function saveFixedBet(body,req,res){
	var _uPermute={};
    function uPermute(numb){
        if(_uPermute[numb]) return _uPermute[numb];
        _uPermute[numb]=uniqPermute(numb);
        return _uPermute[numb];
    }
    var _wildcard={};
    function Wildcard(numb){
        if(_wildcard[numb]) return _wildcard[numb];
        _wildcard[numb]=wildChar(numb);
        return _wildcard[numb];
    }
	//numbers=(bt==='R'?uPermute(n):(bt==='*'?Wildcard(n):[n])).length,
    function valida(v){
        let {g,bt}=v;
        if(bt==='*'){
            if(g==='4D') return validate4DitemW;
            if(g==='3D') return validate3DitemW;
            if(g==='2D') return validate2DitemW;
        }
        if(g==='4D') return validate4Ditem;
        if(g==='3D') return validate3Ditem;
        if(g==='2D') return validate2Ditem;
    }
    let e=[];
    body.detail.forEach((v)=>{
        let vali=valida(v);
        if(!vali(v)) e.push(vali);
    });
    if(e.length) return res.status(500).json(error.schema(e[0]));
    
	let {form_type,place_to,place_by}=body,
		page={form_type,place_to,place_by},
		tickets=[];
	body.detail.forEach((v)=>{
		let {g,n,b,s,bt}=v,
		permute_length=(bt==='R'?uPermute(n):(bt==='*'?Wildcard(n):[n])).length;
		tickets.push({
			place_to,
			place_by,
			game_id:g,
			num:n,
			big:b,
			small:s,
			bt,
			permute_length
		});
	});
	sql.insertQuery('m_fix_ticket_page',page,{},(q,v)=>{
		dbconnect.query(q,v,(e,rPage)=>{
			if(e) {
				if(e.code) return res.status(500).json(error.db(e));
				return res.status(500).json({s:0,e:e.toString()});
			}
			let tPage=rPage.rows[0];
			tickets.forEach((vTicket)=>{
				vTicket.page_id=tPage.id;					
				sql.insertQuery('m_fix_ticket',vTicket,{},(q,v)=>{
					console.log(JSON.stringify({q,v}));
					dbconnect.query(q,v,(e)=>{if(e) console.log(new Error(e));});
				});
			});
			res.status(200).send({ s:1,d:{page:tPage,tickets}});
		});
	});
}

function fixBetPageList(query,cb,res,history){
	query=query||{};
	let qo={};
		
	query.limit=query.limit||'100';
	query.page=query.page||'1';
	if(query.orderby) qo[query.orderby]=query.order||'DESC';
	if(query.orderby != 'date') qo.date='DESC';
	
	let addFields=[];
	addFields.push('(select sum((big+small)*permute_length) from m_fix_ticket t where page_id=p.id) as page_amount');
	let customFilter=[];
	//if(history){
	//	addFields.push('(select sum((big+small+amount)*permute_length*(day_count-day_length)) from t_active_ticket t where page_id=p.id) as page_amount');
	//	customFilter.push({f:'p.id',o:'in',vv:'(select distinct page_id from t_active_ticket where day_length < day_count)'});
	//} else {
	//	addFields.push('(select sum((big+small+amount)*permute_length*day_length) from t_active_ticket t where page_id=p.id) as page_amount');
	//	customFilter.push({f:'p.id',o:'in',vv:'(select distinct page_id from t_active_ticket where day_length > 0)'});
	//}
		
	sql.ooSelectQuery('m_fix_ticket_page',{
		query,
		alias:'p',
		orders:qo,
		searchFields:['place_to','place_by'],
		filterFields:['id','place_to','place_by'],
		addFields,
		customFilter,
		//full:true
	},(e,{q,v})=>{
		dbconnect.query(q,v, function(e, r){
			if(res){
				if(e) {
					if(e.code) return res.status(500).json(error.db(e));
					return res.status(500).json({s:0,e:e.toString()});
				}
				return res.status(200).json({s:1,d:r.rows});
			}
			if(e){
				console.error(new Error(e));
				if(cb) return cb(e);
				return;
			}
			if(cb) return cb(null,r.rows);
		});
	});
}

function fixBetByPage(pageid,query,cb,res,history){
	query=query||{};
	let qo={};
		
	query.limit=query.limit||'100';
	query.page=query.page||'1';
	if(query.orderby) qo[query.orderby]=query.order||'DESC';
	if(query.orderby != 'date') qo.date='DESC';
	
	let addFields=[];
	let customFilter=[{
		f:'page_id',
		v:pageid
	}];
	sql.ooSelectQuery('m_fix_ticket',{
		query,
		//alias:'p',
		orders:qo,
		searchFields:['place_to','place_by'],
		filterFields:['id','place_to','place_by'],
		addFields,
		customFilter,
		//full:true
	},(e,{q,v})=>{
		dbconnect.query(q,v, function(e, r){
			if(res){
				if(e) {
					if(e.code) return res.status(500).json(error.db(e));
					return res.status(500).json({s:0,e:e.toString()});
				}
				return res.status(200).json({s:1,d:r.rows});
			}
			if(e){
				console.error(new Error(e));
				if(cb) return cb(e);
				return;
			}
			if(cb) return cb(null,r.rows);
		});
	});
}

exports.saveFixBet=function(req, res){
	var body=Object.assign({},req.body||{}),
        validate=validateFixed;

	body.place_to=body.place_to||req.user.id;
	body.place_by=req.user.id;
	body.form_type=body.form_type||'fixed';
    
    if(!validate(body)) return res.status(500).json(error.schema(validate));

    return saveFixedBet(body,req,res);
};

exports.updateFixBet=function(req, res){
	var params=req.params,
		body=req.body,
        id=params.id,
		active=body.active;
	dbconnect.query('update m_fix_ticket_page set active=$1 where id=$2 returning *',[active,id],(e,r)=>{
		if(e) {
			if(e.code) return res.status(500).json(error.db(e));
			return res.status(500).json({s:0,e:e.toString()});
		}
		res.status(200).send({ s:1,d:(r && r.rows && r.rows[0])});
	});
};

exports.listFixBetPage=function(req, res){
	var query=req.query||{};
	fixBetPageList(query,null,res);
};

exports.fixBetByPage=function(req, res){
	var query=req.query||{},
		params=req.params||{},
		pageid=params.pageid;
	fixBetByPage(pageid,query,null,res);
};
