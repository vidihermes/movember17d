var dbconnect = require('../lib/dbconnect'),
	sql = require('../lib/sql');

var exp=3600, //1 jam
	//firstId=10001,
	firstId=1001,
	idPrefix={
		player:'PL',
		agent:'AG',
		sagent:'SAG',
		admin:'AD',
		sadmin:'SAD',
		developer:'DEV',
	}

//https://stackoverflow.com/a/34483399
function nextChar(c) {
	function nextLetter(l){
        if(l<90){
            return String.fromCharCode(l + 1);
        }
        else{
            return 'A';
        }
    }
    
    function same(str,char){
        var i = str.length;
        while (i--) {
            if (str[i]!==char){
                return false;
            }
        }
        return true;
    }
	
	var u = c.toUpperCase();
	if (same(u,'Z')){
		var txt = '';
		var i = u.length;
		while (i--) {
			txt += 'A';
		}
		return (txt+'A');
	} else {
		var p = "";
		var q = "";
		if(u.length > 1){
			p = u.substring(0, u.length - 1);
			q = String.fromCharCode(p.slice(-1).charCodeAt(0));
		}
		var l = u.slice(-1).charCodeAt(0);
		var z = nextLetter(l);
		if(z==='A'){
			return p.slice(0,-1) + nextLetter(q.slice(-1).charCodeAt(0)) + z;
		} else {
			return p + z;
		}
	}
}

function newSagentID(cb){
	let q=`with s as(SELECT id,length(id::text) l FROM m_user where type='sagent' and id not SIMILAR TO  '%(0|1|2|3|4|5|6|7|8|9)%')
		select max(id) id from s where l=(select(max(l)) from s)`;
	dbconnect.query(q, [], (e, r)=>{
		if(e) return cb(e);
		if(!(r && r.rows && r.rows[0] && r.rows[0].id)) return cb(null,'A');
		cb(null,nextChar(r.rows[0].id));
	});
}

function pad(s,n){
	s=s.toString();
	return ('0'.repeat(n-s.length))+s;
}

function getId(prefix,cb){
	let v=[prefix],
		s='select (select max(id) from m_id where prefix = $1) as id_max, '+
		'(select min(id) from m_id where prefix = $1 and created < now() - interval \''+exp+' second\') as id_exp';
	
	dbconnect.query(s, v, function(e, r){
		//console.log('r.rows',r.rows)
		if (e) return cb(e);
		if(!(r && r.rows && r.rows.length)) return cb('can not get data');
		var id_max=r.rows[0].id_max,
			id_exp=r.rows[0].id_exp;
		if(id_exp) {
			console.log('masuk id_exp');
			dbconnect.query('update m_id set created=now() where id=$1 and prefix=$2',[id_exp,prefix], function(){});
			return cb(null,id_exp);
		}
		if(id_max) {
			console.log('masuk id_max');
			var cid=parseInt(id_max,10)+1;
			dbconnect.query('insert into m_id (id,created,prefix) values($1,now(),$2)',[cid,prefix], function(e){
				if(e) console.error(new Error(e));
			});
			return cb(null,cid);
		}
		dbconnect.query('insert into m_id (id,created,prefix) values($1,now(),$2)',[firstId,prefix], function(e){
			if(e) console.error(new Error(e));
		});
		return cb(null,firstId);
	});
}

function useId(fullId,prefix){
	//var id=parseInt(fullId.substr(prefix.length));
	//dbconnect.query('update m_id set created=null where id=$1 and prefix=$2',[id,prefix], function(e){
	//	if(e) console.error(new Error(e));
	//});
	
	dbconnect.query('update m_id set created=null where prefix||id=$1',[fullId], function(e){
		if(e) console.error(new Error(e));
	});
}

exports.getId=function(req, res){
	var query=req.query||{},
		prefix=idPrefix[query.type]||'';
	getId(prefix,function(e,r){
		if(e) return res.status(500).json({s:0,e:e.toString()});
		res.status(200).json({s:1,d:prefix+pad(r,5)});
	});
};

exports.sagentId=(req,res)=>{
	newSagentID((e,r)=>{
		if(e) return res.status(500).json({s:0,e:e.toString()});
		res.status(200).json({s:1,d:r});
	});
};

exports.nestedId=(req,res)=>{
	var query=req.query||{},
		type=query.type||'player',
		parent=query.parent;
	if(!(type==='player' || type==='agent')) return res.status(500).json({s:0,e:'invalid user type'});
	if(!parent){
		if(['sagent','agent'].indexOf(req.user.type) > -1){
			parent=req.user.id;
		} else {
			return res.status(500).json({s:0,e:'invalid parent parameter'});
		}
	}
	let User=require('./user').model;
	User.getUserRoot(parent,(e,sagentId)=>{
		if(e) return res.status(500).json({s:0,e:e.toString()});
		if(!sagentId) return res.status(500).json({s:0,e:'super agent not found'});
		//console.log('sagentId==>',sagentId);
		if(!(sagentId.match(/^[A-Z]+$/))) return res.status(500).json({s:0,e:'invalid super agent id'});
		getId(sagentId+idPrefix[type],function(e,r){
			if(e) return res.status(500).json({s:0,e:e.toString()});
			res.status(200).json({s:1,d:sagentId+idPrefix[type]+pad(r,4)});
		});
	});
};

exports.model={
	getId:getId,
	useId:useId,
	idPrefix:idPrefix
};