var dbconnect = require('../lib/dbconnect'),
	sql = require('../lib/sql');	

function addLog(data,cb){
	sql.insertQuery('t_user_history',data,{},function(q,v){
		dbconnect.query(q, v, function(e, r){
			if(e){
				console.error(new Error(e));
				if(cb) return cb(e);
			}
			if(!(r && r.rows && r.rows.length)) {
				console.log('insert log failed');
				if(cb) return cb('insert log failed');
			}
			if(cb) return cb(null,r.rows[0]);
		});
	});
}

exports.getMyLog=function(req,res){
	let query=req.query||{};
	var qo={},
		limit=parseInt(query.limit||'25'),
		page=parseInt(query.page||'1');
	if(query.orderby) qo[query.orderby]=query.order||'DESC';
	if(query.orderby != 'date') qo.date='DESC';
	query.user_id=req.user.id;
	
	let {q,v}=sql.selectQuery('t_user_history',query,qo,['description'],['id','user_id','by_id','by_system','type']);
	dbconnect.query(q,v, function(e, r){
		if(e){
			console.error(new Error(e));
			return res.status(500).json({s:0,e:e.toString()});
		}
		res.status(200).json({s:1,d:(r||{}).rows||[]});
	});
}

exports.model={
	addLog:addLog
};