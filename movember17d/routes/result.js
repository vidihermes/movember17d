var dbconnect = require('../lib/dbconnect'),
	sql = require('../lib/sql'),
	Game = require('./game').model,
	Ajv = require('ajv'),
	ajv = new Ajv({useDefaults:true,coerceTypes:true})

let schema={
	type:'object',
	properties:{
		'1':{type:'string'},
		'2':{type:'string'},
		'3':{type:'string'},
		's':{
			type:'array',
			items: {type: 'string'},
			minItems:10,
			maxItems:10
		},
		'c':{
			type:'array',
			items: {type: 'string'},
			minItems:10,
			maxItems:10
		}
	},
	required: ['1','2','3','s','c']
};

let drawSchema={
	type:'object',
	properties:{
		'1':{type:'string',pattern: '^\\d{4}$'},
		'2':{type:'string',pattern: '^\\d{4}$'},
		'3':{type:'string',pattern: '^\\d{4}$'},
		's':{
			type:'array',
			items: {type: 'string',pattern: '^\\d{4}$'},
			minItems:10,
			maxItems:10
		},
		'c':{
			type:'array',
			items: {type: 'string',pattern: '^\\d{4}$'},
			minItems:10,
			maxItems:10
		}
	},
	required: ['1','2','3','s','c']
};

let validate=ajv.compile(schema);
let validDraw=ajv.compile(drawSchema);


function toDbDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

function firstDdsKey(dds){
	dds=dds||{};
	let ks=Object.keys(dds);
	for(let i=0;i<ks.length;i++){
		if(!dds[ks[i]].drawn) return ks[i];
	}	
}

function createBlankResult(){
	let rslt={
		'1':'',
		'2':'',
		'3':'',
		's':[],
		'c':[],
	};
	for(let i=0;i<10;i++){
		rslt.s.push('');
		rslt.c.push('');
	}
	return rslt;
}

exports.getCreateResult=function(req,res){
	Game.getDrawDate((e,dds)=>{
		if(e) {
			console.error(new Error(e));
			return res.status(500).json({s:0,e:e.toString()});
		}
		let draw_day=parseInt(firstDdsKey(dds),10);
		
		let dbDate=toDbDate(dds[draw_day.toString()].draw);
		dbconnect.query('select * from m_result where apply_date=$1',[dbDate], function(e, rslt){
			if(e) {
				console.error(new Error(e));
				return res.status(500).json({s:0,e:e.toString()});
			}
			if(rslt && rslt.rows && rslt.rows.length){
				return res.status(200).json({s:1,d:rslt.rows[0]});
			}
			dbconnect.query('insert into m_result(apply_date,draw_day,result,create_by) values($1,$2,$3,$4) returning *',[dbDate,draw_day,createBlankResult(),req.user.id], function(e, rslt){
				if(e) {
					console.error(new Error(e));
					return res.status(500).json({s:0,e:e.toString()});
				}
				if(rslt && rslt.rows && rslt.rows.length){
					return res.status(200).json({s:1,d:rslt.rows[0]});
				}
				return res.status(200).json({s:1,d:{}});
			});
		});
	});
}

exports.updateResult=function(req,res){
	let body=req.body,
		result=body.result;
	
	if(!validate(result)){
		return res.status(500).json({s:0,e:((validate.errors||[])[0]||{}).message});
	}
	
	dbconnect.query('update m_result set result=$1, update_by=$2, last_update=now() where id=$3 returning *',[result,req.user.id,req.params.id], function(e, rslt){
		if(e) {
			console.error(new Error(e));
			return res.status(500).json({s:0,e:e.toString()});
		}
		if(rslt && rslt.rows && rslt.rows.length){
			return res.status(200).json({s:1,d:rslt.rows[0]});
		}
		return res.status(200).json({s:1,d:{}});
	});
};

//SELECT * from t_ticket where string_to_array(array_to_string(numbers,','),',') && ARRAY['1324','2']
//SELECT * from t_ticket where string_to_array(array_to_string(day_left,','),',') @> ARRAY['7']

function processDraw(result_id,user_id,force){
	var fs = require('fs'),
		path=require('path'),
		spawn = require('child_process').spawn,
		out = fs.openSync(path.resolve(process.cwd(),'logs','draw.log'), 'a'),
		err = fs.openSync(path.resolve(process.cwd(),'logs','draw.log'), 'a');
	console.log('draw:called...');
	let arg=['-r',result_id,'-u',user_id];
	if(force) arg.push('-f');
	let ps=spawn(path.resolve(process.cwd(),'draw'),arg, {
		stdio: [ 'ignore', out, err ], // piping stdout and stderr to out.log
		detached: true
	});
	if(ps){
		console.log('draw:',ps.pid);
	}
	ps.unref();
}

exports.drawResult=function(req,res){
	let body=req.body,
		force=JSON.parse(body.force),
		result=body.result;
	if(!validDraw(result)){
		console.log(validDraw.errors);
		return res.status(500).json({s:0,e:((validDraw.errors||[])[0]||{}).message});
	}
	
	dbconnect.query('update m_result set result=$1, update_by=$2, last_update=now() where id=$3 returning *',[result,req.user.id,req.params.id], function(e, rslt){
		if(e) {
			console.error(new Error(e));
			return res.status(500).json({s:0,e:e.toString()});
		}
		if(rslt && rslt.rows && rslt.rows.length){
			let d=rslt.rows[0];
			processDraw(d.id,req.user.id,force);
			return dbconnect.query('update m_result set drawn=$1, update_by=$2, last_update=now() where id=$3 returning *',[true,req.user.id,req.params.id], function(e, rslt){
				res.status(200).json({s:1,d:rslt.rows[0]});
			});
		}
		return res.status(200).json({s:1,d:{}});
	});
}

exports.lastDrawResult=(req,res)=>{
	let q='select * from m_result where drawn=\'true\' and apply_date=(select max(apply_date) from m_result where drawn=\'true\')';
	dbconnect.query(q,[], function(e, rslt){
		if(e) {
			console.error(new Error(e));
			return res.status(500).json({s:0,e:e.toString()});
		}
		return res.status(200).json({s:1,d:rslt.rows[0]});
	});
};

exports.resultByDrawDate=(req,res)=>{
	let draw_date=req.params.drawdate;
	if(!((draw_date||'').match(/^\d{4}-\d{2}-\d{2}$/))) return res.status(500).json({s:0,e:'invalid fromat of drawdate parameters'});
	let q='select result from m_result where drawn=\'true\' and apply_date=$1';
	dbconnect.query(q,[draw_date], function(e, rslt){
		if(e) {
			console.error(new Error(e));
			return res.status(500).json({s:0,e:e.toString()});
		}
		if(!(rslt && rslt.rows && rslt.rows.length)) return res.status(500).json({s:0,e:'no result found'});
		return res.status(200).json({s:1,d:rslt.rows[0].result});
	});
};


exports.model={};