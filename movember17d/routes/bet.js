var dbconnect = require('../lib/dbconnect'),
    sql = require('../lib/sql'),
    dateUtil = require('../lib/date'),
    Game = require('./game').model,
    User = require('./user').model,
    ajv = require('../lib/ajv'),
	schema = require('../schema/bet'),
	validate1D=ajv(schema['1D']),
	validateNormal=ajv(schema.normal),
    validate2Ditem=ajv(schema['2Ditem']),
    validate3Ditem=ajv(schema['3Ditem']),
    validate4Ditem=ajv(schema['4Ditem']),
    validate2DitemW=ajv(schema['2D*item']),
    validate3DitemW=ajv(schema['3D*item']),
    validate4DitemW=ajv(schema['4D*item']),
	error = require('../lib/error'),
    Ticket = require('./ticket').model;

function toNumb(n){
	return parseFloat(n.toFixed(5));
}

var dow={
	'1':'Mon',
	'2':'Tue',
	'3':'Wed',
	'4':'Thu',
	'5':'Fri',
	'6':'Sat',
	'7':'Sun',
};

function getDow(d){
	d=Array.isArray(d)?d:[d];
	var dowV=Object.values(dow).map(function(v){
		return v.toUpperCase();
	});
	return d.map(function(v){
		var i=dowV.indexOf(v.toUpperCase());
		return parseInt(Object.keys(dow)[i]);
	});
}

function perm(s){
	var permArr = [],
		usedChars = [];
	
	function permute(input) {
		var i, ch;
		for (i = 0; i < input.length; i++) {
			ch = input.splice(i, 1)[0];
			usedChars.push(ch);
			if (!input.length) {
				permArr.push(usedChars.slice());
			}
			permute(input);
			input.splice(i, 0, ch);
			usedChars.pop();
		}
		return permArr;
	}
	return permute(s.split('')).map(function(v){return v.join('');});
}

function uniqPermute(s){
	return Array.from(new Set(perm(s)));
}

function wildChar(s){
	return ('0 1 2 3 4 5 6 7 8 9'.split(' ')).map(function(v){
		return s.replace(/\*/,v);
	});
}

function processBet(body,req,res,process){
    Game.getServerConfig(function(e,config){ //get dow
		let dows=getDow(config.draw_day[body.day_id]);
		body.day_left=dows;
		User.getUserParents(body.place_to,function(e,parents){ //get parents
			if(e) return res.status(500).json({s:0,e:e.toString()});
			body.users=parents;
            User.getUserConfigs(parents,function(e,configs){ //get user config
                if(e) return res.status(500).json({s:0,e:e.toString()});
                
                Game.currentPrizeId(function(e,prizeId){ //get current prize
                    if(e) return res.status(500).json({ s:0,e:e.toString()});
                    body.prize_id=prizeId;
                    
                    Game.getDrawDate((e,dd)=>{
                        let draw_days=dows.map(sd=>{
                            return dateUtil.toDbDate(new Date(dd[sd.toString()].close));
                        });
                        body.draw_days=draw_days;
                        
                        process(body,({page,tickets})=>{
                            //console.log(JSON.stringify({page,tickets}));
                            //return res.status(200).send({ s:1,d:{}});
                            //console.log('body==>',body);
                            
                            sql.insertQuery('t_ticket_page',page,{},(q,v)=>{
                                dbconnect.query(q,v,(e,rPage)=>{
                                    if(e) {
                                        if(e.code) return res.status(500).json(error.db(e));
                                        return res.status(500).json({s:0,e:e.toString()});
                                    }
                                    let tPage=rPage.rows[0];
                                    tickets.forEach((vTicket)=>{
                                        let user_configs={};
                                        Object.keys(configs).forEach(uid=>{
                                            let ci=(configs[uid].agent_config||{}).ci||false,
                                                cf=(configs[uid].config||{})[vTicket.game_id]||{},
                                                bit=ci?cf.bit||cf.it||0:0,
                                                sit=ci?cf.bit||cf.it||0:0,
                                                it=ci?cf.it||0:0,
                                                rb=cf.rb||0,
                                                sc=parseFloat(ci.sc)||0;
                                            if(vTicket.game_id==='1D'){
                                                bit=0;
                                                sit=0;
                                                rb=vTicket.place_to===uid?0:2;
                                                //it=vTicket.place_to===uid?0:(ci?cf.it||0:0);
                                                it=ci?cf.it||0:0;
                                                sc=0;
                                            }
                                            user_configs[uid]={
                                                bit,sit,it,rb,sc
                                            };
                                        });
                                        vTicket.user_configs=user_configs;
                                        vTicket.page_id=tPage.id;
                                        
                                        sql.insertQuery('t_active_ticket',vTicket,{},(q,v)=>{
                                            console.log(JSON.stringify({q,v}));
                                            dbconnect.query(q,v,(e)=>{if(e) console.log(new Error(e));});
                                        });
                                    });
                                    
                                    let tickets2=tickets.map((v)=>{
                                        let j=JSON.parse(JSON.stringify(v));
                                        j.bet_amount=((j.big||0)+(j.small||0)+(j.amount||0))*j.day_length*(j.permute_length||1);
                                        delete j.users;
                                        delete j.numbers;
                                        delete j.user_configs;
                                        delete j.bets;
                                        return j;
                                    });
                                    
                                    res.status(200).send({ s:1,d:{page:tPage,tickets:tickets2}});
                                });
                            });
                        });
                    });
                });
            });
		});
	});
}


function placeBet1D(body,req,res){
    const _permute={
        o:'1 3 5 7 9'.split(' '),
        e:'0 2 4 6 8'.split(' '),
        b:'5 6 7 8 9'.split(' '),
        s:'0 1 2 3 4'.split(' '),
    };
    
    processBet(body,req,res,(body,cb)=>{
        let {form_type,place_to,place_by,prize_id,users,day_left,day_id,draw_days}=body,
            page={form_type,place_to,place_by,prize_id,users},
            tickets=[];
        console.log('dtl==>',body.detail);
        body.detail.forEach((v)=>{
            let {n,m,a}=v,
                numbers=[n], //_permute[n];
                bets={};
            bets[n]={};
            bets[n]['1D']=a;
            tickets.push({
                place_to,
                place_by,
                game_id:'1D',
                num:n,
                amount:a,
                match:m,
                bt:'-',
                day_left,
                day_list:day_left,
                prize_id,
                day_id,
                users,
                numbers,
                day_length:day_left.length,
                day_count:day_left.length,
                draw_days:draw_days,
                permute_length:1,
                bets
            });
        });
        cb({page,tickets});
    });
}

function placeBetNormal(body,req,res){
    var _uPermute={};
    function uPermute(numb){
        if(_uPermute[numb]) return _uPermute[numb];
        _uPermute[numb]=uniqPermute(numb);
        return _uPermute[numb];
    }
    var _wildcard={};
    function Wildcard(numb){
        if(_wildcard[numb]) return _wildcard[numb];
        _wildcard[numb]=wildChar(numb);
        return _wildcard[numb];
    }
    
    function valida(v){
        let {g,bt}=v;
        if(bt==='*'){
            if(g==='4D') return validate4DitemW;
            if(g==='3D') return validate3DitemW;
            if(g==='2D') return validate2DitemW;
        }
        if(g==='4D') return validate4Ditem;
        if(g==='3D') return validate3Ditem;
        if(g==='2D') return validate2Ditem;
    }
    let e=[];
    body.detail.forEach((v)=>{
        let vali=valida(v);
        if(!vali(v)) e.push(vali);
    });
    if(e.length) return res.status(500).json(error.schema(e[0]));
    
    processBet(body,req,res,(body,cb)=>{
        let {form_type,place_to,place_by,prize_id,users,day_left,day_id,draw_days}=body,
            page={form_type,place_to,place_by,prize_id,users},
            tickets=[];
        body.detail.forEach((v)=>{
            let {g,n,b,s,bt}=v,
                numbers=bt==='R'||bt==='I'?uPermute(n):(bt==='*'?Wildcard(n):[n]),
                bets={};
            numbers.forEach(n=>{
                bets[n]=bt==='I'?{b:toNumb(b/numbers.length),s:toNumb(s/numbers.length)}:{b,s};
            });
            
            tickets.push({
                place_to,
                place_by,
                game_id:g,
                num:n,
                big:b,
                small:s,
                bt,
                day_left,
                day_list:day_left,
                prize_id,
                day_id,
                users,
                numbers,
                day_length:day_left.length,
                day_count:day_left.length,
                draw_days:draw_days,
                permute_length:bt==='R'||bt==='*'?numbers.length:1,
                bets
            });
        });
        cb({page,tickets});
    });
}

exports.placeBet=function(req, res){
	var body=Object.assign({},req.body||{}),
        validate=validateNormal;

	body.place_to=body.place_to||req.user.id;
	body.place_by=req.user.id;
	body.form_type=body.form_type||'normal';
	
    if(body.form_type==='1D') validate=validate1D;
	
    //console.log('body==>',JSON.stringify(body));
    
    if(!validate(body)) return res.status(500).json(error.schema(validate));
    
	if(body.form_type==='1D') return placeBet1D(body,req,res);
    return placeBetNormal(body,req,res);
};

exports.model={
    uniqPermute,
    wildChar
}