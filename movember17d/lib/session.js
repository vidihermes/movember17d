var session = require('express-session'),
	RedisStore = require('connect-redis')(session),
	redisConfig = require('../config/redis.json5');

var sessionMiddleware = session({
	store: new RedisStore(redisConfig.session),
	secret: "enter custom sessions secret here",
	//name: 'cookie_name',
	//store: sessionStore, // connect-mongo session store
	//proxy: true,
	resave: false,
	saveUninitialized: false
});

module.exports=sessionMiddleware;