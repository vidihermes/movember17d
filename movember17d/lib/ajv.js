var Ajv = require('ajv'),
	ajv = new Ajv({useDefaults:true,coerceTypes:true});

module.exports=function(schema){
	return ajv.compile(schema);
};