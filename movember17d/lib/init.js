var initCfg=require('../config/init.json5');

exports.afterDbConnect=(dbc,cb)=>{
	dbc.query('set timezone TO '+initCfg.timezone,(e)=>{
		if(cb) cb(e);
	});
};