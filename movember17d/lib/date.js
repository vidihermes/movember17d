const dbTZ=parseFloat(require('../config/init.json5').timezone);
//var pad=function(s,n){
//	s=s.toString();
//	return ('0'.repeat(n-s.length))+s;
//}
function pad(s,n){
	s=s.toString();
	return n-s.length>0?('0'.repeat(n-s.length))+s:s;
}


function extractDate(d){
	d=new Date(d);
	return {
		y:d.getFullYear(),
		mo:d.getMonth()+1,
		d:d.getDate(),
		h:d.getHours(),
		mi:d.getMinutes(),
		s:d.getSeconds()
	};
}

var simpleDate=function(d){
	if(!d) return '';
	return pad(d.getDate(),2)+'/'+pad(d.getMonth()+1,2)+'/'+pad(d.getFullYear())
};

var mysqlDate=function(d){
	d=d||new Date();
	return pad(d.getFullYear(),4)+'-'+
		pad(d.getMonth()+1,2)+'-'+
		pad(d.getDate(),2)+' '+
		pad(d.getHours(),2)+':'+
		pad(d.getMinutes(),2)+':'+
		pad(d.getSeconds(),2);
};

function toDbDate(date) {
	let {y,mo,d}=extractDate(date);
    return [pad(y,4), pad(mo,2), pad(d,2)].join('-');
}

function toDbDateTime(date) {
	let {y,mo,d,h,mi,s}=extractDate(date);
    return ([pad(y,4), pad(mo,2), pad(d,2)].join('-'))+' '+([pad(h,2), pad(mi,2), pad(s,2)].join(':'));
}

function diffTimeZone(){
	let localTZ=-(((new Date()).getTimezoneOffset())/60);
	console.log('db timezone='+dbTZ);
	return dbTZ-localTZ;
}

function setTimes(date, times){
	let t=Array.isArray(times)?times:times.split(':'),
		h=parseInt(t[0],10),
		m=parseInt(t[1],10),
		s=parseInt(t[2]||'0',10),
		d=new Date(date);
	d.setHours(h);
	d.setMinutes(m);
	d.setSeconds(s);
	return d;
}

module.exports={
	dbTZ,
	pad,
	simpleDate,
	mysqlDate,
	toDbDate,
	toDbDateTime,
	diffTimeZone,
	setTimes
};
