exports.insertQuery=function(table,data,valData,cb){
	data=Array.isArray(data)?data:[data];
	var idata=data[0],
		a=[],
		v=[],
		f=Object.keys(idata),
		ki=1;
		
	data.forEach(r=>{
		let ai=f.map(k=>{
			v.push(r[k]);
			return '$'+(ki++); //a2r[k];
		});
		if(valData){
			Object.values(valData).forEach(vi=>{
				ai.push(vi);
			});
		}
		a.push(ai);
	});
	let as=a.map(ai=>{
		return '('+ai.join(',')+')';
	});
	
	if(valData){
		f.push.apply(f,Object.keys(valData));
	}  
	var q='insert into '+table+'('+f.join(',')+') values \n'+as.join(',\n')+'\n returning *';
		
	if(cb)return cb(q,v);
	return {q:q,v:v};
};

exports.updateQuery=function(table,data,condition,valData,cb){
	var i=0,
		kv=[],
		v=[],
		kCondition=Object.keys(condition)[0],
		vCondition=condition[kCondition];
	for(var k in data){
		if(valData.hasOwnProperty(k)){
			kv.push(k+' = '+valData[k]);
			delete valData[k];
		} else if(data.hasOwnProperty(k)){
			kv.push(k+' = $'+(++i));
			v.push(data[k]);
		}
	}
	for(var ki in valData){
		if(valData.hasOwnProperty(ki)){
			kv.push(ki+'='+valData[ki]);
		}
	}
	v.push(vCondition);
	var q='update '+table+' set '+kv.join(', ')+' where '+kCondition+'= $'+(++i)+' returning *';
	
	if(cb) return cb(q,v);
	return {q:q,v:v};
};

exports.selectQuery=function(table,query,orders,searchFields,filterFields,cb){
	query=query||{};
	orders=orders||{};
	searchFields=searchFields||[];
	filterFields=filterFields||[];
	var search=query.search,
		limit=parseInt(query.limit||'25'),
		page=parseInt(query.page||'1'),
		v=[],
		i=0,
		q='SELECT * FROM '+table,
		aorder=[],
		awhere=[];
	if(search && searchFields.length){
		v.push('%'+search+'%');
		var ii=++i;
		var aqs=searchFields.map(function(vs){
			return vs+' ilike $'+ii;
		});
		awhere.push('('+aqs.join(' OR ')+')');
	}
	if(filterFields.length){
		filterFields.forEach(function(vf){
			if(query.hasOwnProperty(vf)){
				v.push(query[vf]);
				awhere.push(vf+' = $'+(++i));
			}
		});
	}
	if(awhere.length) q+=' WHERE '+awhere.join(' AND ');
	for(var k in orders){
		if(orders.hasOwnProperty(k)) aorder.push(k+' '+orders[k]);
	}
	if(aorder.length) q+=' ORDER BY '+aorder.join(',');
	q+=' LIMIT $'+(++i)+' OFFSET $'+(++i);
	v.push(limit,limit*(page-1));
	
	if(cb)return cb(q,v);
	return {q:q,v:v};
};

/*
 * table <string>
 * query <object>				{id:10,limit:25,page:2}
 * alias
 * orders <object>				{create_date:'desc',id:'asc'}
 * searchFields <array>			[id,name]
 * filterFields <array>			[type,member_of]
 * customFilter <object[]>		[{f:'users',o:'@>',v:['AG0024']},{f:'date',o:'>=',vv:'now()'},{f:'date',o:'<=',v:'2018-01-01'}]
 * valFilter <string[]>			['array_length(day_left,1) > 0','fix_id is null or date > now()'],
 * addFields
 * fields
 * filterJoinOperator <string>	'and'
 * */


exports.ooSelectQuery=(table,{query,alias,orders,searchFields,filterFields,valFilter,customFilter,filterJoinOperator,before,addFields,fields,addTables,full},cb)=>{
	query=query||{};
	orders=orders||{};
	searchFields=searchFields||[];
	filterFields=filterFields||[];
	customFilter=customFilter||[];
	valFilter=valFilter||[];
	before=before||'';
	addFields=addFields||[];
	fields=fields||[];
	alias=alias||'';
	addTables=addTables||[];
	filterJoinOperator=filterJoinOperator||'AND';
	
	function field(){
		if(fields.length){
			if(alias) return (fields.map(v=>alias+'.'+v)).join(',');
			return fields.join(',');
		}
		if(alias) return alias+'.*';
		return '*';
	}
	var search=query.search,
		limit=parseInt(query.limit||'25'),
		page=parseInt(query.page||'1'),
		v=[],
		i=0,
		q='SELECT '+field()+(addFields.length?', '+addFields.join(','):'')+' FROM '+table+(alias?' '+alias:'')+(addTables.length?', '+addTables.join(','):''),
		aorder=[],
		awhere=[];

	function addValue(vlu){
		v.push(vlu);
		return ++i;
	}
	try{
		if(search && searchFields.length){
			//v.push('%'+search+'%');
			let ii=addValue('%'+search+'%');
			//var ii=++i;
			var aqs=searchFields.map(function(vs){
				return vs+' ilike $'+ii;
			});
			awhere.push('('+aqs.join(' OR ')+')');
		}
		if(filterFields.length){
			filterFields.forEach(function(vf){
				if(query.hasOwnProperty(vf)){
					//v.push(query[vf]);
					awhere.push((alias?alias+'.':'')+vf+' = $'+(addValue(query[vf])));
				}
			});
		}
		if(customFilter.length){
			customFilter.forEach(function(vf,k){
				if(!vf.f) throw 'customFilter['+k+'], f required';
				vf.o=vf.o||'=';
				if(vf.hasOwnProperty('vv')){
					awhere.push(vf.f+' '+vf.o+' '+vf.vv);
				} else {
					awhere.push(vf.f+' '+vf.o+' $'+(addValue(vf.v)));
				}
			});
		}
		if(valFilter.length){
			awhere.push.apply(awhere,valFilter);
		}
		
		if(awhere.length) q+=' WHERE '+awhere.join(' '+filterJoinOperator+' ');
		for(var k in orders){
			if(orders.hasOwnProperty(k)) aorder.push(k+' '+orders[k]);
		}
		if(aorder.length) q+=' ORDER BY '+aorder.join(',');
		q+=' LIMIT $'+(addValue(limit))+' OFFSET $'+(addValue(limit*(page-1)));
		//v.push(limit,limit*(page-1));
	} catch (error){
		if(cb)return cb(error,{q,v});
		return {q:q,v:v,e:error};
	}
	q=before+q;
	
	if(full){
		let s=q;
		v.forEach((vi,k)=>{
			s=s.replace('$'+(k+1),'\''+vi+'\'');
		})
		console.log('s==>',s);
	}
	
	if(cb)return cb(null,{q,v});
	return {q:q,v:v};
};