var pg=require('pg'),
	init=require('../lib/init'),
	dbconfig=require('../config/db.json5');

const client = new pg.Client(dbconfig);

client.connect(function(e){
	if(e) return console.log(e);
	init.afterDbConnect(client,()=>{
		console.log('db connected');
	});
	//return console.log('db connected');
});

module.exports=client;
