var JSON5 = require('json5'),
    JSON6 = require('json-6'),
    redis = require('redis'),
	redisConfig = require('../config/redis.json5'),
    
    _redisSession = redis.createClient(Object.assign({},{
        host:redisConfig.session.host,
        db:redisConfig.session.db,
        password:redisConfig.session.pass,
    })),
	redisUser = redis.createClient(redisConfig.user),
    redisPrize = redis.createClient(redisConfig.prize),
    redisIntake = redis.createClient(redisConfig.intake),
    redisSimple = redis.createClient(redisConfig.simple);

function getJSON(rclient){
    return function(id,cb,reviver){
        rclient.get(id,function(e,r){
            if(!e && r){
                try {
                    d=JSON6.parse(r,reviver);
                } catch(err){
                    return cb(err);
                }
                return cb(null,d);
            }
            cb(e,r);
        });
    };
}

function getMulti(rclient){
    return function(ids,cb){
        ids=JSON.parse(JSON.stringify(ids||[]));
        if(!(ids && ids.length)) return cb(null,{});
        rclient.mget(ids,function(e,d){
            if(e){
                console.error(new Error(e));
                return cb(e);
            }
            var rs={};
            ids.forEach(function(id,k){
                if(d[k]!==null) rs[id]=d[k];
            });
            cb(null,rs);
        });
    };
}

function setMulti(rclient){
    return function(data,cb,exp){
        data=data||{};
        var p=[];
        Object.keys(data).forEach(k=>{
            p.push(k);
            p.push(data[k]);
        });
        rclient.mset(p,e=>{
            if(e){
                console.error(new Error(e));
                return cb(e);
            }
            if(exp){
                Object.keys(data).forEach(k=>{
                    rclient.expire(k,exp,e=>{
                        if(e) console.error(new Error(e));
                    });
                });
            }
            //var rs={};
            //ids.forEach(function(id,k){
            //    if(d[k]!==null) rs[id]=d[k];
            //});
            //cb(null,rs);
        });
        
        //ids=JSON.parse(JSON.stringify(ids||[]));
        //if(!(ids && ids.length)) return cb(null,{});
        //rclient.mget(ids,function(e,d){
        //    if(e){
        //        console.error(new Error(e));
        //        return cb(e);
        //    }
        //    var rs={};
        //    ids.forEach(function(id,k){
        //        if(d[k]!==null) rs[id]=d[k];
        //    });
        //    cb(null,rs);
        //});
    };
}

//function getMultiJSON(rclient){
//    return function(ids,cb,reviver){
//        ids=JSON.parse(JSON.stringify(ids||[]));
//        var cmd=['mget'];
//        cmd.push.apply(cmd,ids);
//        rclient.multi([cmd]).exec(function (e, r){
//            if(!e && r && r.length){
//                var d=r[0],
//                    rs={};
//                ids.forEach(function(id,k){
//                    if(d[k]) rs[id]=JSON6.parse(d[k],reviver);
//                });
//                return cb(null,rs);
//            }
//            cb(e,r);
//        });
//    };
//}

function getMultiJSON(rclient){
    return function(ids,cb,reviver){
        ids=JSON.parse(JSON.stringify(ids||[]));
        if(!(ids && ids.length)) return cb(null,{});
        rclient.mget(ids,function(e,d){
            if(e){
                console.error(new Error(e));
                return cb(e);
            }
            var rs={};
            ids.forEach(function(id,k){
                if(d[k]) rs[id]=JSON6.parse(d[k],reviver);
            });
            cb(null,rs);
        });
    };
}

function setJSON(rclient){
    return function(id,data){
        rclient.set(id,JSON5.stringify(data));
    };
}

function initClient(rclients){
    rclients.forEach(function(rclient){
        rclient.getMulti=getMulti(rclient);
        rclient.setMulti=setMulti(rclient);
        rclient.setJSON=setJSON(rclient);
        rclient.getJSON=getJSON(rclient);
        rclient.getMultiJSON=getMultiJSON(rclient);
    });
}

class Simple {
    constructor(prefix){
        this.prefix=prefix;
    }
    set(id,value,cb){
        redisSimple.set(this.prefix+id,value,cb);
    }
    get(id,cb){
        redisSimple.get(this.prefix+id,cb);
    }
    del(id,cb){
        redisSimple.del(this.prefix+id,cb);
    }
    setMulti(data,cb){
        data=data||{};
        let d={};
        Object.keys(data).forEach(k=>{
            d[this.prefix+k]=data[k];
        })
        redisSimple.setMulti(d,cb);
    }
    getMulti(ids,cb){
        let ids2=ids.map(id=>{return this.prefix+id;});
        if(!ids2.length){
            return cb(null,{});
        }
        redisSimple.mget(ids2,(e,d)=>{
            if(e){
                console.error(new Error(e));
                return cb(e);
            }
            var rs={};
            ids.forEach((id,k)=>{
                if(d[k]!==null) rs[id]=parseFloat(d[k]);
            });
            cb(null,rs);
        });
    }
}

var redisBalance=new Simple('bal:');

initClient([
    _redisSession,
    redisUser,
    redisPrize,
    redisIntake,
    redisSimple,
]);

exports._session=_redisSession;
exports.user=redisUser;
exports.prize=redisPrize;
exports.intake=redisIntake;
exports.simple=redisSimple;
exports.balance=redisBalance;