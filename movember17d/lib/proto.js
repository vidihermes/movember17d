if(typeof Object.values === 'function'){
	Object.prototype.values=function(obj){
		//var obj = ES.RequireObjectCoercible(O);
		var vals = [];
		for (var key in obj) {
			if(obj.hasOwnProperty(key)){
				vals.push(obj[key]);
			}
		}
		return vals;
	};
}
