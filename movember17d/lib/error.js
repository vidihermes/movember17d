module.exports={
	//[
	//	{
	//		"keyword": "type",
	//		"dataPath": ".config['4D'].it",
	//		"schemaPath": "#/properties/config/properties/4D/properties/it/type",
	//		"params": {
	//			"type": "number"
	//		},
	//		"message": "should be number"
	//	}
	//]
	schema:(validator)=>{
		let r={s:0};
		console.error(validator.errors[0]);
		if(validator.errors && validator.errors.length){
			r.e=validator.errors[0].message;
			r.schema=validator.errors[0];
			return r;
		}
		return {s:0,e:'unknown error'};
	},
	//{
	//	"name": "error",
	//	"length": 181,
	//	"severity": "ERROR",
	//	"code": "23505",
	//	"detail": "Key (id)=(SAG10005) already exists.",
	//	"schema": "public",
	//	"table": "m_user",
	//	"constraint": "pk_user_id",
	//	"file": "nbtinsert.c",
	//	"line": "398",
	//	"routine": "_bt_check_unique"
	//}
	db:(e)=>{
		console.log(new Error(e));
		let r={s:0};
		if(e.detail){
			r.e=e.detail;
			r.db=e;
			return r;
		}
		return {s:0,e:'unknown error'};
	}
};