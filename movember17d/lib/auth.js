var CryptoJS = require("crypto-js"),
	passport = require('passport'),
	JSON5 = require('json5'),
	LocalStrategy = require('passport-local').Strategy,
	dbconnect = require('./dbconnect'),
	redisUser = require('./redis').user,
	sql = require('./sql'),
	Notif = require('../routes/notif').model;

passport.use(new LocalStrategy(
	function(username, password, done) {
		username=(username||'').toUpperCase();
		//console.log('LocalStrategy kepanggil');
		dbconnect.query('select * from m_user where id=$1 or login_id=$1', [username], function(err, r){
			//console.log('user err==>',err);
			//console.log('user r==>',r);
			if (err) return done(err);
			if(!(r && r.rows && r.rows.length)) return done('user not found');
			var user=r.rows[0];
			redisUser.del(username);
			if(CryptoJS.SHA256(password+user.pass_salt).toString() == user.pass_sha ) {
				if(!user.active || user.status=='locked'){
					if(!user.active) return done({e:'account_inactive',id:username});
					return done({e:'account_locked',id:username});
				}
				return done(null, user);
			}
			if(CryptoJS.SHA256(password+user.fake_pass_salt).toString() == user.fake_pass_sha ) {
				return Notif.sendNotif(Notif.type.DOWNLINE_FAKE_LOGIN,user.member_of,{userid:user.id},user.id,false,function(){
					done({e:'fake_login',id:username});
				});
			}
			if(user.login_err >= 4){
				return sql.updateQuery('m_user',{status:'locked'},{id:username},{login_err:'login_err + 1'},function(q,v){
					dbconnect.query(q,v,function(e){
						if(!e) return Notif.sendNotif(Notif.type.MEMBER_INVALID_PASSWORD,user.member_of,{userid:user.id},user.id,false,function(){
								done({e:'account_locked',id:username});
							});
						return done({e:'account_locked',id:username});
					});
				});
			}
			sql.updateQuery('m_user',{},{id:username},{login_err:'login_err + 1'},function(q,v){
				dbconnect.query(q,v,function(){
					return done('invalid password');
				});
			});
		});
	}
));

passport.serializeUser(function(user, done) {
	done(null, user.id);
});

passport.deserializeUser(function(id, done) {
	//redisUser.get(id,function(e,r){
	redisUser.getJSON(id,function(e,r){
		if(!e && r) {
			//console.log('dapat dari redis...');
			//return done(null, JSON5.parse(r));
			return done(null, r);
		}
		dbconnect.query('select * from m_user where id=$1', [id], function(err, res2){
			if (err) {
				console.log(err.stack);
			} else if(res2.rows && res2.rows.length){
				var u=res2.rows[0];
				//redisUser.set(id,JSON5.stringify(u));
				redisUser.setJSON(id,u);
				console.log('dapat dari db...');
				done(null,u);
			} else {
				done(null, null);
			}
		});
	});
});

exports.isAuthenticated=function(req, res, next) {
	if (req.isAuthenticated()) return next();
	return res.status(401).send({s:0,e:'unauthorized'});
};

exports.isDeveloper=function(req, res, next) {
	if (req.user && req.user.type == 'developer') return next();
	return res.status(401).send({s:0,e:'unauthorized'});
};

exports.isSuperAdmin=function(req, res, next) {
	if (req.user && req.user.type == 'sadmin') return next();
	return res.status(401).send({s:0,e:'unauthorized'});
};

exports.isSuperAdminOrDeveloper=function(req, res, next) {
	if (req.user && (req.user.type == 'sadmin' || req.user.type == 'developer')) return next();
	return res.status(401).send({s:0,e:'unauthorized'});
};

exports.isAdmin=function(req, res, next) {
	if (req.user && req.user.type == 'admin') return next();
	return res.status(401).send({s:0,e:'unauthorized'});
};

exports.isAdminOrSuper=function(req, res, next) {
	if (req.user && (req.user.type == 'admin') || (req.user.type == 'sadmin') || (req.user.type == 'developer')) return next();
	return res.status(401).send({s:0,e:'unauthorized'});
};

exports.isSuperAgent=function(req, res, next) {
	if (req.user && req.user.type == 'sagent') return next();
	return res.status(401).send({s:0,e:'unauthorized'});
};

exports.isAgent=function(req, res, next) {
	if (req.user && req.user.type == 'agent') return next();
	return res.status(401).send({s:0,e:'unauthorized'});
};

exports.isAgentOrSuper=function(req, res, next) {
	if (req.user && (req.user.type == 'agent'||req.user.type == 'sagent')) return next();
	return res.status(401).send({s:0,e:'unauthorized'});
};
