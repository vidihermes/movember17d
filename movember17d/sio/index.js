var sessionMiddleware=require('../lib/session'),
	User=require('../routes/user').model,
	Notif=require('../routes/notif').model;
var io;

exports.listen=(server)=>{
	io=require("socket.io")(server);
	io.use(function(socket, next) {
		sessionMiddleware(socket.request, socket.request.res, next);
	});
	io.sockets.on("connection", function(socket) {
		//socket.request.session
		let userId=(((socket.request||{}).session||{}).passport||{}).user;
		
		if(!userId) return socket.disconnect('not authorized');

		socket.request.userId=userId;
		socket.request.getUser=(cb)=>{User.getUser(userId,cb)};
		socket.join('U:'+userId);
		User.getUser(userId,(e,r)=>{
			if(r){
				let role=[];
				if(['developer','sadmin','admin'].indexOf(r.type) >= 0){
					role.push('SUPERUSER');
				} else if(['agent','player'].indexOf(r.type) >= 0 && r.can_play){
					role.push('PLAYER');
				} else if(['sagent','agent'].indexOf(r.type) >= 0){
					role.push('AGENT');
				}
				role.forEach(v=>{
					if(role) socket.join('R:'+v);
				})
				socket.join('T:'+(r.type.toUpperCase()));
				console.log('new user socket:',userId+':'+r.type.toUpperCase(),role);
				
				
				socket.on('notifView', function () {
					Notif.notifView(socket.request.userId,(e,r)=>{
						io.to('U:'+socket.request.userId).emit('notifViewedCount',r||0);
					})
				});
			}
		});
	});
	return io;
};

exports.io=()=>{return io;};
exports.broadcast=(key,data)=>{
	io.sockets.emit(key,data);
}
exports.Push=(to,key,data)=>{
	io.to(to).emit(key,data);
}
