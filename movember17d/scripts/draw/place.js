var User=require('../../routes/user').model,
	Bet=require('../../routes/bet').model,
	Strike=require('./strike');

let dbclient;

const prepare=function(data){
	return new Promise((resolve,reject)=>{
		let {d,ticket}=data;
		let noConfigUser=ticket.users.filter(u=>{return !d.userConfig[u];});
		if(noConfigUser.length){
			return User.getUserConfigs(noConfigUser,(e,r)=>{
				if(e) reject(e);
				Object.assign(d.userConfig,r);
				resolve(data);
			},dbclient);
		}
		resolve(data);
	});
};

function toNumb(n){
	return parseFloat(n.toFixed(5));
}

const agentUp1D=(agent,first,last)=>{
	return function(data){
		return new Promise((resolve,reject)=>{
			let pass_data;
			try{
				pass_data=JSON.parse(JSON.stringify(data.passData));
			} catch(e){
				reject(e);
			}
			let {d,placeBalance}=data;
			let {ticket_id,game_id,num,match,passup}=pass_data;
			let cf=(d.userConfig[agent]||{}).config||{},
				ac=(d.userConfig[agent]||{}).agent_config||{},
				intake=last?1000000000000:(ac.it?(cf[game_id]||{}).it||0:0);
			if(!d.intake[agent]) d.intake[agent]={};
			if(!d.intake[agent][game_id]) d.intake[agent][game_id]={};
			let intakeLeft=intake,
				intakeInUse=d.intake[agent][game_id][num]||0;
			intakeLeft-=intakeInUse;
			let realIntake=pass_data.passup < intakeLeft?pass_data.passup:intakeLeft;
			intakeInUse += realIntake;
			d.intake[agent][game_id][num]=intakeInUse;
			
			let amount;
			if(first){ amount=realIntake + ((2/100) * (passup - realIntake)); }
			else { amount=realIntake * (realIntake < passup ? 0.98 : 1); }
			
			let myPassUp=passup-amount;
			let balanceData={
				user_id:agent,
				ticket_id,
				type:first?'member_bet':'downline_bet',
				game_id:game_id,
				num,
				match:pass_data.match,
				intake:realIntake,
				amount:amount,
				passup:myPassUp,
				to_upline:toNumb(myPassUp),
			};
			placeBalance.push(balanceData);
			Object.assign(data,{passData:balanceData});
			resolve(data);
		});
	};
};

const playerUp1D=(player)=>{
	return function(data){
		return new Promise((resolve,reject)=>{
			let {ticket,placeBalance}=data;
			let {id,game_id,num}=ticket;
			//console.log('ticket==>',ticket)
			let bet_amt=parseFloat(ticket.amount);
			
			let balanceData={
				user_id:player,
				ticket_id:id,
				type:'place_bet',
				game_id:game_id,
				num,
				match:ticket.match,
				bet_amt,
				amount:-bet_amt,
				passup:bet_amt,
				to_upline:toNumb(bet_amt),
			};
			placeBalance.push(balanceData);
			Object.assign(data,{passData:balanceData});
			resolve(data);
		});
	};
};


const agentUp=(agent,first,last)=>{
	return function(data){
		return new Promise((resolve,reject)=>{
			let pass_data;
			try{
				pass_data=JSON.parse(JSON.stringify(data.passData));
			} catch(e){
				//this.reject(e);
				reject(e);
			}
			if(!(pass_data && (pass_data.big || pass_data.small))){
				//return this.reject('habis');
				return reject('stop');
			}
			
			let {d,placeBalance}=data;
			let {ticket_id,game_id,num,bt,permute_length,pass}=pass_data;
			//console.log('AGENT : '+agent+' last '+(JSON.stringify(last)));
			
			let big=0,
				small=0,
				cf=(d.userConfig[agent]||{}).config||{},
				ac=(d.userConfig[agent]||{}).agent_config||{},
				rebate=(cf[game_id]||{}).rb||0,
				intake=last?1000000000000:(ac.it?(cf[game_id]||{}).it||0:0),
				amount=0;
			if(!d.intake[agent]) d.intake[agent]={};
			if(!d.intake[agent][game_id]) d.intake[agent][game_id]={};

			let intakeLeft=intake;

			let myPass={};
			Object.keys(pass).forEach(k=>{
				let {b,s,pb,ps}=pass[k],
					intakeInUse=d.intake[agent][game_id][k]||0;
				intakeLeft-=intakeInUse; //7
				
				let bit=(intakeLeft > 0 ? (intakeLeft < b?intakeLeft:b):0);
				intakeInUse += bit;
				intakeLeft -= bit;
				
				let sit=(intakeLeft > 0 ? (intakeLeft < s?intakeLeft:s):0);
				intakeInUse += sit;
				
				d.intake[agent][game_id][k]=intakeInUse;
				
				let passI={
					b:b - bit,
					s:s - sit,
					pb: toNumb((b - bit) - ((b - bit) * (rebate/100))),
					ps: toNumb((s - sit) - ((s - sit) * (rebate/100))),
					bit,
					sit,
				};
				passI.bac=toNumb(pb-passI.pb);
				passI.sac=toNumb(ps-passI.ps);
				amount += passI.bac + passI.sac;
				big += passI.b;
				small += passI.s;
				myPass[k]=passI;
			});
			
			let balanceData={
				user_id:agent,
				ticket_id,
				type:first?'member_bet':'downline_bet',
				game_id:game_id,
				num,
				big,
				small,
				bt,
				intake,
				rebate,
				amount:toNumb(amount),
				to_upline:toNumb(pass_data.to_upline-amount),
				permute_length,
				pass:myPass
			};
			placeBalance.push(balanceData);
			data=Object.assign(data,{passData:balanceData});
			//this.resolve(data);
			resolve(data);
		});
	};
};

const playerUp=(player)=>{
	return function(data){
		return new Promise((resolve,reject)=>{
			let {d,ticket,placeBalance}=data;
			let {id,game_id,num,big,small,bt,permute_length}=ticket;
			big=parseFloat(big)||0;
			small=parseFloat(small)||0;
			let cf=(d.userConfig[player]||{}).config;
			//console.log('cf==>',cf);
			let rebate=(cf[game_id]||{}).rb||0;
			let big_rebate_left=big*(rebate/100);
			let small_rebate_left=small*(rebate/100);
						
			let passup_big=big-big_rebate_left,
				passup_small=small-small_rebate_left;
			
			let pass={};
			if(bt==='*' || bt==='R'){
				if(bt==='*'){
					Bet.wildChar(num).forEach(k=>{
						pass[k]={b:big,s:small,pb:passup_big,ps:passup_small};
					});
				} else {
					Bet.uniqPermute(num).forEach(k=>{
						pass[k]={b:big,s:small,pb:passup_big,ps:passup_small};
					});
				}
			} else if(bt==='I'){
				let up=Bet.uniqPermute(num);
				Bet.uniqPermute(num).forEach(k=>{
					let ul=up.length;
					pass[k]={b:big/ul,s:small/ul,pb:passup_big/ul,ps:passup_small/ul};
				});
			} else {
				pass[num]={b:big,s:small,pb:passup_big,ps:passup_small};
			}
			
			let balanceData={
				user_id:player,
				ticket_id:id,
				type:'place_bet',
				game_id:game_id,
				num,
				big,
				small,
				bt,
				rebate,
				to_upline:toNumb((passup_big+passup_small)*permute_length),
				amount:toNumb(-((passup_big+passup_small)*permute_length)),
				permute_length,
				pass
			};
			placeBalance.push(balanceData);
			Object.assign(data,{passData:balanceData});
			resolve(data);
		});
	};
};

exports.calc=(ticket,dbclientConnection)=>{
	if(!dbclient) dbclient=dbclientConnection;
	return (d,cb)=>{
		let data={d,ticket,placeBalance:[]},
			users=JSON.parse(JSON.stringify(ticket.users)),
			player=users.pop(),
			agent=users.pop();
		console.log('  up ticket:'+data.ticket.id);
		let x=Promise.resolve(data);
		x=x.then(prepare);
		if(data.ticket.game_id==='1D'){
			x=x.then(playerUp1D(player));
			x=x.then(agentUp1D(agent,true,!users.length));
			
			while (users.length){
				agent=users.pop();
				x=x.then(agentUp1D(agent,false,!users.length));
			}
		} else {
			x=x.then(playerUp(player));
			//let agent=users.pop();
			x=x.then(agentUp(agent,true,!users.length));
			
			while (users.length){
				agent=users.pop();
				x=x.then(agentUp(agent,false,!users.length));
			}
		}
		x.then((data)=>{
			console.log('\t'+data.ticket.id+':finish:sagent');
			//console.log('r=='+data.ticket.id+'==>finish');
			//console.log(JSON.stringify(data.placeBalance));
			Strike.calc(data,(e)=>{
				if(e) return cb(e,{});
				let {placeBalance,strikeBalance}=data||{};
				cb(null,{placeBalance,strikeBalance});
			},dbclient);
			//cb(null,data.placeBalance);
		})
		.catch((e)=>{
			//console.log('e=='+data.ticket.id+'==>',e);
			if(e==='stop'){
				console.log('\t'+data.ticket.id+':finish');
				return Strike.calc(data,(e)=>{
					if(e) return cb(e);
					let {placeBalance,strikeBalance}=data||{};
					cb(null,{placeBalance,strikeBalance});
				},dbclient);
			}
			//return cb(null,data.placeBalance);
			cb(e,{});
		});
	};
};
