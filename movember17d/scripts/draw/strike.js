var Game=require('../../routes/game').model;
var dbclient;

function toNumb(n){
	return parseFloat(n.toFixed(5));
}

const prepareStrikeBalance=(data)=>{
	return new Promise((resolve,reject)=>{
		console.log('  down strike ticket :'+data.ticket.id);
		data.backupPlaceBalance=JSON.parse(JSON.stringify(data.placeBalance));
		data.strikeBalance=[];
		resolve(data);
	});
};


const prepare=function(data){
	return new Promise((resolve,reject)=>{
		let {d,ticket}=data;
		let prizeId=ticket.prize_id;
		if(!(d.prize && d.prize[prizeId])){
			if(!d.prize) d.prize={};
			return Game.getPrize(prizeId,(e,r)=>{
				if(e) return reject(e);
				d.prize[prizeId]=r;
				data.prize=d.prize[prizeId];
				console.log('prize==>',JSON.stringify(data.prize));
				resolve(data);
			},dbclient);
		}
		data.prize=d.prize[prizeId];
		resolve(data);
	});
};

function getNormalRank({prizeRef,n,v,result}){
	let l=n.length;
	let posi=(bk)=>{
		for(let i=0;i<bk.length;i++){
			if( !(bk[i]==='s'|| bk[i]==='c') && result[bk[i]].substr(0,l)===n) return bk[i];
			else if(bk[i]==='s' && result.s.map(v=>{return v.substr(0,l);}).indexOf(n) > -1 ) return 's';
			else if(bk[i]==='c' && result.c.map(v=>{return v.substr(0,l);}).indexOf(n) > -1 ) return 'c';
		}
		return false;
	};
	let rnkB=!!v.b && !!prizeRef.big && posi(Object.keys(prizeRef.big)),
		rnkS=!!v.s && !!prizeRef.small && posi(Object.keys(prizeRef.small));
	if(!(rnkB || rnkS)) return false;
	return Object.assign({},
			rnkB?{
				b:{
					v:v.b,
					rank:rnkB,
					prize:prizeRef.big[rnkB],
					strike:v.b*prizeRef.big[rnkB]
				}
			}:{},
			rnkS?{
				s:{
					v:v.s,
					rank:rnkS,
					prize:prizeRef.small[rnkS],
					strike:v.s*prizeRef.small[rnkS]
				}
			}:{});
}


const checkWin=function(data){
	return new Promise((resolve,reject)=>{
		let {d,ticket,prize,placeBalance}=data,
			{result}=d,
			{game_id,bt,num}=ticket,
			firstBalance=placeBalance[0];
		let sub='normal';//bt==='I'?'i':'normal';
		if(prize[game_id][sub]){
			if(sub==='normal'){
				let wins={};
				Object.keys(firstBalance.pass).forEach(n=>{
					let wn=getNormalRank({
							prizeRef:prize[game_id][sub],
							n,
							v:firstBalance.pass[n],
							result});
					if(wn){
						wins[n]=wn;
					}
				});
				if(!Object.keys(wins).length) return reject('lose');
				data.wins=wins;
				resolve(data);
			} else {
				return reject('lose'); //nanti diganti untuk ibet
			}
		}
		return reject('lose');
	});
};

const _permute={
	o:'1 3 5 7 9'.split(' '),
	e:'0 2 4 6 8'.split(' '),
	b:'5 6 7 8 9'.split(' '),
	s:'0 1 2 3 4'.split(' '),
}

const checkWin1D=function(data){
	return new Promise((resolve,reject)=>{
		let {d,ticket,prize,placeBalance}=data,
			{result}=d,
			{num,amount}=ticket,
			firstBalance=placeBalance[0];
		if(prize['1D']['2']){
			let m=ticket.match;
			//console.log('ticket==>',ticket);
			let win=_permute[num].indexOf(result[m.toString()].substr(0,1)) > -1;
			if(!win) return reject('lose');
			
			let wins={
				v:amount,
				prize:prize['1D']['2'],
				strike:amount*prize['1D']['2']
			};
			data.wins=wins;
			resolve(data);
		}
		return reject('lose');
	});
};

const agentStrike1D=(data)=>{
	return new Promise((resolve,reject)=>{
		if(!data.backupPlaceBalance.length) return reject('finish');
		let {backupPlaceBalance,wins,passData}=data,
			agentBalance=backupPlaceBalance.shift(),
			{user_id,ticket_id,game_id,num,passup,amount,pass,intake}=agentBalance;
		let po=passup<=0 && amount>0?passData.pass.ref:intake+intake*0.95;
		let ref=passData.pass.ref-po;
		let balanceData={
			user_id,
			ticket_id,
			type:'downline_strike',
			game_id:game_id,
			num,
			match:agentBalance.match,
			amount:-po,  //toNumb(wins.strike),
			from_upline:toNumb(passData.from_upline-po),
			pass:{ref,intake,mref:passData.pass.ref}
		};
		data.strikeBalance.push(balanceData);
		resolve(data);
	});
};

const playerStrike1D=(data)=>{
	return new Promise((resolve,reject)=>{
		let {backupPlaceBalance,wins}=data,
			playerBalance=backupPlaceBalance.shift(),
			{user_id,ticket_id,game_id,num}=playerBalance;
		
		let balanceData={
			user_id,
			ticket_id,
			type:'strike',
			game_id:game_id,
			num,
			match:playerBalance.match,
			amount:toNumb(wins.strike),
			from_upline:toNumb(wins.strike),
			pass:{ref:wins.strike,prize:wins.prize,v:playerBalance.passup}
		};
		data.passData=balanceData;
		data.strikeBalance.push(balanceData);
		resolve(data);
	});
};

const playerStrike=(data)=>{
	return new Promise((resolve,reject)=>{
		let {backupPlaceBalance,wins}=data,
			playerBalance=backupPlaceBalance.shift(),
			amount=0,
			{pass,user_id,ticket_id,game_id,num,bt,rebate,permute_length}=playerBalance,
			strikePass={},
			big=0,
			small=0;

		Object.keys(wins).forEach(n=>{
			let {b,s}=wins[n],
				npass={};
			
			if(b) npass.b=Object.assign({},b,{v:pass[n].b});
			if(s) npass.s=Object.assign({},s,{v:pass[n].s});
			big += ((npass.b||{}).strike||0);
			small += ((npass.s||{}).strike||0);
			amount += ((npass.b||{}).strike||0) + ((npass.s||{}).strike||0);
			strikePass[n]=npass;
		});
		let balanceData={
			user_id,
			ticket_id,
			type:'strike',
			game_id:game_id,
			num,
			big:toNumb(big),
			small:toNumb(small),
			bt,
			rebate,
			amount:toNumb(amount),
			from_upline:toNumb(amount),
			permute_length,
			pass:strikePass
		};
		data.passData=balanceData;
		data.strikeBalance.push(balanceData);
		resolve(data);
	});
};

const agentStrike=(data)=>{
	return new Promise((resolve,reject)=>{
		if(!data.backupPlaceBalance.length) return reject('finish');
		let {backupPlaceBalance,wins,passData}=data,
			agentBalance=backupPlaceBalance.shift(),
			amount=0,
			{pass,user_id,ticket_id,game_id,num,bt,rebate,permute_length}=agentBalance,
			strikePass={},
			big=0,
			small=0;
			
			
		Object.keys(wins).forEach(n=>{
			let {b,s}=wins[n],
				npass={};
			
			if(b) npass.b=Object.assign({},b,{pay:b.prize*pass[n].bit,it:pass[n].bit});
			if(s) npass.s=Object.assign({},s,{pay:s.prize*pass[n].sit,it:pass[n].sit});
			big -= ((npass.b||{}).pay||0);
			small -= ((npass.s||{}).pay||0);
			amount -= (((npass.b||{}).pay||0) + ((npass.s||{}).pay||0));
			strikePass[n]=npass;
		});
		let balanceData={
			user_id,
			ticket_id,
			type:'downline_strike',
			game_id:game_id,
			num,
			big:toNumb(big),
			small:toNumb(small),
			bt,
			rebate,
			amount:toNumb(amount),
			from_upline:toNumb(passData.from_upline+amount),
			permute_length,
			pass:strikePass
		};
		data.passData=balanceData;
		data.strikeBalance.push(balanceData);
		resolve(data);
	});
};


exports.calc=(data,cb,dbclientConnection)=>{
	if(!dbclient) dbclient=dbclientConnection;
	let users=JSON.parse(JSON.stringify(data.ticket.users));
		player=users.pop();
	let x=Promise.resolve(data);
		x=x.then(prepare);
		if(data.ticket.game_id==='1D'){
			x=x.then(checkWin1D);
			x=x.then(prepareStrikeBalance);
			x=x.then(playerStrike1D);
			while (users.length){
				agent=users.pop();
				x=x.then(agentStrike1D);
			}
		} else {
			x=x.then(checkWin);
			x=x.then(prepareStrikeBalance);
			x=x.then(playerStrike);
			while (users.length){
				agent=users.pop();
				x=x.then(agentStrike);
			}
		}
		x.then((data)=>{
			//console.log('r=='+data.ticket.id+'='+data.ticket.game_id+'=>win');
			//console.log('data.strikeBalance=='+data.ticket.id+'=>'+JSON.stringify(data.strikeBalance));
			//console.log(JSON.stringify(data.wins));
			console.log('  \tstrike ticket :'+data.ticket.id+' finish');
			cb(null);
		})
		.catch((e)=>{
			//console.log('e=='+data.ticket.id+'='+data.ticket.game_id+'=>',e);
			if(e==='finish') return cb(null);
			if(e==='lose') return cb(null);
			cb(e);
		});
};