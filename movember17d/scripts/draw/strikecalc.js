var Game=require('../../routes/game').model;
var dbclient;

function toNumb(n){
	return parseFloat(n.toFixed(5));
}

const prepare=(ticketnumb)=>{
	return function(data){
		return new Promise((resolve,reject)=>{
			//console.log('ticketnumb==>',JSON.stringify(ticketnumb));
			
			let {d}=data;
			let prizeId=ticketnumb.prize_id;
			if(!(d.prize && d.prize[prizeId])){
				if(!d.prize) d.prize={};
				return Game.getPrize(prizeId,(e,r)=>{
					if(e) return reject(e);
					d.prize[prizeId]=r;
					data.prize=d.prize[prizeId];
					console.log('prize==>',JSON.stringify(data.prize));
					resolve(data);
				},dbclient);
			}
			data.prize=d.prize[prizeId];
			resolve(data);
		});
	};
};

//const prepareStrikeBalance=(data)=>{
//	return new Promise((resolve,reject)=>{
//		console.log('  down strike ticket :'+data.ticket.id);
//		data.backupPlaceBalance=JSON.parse(JSON.stringify(data.placeBalance));
//		data.strikeBalance=[];
//		resolve(data);
//	});
//};

function getNormalRank({prizeRefBig,prizeRefSmall,n,v,result}){
	let l=n.length;
	let posi=(bk)=>{
		for(let i=0;i<bk.length;i++){
			if( !(bk[i]==='s'|| bk[i]==='c') && result[bk[i]].substr(0,l)===n) return bk[i];
			else if(bk[i]==='s' && result.s.map(v=>{return v.substr(0,l);}).indexOf(n) > -1 ) return 's';
			else if(bk[i]==='c' && result.c.map(v=>{return v.substr(0,l);}).indexOf(n) > -1 ) return 'c';
		}
		return false;
	};
	let rnkB=!!v.big_bet && !!prizeRefBig && posi(Object.keys(prizeRefBig)),
		rnkS=!!v.small_bet && !!prizeRefSmall && posi(Object.keys(prizeRefSmall));
	if(!(rnkB || rnkS)) return false;
	return Object.assign({},{big_prize:0,small_prize:0},
			rnkB?{
				rank:rnkB,
				big_prize:prizeRefBig[rnkB],
			}:{},
			rnkS?{
				rank:rnkS,
				small_prize:prizeRefSmall[rnkS],
			}:{});
}


const checkWin=(ticketnumb)=>{
	if(ticketnumb.game_id==='1D'){
		return checkWin1D(ticketnumb);
	}
	return (data)=>{
		//console.log('data==>',JSON.stringify(data));
		return new Promise((resolve,reject)=>{
			let {d,prize,strikeData}=data,
				{result,draw_date}=d,
				{ticket_id,game_id,ibet,num,r,big_bet,small_bet}=ticketnumb;
			let sub=ibet?'i':'normal';//bt==='I'?'i':'normal';
			if(prize[game_id][sub]){
				if(sub==='normal'){
					let wn=getNormalRank({
							prizeRefBig:prize[game_id][sub].big,
							prizeRefSmall:prize[game_id][sub].small,
							n:num,
							v:{big_bet,small_bet},
							result});
					if(wn){
						let {big_prize,small_prize,rank}=wn;
						strikeData.push({
							ticket_id,num,draw_date,
							big_prize,small_prize,rank
						});
					}
					return resolve(data);
				} else if(sub==='i'){
					if(!(r && prize[game_id][sub])) return resolve(data);
					let wn=getNormalRank({
							prizeRefBig:prize[game_id][sub].big[r],
							prizeRefSmall:prize[game_id][sub].small[r],
							n:num,
							v:{big_bet,small_bet},
							result});
					if(wn){
						let {big_prize,small_prize,rank}=wn;
						strikeData.push({
							ticket_id,num,draw_date,
							rank,
							big_prize:big_prize*r,
							small_prize:small_prize*r,
						});
					}
					return resolve(data);
				} else {
					return resolve(data); //nanti diganti untuk ibet
				}
			}
			return resolve(data);
		});
	};
};

const _permute={
	o:'1 3 5 7 9'.split(' '),
	e:'0 2 4 6 8'.split(' '),
	b:'5 6 7 8 9'.split(' '),
	s:'0 1 2 3 4'.split(' '),
}

const checkWin1D=(ticketnumb)=>{
	return (data)=>{
		return new Promise((resolve,reject)=>{
			let {d,prize,strikeData}=data,
				{result,draw_date}=d,
				{ticket_id,num}=ticketnumb;
			if(prize['1D']['2']){
				let m=ticketnumb.match;
				let wn=_permute[num].indexOf(result[m.toString()].substr(0,1)) > -1;				
				console.log(' 1D='+ticket_id+'=>',wn,prize['1D']['2']);
				if(wn){
					strikeData.push({
						ticket_id,num,draw_date,
						prize_1d:prize['1D']['2'],
						rank:m.toString()
					});
				}
				return resolve(data);
			}
			return resolve(data);
		});
	};
};

module.exports=(ticketnumb,dbclientConnection)=>{
	if(!dbclient) dbclient=dbclientConnection;
	//if(ticketnumb.game_id!=='1D') return checkWin(ticketnumb);
	return (data)=>{
		return new Promise((resolve,reject)=>{
			let x=Promise.resolve(data)
				.then(prepare(ticketnumb))
				.then(checkWin(ticketnumb))
				.then(data=>{
					resolve(data);
				})
				.catch(e=>{
					console.log(new Error(e));
					reject(e);
				});
		});
	};
	//}
};
