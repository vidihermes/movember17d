require('json5/lib/require');
require('prototypes');

var pg=require('pg'),
	redis=require("redis"),
	redisConfig = require('../config/redis.json5'),
	init=require('../lib/init'),
	dbconfig=require('../config/db.json5'),
	Game=require('../routes/game').model,
	dateUtil=require('../lib/date'),
	sql=require('../lib/sql'),
    strikecalc=require('./draw/strikecalc');
	//place=require('./draw/place');

let redisClient = redis.createClient(redisConfig.pubsub);

let dbclient=new pg.Client(dbconfig);

if (typeof process === 'object') {
    process.on('unhandledRejection', (error, promise) => {
        console.error("== Node detected an unhandled rejection! ==");
        console.error(error.stack);
    });
}

const start=(d)=>{
	return new Promise((resolve,reject)=>{
		redisClient.publish('draw',JSON.stringify({m:'draw_start',user_id:d.user_id,result_id:d.result_id}));
		console.log('\n---\nstart:',dateUtil.toDbDateTime(new Date()));
		console.log('detail:',{user_id:d.user_id,result_id:d.result_id,force:!!d.force});
		resolve(d);
	});
};

const dbconnect=(d)=>{
	return new Promise((resolve,reject)=>{
		dbclient.connect(function(e){
			if(e) return reject(new Error(e));
			init.afterDbConnect(dbclient,(e)=>{
				if(e) return reject(new Error(e));
				resolve(d);
			});
		});
	});
};

function collectResultNumber(result){
	let n=[];
	'1 2 3'.split(' ').forEach(k=>{
		n.push(result[k]);
		[3,2,1].forEach(i=>{
			n.push(result[k].substr(0,i));
		});
	});
	n.push.apply(n,result.s);
	n.push.apply(n,result.c);
	return Array.from(new Set(n));
}

const getResult=(d)=>{
	d=d||{};
	return new Promise((resolve,reject)=>{
		if(!d.result_id) return reject('result_id required');
		dbclient.query('select * from m_result where id=$1',[d.result_id],function(e,r){
			if(e) return reject(new Error(e));
			let rslt=(((r||{}).rows||[])[0]);
			//console.loh('rslt==>',rslt)
			if(!rslt) return reject('result '+d.result_id+' not found');
			//rslt.drawn
			//let numbers=collectResultNumber(rslt.result);
            d.draw_date=dateUtil.toDbDate(new Date(rslt.apply_date));
			d=Object.assign(d,{result:rslt.result,today_id:rslt.draw_day/*,numbers*/});
			resolve(d);
		});
	});
};

const getCollectDate=(data)=>{
	data=data||{};
	//console.log('getCollectDate ==> ',data);
	return new Promise((resolve,reject)=>{
		Game.getDrawDate((e,r)=>{
			if(e) return reject(e);
			let drawDate=r;
			let startCollectDate=new Date(drawDate[Object.keys(drawDate)[2]].draw);
			startCollectDate.setDate(startCollectDate.getDate()-7);
			data=Object.assign(data,{drawDate,startCollectDate});
			resolve(data);
		},dbclient);
	});
};

const getDowForToday=(d)=>{
	d=d||{};
	//console.log('getDowForToday ==> ',d);
	return new Promise((resolve,reject)=>{
		if(!d.drawDate) return reject('drawDate required');
		if(d.force){
			if(!d.today_id) return reject('today_id required for forced process');
			let validDrawDay=d.drawDate[d.today_id.toString()];
			if(!validDrawDay) return reject('today_id '+today_id+' is not in drawday list');
			let rday=validDrawDay.draw,
				today=dateUtil.toDbDate(rday),
				now=dateUtil.toDbDateTime(rday);
			d=Object.assign(d,{today,now,today_id:parseInt(d.today_id),validDrawDay});
			return resolve(d);
		}
		dbclient.query('select now() as today',function(e,r){
			if(e) return reject(new Error(e));
			let rday=r.rows[0].today;
			let today=dateUtil.toDbDate(rday),
				now=dateUtil.toDbDateTime(rday),
				today_id=((new Date(rday)).getDay()+6)%7+1,
				validDrawDay=d.drawDate[today_id.toString()];
			if(!validDrawDay) return reject('today '+today_id+' is not in drawday list');
			let dDate=new Date(rday);
			let rDate=(dDate).getTime();
			if(!(rDate >= validDrawDay.close && rDate <= validDrawDay.draw)) {
				console.log('\tcurrent time\t: ',dDate.toLocaleString());
				console.log('\tclose time\t: ',(new Date(validDrawDay.close)).toLocaleString());
				console.log('\tdraw time\t: ',(new Date(validDrawDay.draw)).toLocaleString());
				return reject('current time is not in closing time');
			}
			d=Object.assign(d,{today,now,today_id,validDrawDay});
			resolve(d);
		});
	});
};

const updateTicket=(d)=>{
	return new Promise((resolve,reject)=>{
		dbclient.query('update t_active_ticket set day_left = array_remove(day_left,$1) ,day_length=day_length-1 where day_left @> $2 and date > $3',[d.today_id,[d.today_id],dateUtil.toDbDateTime(d.startCollectDate)],(e)=>{
			if(e) return  reject(e);
			resolve(d);
		});
	});
}



const prepare=(d)=>{
	d=d||{};
	return new Promise((resolve,reject)=>{
		d.errorUp=[];
		d.successUp=[];
		d.errorDown=[];
		d.successDown=[];
		d.intake={};
		//d.userConfig={};
		//d.tickets=[];
		resolve(d);
	});
};

const checkTicketNumCount=(game_id)=>{
    return (d)=>{
        d=d||{};
        return new Promise((resolve,reject)=>{
            //let q='select count(id) as cnt from t_active_ticket where day_left @> $1';
            let q=`with
        cfg as (select $1::text gid,$2::date dd,$3::int drawday_id),
        nums as (select id ticket_id,UNNEST(numbers) num from t_active_ticket)
        select 
            count(n.num) cnt
        from t_active_ticket t, nums n, cfg
        where n.ticket_id=t.id and t.game_id=cfg.gid and t.draw_days @> ARRAY[cfg.dd] and day_left @> ARRAY[cfg.drawday_id]`;
            dbclient.query(q,[game_id,d.draw_date,d.today_id],function(e,r){
                d.current_game_id=game_id;
                if(e) return reject(new Error(e));
                let ticketNumCount=((r.rows||[])[0]).cnt||0;
                d.ticketNumCount=d.ticketNumCount||{};
                d.ticketNumCount[game_id]=parseInt(ticketNumCount,10);
                resolve(d);
            });
        });
    };
};

const processNumTicket=(limit,page)=>{
    return d=>{
        return new Promise((resolve,reject)=>{
            console.log(d.current_game_id+'==> dikerjakan');
            //let q='select count(id) as cnt from t_active_ticket where day_left @> $1';
            let q=`with
        cfg as (select $1::text gid,$2::date dd,$3::int drawday_id),
        nums as (select id ticket_id,UNNEST(numbers) num from t_active_ticket)
        select 
            t.id ticket_id,
            game_id,
            prize_id,
            n.num,
            t.match,
            COALESCE(cast(bets #>> ARRAY[n.num,'b'] as float),0) big_bet,
            COALESCE(cast(bets #>> ARRAY[n.num,'s'] as float),0) small_bet,
            COALESCE(cast(bets #>> ARRAY[n.num,'1D'] as float),0) bet_1d,
            CASE WHEN bt='I' THEN true ELSE false END ibet,
            array_length(t.numbers,1) r
        from t_active_ticket t, nums n, cfg
        where n.ticket_id=t.id and t.game_id=cfg.gid and t.draw_days @> ARRAY[cfg.dd] and day_left @> ARRAY[cfg.drawday_id]
        order by t.id, n.num limit $4 offset $5`;
            dbclient.query(q,[d.current_game_id,d.draw_date,d.today_id,limit,limit*(page-1)],function(e,r){
                if(e) return reject(new Error(e));
                
                let data={d,strikeData:[]};
                console.log(JSON.stringify(r.rows));
                let x=Promise.resolve(data);
				r.rows.forEach(ticketnumb=>{
                    x=x.then(strikecalc(ticketnumb,dbclient));
				});
				x.then(data=>{
                    //console.log('============ kerja ============ ');
                    console.log('data.strikeData==>',data.strikeData);
                    
                    let sqlData=data.strikeData.map(v=>{
                        let {ticket_id,num,draw_date,big_prize,small_prize,prize_1d,rank}=v;
                        return {ticket_id,num,draw_date,big_prize:big_prize||0,small_prize:small_prize||0,prize_1d:prize_1d||0,rank};
                    });
                    //console.log('sqlData==>',sqlData);
                    if(!sqlData.length) return resolve(d);
                    sql.insertQuery('t_strike',sqlData,{},(q,v)=>{
                        dbclient.query(q,v,(e)=>{
                            if(e) return reject(e);
                            resolve(d);
                        });
                    });
				}).catch(e=>{
                    console.log(e.stack);
					reject(e);
				});
                //resolve(d);
            });
        });
    };
}

const processDraw=(d)=>{
    return new Promise((resolve,reject)=>{
        //console.log('d.ticketNumCount==>',d.ticketNumCount[d.current_game_id]);
        let limit=100;
        let pageCount=Math.ceil(d.ticketNumCount[d.current_game_id]/limit);
        console.log(' pageCount'+[d.current_game_id]+':',pageCount);
        let x=Promise.resolve(d);
        //x=x.then(prepare);
        for(let i=1;i<=pageCount;i++){
            x=x.then(processNumTicket(limit,i));
        }
        //x=x.then(updateTicket);
        x.then((d)=>{
            resolve(d);
        }).catch(e=>{
            console.log(new Error(e));
            reject(e);
        });
    });
};

function draw({force,result_id,user_id}){
	let d={force:!!parseInt(force),result_id,user_id};
	d.userConfig={};
	
	let x=start(d)
		.then(dbconnect)
		.then(getResult)
		.then(getCollectDate)
		.then(getDowForToday);
    ['4D','3D','2D','1D'].forEach(g=>{
        x=x.then(checkTicketNumCount(g));
        x=x.then(processDraw);
    });
    //.then(processDraw)
    x.then(updateTicket)
    .then(d=>{
        console.log('finish:',d);
        redisClient.publish('draw',JSON.stringify({m:'draw_finish'}));
        process.exit();
    })
    .catch(e=>{
        console.log('error:',e);
        if(e==='stop'){
            console.log('finish:',d.msg);
            redisClient.publish('draw',JSON.stringify({m:'draw_finish',ticket_count:d.ticketCount,result_id:d.result_id,e:d.msg}));
            process.exit();
        }
        console.log('error:',e);
        let es=typeof e === 'string'?e:e.toString();
        redisClient.publish('draw',JSON.stringify({m:'draw_error',e:es}));
        process.exit();
    });
}
//
//process.stdin.resume();//so the program will not close instantly
//
//function exitHandler(options, err) {
//    if (options.cleanup) console.log('clean');
//    if (err) console.log(err.stack);
//    if (options.exit) process.exit();
//}
//
////do something when app is closing
//process.on('exit', exitHandler.bind(null,{cleanup:true}));
//
////catches ctrl+c event
//process.on('SIGINT', exitHandler.bind(null, {exit:true}));
//
//// catches "kill pid" (for example: nodemon restart)
//process.on('SIGUSR1', exitHandler.bind(null, {exit:true}));
//process.on('SIGUSR2', exitHandler.bind(null, {exit:true}));
//
////catches uncaught exceptions
//process.on('uncaughtException', exitHandler.bind(null, {exit:true}));

module.exports=draw;