var path=require('path'),
	fs=require('fs'),
	rimraf=require('rimraf'),
	copy = require('copy'),
	cwd=process.cwd(),
	bjsPath=path.resolve(cwd,'../movember17d/public/static/js'),
	bViews=path.resolve(cwd,'../movember17d/views'),
	parentPath=path.resolve(cwd,'../'),
	cView=path.resolve(cwd,'./build/index.html');

rimraf.sync(bjsPath+'/*');
['web-admin','web-sagent','web-agent'].forEach(function(f){
	var cjsPath=path.resolve(parentPath,f,'./build/static/js');
	copy(cjsPath+'/*', bjsPath, function() {});
	console.log('-->',cjsPath);
})

console.log(cView+' -> '+bViews+'/sagent.ejs');
fs.writeFileSync(bViews+'/sagent.ejs',fs.readFileSync(cView,'utf8'),'utf8');
