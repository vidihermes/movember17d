import io from 'socket.io-client';

import {UpdateConfig} from '../components/game';
import {appEvent} from '../components/events';

const socket = io('/');

socket.on('connect', () => {
	console.log('koneksi nyambung');
	socket.on('runningText',(data)=>{
		UpdateConfig({running_text:data});
	});
	socket.on('notif',(data)=>{
		appEvent.fire('notiveReceive',data);
	});
	socket.on('notifViewedCount',(data)=>{
		appEvent.fire('setNotifCount',data);
	});
});

export const Talk=(key,data)=>{
	socket.emit(key,data);
}

export default socket;