import React,{Component} from 'react';
import Divider from 'material-ui/Divider';
import List,{ ListItem} from 'material-ui/List';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import IconButton from 'material-ui/IconButton';
//import Button from 'material-ui/Button';
import Collapse from 'material-ui/transitions/Collapse';
//import PersonIcon from 'material-ui-icons/Person';
import CloseIcon from 'material-ui-icons/Close';
import ArrowForwardIcon from 'material-ui-icons/ArrowForward';

//import OpenInNewIcon from 'material-ui-icons/OpenInNew';
import ExpandLess from 'material-ui-icons/ExpandLess';
import ExpandMore from 'material-ui-icons/ExpandMore';

import withRoot from '../components/withRoot';
import {appEvent} from '../components/events';
import {saveTmpBet} from '../components/game';
import {Get,Delete} from '../components/ajax';
import {SnackBar,Confirm,TableClasses,Classes} from '../components/dialog';

import {bList,mList} from './placebet1d';

const styles = theme => ({
	...TableClasses(theme),
	...Classes(theme),
	root: {
		width: '100%',
		display:'flex',
		flexDirection:'column',
		flex:1,
		height:'100%',
	},
	sub:{
		width: '100%',
		padding:5,
		boxSizing:'border-box',
		flex:1,
		overflow:'auto',
	},
	sub2:{
		width: '100%',
		paddingLeft:24,
		boxSizing:'border-box',
	},
	item:{
		display:'flex',
	},
	item2:{
		display:'flex',
		padding:[6,0,6,20]
	},
	button: {
		//margin:[-10,0],//margin: theme.spacing.unit,
		//margin:0,
		width:24,
		height:24,
	},
	button2: {
		margin: theme.spacing.unit,
	},
	collapsible:{
		margin:[0,0,0,0],
		padding:[0,0,0,0],
	},
});

class SavedItem extends Component{
	state={
		exp:false,
	}
	click=()=>{
		this.setState({exp:!this.state.exp})
	}
	render(){
		let {classes,v,k,onLoad,onDelete}=this.props;
		return <ListItem className={classes.tableRow} ><List className={classes.collapsible} >
			<ListItem className={classes.tableRow} key={v.id} onClick={this.click}>
				<div className={classes.col+' '+classes.colA}><IconButton className={classes.button} >{this.state.exp ? <ExpandLess /> : <ExpandMore />}</IconButton></div>
				<div className={classes.col}>{k+1}</div>
				<div className={classes.col+' '+classes.col4+' '+classes.cols}>{(new Date(v.date)).toLocaleString()}</div>
				<div className={classes.col+' '+classes.col2+' '+classes.cols}>{v.form_type}</div>
				<div className={classes.col+' '+classes.col2+' '+classes.colA}>
					<IconButton className={classes.button} onClick={onDelete(v.id)} style={{color:'#a00'}}>
						<CloseIcon />
					</IconButton>
					<IconButton className={classes.button} onClick={onLoad(v.id)} color={'primary'}>
						<ArrowForwardIcon />
					</IconButton>
				</div>
			</ListItem>
			<Collapse component="li" in={this.state.exp} timeout="auto" unmountOnExit>
				<div className={classes.tableRow}>
					<div className={classes.col}>&nbsp;</div>
					<div className={classes.col+' '+classes.colT}>
						{v.form_type==='normal' && <table style={{width:'100%',maxWidth:'500px'}}>
							<thead>
								<tr>
									<th className={classes.sItem2}>Bet</th>
									<th className={classes.sItem2}>Num</th>
									<th className={classes.sItem2}>Big</th>
									<th className={classes.sItem2}>Small</th>
									<th className={classes.sItem2}>T</th>
								</tr>
							</thead>
							<tbody>
							{((v.detail||{}).bets||[]).map((vi,idx)=><tr className={classes.sItem} key={idx}>
								<td className={classes.sItem2}>{vi.g}</td>
								<td className={classes.sItem2}>{vi.n}</td>
								<td className={classes.sItem2}>{vi.b}</td>
								<td className={classes.sItem2}>{vi.s}</td>
								<td className={classes.sItem2}>{vi.bt}</td>
							</tr>)}
							</tbody>
						</table>}
						{v.form_type==='1D' && <table style={{width:'100%',maxWidth:'500px'}}>
							<thead>
								<tr>
									<th className={classes.sItem2}>Bet</th>
									<th className={classes.sItem2}>Match</th>
									<th className={classes.sItem2}>Amount</th>
								</tr>
							</thead>
							<tbody>
							{((v.detail||{}).bets||[]).map((vi,idx)=><tr className={classes.sItem} key={idx}>
								<td className={classes.sItem2}>{bList[vi.n]}</td>
								<td className={classes.sItem2}>{mList[vi.m]}</td>
								<td className={classes.sItem2}>{vi.a}</td>
							</tr>)}
							</tbody>
						</table>}
					</div>
				</div>
			</Collapse>
		</List></ListItem>
	}
}

class SavedBets extends Component{
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}
	state={
		members:[],
		confirmOpen:false,
		successOpen:false,
		errorOpen:false,
	}
	ready=()=>{
		Get('savebet',{},(e,r)=>{
			if(e) return console.error(e);
			if(r && r.d) this.setState({members:r.d});
		});
	}
	componentWillMount(){
		appEvent.fire('titleChange','Saved Bets');
		this.ready();
	}
	componentWillUnmount(){
		//appEvent.off('meChange',this.meChange);
	}
	route=uri=>{
		return (e)=>{
			if(e && e.stopPropagation) e.stopPropagation();
			console.log(uri)
			this.props.history.push(uri);
		}
	}
	deleteItem=id=>{
		return (e)=>{
			e.stopPropagation();
			this.setState({
				confirmOpen:true,
				confirmOnOK:()=>{
					this.setState({confirmOpen:false});
					Delete('savebet/'+id,{},(e,r)=>{
						if(e) {
							this.setState({
								errorOpen:true,
								errorMsg:e.toString()
							})
						}
						if(r && r.d) {
							this.setState({
								successOpen:true,
								successMsg:'saved bet item has been deleted'
							},()=>{
								this.ready();
							})
						}
					});
				},
			})
		}
	}
	loadItem=id=>{
		return (e)=>{
			e.stopPropagation();
			let memberF=(this.state.members||[]).filter(v=>v.id===id),
				member=memberF[0];
			if(!member) return;
			if(member.form_type==='normal'){
				let dt={};
				((member.detail||{}).bets||[]).forEach((v,k)=>{
					let {g,n,b,s,bt}=v;
					dt[k+1]={g,n,b,s,bt};
				});
				saveTmpBet('normal',dt);
				this.route('/placebet')();
			} else if(member.form_type==='1D'){
				let dt=((member.detail||{}).bets||[]).map(v=>{
					let {n,m,a}=v;
					return {n,m,a,er:false,em:false};
				});
				saveTmpBet('1D',dt);
				this.route('/placebet1d')();
			}
		}
	}
	confirmClose=()=>{
		this.setState({confirmOpen:false});
	}
	onSuccessClose=()=>{
		this.setState({successOpen:false});
	}
	onErrorClose=()=>{
		this.setState({errorOpen:false});
	}
	render(){
		const { classes } = this.props,
			{members,confirmOpen,confirmOnOK,
			successOpen,errorOpen,successMsg,errorMsg} = this.state;
		return <div className={classes.root}>
			<List className={classes.sub}>
				<Divider />
				<ListItem className={classes.tableTitle}>
					<div className={classes.col}>&nbsp;</div>
					<div className={classes.col}>No</div>
					<div className={classes.col+' '+classes.col4}>Create Date</div>
					<div className={classes.col+' '+classes.col2}>Form</div>
					<div className={classes.col+' '+classes.col2}>&nbsp;</div>
				</ListItem>
				{members.map((v,k)=>
					<SavedItem key={v.id} k={k} classes={classes} v={v}
						onLoad={this.loadItem}
						onDelete={this.deleteItem}/>)}
				<Divider />
				{!members.length && <div className={classes.empty} >there is no data found</div>}
			</List>
			<Confirm open={confirmOpen} title={'Delete Item?'} msg={'Are you sure you want to delete this item?'} onClose={this.confirmClose} onOK={confirmOnOK}/>
			<SnackBar open={successOpen} onClose={this.onSuccessClose} message={successMsg} classes={{root:classes.succesBox}}/>
			<SnackBar open={errorOpen} onClose={this.onErrorClose} message={errorMsg} classes={{root:classes.errorBox}}/>
		</div>
	}
}

export default withRoot(withStyles(styles, { withTheme: true })(SavedBets));