import React,{Component} from 'react';
import {Get} from '../components/ajax';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';

import {appEvent} from '../components/events';
import {humanize} from '../components/NotifMenu';

const $=window.$;

const styles = theme => ({
	root:{
		padding:20
	}
});

class Announcement extends Component{
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}
	componentWillMount(){
		let pageType=(this.props.match||{}).path.replace(/^\/(\w*)/,'$1');
		if(!pageType) pageType='announcement';
		appEvent.fire('titleChange',humanize(pageType));
		Get(pageType,{},(e,r)=>{
			if(e) {
				//_Me={};
				console.log('e==>',e);
				//return this.logedOut();
				//return this.setState({userId:''});
			}
			if(r && r.d && r.d.content){
				$('#announc').html(r.d.content);
			}
		});
	}
	render(){
		return <div id={"announc"} className={this.props.classes.root+" markdown-body"} />
	}
}

//export default Announchment;
export default withStyles(styles, { withTheme: true })(Announcement);