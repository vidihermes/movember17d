import React,{Component} from 'react';
import Grid from 'material-ui/Grid';
import Divider from 'material-ui/Divider';
import List,{ ListItem,ListItemText} from 'material-ui/List';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import Collapse from 'material-ui/transitions/Collapse';

import ExpandLess from 'material-ui-icons/ExpandLess';
import ExpandMore from 'material-ui-icons/ExpandMore';

import withRoot from '../components/withRoot';
import {appEvent} from '../components/events';
import {Me, Game, Alias, UpdateMe, UserType} from '../components/game';
import {Put} from '../components/ajax';
import {SnackBar,ChangePassword,EditableItem,Classes} from '../components/dialog';

const styles = theme => ({
	...Classes(theme),
	root: {
		display:'flex',
		flexDirection:'column',
		width: '100%',
		height: '100%',
	},
	content:{
		flex:1,
		overflowY:'auto',
		[theme.breakpoints.up('sm')]: {
			display:'flex',
			flexDirection:'row',
			overflowY:'hidden',
		}
		
	},
	sub:{
		width: '100%',
		float:'left',
		padding:5,
		boxSizing:'border-box',
		[theme.breakpoints.up('sm')]: {
			width: '50%',
			flex:1,
			overflowY:'auto'
		},
	},
	sub2:{
		width: '100%',
		paddingLeft:24,
		boxSizing:'border-box',
	},
	item:{
		display:'flex',
		padding:[8,16]
	},
	label:{
		whiteSpace:'nowrap',
		fontSize:'12pt',
		padding:'0px 10px',
	},
	value:{
		fontSize:'12pt',
		paddingLeft:5,
	},
	value2:{
		fontSize:'12pt',
		paddingLeft:0,
		display:'flex',
		alignItems:'center',
	},
	button: {
		//margin:[-10,0],//margin: theme.spacing.unit,
		width:30,
		height:30,
	},
	wrapper: {
		//margin: theme.spacing.unit,
		position: 'relative',
		display:'inline-block'
	},
	buttonProgress: {
		color: 'teal',//green[500],
		position: 'absolute',
		top: '50%',
		left: '50%',
		marginTop: -12,
		marginLeft: -12,
	},
	button2: {
		margin: theme.spacing.unit,
	},
	title:{
		...theme.typography.title,
		justifyContent:'center'
	},
	input:{
		padding:[5,10],
		fontSize:'12pt',
		flex:1,
		width:'100%',
		marginLeft:-6
	},
	after:{
		fontSize:'10pt',
		padding:[0,5],
		color:'rgba(0,0,0,0.5)'
	},
	err:{
		borderColor:'#f00',
		outlineColor:'rgba(255,0,0,0.1)'
	},
	footer:{
		padding:[0,10],
		borderTop:[1,'solid','#eee'],
		justifyContent:'space-between',
		display:'flex',
		alignItems:'center',
		[theme.breakpoints.down('sm')]: {
			display:'block'
		},
	},
	footerLeft:{
		display:'flex',
		alignItems:'center',
		flex:1,
		flexWrap:'wrap'
	},
	bc:{
		flex:1
	},
	bcContent:{
		whiteSpace:'nowrap'
	},
	footerRight:{
		[theme.breakpoints.down('md')]: {
			display:'flex',
			justifyContent:'space-around',
		},
	},
	bold:{
		fontWeight:'bold'
	}
});

const Item=({c,label,value,valueCn})=><ListItem className={c.item}>
	<Grid item xs={4} className={c.label}>{label}</Grid>
	<Grid item xs={1} className={c.label}>{":"}</Grid>
	<Grid item xs={7} className={c.value+(valueCn?' '+valueCn:'')}>{value}</Grid>
</ListItem>

class Mine extends Component{
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}
	static contextTypes = {
		intl: PropTypes.object
	}
	state={
		id:'',
		name:'',
		credit:0,
		balance:0,
		//config:{}
		game:{},
		errOpen:false,
		errMessage:'',
		cpOpen:false,
		cepOpen:false,
		successOpen:false,
	}
	cpClose=()=>{
		this.setState({cpOpen:false});
	}
	cpOK=({password,confirm})=>{
		if(!(!!password && password === confirm))
			return this.setState({errOpen:true,errMessage:'invalid new password or confirm new password'});
		//this.setState({cpOpen:false});
		Put('me',{password},(e,r)=>{
			if(e) {
				console.log('e==>',e);
				return this.setState({errOpen:true,errMessage:e.toString()});
			}
			console.log('r==>',r);
			this.setState({
				cpOpen:false,
				successOpen:true,
				successMsg:'Password updated'
			});
		});
	}
	cepClose=()=>{
		this.setState({cepOpen:false});
	}
	cepOK=({password,confirm})=>{
		if(!(!!password && password === confirm))
			return this.setState({errOpen:true,errMessage:'invalid new password or confirm emergency password'});
		Put('me',{fake_password:password},(e,r)=>{
			if(e) {
				console.log('e==>',e);
				return this.setState({errOpen:true,errMessage:e.toString()});
			}
			this.setState({
				cepOpen:false,
				successOpen:true,
				successMsg:'Emergency password updated'
			});
		});
	}
	onSuccessClose=()=>{
		this.setState({successOpen:false});
	}
	meChange=me=>{
		this.setState({...this.state,...me},()=>{
		});
	}
	nameSave=(name,cb)=>{
		Put('me',{name},(e,r)=>{
			if(e){
				console.log('e==>',e);
				return this.setState({errOpen:true,errMessage:e.toString()},()=>{
					cb(e);
				});
			}
			cb(null,r);
			if(r && r.d) UpdateMe(r.d);
		});
	}
	intakeSave=(g)=>{
		return (it,cb)=>{
			let fld={};
			fld['config['+g+'][it]']=it;
			Put('me',fld,(e,r)=>{
				if(e) {
					return this.setState({errOpen:true,errMessage:e.toString()},()=>{
						cb(e);
					});
				}
				cb(null,r);
				if(r && r.d) UpdateMe(r.d);
			})
		}
	}

	configValidation=(game,k)=>{
		return v=>{
			v=(v||'0').toString();
			if(!(v.match(/^[\d,.]+$/))) return false;
			let v2=parseFloat(v);
			if(isNaN(v2)) return false;
			return true;
		}
	}
	loginIdValidation=v=>{
		if(!v) return true;
		v=v.toUpperCase();
		return v.match(/^\w+$/) && !(v.match(/^[A-Z]+PL\d+$/) || v.match(/^[A-Z]+AG\d+$/));
	}
	loginIdSave=(value,cb)=>{
		let fld={};
		fld['login_id']=value;
		Put('user/'+this.state.id,fld,(e,r)=>{
			if(e) {
				return this.setState({errOpen:true,errMessage:e.toString()},()=>{
					cb(e);
				});
			}
			cb(null,r);
		})
	}
	closeErr=()=>{
		this.setState({errOpen:false})
	}
	route=uri=>{
		return (e)=>{
			if(e && e.stopPropagation) e.stopPropagation();
			this.props.history.push(uri);
		}
	}
	componentWillMount(){
		appEvent.fire('titleChange','My Profile');
		//appEvent.on('MeErr',this.showError);
		Me((e,me)=>{
			this.meChange(me);
			setTimeout(()=>{
				appEvent.on('meChange',this.meChange);
			},1);
		},true);
		Game((e,game)=>{
			this.setState({game});
		});
	}
	componentWillUnmount(){
		appEvent.off('meChange',this.meChange);
		//appEvent.off('MeErr',this.showError);
	}
	formatNumb=n=>{
		return this.context.intl.formatNumber(n,{style: 'decimal',minimumFractionDigits:2,maximumFractionDigits:2});
	}
	render(){
		let { classes } = this.props,
			{id,name,credit,balance,credit_allocated,active_bet,config,type,agent_config,status,active,game,errOpen,errMessage,cpOpen,cepOpen,successOpen,successMsg,login_id} = this.state;
			status=status||'Active';
		config=config||{};
		//	console.log('agent_config==>',agent_config);
		return <div className={classes.root}>
			<div className={classes.content}>
				<List className={classes.sub}>
					<Typography type="subheading" gutterBottom>Profile</Typography>
					<Divider />
					<Item c={classes} label={"Id"} value={id} valueCn={classes.bold}/>
					<EditableItem c={classes}
						value={name}
						//onChange={this.nameChange}
						label={"Name"}
						rootClass={"item"}
						type={"text"}
						onSave={this.nameSave}/>
					<EditableItem c={classes}
						value={login_id}
						//onChange={this.nameChange}
						upperCase={true}
						label={"Login Id"}
						rootClass={"item"}
						type={"text"}
						validation={this.loginIdValidation}
						onSave={this.loginIdSave}/>
					<Item c={classes} label={"Type"} value={UserType[type]} />
					<Item c={classes} label={"Credit Limit"} value={this.formatNumb(credit||0)} />
					{!!balance && <Item c={classes} label={"Balance"} value={this.formatNumb(balance||0)} />}
					{!!credit_allocated && <Item c={classes} label={"Credit Allocated"} value={this.formatNumb(credit_allocated||0)} />}
					{!!active_bet && <Item c={classes} label={"Active Bet"} value={this.formatNumb(active_bet||0)} />}
					<Item c={classes} label={"Credit Left"} value={this.formatNumb(parseFloat(credit||0)+parseFloat(balance||0)-parseFloat(credit_allocated||0)-parseFloat(active_bet||0))} />
					<Divider />
					<Item c={classes} label={"Status"} value={!active?'Terminated':status||'Active'} />
					
				</List>
				<List className={classes.sub}>
					<Typography type="body2" gutterBottom>Configuration</Typography>
					<Divider />
						{['4D','3D','2D','1D'].map(g=><div key={g} className={classes.sub2}>
							<Typography type="subheading" gutterBottom>{g}</Typography>
							<EditableItem
								c={classes}
								value={(config[g]||{}).it}
								def={'0'}
								after={`$`}
								//onChange={this.intakeChange(g)}
								label={'Maximum Intake'}
								onSave={this.intakeSave(g)}
								validation={this.configValidation(g,'it')}/>
						</div>)}
				</List>
			</div>
			<div className={classes.footer}>
				<div className={classes.footerLeft}>
					<Button raised color={"secondary"} className={classes.button2} onClick={this.route('/downline')}>My Downline</Button>
				</div>				
				<div className={classes.footerRight}>
					<Button raised className={classes.button2} color={"secondary"} onClick={()=>{this.setState({cpOpen:true})}}>Change Password</Button>
					<Button raised className={classes.button2} color={"secondary"} onClick={()=>{this.setState({cepOpen:true})}}>Change Emergency Password</Button>
				</div>
			</div>
			<SnackBar open={errOpen} onClose={this.closeErr} message={errMessage} classes={{root:classes.errorBox}}/>
			<SnackBar open={successOpen} onClose={this.onSuccessClose} message={successMsg} classes={{root:classes.succesBox}}/>
			{cpOpen && <ChangePassword open={cpOpen} onClose={this.cpClose} onOK={this.cpOK}/>}
			{cepOpen && <ChangePassword open={cepOpen} onClose={this.cepClose} onOK={this.cepOK}
					title={'Change Emergency Password'}
					label={'New Emergency Password'}
					confirmLabel={'Confirm Emergency Password'}
				/>}
		</div>
	}
}

export default withRoot(withStyles(styles, { withTheme: true })(Mine));