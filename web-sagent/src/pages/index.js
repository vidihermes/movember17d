import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Drawer from 'material-ui/Drawer';
import Typography from 'material-ui/Typography';
import Hidden from 'material-ui/Hidden';
import Divider from 'material-ui/Divider';
import {blueGrey} from 'material-ui/colors';
import LeftMenu from '../components/leftMenu';
import withRoot from '../components/withRoot';
import TitleBar from '../components/TitleBar';
import NotifMenu from '../components/NotifMenu';
import {appEvent} from '../components/events';

import { BrowserRouter as Router, Route, } from 'react-router-dom';

import Mine from './me';
import UserDetail from './userdetail';
import Users from './users';
import Announcement from './announcement';
import PlaceFixBet from './placefixbet';
import ViewFixBets from './viewfixbet';
import PlaceBet from './placebet';
import PlaceBet1D from './placebet1d';
import PlaceMassBet from './placemassbet';
import PlaceWildcardBet from './placewildcardbet';
import AddMember from './addmember';
import SavedBets from './savedbets';
import Tickets from './tickets';
import Prize from './prize';
import Result from './result';
import Notifs from './notif';
import Logs from './logs';
import SummaryBets from './summarybets';
import SummaryReport from './summaryreport';
import StrikeReport from './strikereport';
import FullReport from './fullreport';
import Payment from './payment';
import HPayment from './hpayment';

import io from '../sio/';
const drawerWidth = 240;

const styles = theme => ({
	root: {
		//width: '100%',
		height: '100%',
		//marginTop: theme.spacing.unit * 3,
		margin:'0 0 0 0',
		zIndex: 1,
		overflow: 'hidden',
		fontFamily: `"Roboto", "Helvetica", "Arial", sans-serif`,
	},
	appFrame: {
		position: 'relative',
		display: 'flex',
		width: '100%',
		height: '100%',
	},
	drawerHeader:{
		//theme.mixins.toolbar,
		background:theme.palette.primary.dark,
		backgroundImage:'url(/logo.png)',
		backgroundRepeat:'no-repeat',
		backgroundPosition:'50% 50%',
		height:76,
	},
    logo:{
        //height:64,
        width:'100%',
        textAlign:'center',
        padding:'10px 0px',
		color:theme.palette.secondary.dark,
    },
    logoTop:{
        fontSize:26,
        fontWeight:'bold',
        color:theme.palette.secondary.main,
        textShadow:'rgba(0,0,0,0.5) 1px 1px 5px'
    },
    logoBottom:{
        height:9,
        fontWeight:'bold',
        color:theme.palette.secondary.main,
    },
	drawerPaper: {
		width: 250,
		//borderRight:[0.5,'solid',theme.palette.secondary.dark],
		borderRight:[0.5,'solid',theme.palette.secondary.main],
		background:theme.palette.secondary.light,
		[theme.breakpoints.up('md')]: {
			width: drawerWidth,
			//position: 'relative',
			height: '100%',
			//maxHeight:'100%',
			//flex:1,
		},
	},
	content: {
		//backgroundColor: theme.palette.background.default,
		backgroundColor:'white',
		width: '100%',
		//padding: theme.spacing.unit * 3,
		//padding:'24px 2px',
		//marginTop: 56+32+18, //56+32+18
		marginTop: 48+28, //64+30
        //height: 'calc(100% - '+(56+32+18)+'px)', //56+48+32+18
		height: 'calc(100% - '+(48+28)+'px)', //64+48+30
        overflowY:'auto',
		//[theme.breakpoints.up('sm')]: {
		//	//height: 'calc(100% - '+(48+33)+'px)', //64+48+30
		//	//marginTop: 48+33, //64+30
		//	//padding: theme.spacing.unit * 3,
		//},
	},
});

class Index extends React.Component {
	state = {
		mobileOpen: false,
	}
	handleDrawerToggle = () => {
		this.setState({ mobileOpen: !this.state.mobileOpen });
	}

	componentWillMount(){
		appEvent.on('drawerToggle',this.handleDrawerToggle);
	}
	componentDidMount(){
		//subscribeToTimer((e,r)=>{
		//	console.log('e==>',e);
		//	console.log('r==>',r);
		//})
	}
	componentWillUnmount(){
		appEvent.off('drawerToggle',this.handleDrawerToggle);
	}
	render() {
		const { classes, theme } = this.props;
		const Drw =(p,c)=>(
			<div>
				<div className={classes.drawerHeader}>
                    
                </div>
				{p.children}
			</div>
		);
		return (
			<div className={classes.root}>
				<Router>
				<div className={classes.appFrame}>
					<Route path="/" component={TitleBar}/>
					<Hidden mdUp>
						<Drawer
							type="temporary"
							anchor={theme.direction === 'rtl' ? 'right' : 'left'}
							open={this.state.mobileOpen}
							classes={{
								paper: classes.drawerPaper,
							}}
							onClose={this.handleDrawerToggle}
							ModalProps={{
								keepMounted: true, // Better open performance on mobile.
							}}
						>
							<Drw>
								<Route path="/" component={LeftMenu}/>
							</Drw>
						</Drawer>
					</Hidden>
					<Hidden smDown implementation="css">
						<div style={{width:'240px'}}>
						<Drawer
							type="permanent"
							open
							classes={{
								paper: classes.drawerPaper,
							}}
						>
							<Drw>
								<Route path="/" component={LeftMenu}/>
							</Drw>
						</Drawer>
						</div>
					</Hidden>
					<Route path="/" component={NotifMenu}/>
					<main className={classes.content}>
						<Route exact path="/" component={Announcement}/>
						<Route path="/announcement" component={Announcement}/>
						<Route path="/help" component={Announcement}/>
						<Route path="/me" component={Mine}/>
						{/*<Route path="/downline/:id?/:action?" component={Users}/>*/}
						<Route exact path="/downline/:id?" component={Users}/>
						<Route path="/downline/:id/:action" component={AddMember}/>
						<Route path="/user/:id" component={UserDetail}/>
						<Route path="/placebet" component={PlaceBet}/>
						<Route path="/placebet1d" component={PlaceBet1D}/>
						<Route path="/placemassbet" component={PlaceMassBet}/>
						<Route path="/placewildcardbet" component={PlaceWildcardBet}/>
						<Route path="/savedbets" component={SavedBets}/>
						<Route path="/placefixbet/:userid?" component={PlaceFixBet}/>
						<Route path="/viewfixbet/:userid?" component={ViewFixBets}/>
						<Route path="/tickets" component={Tickets}/>
						<Route path="/htickets" component={Tickets}/>
						<Route path="/prize" component={Prize}/>
						<Route path="/result" component={Result}/>
						<Route path="/notif" component={Notifs}/>
						<Route path="/logs" component={Logs}/>
						<Route path="/summarybets/:userid?/:date?" component={SummaryBets}/>
						<Route path="/detailbets/:userid?/:date?" component={SummaryBets}/>
						<Route path="/tallybets/:userid?/:date?" component={SummaryBets}/>
						<Route path="/summaryreport" component={SummaryReport}/>
						<Route path="/strikereport" component={StrikeReport}/>
						<Route path="/fullreport" component={FullReport}/>
						<Route path="/payment" component={Payment}/>
						<Route path="/hpayment" component={HPayment}/>
					</main>
				</div>
				</Router>
			</div>
		);
	}
}

Index.propTypes = {
	classes: PropTypes.object.isRequired,
	theme: PropTypes.object.isRequired,
};

export default withRoot(withStyles(styles, { withTheme: true })(Index));
