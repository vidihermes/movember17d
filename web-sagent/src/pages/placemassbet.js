import React,{Component} from 'react';
import PlaceBet,{createNs,Table} from './placebet';
import {BetRow} from '../components/BetRow';

export default class PlaceMassBet extends Component{
	render(){
		return <PlaceBet
			pageName={'mass'}
			ns={createNs(1,60)}
			renderContent={({classes,ready,loading,day,currentPrize,comp})=><div>{[1,21,41].map((nsk)=><div key={nsk}>
						<Table classes={classes}>
							{createNs(nsk,nsk+19).map(k=><BetRow disable={!ready || loading}
								ref={e=>{comp.items[k]=e}}
								key={k}
								idx={k}
								day={day}
								classes={classes}
								onNext={comp.onNext}
								onChange={comp.itemChange}
								gameValidation={comp.gameValidation}
								currentPrize={currentPrize}/>)}
						</Table>
						<div>&nbsp;</div>
					</div>)}
			</div>}
			renderFooter={({classes,big,small,available,formatNumber})=><div className={[
						(big+small>available)?classes.hErr:null
					].join(' ')}>
					Big : {formatNumber(big)} &nbsp; &nbsp;
					Small : {formatNumber(small)} &nbsp; &nbsp; &nbsp; &nbsp;
					Total : {formatNumber(big+small)}
				</div>}
		/>
	}
}
