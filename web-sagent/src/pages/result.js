import React,{Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';

import Button from 'material-ui/Button';
import {appEvent} from '../components/events';
import {Get,Put,Post,flatten} from '../components/ajax';
import {PrizeRank,timeOffsetDiff} from '../components/game';
import {SnackBar} from '../components/dialog';

const styles = theme => ({
	root: {
		width: '100%',
		display:'flex',
		flexDirection:'column',
		flex:1,
		height:'100%',
	},
	place:{
		flex:1,
		padding:20,
		overflow:'auto',
	},
	table:{
		textAlign:'center'
	},
	thead:{
		backgroundColor:theme.palette.primary.main,
	},
	input:{
		padding:[0,10],
		boxSizing:'border-box'
	},
	err:{
		border:[1,'solid','red'],
		outline:[1,'solid','red'],
	},
	footer:{
		borderTop:[1,'solid','#ddd'],
		textAlign:'right'
	},
	button2:{
		margin:10
	},
	succesBox:{
		backgroundColor:theme.palette.primary[500]
	},
	errorBox:{
		backgroundColor:theme.palette.error[300]
	},
});

function createBlankResult(){
	let rslt={
		'1':'',
		'2':'',
		'3':'',
		's':[],
		'c':[],
	};
	for(let i=0;i<10;i++){
		rslt.s.push('');
		rslt.c.push('');
	}
	return rslt;
}

class Result extends Component{
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}
	state={
		result:createBlankResult(),
		timeoffset:null,
	}
	componentWillMount(){
		appEvent.fire('titleChange','Results');
		timeOffsetDiff((e,timeoffset)=>{
			this.setState({timeoffset});
		})
		Get('result',{},(e,r)=>{
			console.log('rrr==>',r);
			if(e) return console.error('e==>',e);
			if(r && r.d){
				let {apply_date,...st}=r.d;
				apply_date=(new Date(apply_date)).getTime();
				this.setState({...st,apply_date});
			}
		});
	}
	render(){
		const {classes}=this.props,
			{apply_date,id,result,timeoffset}=this.state;
		return <div className={classes.root}>
			<div className={classes.place+' markdown-body'}>
				<table className={classes.table}>
					<thead><tr>
						<th className={classes.thead}>{apply_date?(new Date(apply_date+timeoffset)).toDateString():'Draw Date'}</th>
						<th className={classes.thead}>Draw No.{id}</th>
					</tr></thead>
					<tbody>
						{['1','2','3'].map(v=><tr key={v}>
							<td>{PrizeRank[v]}</td>
							<td>{result[v]}</td>
						</tr>)}
					</tbody>
					<thead><tr>
						<th className={classes.thead} colSpan={2}>Starter Prizes</th>
					</tr></thead>
					<tbody>
						{[0,1,2,3,4].map(v=><tr key={v}>
							<td>{result.s[v]}</td>
							<td>{result.s[v+5]}</td>
						</tr>)}
					</tbody>
					<thead><tr>
						<th className={classes.thead} colSpan={2}>Consolation Prizes</th>
					</tr></thead>
					<tbody>
						{[0,1,2,3,4].map(v=><tr key={v}>
							<td>{result.c[v]}</td>
							<td>{result.c[v+5]}</td>
						</tr>)}
					</tbody>
				</table>
			</div>
		</div>
	}
}

export default withStyles(styles, { withTheme: true })(Result);