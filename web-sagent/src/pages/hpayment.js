import React,{Component} from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import Divider from 'material-ui/Divider';
import List,{ ListItem} from 'material-ui/List';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import IconButton from 'material-ui/IconButton';
import Button from 'material-ui/Button';
import Collapse from 'material-ui/transitions/Collapse';
//import PersonIcon from 'material-ui-icons/Person';
//import CloseIcon from 'material-ui-icons/Close';
//import ArrowForwardIcon from 'material-ui-icons/ArrowForward';

//import OpenInNewIcon from 'material-ui-icons/OpenInNew';
import ExpandLess from 'material-ui-icons/ExpandLess';
import ExpandMore from 'material-ui-icons/ExpandMore';

import withRoot from '../components/withRoot';
import {appEvent} from '../components/events';
import {/*saveTmpBet,*/Dow,Me,toDbDate} from '../components/game';
import {Get,Post,flatten} from '../components/ajax';
import {SearchBar,ChangeStatus,SnackBar,TableClasses,Classes} from '../components/dialog';
import {bList,mList} from './placebet1d';
import {aMothAgo} from './fullreport';


const styles = theme => ({
	...TableClasses(theme),
	...Classes(theme),
	root: {
		width: '100%',
		display:'flex',
		flexDirection:'column',
		flex:1,
		height:'100%',
	},
	sub:{
		width: '100%',
		padding:5,
		boxSizing:'border-box',
		flex:1,
		overflow:'auto',
	},
	sub2:{
		width: '100%',
		paddingLeft:24,
		boxSizing:'border-box',
	},
	item:{
		display:'flex',
	},
	item2:{
		display:'flex',
		padding:[6,0,6,20]
	},
	button: {
		//margin:[-10,0],//margin: theme.spacing.unit,
		//margin:0,
		width:24,
		height:24,
	},
	button2: {
		margin: theme.spacing.unit,
	},
	tableRow:{
		padding:[0,0],
		borderTop:[1,'solid','#eee'],
		//cursor:'pointer',
		display:'flex',
		flexDirection:'row',
		'&:hover':{
			backgroundColor:'rgba(0,0,255,0.05)'
		}
	},
	collapsible:{
		margin:[0,0,0,0],
		padding:[0,0,0,0],
	},
	sItem2:{
		textAlign:'center'
	},
	sFirstItem:{
		textAlign:'left',
		padding:[5,0,20,0],
		//borderBottom:[1,'solid','#eee']
		fontSize:'14pt'
	},
	link:{
		cursor:'pointer',
		color:'blue',
	}
});

const TicketItem =(p)=>{
		let {classes,v,k,formatNumb}=p;
		return <ListItem className={classes.tableRow} >
				<div className={classes.col}>{v.id}</div>
				<div className={classes.col+' '+classes.col4+' '+classes.cols}>{(new Date(v.date)).toLocaleString()}</div>
				<div className={classes.col+' '+classes.col2+' '+classes.cols}>{v.user_id}</div>
				<div className={classes.col+' '+classes.col2+' '+classes.coln}>{formatNumb(v.amount)}</div>
				<div className={classes.col+' '+classes.col4+' '+classes.cols}>{v.description}</div>
		</ListItem>
	}

class HPayment extends Component{
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}
	static contextTypes = {
		intl: PropTypes.object
	}
	state={
		members:[],
		ready:false,
		me:{},
		id:'',
		//take:0,
		//bring:0
		fromDate:aMothAgo(),
		toDate:new Date(),
	}
	componentWillMount(){
		appEvent.fire('titleChange','Payment History');
		Me((e,r)=>{
			this.meChange(r);
		})
	}
	meChange=me=>{
		this.setState({
			me,
			searchParams:{
				member_of:me.id,
				active:true,
			}
		},this.ready);
	}
	componentWillUnmount(){
		
	}
	route=uri=>{
		return (e)=>{
			if(e && e.stopPropagation) e.stopPropagation();
			console.log(uri)
			this.props.history.push(uri);
		}
	}
	formatNumb=n=>{
		return this.context.intl.formatNumber(n,{style: 'decimal',minimumFractionDigits:2,maximumFractionDigits:2});
	}
	setUser=(d)=>{
		let prm={limit:1000}
		if(d) this.setState({id:d.id},this.ready);
	}
	ready=()=>{
		let {id,fromDate,toDate}=this.state;
		let prm={limit:1000,fromdate:toDbDate(fromDate),todate:toDbDate(toDate)};
		//if(d) prm.user_id=d.id;
		if(id) prm.user_id=id;
		Get('paymenthistory',prm,(e,r)=>{
			if(e) return console.error(e);
			if(r && r.d) this.setState({members:r.d,
				//id:(d||{}).id,
				ready:true});
		});
	}
	dateChange=(k)=>{
		return (value)=>{
			let st={};
			st[k]=value;
			this.setState(st,this.ready);
		}
	}
	onSuccessClose=()=>{
		this.setState({successOpen:false});
	}
	onErrorClose=()=>{
		this.setState({errorOpen:false});
	}
	render(){
		const { classes} = this.props,
			{me,members, ready, id, searchParams,take,bring,
			successOpen,errorOpen,successMsg,errorMsg,fromDate,toDate} = this.state;
		return <div className={classes.root}>
			<div className={classes.tableFooter}>
				<div className={classes.footerSub2}>
					<SearchBar label={'Filter by User Id'}
						searchParams={searchParams}
						userDetailValidate={d=>d.active}
						onSearchMode={()=>{this.setState({ready:false},this.setUser)}}
						onFound={this.setUser}/>
					<div className={classes.footerSub2}>
						&nbsp; &nbsp;
						From &nbsp;
						<DatePicker className={classes.date}
							selected={moment(fromDate)}
							onChange={this.dateChange('fromDate')}
							{...(toDate?{maxDate:toDate}:{maxDate:moment()})}
						/>
						&nbsp; &nbsp;
						To &nbsp;
						<DatePicker className={classes.date}
							selected={moment(toDate)}
							onChange={this.dateChange('toDate')}
							maxDate={moment()}
							{...(fromDate?{minDate:moment(fromDate)}:{})}
						/>
					</div>
				</div>
			</div>
			<List className={classes.sub}>
				<Divider />
				<ListItem className={classes.tableTitle}>
					<div className={classes.col}>ID</div>
					<div className={classes.col+' '+classes.col4}>Date</div>
					<div className={classes.col+' '+classes.col2}>MemberId</div>
					<div className={classes.col+' '+classes.col2}>Amount</div>
					<div className={classes.col+' '+classes.col4}>Description</div>
				</ListItem>
				{(!members.length && ready ) && <div className={classes.empty} >there is no data found</div>}
				{members.map((v,k)=>
					<TicketItem key={v.id} k={k} classes={classes} v={v} formatNumb={this.formatNumb} />)}
				<Divider />
			</List>
			{successOpen && <SnackBar open={successOpen} onClose={this.onSuccessClose} message={successMsg} classes={{root:classes.succesBox}}/>}
			{errorOpen && <SnackBar open={errorOpen} onClose={this.onErrorClose} message={errorMsg} classes={{root:classes.errorBox}}/>}
		</div>
	}
}

export default withRoot(withStyles(styles, { withTheme: true })(HPayment));