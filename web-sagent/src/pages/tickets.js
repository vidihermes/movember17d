import React,{Component} from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import Divider from 'material-ui/Divider';
import List,{ ListItem} from 'material-ui/List';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import IconButton from 'material-ui/IconButton';
//import Button from 'material-ui/Button';
import Collapse from 'material-ui/transitions/Collapse';
//import PersonIcon from 'material-ui-icons/Person';
//import CloseIcon from 'material-ui-icons/Close';
//import ArrowForwardIcon from 'material-ui-icons/ArrowForward';

//import OpenInNewIcon from 'material-ui-icons/OpenInNew';
import ExpandLess from 'material-ui-icons/ExpandLess';
import ExpandMore from 'material-ui-icons/ExpandMore';

import withRoot from '../components/withRoot';
import {appEvent} from '../components/events';
import {/*saveTmpBet,*/Dow,Me,toDbDate} from '../components/game';
import {Get/*,Delete*/} from '../components/ajax';
import {SearchBar,TableClasses,Classes} from '../components/dialog';
import {bList,mList} from './placebet1d';

const styles = theme => ({
	...TableClasses(theme),
	...Classes(theme),
	root: {
		width: '100%',
		display:'flex',
		flexDirection:'column',
		flex:1,
		height:'100%',
	},
	sub:{
		width: '100%',
		padding:5,
		boxSizing:'border-box',
		flex:1,
		overflow:'auto',
	},
	sub2:{
		width: '100%',
		paddingLeft:24,
		boxSizing:'border-box',
	},
	item:{
		display:'flex',
	},
	item2:{
		display:'flex',
		padding:[6,0,6,20]
	},
	button: {
		//margin:[-10,0],//margin: theme.spacing.unit,
		//margin:0,
		width:24,
		height:24,
	},
	button2: {
		margin: theme.spacing.unit,
	},
	collapsible:{
		margin:[0,0,0,0],
		padding:[0,0,0,0],
	},
	sItem2:{
		textAlign:'center'
	},
	sFirstItem:{
		textAlign:'left',
		padding:[5,0,20,0],
		//borderBottom:[1,'solid','#eee']
		fontSize:'14pt'
	},
});

export const TicketDetail=p=>{
	let {classes,v,tickets}=p,
		oneD=v.form_type==='1D';
	return <div className={classes.col+' '+classes.colT}>
		<table style={{width:'100%',maxWidth:'800px'}}>
			<thead><tr>
				<th className={classes.sItem2}>Day</th>
				<th className={classes.sItem2}>Game</th>
				<th className={classes.sItem2}>N</th>
				{!oneD && <th className={classes.sItem2}>B</th>}
				{!oneD && <th className={classes.sItem2}>S</th>}
				{!oneD && <th className={classes.sItem2}>T</th>}
				
				{oneD && <th className={classes.sItem2}>Match</th>}
				{oneD && <th className={classes.sItem2}>Amount</th>}
				<th className={classes.sItem2}>BetAmount</th>
			</tr></thead><tbody>
			{tickets.map((t,k)=><tr key={k}>
				<td className={classes.sItem2}>{t.day_id}</td>
				<td className={classes.sItem2}>{t.game_id}</td>
				<td className={classes.sItem2}>{oneD?bList[t.num]:t.num}</td>
				{!oneD && <td className={classes.sItem2}>{p.formatNumber(t.big)}</td>}
				{!oneD && <td className={classes.sItem2}>{p.formatNumber(t.small)}</td>}
				{!oneD && <td className={classes.sItem2}>{t.bt}</td>}
				{oneD && <td className={classes.sItem2}>{mList[t.match]}</td>}
				{oneD && <td className={classes.sItem2}>{p.formatNumber(t.amount)}</td>}
				<td className={classes.sItem2}>{p.formatNumber(t.bet_amount)}</td>
			</tr>)}
			</tbody></table>
	</div>
}

class TicketItem extends Component{
	state={
		exp:false,
		tickets:[],
	}
	click=()=>{
		let {v}=this.props;
		this.setState({exp:!this.state.exp},()=>{
			//if(v.form_type==='1D') return Get('balance/'+v.balance_id,{},(e,r)=>{
			//	console.log('r==>',r);
			//	if(e) return console.log('e==>',e);
			//	this.setState({balance:r.d});
			//});
			let {pageType}=this.props;
			Get('ticket',{page_id:v.id,...(pageType==='history'?{history:1}:{})},(e,r)=>{
				if(e) return console.log('e==>',e);
				this.setState({tickets:r.d});
			});
		});
	}
	render(){
		let {classes,v,k,formatNumb}=this.props,
			{tickets}=this.state;
		return <ListItem className={classes.tableRow} ><List className={classes.collapsible} >
			<ListItem className={classes.tableRow} key={v.id} onClick={this.click}>
				<div className={classes.col+' '+classes.colA}><IconButton className={classes.button} >{this.state.exp ? <ExpandLess /> : <ExpandMore />}</IconButton></div>
				<div className={classes.col}>{k+1}</div>
				<div className={classes.col}>{v.id}</div>
				<div className={classes.col+' '+classes.col4+' '+classes.cols}>{(new Date(v.date)).toLocaleString()}</div>
				<div className={classes.col+' '+classes.col2}>{v.form_type}</div>
				<div className={classes.col+' '+classes.col2}>{v.place_to}</div>
				<div className={classes.col+' '+classes.col2}>{v.place_by}</div>
				<div className={classes.col+' '+classes.col2+' '+classes.coln}>{formatNumb(v.page_amount)}</div>					
			</ListItem>
			<Collapse component="li" in={this.state.exp} timeout="auto" unmountOnExit>
				<div className={classes.tableRow}>
					<div className={classes.col}>&nbsp;</div>
					<TicketDetail tickets={tickets} formatNumber={formatNumb} v={v} classes={classes}/>
				</div>
			</Collapse>
		</List></ListItem>
	}
}

class Tickets extends Component{
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}
	static contextTypes = {
		intl: PropTypes.object
	}
	state={
		members:[],
		ready:false,
		me:{},
		id:'',
		fromDate:null,
	}
	ready=()=>{
		let {id,fromDate,toDate,pageType}=this.state;
		if(!id) return this.setState({members:[],id:'',ready:false});
		let prm={place_to:id,limit:1000};
		if(fromDate) prm.from_date=toDbDate(fromDate);
		if(toDate) prm.to_date=toDbDate(toDate);
		if(pageType==='history') prm.history=1;

		Get('ticketpage',prm,(e,r)=>{
			if(e) return console.error(e);
			if(r && r.d) this.setState({members:r.d,ready:true});
		});
	}
	componentWillMount(){
		//console.log('this.props.match==>',this.props.match);
		if(this.props.match && this.props.match.path==='/htickets'){
			appEvent.fire('titleChange','History Tickets');
			this.setState({pageType:'history'});
		} else {
			appEvent.fire('titleChange','Active Tickets');
			this.setState({pageType:'active'});
		}
		Me((e,r)=>{
			this.meChange(r);
		})
	}
	meChange=me=>{
		this.setState({
			me,
			id:me.id
		},this.ready);
	}
	componentWillUnmount(){
		
	}
	route=uri=>{
		return (e)=>{
			if(e && e.stopPropagation) e.stopPropagation();
			console.log(uri)
			this.props.history.push(uri);
		}
	}
	formatNumb=n=>{
		return this.context.intl.formatNumber(n,{style: 'decimal',minimumFractionDigits:2,maximumFractionDigits:2});
	}
	setUser=(d)=>{
		this.setState({id:d.id},this.ready);
	}
	dateChange=(k)=>{
		return (value)=>{
			let st={};
			st[k]=value;
			this.setState(st,this.ready);
		}
	}
	render(){
		const { classes } = this.props,
			{me,members, ready, id,pageType,fromDate,toDate} = this.state;
		return <div className={classes.root}>
			<div className={classes.tableFooter}>
				<div className={classes.footerSub2}>
					<SearchBar label={'User Id'}
						id={id}
						me={me}
						userDetailValidate={d=>(d.active)}
						onFound={this.setUser}/>
					{pageType==='history' && <div className={classes.footerSub2}>
						&nbsp; &nbsp;
						From &nbsp;
						<DatePicker className={classes.date}
							selected={fromDate}
							onChange={this.dateChange('fromDate')}
							{...(toDate?{maxDate:toDate}:{maxDate:moment()})}
						/>
						&nbsp; &nbsp;
						To &nbsp;
						<DatePicker className={classes.date}
							selected={toDate}
							onChange={this.dateChange('toDate')}
							maxDate={moment()}
							{...(fromDate?{minDate:fromDate}:{})}
						/>
					</div>}
				</div>
			</div>
			<List className={classes.sub}>
				<Divider />
				<ListItem className={classes.tableTitle}>
					<div className={classes.col}>&nbsp;</div>
					<div className={classes.col}>#</div>
					<div className={classes.col}>ID</div>
					<div className={classes.col+' '+classes.col4}>Date</div>
					<div className={classes.col+' '+classes.col2}>Type</div>
					<div className={classes.col+' '+classes.col2}>UserId</div>
					<div className={classes.col+' '+classes.col2}>PlaceBy</div>
					<div className={classes.col+' '+classes.col2}>Amount</div>
				</ListItem>
				{members.map((v,k)=>
					<TicketItem key={v.id} k={k} classes={classes} v={v} pageType={pageType} formatNumb={this.formatNumb}/>)}
				<Divider />
				{(!members.length && ready) && <div className={classes.empty} >there is no data found</div>}
			</List>
		</div>
	}
}

export default withRoot(withStyles(styles, { withTheme: true })(Tickets));