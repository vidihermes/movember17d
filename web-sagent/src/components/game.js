import {appEvent} from './events';
import {Get} from './ajax';

let game,
	config,
	me,
	dateDiff=null,
	tzDiff=null,
	tmpBet={};

export const Alias={
	rb:'rebate',
	it:'intake',
	sc:'strike_comm'
};

export const UserType={
	'developer':'Developer',
	'sadmin':'Super Admin',
	'admin':'Admin',
	'sagent':'Super Agent',
	'agent':'Agent',
	'player':'Player',
}

export const PrizeRank={
	'1':'First',
	'2':'Second',
	'3':'Third',
	's':'Starter',
	'c':'Consolation',
}

export const Dow={
	'1':'Mon',
	'2':'Tue',
	'3':'Wed',
	'4':'Thu',
	'5':'Fri',
	'6':'Sat',
	'7':'Sun',
};

export const pad=(s,n)=>{
	s=s.toString();
	return n-s.length>0?('0'.repeat(n-s.length))+s:s;
};

export const extractDate=d=>{
	d=new Date(d);
	return {
		y:d.getFullYear(),
		mo:d.getMonth()+1,
		d:d.getDate(),
		h:d.getHours(),
		mi:d.getMinutes(),
		s:d.getSeconds()
	};
};

export const toDbDate=date=>{
	let {y,mo,d}=extractDate(date);
    return [pad(y,4), pad(mo,2), pad(d,2)].join('-');
};

export const saveTmpBet=(id,data)=>{
	tmpBet[id]=data;
};

export const loadTmpBet=(id)=>{
	return tmpBet[id]||[];
};

export const RequestGame=cb=>{
	Get('game',{},(e,r)=>{
		if(e) return cb(e);
		if(r && r.d){
			game=r.d;
			cb(null,game);
		} else {
			cb('no game data');
		}
	});
};

export const RequestConfig=cb=>{
	Get('config',{},(e,r)=>{
		if(e) return cb(e);
		if(r && r.d){
			config=r.d;
			cb(null,config);
		} else {
			cb('no config data');
		}
	});
};

export const RequestDate=cb=>{
	Get('date',{},(e,r)=>{
		if(e) return cb(e);
		if(r && r.d){
			cb(null,r.d);
		} else {
			cb('no date data');
		}
	});
};

export const RequestMe=cb=>{
	Get('me',{},(e,r)=>{
		me=null;
		if(e) return cb(e);
		if(r && r.d && r.d.id){
			me=r.d;
			cb(null,r.d);
		} else {
			cb('no me data');
		}
		appEvent.fire('meChange',me);
	});
};

export const RequestDrawDate=cb=>{
	Get('drawdate',{},(e,r)=>{
		if(e) return cb(e);
		if(r && r.d){
			cb(null,r.d);
		} else {
			cb('no drawdate data');
		}
	});
};

export const RequestDrawDates=cb=>{
	Get('completedrawdate',{},(e,r)=>{
		if(e) return console.log('e==>',e);
		let dd=r.d;
		timeOffsetDiff((e,toDiff)=>{
			let dds={};
			Object.keys(dd).forEach(k=>{
				dds[k]={
					...dd[k],
					close:dd[k].close+toDiff,
					draw:dd[k].draw+toDiff,
				};
			});
			cb(null,dds)
		});
	});	
};


export const CurrentPrize=cb=>{
	Get('currentprize',{},(e,r)=>{
		if(e) return cb(e);
		if(r && r.d){
			cb(null,r.d);
		} else {
			cb('no currentprize data');
		}
	});
};

export const Game=cb=>{
	if(game) return cb(null,game);
	RequestGame(cb);
};

export const Config=cb=>{
	if(config) return cb(null,config);
	RequestConfig(cb);
};

export const UpdateConfig=(o)=>{
	if(!config) return;
	config={...config,...(o||{})};
	appEvent.fire('configChange',config);
};

export const Me=(cb,force)=>{
	if(me && !force) return cb(null,me);
	RequestMe(cb);
};

export const UpdateMe=d=>{
	me=d;
	appEvent.fire('meChange',me);
	//if(me && !force) return cb(null,me);
	//RequestMe(cb);
};

export const DateDiff=(cb,force)=>{
	if(dateDiff === null || force){
		return RequestDate((e,sr)=>{
			if(e) return cb(e);
			let r=sr.date,
				sTZ=sr.TZ,
				localTZ=-(((new Date()).getTimezoneOffset())/60);
			tzDiff=sTZ-localTZ;
			dateDiff=(new Date(r)).getTime() - ((new Date()).getTime()) + tzDiff * 3600 * 1000;
			cb(null,dateDiff);
		});
	}
	cb(null,dateDiff);
};

export const TZDiff=(cb,force)=>{
	if(tzDiff === null || force){
		return RequestDate((e,sr)=>{
			if(e) return cb(e);
			cb(null,tzDiff);
		});
	}
	cb(null,tzDiff);
};

export const timeOffsetDiff=(cb,force)=>{
	TZDiff((e,r)=>{
		if(e) return cb(e);
		cb(null,r * 3600 * 1000);
	},force);
};

export const ServerDate=(cb,force)=>{
	DateDiff((e,r)=>{
		if(e) return cb(e);
		cb(null,(new Date()).getTime() + r);
	},force);
};

let cdd;
export const CompleteDrawDate=(cb)=>{
	if(cdd) return cb(cdd);
	Get('completedrawdate',{},(e,r)=>{
		console.log('r==>',r);
		if(e) {
			cb({});
			return console.log('e==>',e);
		}
		cdd=r.d;
		cb(r.d);
	});
}

function revKV(o){
	let r={};
	Object.keys(o).forEach(k=>{
		r[o[k]]=k;
	});
	return r;
}

export const revDow=revKV(Dow);

let simpleDd;

function toSimpleDd(drawDay){
	simpleDd={};
	Object.keys(drawDay).forEach(k=>{
		simpleDd[k]=drawDay[k].map(v=>revDow[v])
	});
}

export const enabledDay=(k,drawDay,drawDate)=>{
	if(!simpleDd) toSimpleDd(drawDay);
	return simpleDd[k].reduce((r,i)=>{
			return (drawDate[i]||{}).closed?false:r;
		},true);
}

export const defDay=(drawDay,drawDate)=>{
	for(let k in drawDay){
		if(enabledDay(k,drawDay,drawDate)) return k;
	}
}