import React,{Component} from 'react';
import PropTypes from 'prop-types';
import Button from 'material-ui/Button';
import Badge from 'material-ui/Badge';
import Menu, { MenuItem } from 'material-ui/Menu';
import { withStyles } from 'material-ui/styles';
//import withRoot from './withRoot';
import {appEvent} from './events';
import {Get} from './ajax';
import {Talk} from '../sio/';
import Notifications from 'material-ui-icons/Notifications';

const styles = theme => ({
	menuItem:{
		borderBottom:[1,'solid','#eee'],
		padding:[5,12],
		cursor:'pointer',
		whiteSpace:'normal',
		height:'auto',
		'&:hover':{
			backgroundColor:theme.palette.primary[100]
		}
	},
	empty:{
		color:'#aaa',
		textAlign:'center'
	},
	more:{
		//paddingLeft:40
	}
});

export const humanize=(str)=>{
	var frags = str.split('_');
	for (let i=0; i<frags.length; i++) {
		frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1);
	}
	return frags.join(' ');
}

function cTyped(d){
	let t=((d||{}).params||{}).type;
	return t==='edit'?'updated':t+'d'
}

export const notifParser={
	downline_fake_login:(d)=>{
		return 	`${((d||{}).params||{}).downline_id||''} use emergency login !!!`;		
	},
	member_invalid_password:(d)=>{
		return `Your member ${((d||{}).params||{}).member_id||''} locked, repeatedly wrong password`
	},
	profile_update:(d)=>{
		let ks=Object.keys(d.params),
			k=ks[0]!=='update_by'?ks[0]:ks[1],
			prop=k==='name'?'Name "'+d.params.name+'"':humanize(k);
		return 	`Your ${prop} updated by ${d.params.update_by}`;
	},
	member_profile_update:(d)=>{
		return `Your member ${((d||{}).params||{}).member_id||''} profile updated by ${(d||{}).by_id||''}`
	},
	place_bet:(d)=>{
		return `Place bet by ${(d||{}).by_id||''}, ticket id: ${((d||{}).params||{}).ticket_id||''}`
	},
	place_fix_bet:(d)=>{
		return `Fix bet placed, ticket id: ${((d||{}).params||{}).ticket_id||''}`
	},
	member_add:(d)=>{
		return `New member ${((d||{}).params||{}).id||''} added by ${(d||{}).by_id||''}`
	},
	strike:(d)=>{
		return `Ticket id ${((d||{}).params||{}).ticket_id||''} strike`
	},
	downline_strike:(d)=>{
		return `${((d||{}).params||{}).downline_id||''} ticket ${((d||{}).params||{}).ticket_id||''} strike`
	},
	status_update:d=>{
		let desc=(d||{}).description||'';
		return `Your account ${((d||{}).params||{}).status}, by ${(d||{}).by_id||''}${desc?', reason:'+desc:''}`
	},
	member_status_update:d=>{
		let desc=(d||{}).description||'';
		return `Your member ${((d||{}).params||{}).member_id} account ${((d||{}).params||{}).status}, by ${(d||{}).by_id||''}${desc?', reason:'+desc:''}`
	},
	downline_status_update:d=>{
		let desc=(d||{}).description||'';
		return `Your downline ${((d||{}).params||{}).downline_id} account ${((d||{}).params||{}).status}, by ${(d||{}).by_id||''}${desc?', reason:'+desc:''}`
	},
	credit_update:d=>{
		let desc=(d||{}).description||'';
		return `Your credit limit ${cTyped(d)} ${((d||{}).params||{}).amount}, by ${(d||{}).by_id||''}${desc?', reason:'+desc:''}`
	},
	member_credit_update:d=>{
		let desc=(d||{}).description||'';
		return `Credit limit of your member ${((d||{}).params||{}).member_id} ${cTyped(d)} ${((d||{}).params||{}).amount}, by ${(d||{}).by_id||''}${desc?', reason:'+desc:''}`
	},
	downline_credit_update:d=>{
		let desc=(d||{}).description||'';
		return `Credit limit of your downline ${((d||{}).params||{}).downline_id} ${cTyped(d)} ${((d||{}).params||{}).amount}, by ${(d||{}).by_id||''}${desc?', reason:'+desc:''}`
	}
}

export const parseNotif=(t)=>{
	if(notifParser[t]) return notifParser[t];
	return (d)=>JSON.stringify(d);
}

export class NatifButton extends Component{
	state={
		notifCnt:0,
	}
	setCount=(notifCnt)=>{
		this.setState({notifCnt:parseInt(notifCnt,10)});
	}
	onReceive=()=>{
		this.setState({notifCnt:this.state.notifCnt+1});
	}
	componentWillMount(){
		appEvent.on('setNotifCount',this.setCount);
		appEvent.on('notiveReceive',this.onReceive);
	}
	componentWillUnmount(){
		appEvent.off('setNotifCount',this.setCount);
		appEvent.off('notiveReceive',this.onReceive);
	}
	render(){
		let {classes}=this.props,
			{notifCnt}=this.state;
		return <Button className={classes.rightItem} color="inherit" onClick={e=>{
				Talk('notifView');
				appEvent.fire('showNotif',e.currentTarget)
			}}>
			{!!notifCnt && <Badge className={classes.badge} badgeContent={notifCnt} color="error">
				<Notifications/>
			</Badge>}
			{!notifCnt && <Notifications/>}
		</Button>
	}
}

export class NotifMenu extends Component {
	static propTypes = {
		classes: PropTypes.object.isRequired,
	}
	state = {
		anchorEl: null,
		notifs:[]
	};
	menuOpen=(anchorEl)=>{
		this.setState({anchorEl})
	}
	onReceive=(data)=>{
		let {notifs}=this.state;
		notifs.unshift(data);
		this.setState({notifs});
	}
	componentWillMount(){
		appEvent.on('showNotif',this.menuOpen);
		appEvent.on('notiveReceive',this.onReceive);
		Get('mynotif',{},(e,r)=>{
			if(e) return console.error(e);
			this.setState({
				notifs:(r.d||[]) //.filter((v,k)=>k<5)
			});
			appEvent.fire('setNotifCount',r.unviewed||0);
		})
		
	}
	componentWillUnmount(){
		appEvent.off('showNotif',this.menuOpen);
		appEvent.off('notiveReceive',this.onReceive);
	}
	handleClose=()=>{
		this.setState({ anchorEl: null });
	}
	route(uri){
		return ()=>{
			this.props.history.push(uri);
		}
	}
	render(){
		const {classes} = this.props,
			open = Boolean(this.state.anchorEl),
			{notifs}=this.state;
		return <Menu
			id="long-menu"
			anchorEl={this.state.anchorEl}
			open={open}
			onClose={this.handleClose}
			
			transformOrigin={{
				vertical:-48,
				horizontal:250
			}}
			PaperProps={{
				style: {
					width:300,
					top:48,
				}
			}}
        >
			{!notifs.length && <MenuItem onClick={this.handleClose} selected={false} className={classes.menuItem+' '+classes.empty}>there is no data found</MenuItem>}
			{!!notifs.length && notifs.map(v => (
				<MenuItem key={v.id} onClick={this.handleClose} selected={false} className={classes.menuItem}>
				  {parseNotif(v.type)(v)}
				</MenuItem>
			))}
			{!!notifs.length && <MenuItem onClick={()=>{
					this.handleClose();
					this.route('/notif')();
				}} selected={false} className={classes.menuItem+' '+classes.more}>more...</MenuItem>}
        </Menu>
	}
}

export default withStyles(styles, { withTheme: true })(NotifMenu);