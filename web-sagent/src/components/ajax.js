const $=window.$;
const defSetting={
	"async": true,
	"crossDomain": true,
	//"url": "http://localhost:3001/api/me",
	"method": "GET",
	"headers": {
	  "Cache-Control": "no-cache",
	}
}

export const flatten=(data)=>{
    var result = {};
    function recurse (cur, prop) {
        if (Object(cur) !== cur) {
            result[prop] = cur;
        } else if (Array.isArray(cur)) {
             for(var i=0, l=cur.length; i<l; i++)
                 recurse(cur[i], prop + "[" + i + "]");
            if (l === 0)
                result[prop] = [];
        } else {
            var isEmpty = true;
            for (var p in cur) {
                isEmpty = false;
                recurse(cur[p], prop ? prop+"["+p+"]" : p);
            }
            if (isEmpty && prop)
                result[prop] = {};
        }
    }
    recurse(data, "");
    return result;
}

export const Get=(api,params,cb)=>{
	return $.ajax({...defSetting,url:window.api_url+api,data:params||{}})
		.done(r=>{
			cb(null,r)
		})
		.fail(e=>{
			if(e.responseJSON) return cb(e.responseJSON);
			if(e.responseText) return cb({s:0,e:e.responseText});
			cb({s:0,e:e.toString()});
		})
}

export const Post=(api,params,cb)=>{
	return $.ajax({...defSetting,method:'POST',
			url:window.api_url+api,
			headers:{...defSetting.headers,"Content-Type": "application/x-www-form-urlencoded"},
			data:params||{}})
		.done(r=>{
			cb(null,r)
		})
		.fail(e=>{
			if(e.responseJSON) return cb(e.responseJSON);
			if(e.responseText) return cb({s:0,e:e.responseText});
			cb({s:0,e:e.toString()});
		})
}

export const Put=(api,params,cb)=>{
	return $.ajax({...defSetting,method:'PUT',
			url:window.api_url+api,
			headers:{...defSetting.headers,"Content-Type": "application/x-www-form-urlencoded"},
			data:params||{}})
		.done(r=>{
			cb(null,r)
		})
		.fail(e=>{
			if(e.responseJSON) return cb(e.responseJSON);
			if(e.responseText) return cb({s:0,e:e.responseText});
			cb({s:0,e:e.toString()});
		})
}

export const Delete=(api,params,cb)=>{
	return $.ajax({...defSetting,method:'DELETE',
			url:window.api_url+api,
			headers:{...defSetting.headers,"Content-Type": "application/x-www-form-urlencoded"},
			//data:params||{}
			})
		.done(r=>{
			cb(null,r)
		})
		.fail(e=>{
			cb(e.responseJSON);
		})
}
